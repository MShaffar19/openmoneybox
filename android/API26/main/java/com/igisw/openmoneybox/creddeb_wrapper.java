/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-03-10
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

class creddeb_wrapper {
    @SuppressWarnings("FieldCanBeLocal")
    private final int id;
    final String name, value;
    final String badgeUri;

    creddeb_wrapper(int id, String name, String value, String badgeUri) {
        this.id = id;
        this.name = name;
        this.value = value;
        //this.alarm = alarm;
        //this.iconId = iconId;
        this.badgeUri = badgeUri;
    }
}
