/* **************************************************************
 * Name:
 * Purpose:   Shoplist fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-01-31
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShoplistFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShoplistFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShoplistFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;

    public int shop_pos;
    public final List<shoplist_wrapper> shoplist = new ArrayList<>();
    public RecyclerView sv;    // sv: shoplist view

    public ShoplistFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShoplistFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShoplistFragment newInstance(String param1, String param2) {
        ShoplistFragment fragment = new ShoplistFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainactivity activity = (mainactivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shoplist, container, false);

        // Shoplist tab
        int thisYear, alarmYear;
        thisYear = Calendar.getInstance().get(Calendar.YEAR);
        GregorianCalendar alarmCalendar;
        String str_tmp;

        sv = view.findViewById(R.id.sv);
        LinearLayoutManager llm1 = new LinearLayoutManager(activity.getApplicationContext());
        sv.setLayoutManager(llm1);
        recycler_adapter_shoplist shoplist_adapter = new recycler_adapter_shoplist(shoplist);
        shoplist_adapter.frame = activity;
        sv.setAdapter(shoplist_adapter);
        shoplist_adapter.llManager = llm1;

        shoplist.clear();
        for(int i = 0; i < activity.Data.NSho; i++) {
            alarmCalendar = activity.Data.ShopItems.get(i).Alarm;
            alarmYear = alarmCalendar.get(Calendar.YEAR);
            if((alarmYear - thisYear) < 80) str_tmp = omb_library.omb_DateToStr(alarmCalendar);
            else str_tmp = getResources().getString(R.string.alarm_none);

            shoplist.add(new shoplist_wrapper(activity.Data.ShopItems.get(i).Id,
                    activity.Data.ShopItems.get(i).Name,
                    str_tmp));
        }
        shoplist_adapter.notifyDataSetChanged();

        return view;
    }

    /*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    */

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
