/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-04-10
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class mainoptions extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
        if(Opts.getBoolean("GDarkTheme", false)) {
            this.setTheme(R.style.DarkTheme);
        }

        try {
            getClass().getMethod("getFragmentManager");
            getSupportFragmentManager().beginTransaction().replace(android.R.id.content,
                new PF()).commit();
        } catch (NoSuchMethodException e) { //Api < 11

        }
    }

    @SuppressWarnings("WeakerAccess")
    public static class PF extends PreferenceFragmentCompat
    {
        @Override
        public void onCreatePreferences(Bundle bundle, String S)
        {
            //super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.options);
        }
    }

}
