/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-06-20
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_OEMICON;
import static com.igisw.openmoneybox.omb_library.getCustomIcons;
import static com.igisw.openmoneybox.omb_library.getDocumentFolder;
import static com.igisw.openmoneybox.omb_library.setCustomIcons;

public class editcategories extends Activity {

	private final int SELECT_PHOTO = 1;

	private boolean modified;
	private int item_pos;
	private boolean firstAddedId;
	private ArrayList<String> categoryList;
	private ArrayList<Integer> iconList;
	private categoryAdapter catAdapter;
	private ImageButton btn;
	private Button icoBtn;
	private ArrayList<Bitmap> customIcons;
	private AlertDialog iconAlert;
	private ArrayList<String> customIconNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.categories);

		TextView label = findViewById(R.id.Title);
		label.setText(getResources().getString(R.string.categories_edit));

		firstAddedId = false;
		
		Bundle bundle=getIntent().getExtras();
		
	    categoryList = bundle.getStringArrayList("categories");
	    iconList = bundle.getIntegerArrayList("icons");

	    customIcons = getCustomIcons();
	    customIconNames = new ArrayList<>();
	    customIconNames.clear();

	    // Create ArrayAdapter  
		catAdapter = new categoryAdapter(this, categoryList, iconList);

		// Set the ArrayAdapter as the ListView's adapter.
	    ListView categoryView = findViewById( R.id.CategoryView );
	    categoryView.setAdapter( catAdapter );
	    
	    btn = findViewById( R.id.imageButton2 );
	    btn.setEnabled(false);

		icoBtn = findViewById( R.id.iconButton );
		icoBtn.setEnabled(false);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_categories);
			ll.setBackgroundColor(0xff000000);
		}

	    categoryView.setOnItemClickListener(new OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
	        	boolean enabled = position >= 0;
	        	btn.setEnabled(enabled);
	        	icoBtn.setEnabled(enabled);
	        	if(position >= 0)item_pos = position;

				RadioButton radio;
				for (int i = 0; i < adapter.getCount(); i ++)
				{

					View v = adapter.getChildAt(i);
					if(v != null) {
						radio = v.findViewById(R.id.radio);
						radio.setChecked(false);
					}

				}

				radio = view.findViewById(R.id.radio);
				radio.setChecked(true);

	        }
	    });

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		iconAlert = builder.create();
		iconAlert.setTitle(R.string.icon_choose_title);
		iconAlert.setMessage("");

	    modified = false;

	}
	
	public void okBtnClick(View view){
		int ReturnCode;
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putBoolean("modified", modified);
		if(modified){
			bundle.putStringArrayList("categories", categoryList);
			bundle.putIntegerArrayList("icons", iconList);
			bundle.putBoolean("custom_added", firstAddedId);
			if(firstAddedId) bundle.putStringArrayList("custom_files", customIconNames);
		}
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
	
	public void addCategory(View view){
 		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(this.getResources().getString(R.string.categories_add));

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				String value = input.getText().toString();
				if(!value.isEmpty()){
					value = value.trim();
					value = omb_library.iUpperCase(value);
					boolean Ex = false;
					for(int i = 0; i < categoryList.size(); i++){
						if(categoryList.get(i).compareToIgnoreCase(value) == 0){
							Ex = true;
							break;}}
					if(!Ex){
						categoryList.add(value);
						iconList.add(-1);
						catAdapter.notifyDataSetChanged();
						modified = true;}}
				
		  }
		});

		alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
			//return false;
		  }
		});
		
		alert.show();

		//return Response;
		
	}

	public void removeCategory(View view){
		categoryList.remove(item_pos);
		iconList.remove(item_pos);
		catAdapter.notifyDataSetChanged();
		modified = true;
	}

	public void selectIcon(View view){
		TypedArray catIcons = getResources().obtainTypedArray(R.array.category_drawables_values);

		View v = getLayoutInflater().inflate(R.layout.icon_layout, null);
		GridView gridView = (GridView) v;

		iconAdapter adapter = new iconAdapter(this, catIcons);

		gridView.setAdapter(adapter);
		gridView.setNumColumns(5);
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) iconList.set(item_pos, -1);
				else {
					if(position == (_OMB_TOPCATEGORIES_OEMICON - 1)){
						Intent intent = new Intent();
						intent.setType("image/png");
						intent.setAction(Intent.ACTION_PICK);
						startActivityForResult(Intent.createChooser(intent,
								""), SELECT_PHOTO);
					}
					else
						iconList.set(item_pos, position);
				}
				modified = true;
				catAdapter.notifyDataSetChanged();
				iconAlert.cancel();
			}
		});

		iconAlert.setView(gridView);

		iconAlert.show();

		//catIcons.recycle();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		if(requestCode == SELECT_PHOTO) {
			if (resultCode == RESULT_OK) {
				Bitmap bmp;
				final Uri imageUri;
				try {
					imageUri = imageReturnedIntent.getData();
					final InputStream imageStream = getContentResolver().openInputStream(imageUri);
					bmp = BitmapFactory.decodeStream(imageStream);
				} catch (FileNotFoundException e) {
					return;
				}

				if ((bmp.getWidth() > 24) || (bmp.getHeight() > 24))
					bmp = Bitmap.createScaledBitmap(bmp, 24, 24, false);

				// Save image
				// Path selection
				omb_library.appContext = this;
				String dir = getDocumentFolder() + "/ombCategoryImgs";
				File file = new File(dir);
				if (!file.exists())
					if (!file.mkdir()) return;

				String iconName = imageUri.getLastPathSegment();
				String filename = dir + "/" + iconName + ".png";

				try (FileOutputStream out = new FileOutputStream(filename)) {
					bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
				} catch (IOException e) {
					e.printStackTrace();
				}

				// Store icon filename
				customIconNames.add(iconName);

				customIcons.add(bmp);
				setCustomIcons(customIcons);
				if (!firstAddedId) firstAddedId = true;

			}
		}
	}

}
