/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-01-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

public class debt_add extends Activity {

	private boolean browsed_contacts;
	private String id;
	private EditText nameText, valueText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.funds_add);

		browsed_contacts = false;

		TextView caption_label = findViewById(R.id.Title);
		caption_label.setText(getResources().getString(R.string.debt_add));
		nameText = findViewById(R.id.Name);
		nameText.setHint(getResources().getString(R.string.debt_insert));
		valueText = findViewById(R.id.Value);
		valueText.setHint(getResources().getString(R.string.debt_value));

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_funds);

			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				Resources.Theme theme = getTheme();
				ll.setBackgroundColor(getResources().getColor(R.color.black, theme));
				nameText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				nameText.setTextColor(getResources().getColor(R.color.white, theme));
				valueText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				valueText.setTextColor(getResources().getColor(R.color.white, theme));
			}
			else{
				ll.setBackgroundColor(getResources().getColor(R.color.black));
				nameText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				nameText.setTextColor(getResources().getColor(R.color.white));
				valueText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				valueText.setTextColor(getResources().getColor(R.color.white));
			}
		}
	}
	
	public void okBtnClick(View view){
		double cur;
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		//TextView value = (TextView) findViewById(R.id.opValue);
		String name = nameText.getText().toString();
		if(name.isEmpty()){
			omb_library.Error(30, "");
			nameText.requestFocus();
			return;}
		
		if(valueText.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			valueText.requestFocus();
			return;}
		cur = Double.parseDouble(valueText.getText().toString());
		
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		name = omb_library.iUpperCase(name);
		name = name.trim();
		bundle.putString("debt", name);
		bundle.putDouble("value", cur);

		boolean found = false;
		// String name = null;
		if(browsed_contacts){
			Cursor phones = getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI
					, null, null, null, ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC");
			while (phones.moveToNext()) {
				name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));

				if(id != null) {
					if(name.compareTo(nameText.getText().toString()) == 0){
						found = true;
						break;
					}
				}

			}
			phones.close();
		}

		if(found){
			if(name.compareTo(nameText.getText().toString()) == 0) {
				bundle.putString("contact_id", id);
				bundle.putString("contact_name", name);
			}
		}
		else bundle.putString("contact_id", "-1");

		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}

	public void searchContact(View view){
		omb_library.appContext = getApplicationContext();
		if(! omb_library.checkContactPermission(this)) return;

		Intent intent = new Intent(this, ContactActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("str", nameText.getText().toString());
		intent.putExtras(bundle);
		startActivityForResult(intent, 1);
	}

	@Override
	public void onActivityResult(int requestCode,int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != RESULT_OK) return;
		if(requestCode == 1){
			Bundle bundle = data.getExtras();

			nameText.setText(bundle.getString("contact"));
			id = bundle.getString("id");
			browsed_contacts = true;
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (requestCode == constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
			boolean canUseContacts = false;
			if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				canUseContacts = true;
			}

			if (!canUseContacts) {
				Toast.makeText(getApplicationContext(), "Contacts cannot be accessed.", Toast.LENGTH_LONG).show();
				finish();
			}
		}
	}

}
