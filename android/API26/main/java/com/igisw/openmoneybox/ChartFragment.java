/* **************************************************************
 * Name:
 * Purpose:   Chart fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-04-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChartFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;

    private mainactivity activity;
    private View view;

    public ChartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChartFragment newInstance(String param1, String param2) {
        ChartFragment fragment = new ChartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (mainactivity) getActivity();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chart, container, false);

        paintChart();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void paintChart(){

        boolean default_is_small = false;
        int i;
        double percent, sum_small_funds = 0;
        String color;

        Legend legend;
        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(activity);
        boolean dark_theme = Opts.getBoolean("GDarkTheme", false);

        // Fund Chart construction
        List<PieEntry> pie_entries = new ArrayList<>();
        PieChart FundPlot = view.findViewById(R.id.fund_graph);

        //FundPlot.removeSlices();
        int inserted_entries = 0;
        int default_entry = -1;

        for(i = 0; i < activity.Data.NFun; i++){
            percent = activity.Data.Funds.get(i).Value / activity.Data.getTot(omb34core.TTypeVal.tvFou) * 100;
            if(percent > 1) {
                pie_entries.add(new PieEntry((float) activity.Data.Funds.get(i).Value, activity.Data.Funds.get(i).Name));
                inserted_entries++;
                if(activity.Data.Funds.get(i).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0)
                    default_entry = inserted_entries - 1;
            }
            else{
                sum_small_funds += activity.Data.Funds.get(i).Value;
                if(activity.Data.Funds.get(i).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0) default_is_small = true;
            }
        }
        if(sum_small_funds > 0){
            pie_entries.add(new PieEntry((float) sum_small_funds, getResources().getString(R.string.fund_others)));
            inserted_entries++;
            if(default_is_small)default_entry = inserted_entries - 1;
        }
        PieDataSet set = new PieDataSet(pie_entries, getResources().getString(R.string.fund_chart));

        set.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        set.setValueLinePart1OffsetPercentage(100.f);
        set.setValueLinePart1Length(0.8f);
        set.setValueLinePart2Length(0.8f);

        for(i = 1; i < 7; i++){
            switch(i){
                case 1:
                    color = "#FFFF99";
                    break;
                case 2:
                    color = "#3DEB3D";
                    break;
                case 3:
                    color = "#FF0000";
                    break;
                case 4:
                    color = "#00FF00";
                    break;
                case 5:
                    color = "#0000FF";
                    break;
                default:
                    color = "#99CCFF";
            }
            if(i == 1) set.setColor(Color.parseColor(color));
            else set.addColor(Color.parseColor(color));
        }

        PieData data = new PieData(set);
        FundPlot.setData(data);
        FundPlot.getDescription().setEnabled(false);
        if(dark_theme) {
            int cl;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Resources.Theme theme = this.getContext().getTheme();
                cl = getResources().getColor(R.color.white, theme);
            }
            else cl = getResources().getColor(R.color.white);
            set.setValueLineColor(cl);
            FundPlot.setEntryLabelColor(cl);
            legend = FundPlot.getLegend();
            legend.setTextColor(cl);
        }
        else
            FundPlot.setEntryLabelColor(Color.parseColor("#000000"));
        FundPlot.setEntryLabelTextSize(7f);

        // Highlight default fund
        Highlight h = new Highlight((float) default_entry, 0, 0); // dataset index for piechart is always 0
        FundPlot.highlightValues(new Highlight[] { h });
        FundPlot.setDrawHoleEnabled(false);
        FundPlot.invalidate(); // refresh

        // Trend chart construction
        GregorianCalendar month = activity.Data.Day;
        GregorianCalendar lastday = month;
        month.set(Calendar.MONTH, (int) activity.Data.FileData.Month);
        int number_of_days = month.getActualMaximum(Calendar.DAY_OF_MONTH);
        List<Entry> trendvalues = new ArrayList<>();
        double	LastValue = 0;

        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
        String value;

        for(i = 0; i < activity.Data.NLin; i++){
            if(activity.Data.isDate(i)){
                lastday = activity.Data.Lines.get(i).Date;

                // Robustness code to handle localized or unlocalized strings
                value = activity.Data.Lines.get(i).Value;
                if(value.contains(".")) LastValue = Double.parseDouble(value);
                else{
                    try {
                        LastValue = nf.parse(value).doubleValue();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        //e.printStackTrace();
                        LastValue = -1;
                    }
                }
                Entry day_entry = new Entry((float) lastday.getTimeInMillis(), (float) LastValue);
                trendvalues.add(day_entry);
            }}

        LineChart TrendPlot = view.findViewById(R.id.trend_graph);
        LineDataSet setComp1 = new LineDataSet(trendvalues, getResources().getString(R.string.trend_chart));
        setComp1.setAxisDependency(YAxis.AxisDependency.LEFT);
        XAxis x_axis = TrendPlot.getXAxis();
        x_axis.setPosition(XAxis.XAxisPosition.BOTTOM);
        if(dark_theme) {
            int cl;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Resources.Theme theme = this.getContext().getTheme();
                cl = getResources().getColor(R.color.white, theme);
            }
            else cl = getResources().getColor(R.color.white);
            setComp1.setColor(cl);
            setComp1.setValueTextColor(cl);
            x_axis.setTextColor(cl);
            TrendPlot.getAxisLeft().setTextColor(cl);
            TrendPlot.getAxisRight().setTextColor(cl);
            legend = TrendPlot.getLegend();
            legend.setTextColor(cl);
        }
        else
            setComp1.setColor(Color.parseColor("#000000"));
        // use the interface ILineDataSet
        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(setComp1);

        if(! activity.Data.FileData.ReadOnly){
            // trend addition
            double currenttotal = activity.Data.getTot(omb34core.TTypeVal.tvFou);
            month.set(Calendar.DAY_OF_MONTH, number_of_days);
            List<Entry> trendtrendvalues = new ArrayList<>();
            trendtrendvalues.add(new Entry((float) lastday.getTimeInMillis(), (float) LastValue));
            trendtrendvalues.add(new Entry((float) month.getTimeInMillis(), (float) currenttotal));
            LineDataSet setComp2 = new LineDataSet(trendtrendvalues, getResources().getString(R.string.trend_last));
            setComp2.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSets.add(setComp2);

            if(LastValue == currenttotal) color = "#FFFF00";
            else if(LastValue < currenttotal) color = "#006400";
            else color = "#FF0000";
            setComp2.setColor(Color.parseColor(color));
        }

        LineData trenddata = new LineData(dataSets);

        TrendPlot.getXAxis().setValueFormatter(new DateAxisValueFormatter());

        TrendPlot.setData(trenddata);
        TrendPlot.getDescription().setEnabled(false);
        TrendPlot.invalidate(); // refresh

    }

    public void saveChart(int id, File filename){
        View plot = view.findViewById(id);
        Bitmap bmap = Bitmap.createBitmap(plot.getWidth(), plot.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmap);
        plot.draw(canvas);

        OutputStream os = null;
        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        bmap.compress(Bitmap.CompressFormat.PNG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
