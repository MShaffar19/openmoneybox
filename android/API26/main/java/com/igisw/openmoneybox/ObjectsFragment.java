/* **************************************************************
 * Name:
 * Purpose:   Objects fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-07-23
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ObjectsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ObjectsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ObjectsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private OnFragmentInteractionListener mListener;

    public int lent_pos, borrowed_pos;
    public final List<objects_wrapper> objects = new ArrayList<>();
    public ImageButton btn_getbackObj, btn_givebackObj;
    public RecyclerView ov;    // ov: objects view

    public ObjectsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ObjectsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ObjectsFragment newInstance(String param1, String param2) {
        ObjectsFragment fragment = new ObjectsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainactivity activity = (mainactivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_objects, container, false);

        // Objects tab
        String str_tmp;

        btn_getbackObj = view.findViewById(R.id.getbackObj);
        btn_givebackObj = view.findViewById(R.id.givebackObj);
        btn_getbackObj.setEnabled(false);
        btn_givebackObj.setEnabled(false);
        ov = view.findViewById(R.id.ov);
        LinearLayoutManager llm1 = new LinearLayoutManager(activity.getApplicationContext());
        ov.setLayoutManager(llm1);
        recycler_adapter_objects objects_adapter = new recycler_adapter_objects(objects);
        objects_adapter.frame = activity;
        ov.setAdapter(objects_adapter);

        objects.clear();
        int thisYear, alarmYear;
        thisYear = Calendar.getInstance().get(Calendar.YEAR);
        GregorianCalendar alarmCalendar;
        for(int i = 0; i < activity.Data.NLen; i++) {
            alarmCalendar = activity.Data.Lent.get(i).Alarm;
            alarmYear = alarmCalendar.get(Calendar.YEAR);
            if((alarmYear - thisYear) < 80) str_tmp = omb_library.omb_DateToStr(alarmCalendar);
            else str_tmp = getResources().getString(R.string.alarm_none);

            long c_id = activity.Data.Lent.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
            }

            objects.add(new objects_wrapper(0, activity.Data.Lent.get(i).Id,
                    activity.Data.Lent.get(i).Object, activity.Data.Lent.get(i).Name,
                    str_tmp, R.drawable.object_lent, badgeUri));
        }
        for(int i = 0; i < activity.Data.NBor; i++) {
            alarmCalendar = activity.Data.Borrowed.get(i).Alarm;
            alarmYear = alarmCalendar.get(Calendar.YEAR);
            if((alarmYear - thisYear) < 80) str_tmp = omb_library.omb_DateToStr(alarmCalendar);
            else str_tmp = getResources().getString(R.string.alarm_none);

            long c_id = activity.Data.Borrowed.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
            }

            objects.add(new objects_wrapper(1, activity.Data.Borrowed.get(i).Id,
                    activity.Data.Borrowed.get(i).Object, activity.Data.Borrowed.get(i).Name,
                    str_tmp, R.drawable.object_borrow, badgeUri));
        }
        objects_adapter.notifyDataSetChanged();

        return view;
    }

    /*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    */

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
