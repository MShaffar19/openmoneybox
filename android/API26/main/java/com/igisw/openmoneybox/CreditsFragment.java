/* **************************************************************
 * Name:
 * Purpose:   Credits fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-11-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CreditsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CreditsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreditsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private OnFragmentInteractionListener mListener;

    public int credit_pos;
    private final List<creddeb_wrapper> credits = new ArrayList<>();
    public ImageButton btn_removeCredit, btn_condoneCredit;
    public RecyclerView cv;    // cv: credits view

    public CreditsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreditsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreditsFragment newInstance(String param1, String param2) {
        CreditsFragment fragment = new CreditsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainactivity activity = (mainactivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_credits, container, false);

        // Credits tab
        String str_tmp;
        Currency curr = Currency.getInstance(Locale.getDefault());
        TextView total_view;

        btn_removeCredit = view.findViewById( R.id.removeCredit );
        btn_condoneCredit = view.findViewById( R.id.condoneCredit );
        btn_removeCredit.setEnabled(false);
        btn_condoneCredit.setEnabled(false);
        cv = view.findViewById(R.id.cv);
        cv.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));
        recycler_adapter_creddeb credits_adapter = new recycler_adapter_creddeb(credits);
        credits_adapter.frame = activity;
        cv.setAdapter(credits_adapter);

        credits.clear();
        for(int i = 0; i < activity.Data.NCre; i++){
            str_tmp = curr.getSymbol() + " " + omb_library.FormDigits(activity.Data.Credits.get(i).Value, false);

            long c_id = activity.Data.Credits.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
            }

            credits.add(new creddeb_wrapper(activity.Data.Credits.get(i).Id, activity.Data.Credits.get(i).Name,
                    str_tmp, badgeUri));
        }
        credits_adapter.notifyDataSetChanged();

        total_view = view.findViewById(R.id.total_credit);
        str_tmp = getResources().getString(R.string.total) + "<br><small>" + curr.getSymbol() + " ";
        str_tmp += omb_library.FormDigits(activity.Data.Tot_Credits, false) + "</small>";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_COMPACT));
        else total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_LEGACY));

        return view;
    }

    /*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    */

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
