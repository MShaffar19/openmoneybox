/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-01-31
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class recycler_adapter_shoplist extends RecyclerView.Adapter<recycler_adapter_shoplist.ViewHolder>{

    public mainactivity frame;
    private final List<shoplist_wrapper> shoplist;
    public LinearLayoutManager llManager;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int item = getAdapterPosition();
            ImageButton Image;

            View CheckView;
            for(int i = 0; i < getItemCount(); i++){
                try {
                    CheckView = llManager.findViewByPosition(i);
                    Image = CheckView.findViewById(R.id.delShopItem);
                    Image.setVisibility(View.INVISIBLE);
                }
                catch(Exception e){
                    //break;
                }
            }

            Image = view.findViewById(R.id.delShopItem);
            Image.setVisibility(View.VISIBLE);

            frame.updateShoplistRecyclerItem(item);
        }

    }

    public class ObjectViewHolder extends ViewHolder{

        final CardView cv;
        final TextView itemName;
        final TextView itemAlarm;

        ObjectViewHolder(View itemView) {
            super(itemView);

            LinearLayout ll = itemView.findViewById(R.id.ll_cv_shoplist);
            RelativeLayout rl = itemView.findViewById(R.id.rl_cv_shoplist );
            RelativeLayout rl2 = itemView.findViewById(R.id.rl_cv_shoplist_2 );
            this.cv = itemView.findViewById(R.id.cv);
            this.itemName = itemView.findViewById(R.id.item_name);
            this.itemAlarm = itemView.findViewById(R.id.item_alarm);


            if(frame.Opts.getBoolean("GDarkTheme", false))
            {
                ll.setBackgroundColor(0xFF000000);
                cv.setCardBackgroundColor(0xFF333333);
                rl.setBackgroundColor(0xFF333333);
                rl2.setBackgroundColor(0xFF333333);
                itemName.setBackgroundColor(0xFF333333);
                itemAlarm.setBackgroundColor(0xFF333333);
            }
        }

    }

    public recycler_adapter_shoplist(List<shoplist_wrapper> shoplist){
        this.shoplist = shoplist;
    }

    @Override
    public int getItemCount() {
        return shoplist.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_shopitem, viewGroup, false);
        return new ObjectViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull ViewHolder viewHolder, int i) {
        ObjectViewHolder holder = (ObjectViewHolder) viewHolder;
        holder.itemName.setText(shoplist.get(i).item);
        holder.itemAlarm.setText(shoplist.get(i).alarm);
    }

}
