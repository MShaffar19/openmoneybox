/* *************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-11-21
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.igisw.openmoneybox.constants.ombFileFormat.bilFFORMAT_UNKNOWN;
//import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_33;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_332;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_34;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombWRONGPASSWORD;

class omb_library {

	public static Context appContext;
	private static ArrayList<Bitmap> cIcons;	// Custom icons

	public static void Error(int Err, String Opt){
		
		String Msg;
		switch(Err){
			case 1:
				Msg = String.format(appContext.getResources().getString(R.string.error_01), Opt);
				break;
			case 2:
				Msg= String.format(appContext.getResources().getString(R.string.error_02), Opt);
				break;
			case 3:
				Msg = appContext.getResources().getString(R.string.error_03);
				break;
			case 4:
				Msg = appContext.getResources().getString(R.string.error_04);
				break;
			case 5:
				Msg = appContext.getResources().getString(R.string.error_05);
				break;
			case 6:
				Msg = appContext.getResources().getString(R.string.error_06);
				break;
			case 8:
				Msg = appContext.getResources().getString(R.string.error_08);
				break;
			case 9:
				Msg = appContext.getResources().getString(R.string.error_09);
				break;
			case 10:
				Msg = appContext.getResources().getString(R.string.error_10);
				break;
			case 11:
				Msg = String.format(appContext.getResources().getString(R.string.error_11), Opt);
				break;
			case 12:
				Msg = appContext.getResources().getString(R.string.error_12);
				break;
			case 13:
				Msg = appContext.getResources().getString(R.string.error_13);
				break;
			case 14:
				Msg = appContext.getResources().getString(R.string.error_14);
				break;
			case 15:
				Msg = appContext.getResources().getString(R.string.error_15);
				break;
			case 16:
				Msg = appContext.getResources().getString(R.string.error_16);
				break;
			case 17:
				Msg = appContext.getResources().getString(R.string.error_17);
				break;
			case 18:
				Msg = appContext.getResources().getString(R.string.error_18);
				break;
			case 19:
				Msg = appContext.getResources().getString(R.string.error_19);
				break;
			case 20:
				Msg = appContext.getResources().getString(R.string.error_20);
				break;
			case 21:
				Msg = String.format(appContext.getResources().getString(R.string.error_21), Opt);
				break;
			case 22:
				Msg = appContext.getResources().getString(R.string.error_22);
				break;
			case 23:
				Msg = appContext.getResources().getString(R.string.error_23);
				break;
			case 24:
				Msg = appContext.getResources().getString(R.string.error_24);
				break;
			case 25:
				Msg = appContext.getResources().getString(R.string.error_25);
				break;
			/*
			case 26:
				Msg = "You must insert a numeric value!";
				break;
			case 27:
				Msg = "You must insert a value greater than zero!";
				break;
			*/
			case 28:
				Msg = appContext.getResources().getString(R.string.error_28);
				break;
			case 29:
				Msg = appContext.getResources().getString(R.string.error_29);
				break;
			case 30:
				Msg = appContext.getResources().getString(R.string.error_30);
				break;
			case 31:
				Msg = appContext.getResources().getString(R.string.error_31);
				break;
			case 32:
				Msg = appContext.getResources().getString(R.string.error_32);
				break;
			case 33:
				Msg = appContext.getResources().getString(R.string.error_33);
				break;
			case 34:
				Msg = appContext.getResources().getString(R.string.error_34);
				break;
			case 35:
				Msg = appContext.getResources().getString(R.string.error_35);
				break;
			case 36:
				Msg = appContext.getResources().getString(R.string.error_36);
				break;
			case 37:
				Msg = appContext.getResources().getString(R.string.error_37);
				break;
			case 38:
				Msg = appContext.getResources().getString(R.string.error_38);
				break;
			case 39:
				Msg = appContext.getResources().getString(R.string.error_39);
				break;
			case 40:
				Msg = appContext.getResources().getString(R.string.error_40);
				break;
			case 41:
				Msg = String.format(appContext.getResources().getString(R.string.error_41), Opt);
				break;
			/*
			case 42:
				Msg = "You must use this application from OpenMoneyBox application!";
				break;
			case 43:
				Msg = "You can convert only one file at a time!";
				break;
			case 44:
				Msg = "Error during the document conversion!";
				break;
			*/
			case 45:
				Msg = String.format(appContext.getResources().getString(R.string.error_45), Opt);
				break;
			case 46:
				Msg = appContext.getResources().getString(R.string.error_46);
				break;
			case 48:
				Msg = String.format(appContext.getResources().getString(R.string.error_48), Opt);
				break;

			case 49:
				Msg = appContext.getResources().getString(R.string.error_49);
				break;

			default:
				Msg = appContext.getResources().getString(R.string.error_unk);}

		Toast.makeText(appContext, Msg, Toast.LENGTH_LONG).show();
	}

	@SuppressLint("DefaultLocale")
	public static String FormDigits(double Val, boolean input){
		// set input to true when the output needs to be machine-readable
		String Ret;
		if(input) Ret = String.format(Locale.US, "%01.2f", Val);
		else Ret = String.format("%01.2f", Val);
		return Ret;}

	/*
	private static boolean Check301(String File){
		boolean Ret;
		int FL;	// FL: file length
		byte Buffer[];
		String S = "";
		File file = new File(File);
		FL = (int) file.length();
		FileInputStream fh = null;
		try {
			fh = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fh->Seek(0);
		Buffer=new byte[FL + 1];
		try {
			if(fh.read(Buffer, 0, 6) == 6){
				S = new String(Buffer, "UTF-8"); // for UTF-8 encoding
				S = S.substring(0, 6);
			}
			else{
				fh.close();
				//delete fh;
				return false;}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fh.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//delete fh;
		//delete[] Buffer;
		if(S.compareTo("Bil3.0") == 0) Ret = true;
		else Ret = false;
		return Ret;}
	*/

	/*
	private static boolean Check302(String File){
		boolean Ret;
		int FL;	// FL: file length
		byte Buffer[];
		String S = "";
		File file = new File(File);
		FL = (int) file.length();
		FileInputStream fh = null;
		try {
			fh = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Buffer = new byte[FL + 1];
		try {
			if(fh.read(Buffer, 0, 6) == 6){
				S = new String(Buffer, "UTF-8"); // for UTF-8 encoding
				S = S.substring(0, 6);
			}
			else{
				fh.close();
				//delete fh;
				return false;}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fh.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Ret = S.compareTo("Bil302") == 0;
		return Ret;}
	*/

	@SuppressWarnings("SpellCheckingInspection")
	public static constants.ombFileFormat checkFileFormat(String File, String pwd){
		//constants.ombFileFormat Ret = bilFFORMAT_UNKNOWN;
		boolean isSQL = true;

		switch (check34(File, pwd)){
			case -2:
				isSQL = false;
				break;
			case -1:
				return ombWRONGPASSWORD;
			/*
			case 1:
				return ombFFORMAT_33;
			*/
			case 2:
				return ombFFORMAT_332;
			case 3:
				return ombFFORMAT_34;

			//default:
		}
		/*
		if(isSQL) {
			if (Check32(File)) Ret = constants.ombFileFormat.ombFFORMAT_32;
				// else if (Check31(File)) Ret = constants.ombFileFormat.ombFFORMAT_31;
				// else if(Check302(File)) Ret = constants.ombFileFormat.bilFFORMAT_302;
				// else if(Check301(File)) Ret = constants.ombFileFormat.bilFFORMAT_301;
			//else Ret = bilFFORMAT_UNKNOWN;
		}
		*/
		return bilFFORMAT_UNKNOWN;}

	/*
	public static GregorianCalendar omb_StrToDate(String S){
	    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    Date date = new Date();
		try {
			date = df.parse(S);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    GregorianCalendar ret = new GregorianCalendar();
	    ret.setTime(date);
		return ret;
	}
	*/

	/*
	public static GregorianCalendar omb_StrToTime(String S){
	    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		Date time = new Date();
	    //Date date = new Date();
		try {
			time = df.parse(S);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    GregorianCalendar ret = new GregorianCalendar();
	    ret.setTime(time);
		return ret;
		//return double(NULL);
	}
	*/

	public static String omb_DateToStr(GregorianCalendar D){
	    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    Date date = D.getTime();
		return df.format(date);
	}

	public static String omb_TimeToStr(GregorianCalendar T, boolean save){
		// TODO: rework to show leading zero in hour and minute
		SimpleDateFormat df;
		if(save) df = new SimpleDateFormat("HH:mm:ss");
		else df = new SimpleDateFormat("HH:mm");
	    Date time = T.getTime();
		return df.format(time);
	}

	/*
	public static String GetLogString(SharedPreferences Opts, int I){
		if(I>14)return null;

		String RegString = "S";
		if(I < 10) RegString += "0";
		RegString += String.format("%d", I);

		return Opts.getString(RegString, "NULL");
	}
	*/

	/*
	public static String toCDouble(double v, char dec_sep)
	{
		String s = FormDigits(v);
		return s.replace(dec_sep, '.');
	}
	*/
	
	public static String iUpperCase(String S){
		String C = S.substring(0, 1);
		C = C.toUpperCase();
		S = S.substring(1);
		S = C + S;
		return S;}

	public static void copyAsset(String src, String dest) throws IOException {
		InputStream in;
		try {
			in = appContext.getAssets().open(src);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(dest);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		    byte[] buffer = new byte[1024];
		    int read;
		    while((read = in.read(buffer)) != -1){
		      out.write(buffer, 0, read);
		    }
	
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static void copyFile(File src, String dest) throws IOException {
		FileInputStream in;
		in = new FileInputStream(src);

		FileOutputStream out = null;
		try {
			out = new FileOutputStream(dest);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		    byte[] buffer = new byte[1024];
		    int read;
		    while((read = in.read(buffer)) != -1){
		      out.write(buffer, 0, read);
		    }
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean checkAlarm(GregorianCalendar A){
		int alarm_year, today_year;
		Calendar T = Calendar.getInstance();
		alarm_year = A.get(Calendar.YEAR);
		today_year = T.get(Calendar.YEAR);
		return (alarm_year - today_year) < 100;
	}

	/*
	private static boolean Check31(String file){
		SQLiteDatabase db = SQLiteDatabase.openDatabase(file, "", null, SQLiteDatabase.OPEN_READONLY);
		try{

			int version = db.getVersion();
			return version >= 0;	// 0: pre-existing empty file
									// 1 or greater: schema updates (existing database
									// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm

		}
		catch(SQLiteException e){
			return false;
		}}
	*/

	/*
	private static boolean Check32(String file){
		SQLiteDatabase db = SQLiteDatabase.openDatabase(file, "", null, SQLiteDatabase.OPEN_READONLY);
		try{

			int version = db.getVersion();
			db.close();
			return version == 32;	// 0: pre-existing empty file
									// 1 or greater: schema updates (existing database
									// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm

		}
		catch(SQLiteException e){
			db.close();
			return false;
		}
	}
	*/

	/*
	private static int check33(String file, String password) {
		// -2: not a sqlite3 database
		// -1: wrong password
		//  0: database is not omb33 version
		//  1: database is omb33 version

		int result = 0;

		SQLiteDatabaseHook hook = new SQLiteDatabaseHook() {
			public void preKey(SQLiteDatabase database) {
				database.execSQL(constants.cipher_compat_sql);
			}

			public void postKey(SQLiteDatabase database) {
			}
		};

		SQLiteDatabase db;

		try {
			db = SQLiteDatabase.openDatabase(file, password, null, SQLiteDatabase.OPEN_READWRITE, hook);
			Cursor cursor = db.rawQuery("select count(*) from sqlite_master;", new String[]{});
			if (cursor != null) {
				try {
					int version = db.getVersion();

					if ((version == 35) || (version == 34))
						result = 2;
					else if (version == 33) result = 1;
					else if (version == 0) {
						db.setVersion(constants.dbVersion);
						result = 1;
					}
					db.close();

				} catch (SQLiteException e) {
					db.close();
					result = -1;
				}
			}
		} catch (Exception e) {
			result = -2;
		}

		//db.close();
		return result;
	}
	*/

	private static int check34(String file, String password) {
		// -2: not a sqlite3 database
		// -1: wrong password
		//  0: database is not omb33 version
		//  1: database is omb32 version
		//  2: database is omb332 version
		//  3: database is omb34 version

		int result = 0;

		SQLiteDatabaseHook hook = new SQLiteDatabaseHook() {
			public void preKey(SQLiteDatabase database) {
				database.execSQL(constants.cipher_compat_sql);
			}

			public void postKey(SQLiteDatabase database) {
			}
		};

		SQLiteDatabase db;

		try {
			db = SQLiteDatabase.openDatabase(file, password, null, SQLiteDatabase.OPEN_READWRITE, hook);
			Cursor cursor = db.rawQuery("select count(*) from sqlite_master;", new String[]{});
			if (cursor != null) {
				try {
					int version = db.getVersion();

					if ((version == constants.dbVersion) || (version == (constants.dbVersion - 1)))
						result = 3;
					else if ((version == 35) || (version == 34))
						result = 2;
					else if (version == 33) result = 1;
					else if (version == 0) {
						db.setVersion(constants.dbVersion);
						result = 1;
					}
					db.close();

				} catch (SQLiteException e) {
					db.close();
					result = -1;
				}
			}
		} catch (Exception e) {
			result = -1;
		}

		return result;
	}

	/**
	 * Load a contact photo thumbnail and return it as a Bitmap,
	 * resizing the image to the provided image dimensions as needed.
	 * @param photoData photo ID Prior to Honeycomb, the contact's _ID value.
	 * For Honeycomb and later, the value of PHOTO_THUMBNAIL_URI.
	 * @return A thumbnail Bitmap, sized to the provided width and height.
	 * Returns null if the thumbnail is not found.
	 */
	public static Bitmap loadContactPhotoThumbnail(String photoData) {

		// Creates an asset file descriptor for the thumbnail file.
		AssetFileDescriptor afd = null;
		// try-catch block for file not found
		try {
			// Creates a holder for the URI.
			Uri thumbUri;

			{
				// Prior to Android 3.0, constructs a photo Uri using _ID
                /*
                 * Creates a contact URI from the Contacts content URI
                 * incoming photoData (_ID)
                 */
				final Uri contactUri = Uri.withAppendedPath(
						ContactsContract.Contacts.CONTENT_URI, photoData);
                /*
                 * Creates a photo URI by appending the content URI of
                 * Contacts.Photo.
                 */
				thumbUri =  Uri.withAppendedPath(
								contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
			}

			/*
			 * Retrieves an AssetFileDescriptor object for the thumbnail
			 * URI
			 * using ContentResolver.openAssetFileDescriptor
			 */
			try {
				afd = appContext.getContentResolver().openAssetFileDescriptor(thumbUri, "r");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			}
			/*
			 * Gets a file descriptor from the asset file descriptor.
			 * This object can be used across processes.
			 */
			FileDescriptor fileDescriptor = afd.getFileDescriptor();
			// Decode the photo file and return the result as a Bitmap
			// If the file descriptor is valid
			if (fileDescriptor != null) {
				// Decodes the bitmap
				Bitmap retBMap = BitmapFactory.decodeFileDescriptor(
						fileDescriptor, null, null);
				// improvement - see https://stackoverflow.com/questions/14193470/monodroid-bitmapfactory-decodefiledescriptor-bitmap-always-null
				if (retBMap == null)
					retBMap = BitmapFactory.decodeStream(new FileInputStream(fileDescriptor));
				return retBMap;
			}
			// If the file isn't found
		} finally {
			if (afd != null) {
				try {
					afd.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static boolean checkContactPermission(Activity act){
		if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.READ_CONTACTS)
				== PackageManager.PERMISSION_GRANTED) return true;

		// Request the permission.
		ActivityCompat.requestPermissions(act,
				new String[]{Manifest.permission.READ_CONTACTS},
				constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS);
		// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
		// app-defined int constant. The callback method gets the
		// result of the request.

		return false;
	}

	public static String getDocumentFolder(){
		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(appContext);
		String document = Opts.getString("GDDoc", "NULL");
		File file = new File(document);
		return file.getParent();
	}

	public static void setCustomIcons(ArrayList<Bitmap> array){
		cIcons = array;
	}

	public static ArrayList<Bitmap> getCustomIcons(){
		return cIcons;
	}

}
