/* **************************************************************
 * Name:
 * Purpose:   Map fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-04-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import static org.osmdroid.tileprovider.util.StorageUtils.getBestWritableStorage;
import static org.osmdroid.util.TileSystemWebMercator.MaxLatitude;
import static org.osmdroid.util.TileSystemWebMercator.MaxLongitude;
import static org.osmdroid.util.TileSystemWebMercator.MinLatitude;
import static org.osmdroid.util.TileSystemWebMercator.MinLongitude;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.config.IConfigurationProvider;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.mylocation.SimpleLocationOverlay;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements MapEventsReceiver {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private OnFragmentInteractionListener mListener;

    private MapView map;

    //protected MarkerInfoWindow info;

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainactivity activity = (mainactivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        map = view.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        IConfigurationProvider provider = Configuration.getInstance();
        provider.setUserAgentValue(BuildConfig.APPLICATION_ID);
        File StoragePath = new File(getBestWritableStorage(getContext()).path);
        provider.setOsmdroidBasePath(StoragePath);
        provider.setOsmdroidTileCache(StoragePath);

        map.setMultiTouchControls(true);
        map.setTilesScaledToDpi(true);
        
        IMapController mapController = map.getController();
        mapController.zoomTo(18.0);

        SimpleLocationOverlay mMyLocationOverlay = new SimpleLocationOverlay(((BitmapDrawable)
            Objects.requireNonNull(ResourcesCompat.getDrawable(this.getResources(),
            R.drawable.marker_default, null))).getBitmap());
        map.getOverlays().add(mMyLocationOverlay);

        ScaleBarOverlay mScaleBarOverlay = new ScaleBarOverlay(map);
        map.getOverlays().add(mScaleBarOverlay);

        double minLat = MaxLatitude;
        double maxLat = MinLatitude;
        double minLong = MaxLongitude;
        double maxLong = MinLongitude;
        double lat, lon;
        GeoPoint place;
        OverlayItem myLocationOverlayItem;

        Drawable myCurrentLocationMarker = ResourcesCompat.getDrawable(this.getResources(),
            R.drawable.ic_gps_fixed_black_24dp, null);
        final ArrayList<OverlayItem> items = new ArrayList<>();

        if((Objects.requireNonNull(activity).LastLatitude != constants.ombInvalidLatitude) &&
                (activity.LastLongitude != constants.ombInvalidLongitude)) {
            place = new GeoPoint(activity.LastLatitude, activity.LastLongitude);
            myLocationOverlayItem = new OverlayItem("Here", "Current Position", place);
            myLocationOverlayItem.setMarker(myCurrentLocationMarker);
            items.add(myLocationOverlayItem);
            if (place.getLatitude() < minLat)
                minLat = place.getLatitude();
            if (place.getLatitude() > maxLat)
                maxLat = place.getLatitude();
            if (place.getLongitude() < minLong)
                minLong = place.getLongitude();
            if (place.getLongitude() > maxLong)
                maxLong = place.getLongitude();
        }

        Marker startMarker;
        for(int i = 0; i < activity.Data.NLin; i++){
            lat = activity.Data.Lines.get(i).Latitude;
            lon = activity.Data.Lines.get(i).Longitude;
            if(! activity.Data.isDate(i)){
                if((lat != constants.ombInvalidLatitude) && (lon != constants.ombInvalidLongitude)) {

                    startMarker = new Marker(map);
                    place = new GeoPoint(lat, lon);
                    startMarker.setPosition(place);
                    startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);

                    int type;
                    int opType = -1;
                    double val;
                    String val_string;
                    switch (activity.Data.Lines.get(i).Type) {
                        case toGain:
                            type = 1;
                            opType = R.drawable.greenplus;
                            break;
                        case toExpe:
                            type = 2;
                            opType = R.drawable.redminus;
                            break;
                        case toSetCre:
                            type = 3;
                            opType = R.drawable.creditnew;
                            break;
                        case toRemCre:
                            type = 4;
                            opType = R.drawable.creditremove;
                            break;
                        case toConCre:
                            type = 5;
                            opType = R.drawable.creditcondone;
                            break;
                        case toSetDeb:
                            type = 6;
                            opType = R.drawable.debtnew;
                            break;
                        case toRemDeb:
                            type = 7;
                            opType = R.drawable.debtremove;
                            break;
                        case toConDeb:
                            type = 8;
                            opType = R.drawable.debtcondone;
                            break;
                        case toGetObj:
                            type = 9;
                            opType = R.drawable.object_received;
                            break;
                        case toGivObj:
                            type = 10;
                            opType = R.drawable.object_given;
                            break;
                        case toLenObj:
                            type = 11;
                            opType = R.drawable.object_lent;
                            break;
                        case toBakObj:
                            type = 12;
                            opType = R.drawable.object_getback;
                            break;
                        case toBorObj:
                            type = 13;
                            opType = R.drawable.object_borrow;
                            break;
                        case toRetObj:
                            type = 14;
                            opType = R.drawable.object_giveback;
                            break;
                        default:
                            type = -1;
                    }
                    if (type < 9) {
                        val = 0;
                        String weak_str = activity.Data.Lines.get(i).Value;
                        try {
                            val = Double.parseDouble(weak_str);
                        } catch (NumberFormatException e) {
                            NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
                            try {
                                val = Objects.requireNonNull(nf.parse(weak_str)).doubleValue();
                            } catch (ParseException ex) {
                                // TODO Auto-generated catch block
                                ex.printStackTrace();
                            }
                        }
                        Currency curr = Currency.getInstance(Locale.getDefault());
                        val_string = curr.getSymbol() + " " + omb_library.FormDigits(val, false);
                    } else val_string = activity.Data.Lines.get(i).Value;

                    startMarker.setIcon(ResourcesCompat.getDrawable(this.getResources(), opType, null));
                    startMarker.setTitle(omb_library.omb_DateToStr(activity.Data.Lines.get(i).Date));
                    startMarker.setSnippet(val_string + " " + activity.Data.Lines.get(i).Reason);
                    startMarker.setSubDescription(this.getResources().getString(R.string.abbreviation_latitude)
                            + String.format(Locale.US, " %f", lat) + " " +
                            this.getResources().getString(R.string.abbreviation_longitude) +
                            String.format(Locale.US, " %f", lon));

                    map.getOverlays().add(startMarker);

                    if (place.getLatitude() < minLat)
                        minLat = place.getLatitude();
                    if (place.getLatitude() > maxLat)
                        maxLat = place.getLatitude();
                    if (place.getLongitude() < minLong)
                        minLong = place.getLongitude();
                    if (place.getLongitude() > maxLong)
                        maxLong = place.getLongitude();

                }
            }
        }

        ItemizedIconOverlay<OverlayItem> currentLocationOverlay = new ItemizedIconOverlay<>(items,
                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
                        return true;
                    }
                    public boolean onItemLongPress(final int index, final OverlayItem item) {
                        return true;
                    }
                }, omb_library.appContext);
        map.getOverlays().add(currentLocationOverlay);

        if(maxLat == minLat){
            minLat = minLat - 0.1;
            maxLat = maxLat + 0.1;
        }
        if(maxLong == minLong){
            minLong = minLong - 0.1;
            maxLong = maxLong + 0.1;
        }

        if(maxLat > MaxLatitude) maxLat = MaxLatitude;
        if(maxLong > MaxLongitude) maxLong = MaxLongitude;
        if(minLat < MinLatitude) minLat = MinLatitude;
        if(minLong < MinLongitude) minLong = MinLongitude;

        if(minLat > maxLat){
            double tmp = minLat;
            minLat = maxLat;
            maxLat = tmp;
        }
        if(minLong > maxLong){
            double tmp = minLong;
            minLong = maxLong;
            maxLong = tmp;
        }

        final BoundingBox boundingBox = new BoundingBox(maxLat, maxLong, minLat, minLong);

        map.addOnFirstLayoutListener((v, left, top, right, bottom)
            -> new Handler(Looper.getMainLooper()).postDelayed(() -> map.zoomToBoundingBox(boundingBox,
            false, 150), 1000));

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean longPressHelper(GeoPoint geoPoint) {
        return false;
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint geoPoint) {
        //if (debug) Toast.makeText(activity, "Tap on ("+geoPoint.getLatitude()+","+geoPoint.getLongitude()+") zoom " + mMapView.getZoomLevel(), Toast.LENGTH_SHORT).show();

        return false;
    }
}
