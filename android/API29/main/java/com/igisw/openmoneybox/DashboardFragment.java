/* **************************************************************
 * Name:
 * Purpose:   Dashboard fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-02-28
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;

import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_NUMBER;
import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_OEMICON;
import static com.igisw.openmoneybox.omb_library.FormDigits;
import static com.igisw.openmoneybox.omb_library.getCustomIcons;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;

    private mainactivity activity;
    private DatePicker calendar;

    public ArrayList<mainactivity.TCategorySummary> summary;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (mainactivity) getActivity();
        ArrayList<Bitmap> customIcons = getCustomIcons();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        calendar = view.findViewById( R.id.Calendar );
        calendar.init(calendar.getYear(), calendar.getMonth(), calendar.getDayOfMonth(),
                (arg0, arg1, arg2, arg3) -> giveDateTime()
        );

        LinearLayout catLayout = view.findViewById( R.id.catLayout );
        if(summary != null) {

            // Update view
            int iconIndex;
            TextView category, value;
            ImageView icon;
            Currency curr = Currency.getInstance(Locale.getDefault());
            TypedArray catIcons = getResources().obtainTypedArray(R.array.category_drawables_values);

            for(int i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++){
                switch(i){
                    case 1:
                        category = view.findViewById(R.id.teCategory2);
                        value = view.findViewById(R.id.teValue2);
                        icon = view.findViewById(R.id.teIcon2);
                        break;
                    case 2:
                        category = view.findViewById(R.id.teCategory3);
                        value = view.findViewById(R.id.teValue3);
                        icon = view.findViewById(R.id.teIcon3);
                        break;
                    default:
                        category = view.findViewById(R.id.teCategory1);
                        value = view.findViewById(R.id.teValue1);
                        icon = view.findViewById(R.id.teIcon1);
                }
                if (summary.get(i).Init) {
                    category.setText(summary.get(i).Name);
                    value.setText(String.format("%s %s", FormDigits(summary.get(i).Value, true), curr.getSymbol()));
                    iconIndex = summary.get(i).IconIndex;
                    if (iconIndex > 0)
                        if(iconIndex < _OMB_TOPCATEGORIES_OEMICON)
                            icon.setImageResource(catIcons.getResourceId(iconIndex, -1));
                        else {
                            iconIndex -= 100;
                            if(iconIndex < customIcons.size())
                                icon.setImageBitmap(customIcons.get(iconIndex));
                        }
                    else
                        icon.setImageResource(catIcons.getResourceId(0, -1));
                    if (summary.get(i).Value > 0) {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            Resources.Theme theme = this.getContext().getTheme();
                            category.setBackgroundColor(getResources().getColor(R.color.green, theme));
                            value.setBackgroundColor(getResources().getColor(R.color.green, theme));
                            icon.setBackgroundColor(getResources().getColor(R.color.green, theme));
                        }
                        else {
                            category.setBackgroundColor(getResources().getColor(R.color.green));
                            value.setBackgroundColor(getResources().getColor(R.color.green));
                            icon.setBackgroundColor(getResources().getColor(R.color.green));
                        }
                    } else {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Resources.Theme theme = this.getContext().getTheme();
                            category.setBackgroundColor(getResources().getColor(R.color.red, theme));
                            category.setTextColor(getResources().getColor(R.color.white, theme));
                            value.setBackgroundColor(getResources().getColor(R.color.red, theme));
                            value.setTextColor(getResources().getColor(R.color.white, theme));
                            icon.setBackgroundColor(getResources().getColor(R.color.red, theme));
                        }
                        else {
                            category.setBackgroundColor(getResources().getColor(R.color.red));
                            category.setTextColor(getResources().getColor(R.color.white));
                            value.setBackgroundColor(getResources().getColor(R.color.red));
                            value.setTextColor(getResources().getColor(R.color.white));
                            icon.setBackgroundColor(getResources().getColor(R.color.red));
                        }
                    }
                }
                if (summary.get(i).Init) {
                    category.setVisibility(View.VISIBLE);
                    value.setVisibility(View.VISIBLE);
                    icon.setVisibility(View.VISIBLE);
                } else {
                    category.setVisibility(View.GONE);
                    value.setVisibility(View.GONE);
                    icon.setVisibility(View.GONE);
                }
            }
            if (summary.get(0).Init) catLayout.setVisibility(View.VISIBLE);
            else catLayout.setVisibility(View.GONE);

            catIcons.recycle();

        }

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void giveDateTime(){
        int day = calendar.getDayOfMonth();
        int month = calendar.getMonth();
        int year =  calendar.getYear();

        activity.Data.Day.set(year, month, day, 0, 0, 0);
    }

}
