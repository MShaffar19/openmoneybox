/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-07-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class recycler_adapter_operations extends RecyclerView.Adapter<recycler_adapter_operations.ViewHolder>{

    public mainactivity frame;

    private static final int DATE = 0;
    //public static final int OPERATION = 1;

    private final List<lines_wrapper> operations;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View v) {
            super(v);
        }
    }

    class DateViewHolder extends ViewHolder {

        final CardView cv;
        final TextView reportTime, reportValue;

        DateViewHolder(View itemView) {
            super(itemView);

            LinearLayout ll = itemView.findViewById(R.id.ll_cv_date);
            RelativeLayout rl = itemView.findViewById(R.id.rl_cv_date);
            this.cv = itemView.findViewById(R.id.cv);
            this.reportTime = itemView.findViewById(R.id.report_date);
            this.reportValue = itemView.findViewById(R.id.report_total);

            if (frame.Opts.getBoolean("GDarkTheme", false))
                ll.setBackgroundColor(0xFF000000);

            int col;
            Resources.Theme theme = omb_library.appContext.getTheme();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                col = frame.getResources().getColor(R.color.green, theme);
            }
            else{
                col = frame.getResources().getColor(R.color.green);
            }
            cv.setCardBackgroundColor(col);
            rl.setBackgroundColor(col);
            reportTime.setBackgroundColor(col);
            reportValue.setBackgroundColor(col);

            if (frame.Opts.getBoolean("GDarkTheme", false)) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    reportTime.setTextColor(frame.getResources().getColor(R.color.white_bright, theme));
                    reportValue.setTextColor(frame.getResources().getColor(R.color.white_bright, theme));
                }
                else{
                    reportTime.setTextColor(frame.getResources().getColor(R.color.white_bright));
                    reportValue.setTextColor(frame.getResources().getColor(R.color.white_bright));
                }
            }
        }
    }

    class OperationViewHolder extends ViewHolder {

        final CardView cv;
        final TextView reportValue, reportReason, reportTime, reportCategory;
        final ImageView reportIcon, reportBadge, reportMap;

        OperationViewHolder(View itemView) {
            super(itemView);

            LinearLayout ll = itemView.findViewById(R.id.ll_cv_operation);
            RelativeLayout rl = itemView.findViewById(R.id.rl_cv_operation );
            RelativeLayout rl2 = itemView.findViewById(R.id.rl_cv_operation_2 );
            this.cv = itemView.findViewById(R.id.cv);
            this.reportValue = itemView.findViewById(R.id.report_value);
            this.reportReason = itemView.findViewById(R.id.report_reason);
            this.reportTime = itemView.findViewById(R.id.report_time);
            this.reportCategory = itemView.findViewById(R.id.report_category);
            this.reportIcon = itemView.findViewById(R.id.report_icon);
            this.reportBadge = itemView.findViewById(R.id.quickContactBadge);
            this.reportMap = itemView.findViewById(R.id.poi_icon);

            if(frame.Opts.getBoolean("GDarkTheme", false))
            {
                ll.setBackgroundColor(0xFF000000);
                cv.setCardBackgroundColor(0xFF333333);
                rl.setBackgroundColor(0xFF333333);
                rl2.setBackgroundColor(0xFF333333);
                reportValue.setBackgroundColor(0xFF333333);
                reportReason.setBackgroundColor(0xFF333333);
                reportTime.setBackgroundColor(0xFF333333);
                reportCategory.setBackgroundColor(0xFF333333);
                reportIcon.setBackgroundColor(0xFF333333);
                reportBadge.setBackgroundColor(0xFF333333);
                reportMap.setBackgroundColor(0xFF333333);
            }
        }
    }

    public recycler_adapter_operations(List<lines_wrapper> operations){
        this.operations = operations;
    }

    @Override
    public int getItemCount() {
        return operations.size();
    }

    @Override
    public @NonNull ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        if(viewType == DATE){
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_date, viewGroup, false);
            return new DateViewHolder(v);
        }
        else {
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_operation, viewGroup, false);
            return new OperationViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        if(viewHolder.getItemViewType() == DATE){
            DateViewHolder holder = (DateViewHolder) viewHolder;
            holder.reportTime.setText(operations.get(i).time);
            holder.reportValue.setText(operations.get(i).value);
        }
        else {
            OperationViewHolder holder = (OperationViewHolder) viewHolder;
            holder.reportValue.setText(operations.get(i).value);

            int col;
            if(operations.get(i).currencyIndex != -1)
                col = R.color.green;
            else if(frame.Opts.getBoolean("GDarkTheme", false))
                col = R.color.white;
            else
                col = R.color.black;

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.reportValue.setTextColor(frame.getResources().getColor(col, omb_library.appContext.getTheme()));
            }
            else {
                holder.reportValue.setTextColor(frame.getResources().getColor(col));
            }

            holder.reportReason.setText(operations.get(i).reason);
            holder.reportTime.setText(operations.get(i).time);
            holder.reportCategory.setText(operations.get(i).category);
            holder.reportIcon.setImageResource(operations.get(i).iconId);

            String b = operations.get(i).badgeUri;
            if(b != null) {
                Bitmap bitmap = omb_library.loadContactPhotoThumbnail(b);
                holder.reportBadge.setImageBitmap(bitmap);
            }

            if((operations.get(i).latitude != constants.ombInvalidLatitude) ||
                    (operations.get(i).longitude != constants.ombInvalidLongitude))
                holder.reportMap.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return operations.get(position).type;
    }

}
