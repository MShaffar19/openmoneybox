/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-10-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.preference.PreferenceManager;

import java.io.File;
import java.util.GregorianCalendar;

public class documentselection extends Activity {

	private static final int PICKFILE_RESULT_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.documentselection);
	}
	
	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
	public void BrowseClick(View view) {
		// ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
		// browser.
		Intent fileintent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

		// Filter to only show results that can be "opened", such as a
		// file (as opposed to a list of contacts or timezones)
		fileintent.addCategory(Intent.CATEGORY_OPENABLE);

        fileintent.setType("*/*");
        try {
        	startActivityForResult(fileintent, PICKFILE_RESULT_CODE);
        	//Uri uri = fileintent.getData();
        	//FilePath = FileUtils.getPath(this, uri);
            
        } catch (ActivityNotFoundException e) {
            //Log.e("tag", "No activity can handle picking a file. Showing alternatives.");

		}
	}
	
	public void WizardClick(View view) {
		Intent intent = new Intent(this, wizard.class);
       	startActivityForResult(intent, 2);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Fix no activity available
        if (data != null) {
			switch (requestCode) {
				case PICKFILE_RESULT_CODE:
					if (resultCode == RESULT_OK) {
						String filePath ;
						Uri uri = data.getData();

						filePath = RealPathUtil.getRealPath(getApplicationContext(), uri);

						//FilePath is your file as a string
						if (!filePath.isEmpty()) {
							SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
							Editor edit = Opts.edit();
							edit.putString("GDDoc", filePath);
							edit.apply();
						}

						setResult(resultCode);
						finish();
					}
					break;
				case 2:
					Bundle bundle = data.getExtras();
					double saved = bundle.getDouble("saved");
					double cash = bundle.getDouble("cash");

					String extState = Environment.getExternalStorageState();

					if (extState.equals(Environment.MEDIA_MOUNTED)) {
						File sd;
						if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
							sd = getApplicationContext().getExternalFilesDir(null);
						}
						else {
							sd = Environment.getExternalStorageDirectory();
						}

						// Following block defines a not file that does not already exist
						int fileTrail = 0;
						String testPath = sd.getAbsolutePath() + "/openmoneybox";
						String filename = testPath + ".omb";
						File testFile = new File(filename);
						while(testFile.exists()){
							fileTrail++;
							filename = String.format(testPath + "%d.omb", fileTrail);
							testFile = new File(filename);
						}

						omb34core Data_Auto = new omb34core(filename, null);
						String cash_fund = getResources().getString(R.string.wizard_cash_fund);
						Data_Auto.addValue(omb34core.TTypeVal.tvFou, -1,
							getResources().getString(R.string.wizard_saved_fund), saved, -1);
						Data_Auto.addValue(omb34core.TTypeVal.tvFou, -1, cash_fund, cash, -1);
						Data_Auto.setDefaultFund(cash_fund);
						Data_Auto.addDate(new GregorianCalendar(), omb_library.FormDigits(saved + cash, true));
						Data_Auto.database.execSQL("RELEASE roll_back;");
						Data_Auto.database.close();

						SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
						Editor edit = Opts.edit();
						edit.putString("GDDoc", filename);
						edit.apply();

						finish();
					}
					break;
				default:
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		// Do nothing
	}

}
