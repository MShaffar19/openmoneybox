/* **************************************************************
 * Name:
 * Purpose:   Funds fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-11-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FundsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FundsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FundsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private OnFragmentInteractionListener mListener;

    public int fund_pos;
    private mainactivity activity;
    private ImageButton btn_removeFund, btn_editFund;

    public FundsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FundsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FundsFragment newInstance(String param1, String param2) {
        FundsFragment fragment = new FundsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (mainactivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_funds, container, false);

        // Funds tab
        String str_tmp;
        Currency curr = Currency.getInstance(Locale.getDefault());
        TextView total_view;

        btn_removeFund = view.findViewById( R.id.removeFund );
        btn_editFund = view.findViewById( R.id.editFund );
        btn_removeFund.setEnabled(false);
        btn_editFund.setEnabled(false);

        // Find the ListView resource.
        ListView fundView = view.findViewById(R.id.FundView);

        ArrayList<String> fundList = new ArrayList<>();
        //fundList.addAll(Arrays.asList(test));
        for (int i = 0; i < activity.Data.NFun; i++) {
            str_tmp = activity.Data.Funds.get(i).Name + "<br><small>" + curr.getSymbol() + " ";
            str_tmp += omb_library.FormDigits(activity.Data.Funds.get(i).Value, false) + "</small>";
            //str_tmp += " )";
            fundList.add(str_tmp);
        }
        // Create ArrayAdapter
        ArrayAdapter<String> fundAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_single_choice, fundList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    textView.setText(Html.fromHtml(textView.getText().toString(), Html.FROM_HTML_MODE_COMPACT));
                else textView.setText(HtmlCompat.fromHtml(textView.getText().toString(), HtmlCompat.FROM_HTML_MODE_LEGACY));
                return textView;
            }
        };
        // Set the ArrayAdapter as the ListView's adapter.
        fundView.setAdapter(fundAdapter);
        total_view = view.findViewById(R.id.total_fund);
        str_tmp = getResources().getString(R.string.total) + "<br><small>" + curr.getSymbol() + " ";
        str_tmp += omb_library.FormDigits(activity.Data.Tot_Funds, false) + "</small>";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_COMPACT));
        else total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_LEGACY));

        fundView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View arg1, int position, long arg3) {
                boolean enabled = true;
                if (position >= 0) {
                    if (activity.Data.Funds.get(position).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0)
                        enabled = false;
                }
                btn_removeFund.setEnabled(enabled);
                btn_editFund.setEnabled(enabled);
                if (position >= 0) fund_pos = position;
            }
        });

        return view;
    }

    /*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    */

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
