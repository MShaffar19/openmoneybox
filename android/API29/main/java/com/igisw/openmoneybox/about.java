/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * forked from https://tekeye.uk/android/examples/ui/about-box-in-android-app
 * Created:   2022-01-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

class about {

    private static final String WebAddress = "http://igisw-bilancio.sourceforge.net";
    private static final String Email = "igor.cali0@gmail.com";
    private static final String Copyright = "© 2015-2022 Igor Calì <igor.cali0@gmail.com>";
	
	private static String VersionName(Context context) {
    try {
      return context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionName;
    } catch (NameNotFoundException e) {
      return "Unknown";
    }
  }

  public static void Show(Activity callingActivity) {
    //Use a Spannable to allow for links highlighting
    SpannableString aboutText = new SpannableString(
        /*+ */Copyright + "\n\n"
        + callingActivity.getString(R.string.website) + WebAddress + " \n\n"
        + callingActivity.getString(R.string.about_text) + " " + Email);
    //Generate views to pass to AlertDialog.Builder and to set the text
    View about;
    TextView tvAbout;
    try {
      //Inflate the custom view
      LayoutInflater inflater = callingActivity.getLayoutInflater();
      about = inflater.inflate(R.layout.about, callingActivity.findViewById(R.id.aboutView));
      tvAbout = about.findViewById(R.id.aboutText);
    } catch(InflateException e) {
      //Inflater can throw exception, unlikely but default to TextView if it occurs
      about = tvAbout = new TextView(callingActivity);
    }

    //  Linkify the text
    SpannableString versionText = new SpannableString(
      "\n" + callingActivity.getResources().getString(R.string.about_version) + " "
          + VersionName(callingActivity) + "\n");
    Linkify.addLinks(/*tvAbout*/aboutText, Linkify.ALL);
    //Set the about text
    tvAbout.setText(versionText);
    tvAbout.append(aboutText);
    //Build and show the dialog
    new AlertDialog.Builder(callingActivity, R.style.ombDialogTheme)
      .setTitle(callingActivity.getResources().getString(R.string.about_title) + " " + callingActivity.getString(R.string.app_name))
      .setCancelable(true)
      .setIcon(R.mipmap.ic_launcher)
      .setPositiveButton("OK", null)
      .setView(about)
      .show();    //Builder method returns allow for method chaining
  }
}
