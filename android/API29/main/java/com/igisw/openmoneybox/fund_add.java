/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-03-26
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

public class fund_add extends Activity {

	private EditText nameText, valueText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.funds_add);
		
		nameText = findViewById(R.id.Name);
		valueText = findViewById(R.id.Value);
		TextView caption = findViewById(R.id.Title);
		caption.setText(getResources().getString(R.string.fund_add));

		ImageButton findButton = findViewById(R.id.searchButton);
		findButton.setVisibility(View.GONE);

		CheckBox oldItem= findViewById(R.id.OldItemCheck);
		oldItem.setVisibility(View.GONE);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
            LinearLayout ll = findViewById(R.id.ll_funds);
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				Resources.Theme theme = getTheme();
				ll.setBackgroundColor(getResources().getColor(R.color.black, theme));
				nameText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				nameText.setTextColor(getResources().getColor(R.color.white, theme));
				valueText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				valueText.setTextColor(getResources().getColor(R.color.white, theme));
			}
			else{
				ll.setBackgroundColor(getResources().getColor(R.color.black));
				nameText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				nameText.setTextColor(getResources().getColor(R.color.white));
				valueText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				valueText.setTextColor(getResources().getColor(R.color.white));
			}
		}
	}
	
	public void okBtnClick(View view){
		double cur;
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		String name = nameText.getText().toString();
		if(name.isEmpty()){
			omb_library.Error(23, "");
			nameText.requestFocus();
			return;}
		
		if(valueText.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			valueText.requestFocus();
			return;}
		cur = Double.parseDouble(valueText.getText().toString());
		
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		name = omb_library.iUpperCase(name);
		name = name.trim();
		bundle.putString("fund", name);
		bundle.putDouble("value", cur);
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
}
