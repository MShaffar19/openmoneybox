/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * forked from https://github.com/Agilevent/Eula-Sample
 * Created:   2020-07-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

class SimpleEula {

    private final Activity mActivity;
	
	public SimpleEula(Activity context) {
		mActivity = context; 
	}
	
	private PackageInfo getPackageInfo() {
        PackageInfo pi = null;
        try {
             pi = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi; 
    }

     public void show() {
        PackageInfo versionInfo = getPackageInfo();

        // the eulaKey changes every time you increment the version number in the AndroidManifest.xml
        String EULA_PREFIX = "eula_";
        final String eulaKey;
        final String oldKey;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            long ver = versionInfo.getLongVersionCode();
            eulaKey = EULA_PREFIX + ver;
            oldKey = EULA_PREFIX + (ver - 1);
        }
        else{
            int ver = versionInfo.versionCode;
            eulaKey = EULA_PREFIX + ver;
            oldKey = EULA_PREFIX + (ver - 1);
        }

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        boolean hasBeenShown = prefs.getBoolean(eulaKey, false);
        
        if(! hasBeenShown){

        	// Show the Eula
            String title = mActivity.getString(R.string.app_name) + " v" + versionInfo.versionName;
            
            //Includes the updates as well so users know what changed. 

            String eula = "";

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                try (BufferedReader reader = new BufferedReader(
                        new InputStreamReader(mActivity.getAssets().open(
                                mActivity.getString(R.string.lang) + "/license.txt"), StandardCharsets.UTF_8))) {

                    // do reading, usually loop until end of file reading
                    String mLine;
                    while ((mLine = reader.readLine()) != null) {
                        //process line
                        eula += mLine + "\n";
                    }
                } catch (IOException e) {
                    //log the exception
                }
            }

            String message = mActivity.getString(R.string.updates) + "\n\n" + eula;

            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new Dialog.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Mark this version as read.
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(eulaKey, true);
                            editor.apply();
                            dialogInterface.dismiss();

                            if(prefs.getBoolean(oldKey, false)){
	                            editor.remove(oldKey);
	                            editor.apply();}
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new Dialog.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// Close the activity as they have declined the EULA
							mActivity.finish(); 
						}
                    	
                    });
            builder.create().show();
        }
    }
	
}
