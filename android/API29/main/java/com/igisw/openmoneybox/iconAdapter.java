/* **************************************************************
 * Name:
 * Purpose:   Icon adapter for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-07-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.preference.PreferenceManager;

import java.util.ArrayList;

import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_OEMICON;
import static com.igisw.openmoneybox.omb_library.getCustomIcons;

public class iconAdapter extends BaseAdapter {
    private final Context mContext;
    private final TypedArray catIcons;
    private final ArrayList<Bitmap> customIcons;

    // Constructor
    public iconAdapter(Context c, TypedArray iconArray){
        mContext = c;
        catIcons = iconArray;
        customIcons = getCustomIcons();
    }

    @Override
    public int getCount() {
        return catIcons.length() + customIcons.size();
    }

    @Override
    public Object getItem(int position) {
        return catIcons.getIndex(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);

        if(position < _OMB_TOPCATEGORIES_OEMICON)
            imageView.setImageResource(catIcons.getResourceId(position, -1));
        else{
            imageView.setImageBitmap(customIcons.get(position - _OMB_TOPCATEGORIES_OEMICON));
        }

        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(mContext);
        if(Opts.getBoolean("GDarkTheme", false)) {
            int cl;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Resources.Theme theme = mContext.getTheme();
                cl = mContext.getResources().getColor(R.color.white, theme);
            }
            else{
                cl = mContext.getResources().getColor(R.color.white);
            }
            imageView.setBackgroundColor(cl);
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(70, 70));
        return imageView;
    }

}
