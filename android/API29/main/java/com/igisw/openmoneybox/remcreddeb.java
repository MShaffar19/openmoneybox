/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-07-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

public class remcreddeb extends Activity {

  private EditText edittext;
  private TextView label;
  private RadioButton total, partial;
  
  private int labels;
  private double max_value;
 
  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.remove_creddeb);
 
	label = findViewById(R.id.Title);
	total = findViewById(R.id.total);
	partial = findViewById(R.id.partial);

	  SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
	  if(Opts.getBoolean("GDarkTheme", false)) {
		  this.setTheme(R.style.DarkTheme);
		  LinearLayout ll = findViewById(R.id.ll_remcreddeb);

		  Resources.Theme theme = getTheme();
		  ColorStateList colorStateList;

		  if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			  ll.setBackgroundColor(getResources().getColor(R.color.black, theme));
			  colorStateList = new ColorStateList(
					  new int[][]{
							  new int[]{-android.R.attr.state_enabled}, //disabled
							  new int[]{android.R.attr.state_enabled} //enabled
					  },
					  new int[] {
							  getResources().getColor(R.color.highlight_dark, theme) , //disabled
							  getResources().getColor(R.color.white, theme) //enabled
					  }
			  );
		  }
		  else{
			  ll.setBackgroundColor(getResources().getColor(R.color.black));
			  colorStateList = new ColorStateList(
					  new int[][]{
							  new int[]{-android.R.attr.state_enabled}, //disabled
							  new int[]{android.R.attr.state_enabled} //enabled
					  },
					  new int[] {
							  getResources().getColor(R.color.highlight_dark) , //disabled
							  getResources().getColor(R.color.white) //enabled
					  }
			  );
		  }

		  total.setTextColor(colorStateList);//set the color tint list
		  partial.setTextColor(colorStateList);//set the color tint list

		  EditText nameText = findViewById(R.id.editText);
		  if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			  nameText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
			  nameText.setTextColor(getResources().getColor(R.color.white, theme));
		  }
		  else{
			  nameText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
			  nameText.setTextColor(getResources().getColor(R.color.white));
		  }
	  }

	Bundle bundle = getIntent().getExtras();
	labels = bundle.getInt("labels");
	initLabels(labels);
	max_value = bundle.getDouble("max_value");
	
	addListenerOnButton();
 
  }
 
  private void addListenerOnButton() {
	  edittext = findViewById(R.id.editText);
	  edittext.setEnabled(false);
	  RadioGroup radioGroup = findViewById(R.id.radioGroup);
	
	radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			boolean enabled = (checkedId == R.id.partial);
			edittext.setEnabled(enabled);
		}
		
	});
	
  }
  
	public void okBtnClick(View view){
		double cur = -1;
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		if(edittext.isEnabled()){
			if(edittext.getText().toString().isEmpty()){
				omb_library.Error(25, "");
				edittext.requestFocus();
				return;}

			cur = Double.parseDouble(edittext.getText().toString());

		if(cur > max_value){
			String Opt;
			switch(labels){
				case 2:
				case 3:
					Opt = getResources().getString(R.string.debt_term);
					break;
				default:
					Opt = getResources().getString(R.string.credit_term);
			}
			omb_library.Error(21, Opt);
			edittext.requestFocus();
			return;}
		}
		
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putBoolean("total", ! edittext.isEnabled());
		bundle.putDouble("value", cur);
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}

	private void initLabels(int value) {
		switch(value){
			case 1:	// Condone credit
				label.setText(getResources().getString(R.string.credit_condone2));
				total.setText(getResources().getString(R.string.condone_total));
				partial.setText(getResources().getString(R.string.condone_partially));
				break;
			case 2:	// Remove debt
				label.setText(getResources().getString(R.string.debt_delete));
				break;
			case 3:	// Condone debt
				label.setText(getResources().getString(R.string.debt_condone2));
				total.setText(getResources().getString(R.string.condone_total));
				partial.setText(getResources().getString(R.string.condone_partially));
				break;
			default:
				label.setText(getResources().getString(R.string.credit_delete));
		}
	}
	
}

