/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-01-31
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

class shoplist_wrapper {
    final int id;
    final String item, alarm;

    shoplist_wrapper(int id, String item, String alarm) {
        this.id = id;
        this.item = item;
        this.alarm = alarm;
    }
}
