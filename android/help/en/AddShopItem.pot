# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-03 16:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: AddShopItem.xml:1
#, no-c-format
msgid "Add an item to the shop-list"
msgstr ""

#. Tag: para
#: AddShopItem.xml:2
#, no-c-format
msgid "Tap the button <guiicon>Add shop-list item</guiicon> in the Shopping List tab to store a new shop-list item."
msgstr ""

#. Tag: para
#: AddShopItem.xml:6
#, no-c-format
msgid "Insert in the text box the object to de added in the shop-list."
msgstr ""

#. Tag: para
#: AddShopItem.xml:7
#, no-c-format
msgid "If you want to set an <link linkend=\"alm\">alarm</link> for this item, mark the alarm checkbox and select the expiration day."
msgstr ""

#. Tag: para
#: AddShopItem.xml:8
#, no-c-format
msgid "In case of missing or wrong data, an error message will appear."
msgstr ""

