# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-03 17:01+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: New_fund.xml:1
#, no-c-format
msgid "Store a new fund"
msgstr ""

#. Tag: para
#: New_fund.xml:2
#, no-c-format
msgid "Tap the button <guiicon>New fund</guiicon> int the Funds tab to store a new fund."
msgstr ""

#. Tag: para
#: New_fund.xml:8
#, no-c-format
msgid "Type in the first edit box the name to identify the new fund: it will be added to the list view."
msgstr ""

#. Tag: para
#: New_fund.xml:10
#, no-c-format
msgid "Type in the second edit box the value to be assigned to the fund: it will be subtracted to the <link linkend=\"deffund\">default fund</link>; in case the specified value is bigger of that fund an error message will be shown."
msgstr ""

#. Tag: para
#: New_fund.xml:14
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: New_fund.xml:16
#, no-c-format
msgid "Related topics: <link linkend=\"deffund\">Set the defaut fund</link>."
msgstr ""

