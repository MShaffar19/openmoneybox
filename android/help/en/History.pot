# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-30 16:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: History.xml:1
#, no-c-format
msgid "History"
msgstr ""

#. Tag: title
#: History.xml:3
#, no-c-format
msgid "Change log"
msgstr ""

#. Tag: entry
#: History.xml:12
#, no-c-format
msgid "DATE"
msgstr ""

#. Tag: entry
#: History.xml:13
#, no-c-format
msgid "VERSION"
msgstr ""

#. Tag: entry
#: History.xml:14
#, no-c-format
msgid "DESCRIPTION"
msgstr ""

#. Tag: entry
#: History.xml:21
#, no-c-format
msgid "08/03/2022"
msgstr ""

#. Tag: entry
#: History.xml:22
#, no-c-format
msgid "3.4.1.13"
msgstr ""

#. Tag: listitem
#: History.xml:26
#, no-c-format
msgid "User authentication added [#23];"
msgstr ""

#. Tag: listitem
#: History.xml:29
#, no-c-format
msgid "Show 2 decimal digits in chart values [#21];"
msgstr ""

#. Tag: listitem
#: History.xml:32
#, no-c-format
msgid "Fix unreadable text with dark theme [#22];"
msgstr ""

#. Tag: listitem
#: History.xml:35
#, no-c-format
msgid "Notifications start on boot;"
msgstr ""

#. Tag: listitem
#: History.xml:38
#, no-c-format
msgid "Swedish translation added, thanks to Simon Nilsson (Simon@Observeramera.com);"
msgstr ""

#. Tag: listitem
#: History.xml:41
#, no-c-format
msgid "Code refreshed (removed deprecations)."
msgstr ""

#. Tag: listitem
#: History.xml:25
#, no-c-format
msgid "Fixed crash in Edit Categories [gitlab #19];"
msgstr ""

#. Tag: listitem
#: History.xml:28
#, no-c-format
msgid "Fixed clumsy items in Edit Categories [gitlab #20];"
msgstr ""

#. Tag: listitem
#: History.xml:31
#, no-c-format
msgid "About dialog readability improved;"
msgstr ""

#. Tag: listitem
#: History.xml:34
#, no-c-format
msgid "sqlcipher 4.5.1."
msgstr ""

#. Tag: entry
#: History.xml:42
#, no-c-format
msgid "15/08/2021"
msgstr ""

#. Tag: entry
#: History.xml:43
#, no-c-format
msgid "3.4.1.11"
msgstr ""

#. Tag: listitem
#: History.xml:46
#, no-c-format
msgid "Global file permission fixed on Android 11 [gitlab #13];"
msgstr ""

#. Tag: listitem
#: History.xml:49
#, no-c-format
msgid "Fixed crash when active locale has non-latin numbers [gitlab #16];"
msgstr ""

#. Tag: listitem
#: History.xml:52
#, no-c-format
msgid "Italian translation updated;"
msgstr ""

#. Tag: listitem
#: History.xml:55
#, no-c-format
msgid "sqlcipher 4.4.3;"
msgstr ""

#. Tag: listitem
#: History.xml:58
#, no-c-format
msgid "osmdroid 6.1.11;"
msgstr ""

#. Tag: listitem
#: History.xml:61
#, no-c-format
msgid "New icon for Add Shopping-list item."
msgstr ""

#. Tag: entry
#: History.xml:69
#, no-c-format
msgid "06/04/2021"
msgstr ""

#. Tag: entry
#: History.xml:70
#, no-c-format
msgid "3.4.1.10"
msgstr ""

#. Tag: entry
#: History.xml:71
#, no-c-format
msgid "Spanish translation added (thanks to einerds@nauta.cu)."
msgstr ""

#. Tag: entry
#: History.xml:76
#, no-c-format
msgid "27/03/2021"
msgstr ""

#. Tag: entry
#: History.xml:77
#, no-c-format
msgid "3.4.1.9"
msgstr ""

#. Tag: entry
#: History.xml:78
#, no-c-format
msgid "Fixed crash on entering the Map view [gitlab #15]."
msgstr ""

#. Tag: entry
#: History.xml:83
#, no-c-format
msgid "26/03/2021"
msgstr ""

#. Tag: entry
#: History.xml:84
#, no-c-format
msgid "3.4.1.8"
msgstr ""

#. Tag: entry
#: History.xml:85
#, no-c-format
msgid "Added the capability to store old debits and credits."
msgstr ""

#. Tag: entry
#: History.xml:89
#, no-c-format
msgid "28/02/2021"
msgstr ""

#. Tag: entry
#: History.xml:90
#, no-c-format
msgid "3.4.1.7"
msgstr ""

#. Tag: listitem
#: History.xml:93
#, no-c-format
msgid "Fixed crash on open on Android 10 [gitlab #14];"
msgstr ""

#. Tag: listitem
#: History.xml:96
#, no-c-format
msgid "Top Categories color contrast improved [gitlab #12]."
msgstr ""

#. Tag: entry
#: History.xml:104
#, no-c-format
msgid "05/02/2021"
msgstr ""

#. Tag: entry
#: History.xml:105
#, no-c-format
msgid "3.4.1.6"
msgstr ""

#. Tag: listitem
#: History.xml:108
#, no-c-format
msgid "Top categories improved when less than three items are shown;"
msgstr ""

#. Tag: listitem
#: History.xml:111
#, no-c-format
msgid "Shopping List user interface improved;"
msgstr ""

#. Tag: listitem
#: History.xml:114 History.xml:207
#, no-c-format
msgid "dark theme refined;"
msgstr ""

#. Tag: listitem
#: History.xml:117
#, no-c-format
msgid "sqlcipher v4.4.2;"
msgstr ""

#. Tag: listitem
#: History.xml:120
#, no-c-format
msgid "osmdroid v6.1.10."
msgstr ""

#. Tag: entry
#: History.xml:128
#, no-c-format
msgid "22/11/2020"
msgstr ""

#. Tag: entry
#: History.xml:129
#, no-c-format
msgid "3.4.1.5"
msgstr ""

#. Tag: listitem
#: History.xml:132
#, no-c-format
msgid "Fixed crash for Credits and Debts [gitlab #8];"
msgstr ""

#. Tag: listitem
#: History.xml:135
#, no-c-format
msgid "Fixed crash on searching contacts [gitlab #9];"
msgstr ""

#. Tag: listitem
#: History.xml:138
#, no-c-format
msgid "Fixed crash on storing objects [gitlab #10];"
msgstr ""

#. Tag: listitem
#: History.xml:141
#, no-c-format
msgid "sqlcipher v4.4.1."
msgstr ""

#. Tag: entry
#: History.xml:149
#, no-c-format
msgid "16/08/2020"
msgstr ""

#. Tag: entry
#: History.xml:150
#, no-c-format
msgid "3.4.1.4"
msgstr ""

#. Tag: listitem
#: History.xml:153
#, no-c-format
msgid "Fixed crash during creation wizard;"
msgstr ""

#. Tag: listitem
#: History.xml:156
#, no-c-format
msgid "Dialog UI refreshed;"
msgstr ""

#. Tag: listitem
#: History.xml:159
#, no-c-format
msgid "Bug report link updated;"
msgstr ""

#. Tag: listitem
#: History.xml:162
#, no-c-format
msgid "Fixed missing translation;"
msgstr ""

#. Tag: listitem
#: History.xml:165
#, no-c-format
msgid "osmdroid v6.1.8;"
msgstr ""

#. Tag: listitem
#: History.xml:168
#, no-c-format
msgid "sqlcipher v4.4.0."
msgstr ""

#. Tag: entry
#: History.xml:176
#, no-c-format
msgid "09/03/2020"
msgstr ""

#. Tag: entry
#: History.xml:177
#, no-c-format
msgid "3.4.1.3"
msgstr ""

#. Tag: listitem
#: History.xml:180
#, no-c-format
msgid "Fixed crash due to old master format [gitlab #5];"
msgstr ""

#. Tag: listitem
#: History.xml:183
#, no-c-format
msgid "osmdroid v6.1.5;"
msgstr ""

#. Tag: listitem
#: History.xml:186
#, no-c-format
msgid "sqlcipher v4.3.0."
msgstr ""

#. Tag: entry
#: History.xml:194
#, no-c-format
msgid "04/12/2019"
msgstr ""

#. Tag: entry
#: History.xml:195
#, no-c-format
msgid "3.4.1.1"
msgstr ""

#. Tag: listitem
#: History.xml:198
#, no-c-format
msgid "Category icons and Top categories added;"
msgstr ""

#. Tag: listitem
#: History.xml:201
#, no-c-format
msgid "fix LP #1848840 - Wrong category id assigned in operation;"
msgstr ""

#. Tag: listitem
#: History.xml:204
#, no-c-format
msgid "fix LP #1848370 - Custom currency fields not exported in archive;"
msgstr ""

#. Tag: listitem
#: History.xml:210
#, no-c-format
msgid "osmdroid v6.1.4."
msgstr ""

#. Tag: entry
#: History.xml:218
#, no-c-format
msgid "04/09/2019"
msgstr ""

#. Tag: entry
#: History.xml:219
#, no-c-format
msgid "3.3.1.7"
msgstr ""

#. Tag: entry
#: History.xml:220
#, no-c-format
msgid "Fixed crash during wizard [gitlab #3]."
msgstr ""

#. Tag: entry
#: History.xml:224
#, no-c-format
msgid "11/08/2019"
msgstr ""

#. Tag: entry
#: History.xml:225
#, no-c-format
msgid "3.3.1.6"
msgstr ""

#. Tag: listitem
#: History.xml:228
#, no-c-format
msgid "sqlcipher v4.2.0;"
msgstr ""

#. Tag: listitem
#: History.xml:231
#, no-c-format
msgid "deprecate omb31x format."
msgstr ""

#. Tag: entry
#: History.xml:239
#, no-c-format
msgid "26/05/2019"
msgstr ""

#. Tag: entry
#: History.xml:240
#, no-c-format
msgid "3.3.1.4"
msgstr ""

#. Tag: entry
#: History.xml:241
#, no-c-format
msgid "Fixed error on profit or expense [gitlab #2]."
msgstr ""

#. Tag: entry
#: History.xml:245
#, no-c-format
msgid "21/05/2019"
msgstr ""

#. Tag: entry
#: History.xml:246
#, no-c-format
msgid "3.3.1.3"
msgstr ""

#. Tag: listitem
#: History.xml:249
#, no-c-format
msgid "Battery consumption improved (location stop on exit);"
msgstr ""

#. Tag: listitem
#: History.xml:252
#, no-c-format
msgid "dark theme improved;"
msgstr ""

#. Tag: listitem
#: History.xml:255
#, no-c-format
msgid "osmdroid v6.1.0;"
msgstr ""

#. Tag: listitem
#: History.xml:258
#, no-c-format
msgid "MPAndroidChart v3.1.0."
msgstr ""

#. Tag: entry
#: History.xml:266
#, no-c-format
msgid "07/04/2019"
msgstr ""

#. Tag: entry
#: History.xml:267
#, no-c-format
msgid "3.3.1.2"
msgstr ""

#. Tag: entry
#: History.xml:268
#, no-c-format
msgid "Fixed crash on startup."
msgstr ""

#. Tag: entry
#: History.xml:272
#, no-c-format
msgid "02/04/2019"
msgstr ""

#. Tag: entry
#: History.xml:273
#, no-c-format
msgid "3.3.1.1"
msgstr ""

#. Tag: listitem
#: History.xml:276
#, no-c-format
msgid "Encryption added;"
msgstr ""

#. Tag: listitem
#: History.xml:279
#, no-c-format
msgid "launcher shortcuts added."
msgstr ""

#. Tag: entry
#: History.xml:287
#, no-c-format
msgid "08/12/2018"
msgstr ""

#. Tag: entry
#: History.xml:288
#, no-c-format
msgid "3.2.2.10"
msgstr ""

#. Tag: listitem
#: History.xml:291
#, no-c-format
msgid "Keep active page on rotation;"
msgstr ""

#. Tag: listitem
#: History.xml:294
#, no-c-format
msgid "osmdroid v6.0.3."
msgstr ""

#. Tag: entry
#: History.xml:302
#, no-c-format
msgid "01/12/2018"
msgstr ""

#. Tag: entry
#: History.xml:303
#, no-c-format
msgid "3.2.2.8"
msgstr ""

#. Tag: entry
#: History.xml:304
#, no-c-format
msgid "XML export fixed (quotes in values)."
msgstr ""

#. Tag: entry
#: History.xml:308
#, no-c-format
msgid "24/11/2018"
msgstr ""

#. Tag: entry
#: History.xml:309
#, no-c-format
msgid "3.2.2.7"
msgstr ""

#. Tag: entry
#: History.xml:310
#, no-c-format
msgid "Dark theme improved."
msgstr ""

#. Tag: entry
#: History.xml:314
#, no-c-format
msgid "19/11/2018"
msgstr ""

#. Tag: entry
#: History.xml:315
#, no-c-format
msgid "3.2.2.6"
msgstr ""

#. Tag: listitem
#: History.xml:318
#, no-c-format
msgid "Improved theme;"
msgstr ""

#. Tag: listitem
#: History.xml:321
#, no-c-format
msgid "Dark theme added."
msgstr ""

#. Tag: entry
#: History.xml:329
#, no-c-format
msgid "30/09/2018"
msgstr ""

#. Tag: entry
#: History.xml:330
#, no-c-format
msgid "3.2.2.4"
msgstr ""

#. Tag: entry
#: History.xml:331
#, no-c-format
msgid "Contact information update when contacts are changed in the phone."
msgstr ""

#. Tag: entry
#: History.xml:335
#, no-c-format
msgid "28/08/2018"
msgstr ""

#. Tag: entry
#: History.xml:336
#, no-c-format
msgid "3.2.2.3"
msgstr ""

#. Tag: listitem
#: History.xml:339
#, no-c-format
msgid "Oreo notifications;"
msgstr ""

#. Tag: listitem
#: History.xml:342
#, no-c-format
msgid "improved dialogs."
msgstr ""

#. Tag: entry
#: History.xml:350
#, no-c-format
msgid "23/08/2018"
msgstr ""

#. Tag: entry
#: History.xml:351
#, no-c-format
msgid "3.2.2.2"
msgstr ""

#. Tag: entry
#: History.xml:352
#, no-c-format
msgid "Fixed crash if null contact photo."
msgstr ""

#. Tag: entry
#: History.xml:356
#, no-c-format
msgid "07/08/2018"
msgstr ""

#. Tag: entry
#: History.xml:357
#, no-c-format
msgid "3.2.1.3"
msgstr ""

#. Tag: entry
#: History.xml:358
#, no-c-format
msgid "Fixed bug LP #1785375 - Crash on startup."
msgstr ""

#. Tag: entry
#: History.xml:362
#, no-c-format
msgid "01/08/2018"
msgstr ""

#. Tag: entry
#: History.xml:363
#, no-c-format
msgid "3.2.1.2"
msgstr ""

#. Tag: listitem
#: History.xml:366
#, no-c-format
msgid "Fixed bug LP #1779746 - Crash during monthly backup;"
msgstr ""

#. Tag: listitem
#: History.xml:369
#, no-c-format
msgid "document browser fixed;"
msgstr ""

#. Tag: listitem
#: History.xml:372
#, no-c-format
msgid "simplified dialogs."
msgstr ""

#. Tag: entry
#: History.xml:380
#, no-c-format
msgid "04/06/2018"
msgstr ""

#. Tag: entry
#: History.xml:381
#, no-c-format
msgid "3.2.1.1"
msgstr ""

#. Tag: listitem
#: History.xml:384
#, no-c-format
msgid "added support for phone contacts;"
msgstr ""

#. Tag: listitem
#: History.xml:387
#, no-c-format
msgid "added storage of locations;"
msgstr ""

#. Tag: listitem
#: History.xml:390
#, no-c-format
msgid ""
"fixed bug LP #1737495 - Shoplist item not removed when confirmed on external "
"alertBox."
msgstr ""

#. Tag: entry
#: History.xml:398
#, no-c-format
msgid "30/10/2017"
msgstr ""

#. Tag: entry
#: History.xml:399
#, no-c-format
msgid "3.1.2.5"
msgstr ""

#. Tag: listitem
#: History.xml:402
#, no-c-format
msgid "fixed crash on BACK press;"
msgstr ""

#. Tag: listitem
#: History.xml:405
#, no-c-format
msgid "publication in F-Droid."
msgstr ""

#. Tag: entry
#: History.xml:413
#, no-c-format
msgid "18/10/2017"
msgstr ""

#. Tag: entry
#: History.xml:414
#, no-c-format
msgid "3.1.2.4"
msgstr ""

#. Tag: entry
#: History.xml:415
#, no-c-format
msgid "Check write permission on startup."
msgstr ""

#. Tag: entry
#: History.xml:419
#, no-c-format
msgid "20/08/2017"
msgstr ""

#. Tag: entry
#: History.xml:420
#, no-c-format
msgid "3.1.2.3"
msgstr ""

#. Tag: entry
#: History.xml:421
#, no-c-format
msgid "Fixed bug LP #1711917 - Wizard malformed document."
msgstr ""

#. Tag: entry
#: History.xml:425
#, no-c-format
msgid "06/08/2017"
msgstr ""

#. Tag: entry
#: History.xml:426
#, no-c-format
msgid "3.1.2.2"
msgstr ""

#. Tag: entry
#: History.xml:427
#, no-c-format
msgid "Updated chart rendering."
msgstr ""

#. Tag: entry
#: History.xml:431
#, no-c-format
msgid "02/06/2017"
msgstr ""

#. Tag: entry
#: History.xml:432
#, no-c-format
msgid "3.1.2.1"
msgstr ""

#. Tag: listitem
#: History.xml:435
#, no-c-format
msgid "Renamed to OpenMoneyBox and rebranded;"
msgstr ""

#. Tag: listitem
#: History.xml:438
#, no-c-format
msgid "Feature LP #1664378 - User setting for export prefix;"
msgstr ""

#. Tag: listitem
#: History.xml:441
#, no-c-format
msgid "Fixed bug LP #1664735 - Wrong export folder name."
msgstr ""

#. Tag: entry
#: History.xml:449
#, no-c-format
msgid "27/12/2016"
msgstr ""

#. Tag: entry
#: History.xml:450
#, no-c-format
msgid "3.1.1.3"
msgstr ""

#. Tag: entry
#: History.xml:451
#, no-c-format
msgid "Report with cards."
msgstr ""

#. Tag: entry
#: History.xml:455
#, no-c-format
msgid "09/12/2016"
msgstr ""

#. Tag: entry
#: History.xml:456
#, no-c-format
msgid "3.1.1.2"
msgstr ""

#. Tag: entry
#: History.xml:457
#, no-c-format
msgid "XML Export improved."
msgstr ""

#. Tag: entry
#: History.xml:461
#, no-c-format
msgid "27/11/2016"
msgstr ""

#. Tag: entry
#: History.xml:462
#, no-c-format
msgid "3.1.1.1"
msgstr ""

#. Tag: entry
#: History.xml:463
#, no-c-format
msgid "Version 3.1 (Linux, Windows and Android)"
msgstr ""

#. Tag: entry
#: History.xml:467
#, no-c-format
msgid "02/06/2016"
msgstr ""

#. Tag: entry
#: History.xml:468
#, no-c-format
msgid "3.0.2.9"
msgstr ""

#. Tag: listitem
#: History.xml:471
#, no-c-format
msgid "fixed bug https://goo.gl/91Hzuf;"
msgstr ""

#. Tag: listitem
#: History.xml:474
#, no-c-format
msgid "Lollipop notification icon improved;"
msgstr ""

#. Tag: listitem
#: History.xml:477
#, no-c-format
msgid "Lollipop Material theme."
msgstr ""

#. Tag: entry
#: History.xml:485
#, no-c-format
msgid "02/05/2016"
msgstr ""

#. Tag: entry
#: History.xml:486
#, no-c-format
msgid "3.0.2.8"
msgstr ""

#. Tag: listitem
#: History.xml:489
#, no-c-format
msgid "Lollipop notification icon fixed;"
msgstr ""

#. Tag: listitem
#: History.xml:492
#, no-c-format
msgid "report visualization improvement;"
msgstr ""

#. Tag: listitem
#: History.xml:495
#, no-c-format
msgid "fixed file manager integration crash;"
msgstr ""

#. Tag: listitem
#: History.xml:498
#, no-c-format
msgid "bug report menu item added."
msgstr ""

#. Tag: entry
#: History.xml:506
#, no-c-format
msgid "02/04/2016"
msgstr ""

#. Tag: entry
#: History.xml:507
#, no-c-format
msgid "3.0.2.6"
msgstr ""

#. Tag: entry
#: History.xml:508
#, no-c-format
msgid "Translation added to XML export notification."
msgstr ""

#. Tag: entry
#: History.xml:512
#, no-c-format
msgid "28/02/2016"
msgstr ""

#. Tag: entry
#: History.xml:513
#, no-c-format
msgid "3.0.2.5"
msgstr ""

#. Tag: entry
#: History.xml:514
#, no-c-format
msgid "Fixed default fund dialog selection."
msgstr ""

#. Tag: entry
#: History.xml:518
#, no-c-format
msgid "02/01/2016"
msgstr ""

#. Tag: entry
#: History.xml:519
#, no-c-format
msgid "3.0.2.4"
msgstr ""

#. Tag: entry
#: History.xml:520
#, no-c-format
msgid "Fixed bug https://goo.gl/4EXWVY."
msgstr ""

#. Tag: entry
#: History.xml:524
#, no-c-format
msgid "30/12/2015"
msgstr ""

#. Tag: entry
#: History.xml:525
#, no-c-format
msgid "3.0.2.3"
msgstr ""

#. Tag: entry
#: History.xml:526
#, no-c-format
msgid "Fixed bug https://goo.gl/97HzE5."
msgstr ""

#. Tag: entry
#: History.xml:530
#, no-c-format
msgid "06/12/2015"
msgstr ""

#. Tag: entry
#: History.xml:531
#, no-c-format
msgid "3.0.2.2"
msgstr ""

#. Tag: listitem
#: History.xml:534
#, no-c-format
msgid "license screen added;"
msgstr ""

#. Tag: listitem
#: History.xml:537
#, no-c-format
msgid "check if shop item already exists when adding a new one."
msgstr ""

#. Tag: entry
#: History.xml:545
#, no-c-format
msgid "12/11/2015"
msgstr ""

#. Tag: entry
#: History.xml:546
#, no-c-format
msgid "3.0.2.1"
msgstr ""

#. Tag: entry
#: History.xml:547
#, no-c-format
msgid "Android first version."
msgstr ""
