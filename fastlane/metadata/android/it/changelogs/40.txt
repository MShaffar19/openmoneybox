Modifiche v3.4.1.7:
  - Risolto crash all'avvio Android 10 [gitlab #14];
  - Contrasto migliorato nelle categorie in evidenza [gitlab #12].

