<!-- last update: 30/04/2022 -->

Tasks to do before releasing a new version of OpenMoneyBox.
===========================================================

## All:  
    1. Update `History.*` in `/help` and `android/help`; update help files  
    2. Increase/check `buildnumber.txt`  
    3. Sync `packaging`, `trunk` and `stable` branches in [Launchpad](https://launchpad.net/bilancio)  
    4. Update and upload tarball in [Launchpad](https://launchpad.net/bilancio)  
    5. Update `version.txt` on [SuorceForge](https://sourceforge.net/projects/igisw-bilancio/)  
        1. check update type  
        2. check applicable OS’s  
    6. Close open bugs in [Launchpad](https://launchpad.net/bilancio) and GitLab

## MSW:  
    1. Check for **Mingw64** updates (`8.1.0 arch:X86_64 thread:posix exception:seh build:0`)  
    2. Check for **MSys2** updates (`20220319`)  
    3. Update `\src\openmoneybox\openmoneybox.rc`  
    4. Update `\installer_win\include\version.nsi`  
        1. Change registry key on major version change
       
## Android:  
    1. Increase `versionCode` in manifest file  
    2. Increase `versionName` in manifest file  
    3. Check library updates  
    4. Update online manual  
    5. Update FastLane changelog and string `updates` for all languages  
    6. Update online Android changelog  
    7. Sync [GitLab](https://gitlab.com/igi0/openmoneybox) `master` branch  

## MacOs:  
    1. Check for **Brew** updates (`brew update && brew upgrade`)  
