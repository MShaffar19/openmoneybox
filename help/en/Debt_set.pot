# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-06-05 15:15+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Debt_set.xml:1
#, no-c-format
msgid "Store a new debt"
msgstr ""

#. Tag: para
#: Debt_set.xml:2
#, no-c-format
msgid ""
"Click the menu item <guimenu>Debts</guimenu> -&gt; <guimenuitem>Set</"
"guimenuitem> (or the related button in the toolbar) to store a new debt."
msgstr ""

#. Tag: para
#: Debt_set.xml:7
#, no-c-format
msgid ""
"Type in the first edit ox the name of new debt (usually the creditor name)."
msgstr ""

#. Tag: para
#: Debt_set.xml:8
#, no-c-format
msgid ""
"Type in the second edit box the debt value. If a debt with this name already "
"exists the new inserted value will be added to the one previously stored."
msgstr ""

#. Tag: chapter
#: Debt_set.xml:10
#, no-c-format
msgid "&systime; &keepbdg;"
msgstr ""

#. Tag: para
#: Debt_set.xml:13
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: Debt_set.xml:14
#, no-c-format
msgid ""
"<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> "
"<keycap>CTRL</keycap> <keycap>ALT</keycap> <keycap>D</keycap> </keycombo>"
msgstr ""
