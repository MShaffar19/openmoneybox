# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-09-06 14:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Object_Given.xml:1
#, no-c-format
msgid "Store an object donation"
msgstr ""

#. Tag: para
#: Object_Given.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Objects</guimenu> -&gt; <guimenuitem>Given</guimenuitem> (or the related button in the toolbar) to store an object donation."
msgstr ""

#. Tag: para
#: Object_Given.xml:6
#, no-c-format
msgid "Type in the first edit-box the donated object."
msgstr ""

#. Tag: para
#: Object_Given.xml:7
#, no-c-format
msgid "Type in the second edito-box the object receiver."
msgstr ""

#. Tag: sect1
#: Object_Given.xml:7
#, no-c-format
msgid "&systime;"
msgstr ""

#. Tag: para
#: Object_Given.xml:9
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: Object_Given.xml:10
#, no-c-format
msgid "<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> <keycap>SHIFT</keycap> <keycap>F2</keycap> </keycombo>"
msgstr ""

