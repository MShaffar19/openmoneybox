# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-09-06 14:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Remove_item.xml:1
#, no-c-format
msgid "Remove an item from the shop-list"
msgstr ""

#. Tag: para
#: Remove_item.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Shop-List</guimenu> -&gt; <guimenuitem>Remove item</guimenuitem> (or the related button in the toolbar) to remove a previously stored item from the shop-list."
msgstr ""

#. Tag: para
#: Remove_item.xml:6
#, no-c-format
msgid "Select from the drop-box the item to be removed."
msgstr ""

#. Tag: chapter
#: Remove_item.xml:6
#, no-c-format
msgid "&systime;"
msgstr ""

#. Tag: para
#: Remove_item.xml:8
#, no-c-format
msgid "In case of missing or wrong data, an error message will appear."
msgstr ""

#. Tag: para
#: Remove_item.xml:9
#, no-c-format
msgid "<link linkend=\"scuts\">Shortcut</link>: <keycap>F6</keycap>"
msgstr ""

