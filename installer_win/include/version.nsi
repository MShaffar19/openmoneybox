;	OpenMoneyBox NSIS installer script for OpenMoneyBox - Constants
;	Last change 30/04/2022

;___________________________________________________________________________________
;Version info
!define MAJ_VER 3	;Major version
!define MIN_VER 4	;Minor version
!define RELEASE 1	;Release
!define BUILD 14	;Build Number

!define MAJ_VER_str "03"
!define MIN_VER_str "04"
!define RELEASE_str "01"
!define BUILD_str "014"

;!define CONVERT_BUILD 1	; Set to 1 to build ombconvert

;Development info
;	!define COMPILER "mingw32-g++"	;Mingw32
;	!define DEBUG "1"	;defined if it is a debug version
;	!define ALPHA "1"	;defined if it is an alpha version
;	!define BETA "1"	;defined if it is a beta version
;	!define RC "1"		;defined if it is a Release Candidate
;___________________________________________________________________________________

;Path info
;___________________________________________________________________________________
!define Files "..\_win\install"
!define Libs "E:\Documenti\Programmazione\MinGW64\mingw64\bin" ; hardcoded for local availability to avoid errors in Android builds (tarball checks)
;___________________________________________________________________________________

;Registry Keys
;___________________________________________________________________________________
!Define SW_Key "SOFTWARE\igiSW\OpenMoneyBox\3.4"	;HKLM\Software
;___________________________________________________________________________________

;Features
;___________________________________________________________________________________

;Update info
;___________________________________________________________________________________
!define REG_VER	1	;Registry check value for installer updates
;___________________________________________________________________________________
