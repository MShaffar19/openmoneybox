;	Last modify 30/04/2022

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
LoadLanguageFile "${NSISDIR}\Contrib\Language files\Italian.nlf"

;ProductName
LangString ProductName ${LANG_ENGLISH} "OpenMoneyBox"
LangString ProductName ${LANG_ITALIAN} "Portamonete"

; Typical Installation
LangString TypicalInstallation ${LANG_ENGLISH} "Typical"
LangString TypicalInstallation ${LANG_ITALIAN} "Tipica"

; Minimum Installation
LangString MinimumInstallation ${LANG_ENGLISH} "Minimum"
LangString MinimumInstallation ${LANG_ITALIAN} "Minima"

; Complete Installation
LangString CompleteInstallation ${LANG_ENGLISH} "Complete"
LangString CompleteInstallation ${LANG_ITALIAN} "Completa"

; Help component
LangString HelpFile ${LANG_ENGLISH} "Help file"
LangString HelpFile ${LANG_ITALIAN} "File della guida"

; Utility components
LangString Utilities ${LANG_ENGLISH} "Utilities"
LangString Utilities ${LANG_ITALIAN} "Utilità"

; Wizard component
LangString Wizard ${LANG_ENGLISH} "First run wizard"
LangString Wizard ${LANG_ITALIAN} "Wizard primo avvio"

; Biltray component
LangString BilTray ${LANG_ENGLISH} "Alarms management"
LangString BilTray ${LANG_ITALIAN} "Gestione promemoria"

; Update component
LangString Update ${LANG_ENGLISH} "Web updates"
LangString Update ${LANG_ITALIAN} "Aggiornamenti in rete"

; Conversion component
LangString Conv ${LANG_ENGLISH} "Old document converter"
LangString Conv ${LANG_ITALIAN} "Convertitore vecchi documenti"

; Dictionaries
LangString LanguagePackets ${LANG_ENGLISH} "Dictionaries"
LangString LanguagePackets ${LANG_ITALIAN} "Dizionari"

; Italian Dictionary
LangString Language_ITA ${LANG_ENGLISH} "Italian"
LangString Language_ITA ${LANG_ITALIAN} "Italiano"

; French Dictionary
LangString Language_FRE ${LANG_ENGLISH} "French"
LangString Language_FRE ${LANG_ITALIAN} "Francese"

; Swedish Dictionary
LangString Language_SWE ${LANG_ENGLISH} "Swedish"
LangString Language_SWE ${LANG_ITALIAN} "Svedese"

; Uninstaller strings
LangString uiText ${LANG_ENGLISH} "Uninstall"
LangString uiText ${LANG_ITALIAN} "Disinstalla"
LangString uiCaption ${LANG_ENGLISH} "Bilancio uninstall"
LangString uiCaption ${LANG_ITALIAN} "Disinstallazione di Bilancio"
LangString uiSub0 ${LANG_ENGLISH} ": Confirmation"
LangString uiSub0 ${LANG_ITALIAN} ": Conferma"
LangString uiSub1 ${LANG_ENGLISH} ": File deletion"
LangString uiSub1 ${LANG_ITALIAN} ": Rimozione file"
LangString uiSub2 ${LANG_ENGLISH} ": Completed"
LangString uiSub2 ${LANG_ITALIAN} ": Completato"
LangString Success ${LANG_ENGLISH} "Program successfully uninstalled"
LangString Success ${LANG_ITALIAN} "Programma disinstallato con successo."

; Errors
LangString InstallKO ${LANG_ENGLISH} "Impossible to install the program!"
LangString InstallKO ${LANG_ITALIAN} "Impossibile installare il programma!"
LangString InstallUncomplete ${LANG_ENGLISH} "The installation has not been completed!"
LangString InstallUncomplete ${LANG_ITALIAN} "L'installazione non è stata completata!"
LangString UninstallUncomplete ${LANG_ENGLISH} "The uninstallation has not been completed!"
LangString UninstallUncomplete ${LANG_ITALIAN} "La disinstallazione non è stata completata!"

;Patches
LangString PatchInstalled ${LANG_ENGLISH} "Program successfully updated."
LangString PatchInstalled ${LANG_ITALIAN} "Il programma è stato aggiornato con successo."
LangString OlderInstaller ${LANG_ENGLISH} "A newer version is already installed."
LangString OlderInstaller ${LANG_ITALIAN} "Una versione più recente è già installata."
LangString UpdateConfirmation ${LANG_ENGLISH} "Do you want to only patch the program?"
LangString UpdateConfirmation ${LANG_ITALIAN} "Vuoi semplicemente aggiornare il programma?"
