/***************************************************************
 * Name:      ombupdate.cpp
 * Purpose:   Code for OpenMoneyBox update
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-10-21
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBUPDATE_HEADER_INCLUDED
	#define OMBUPDATE_HEADER_INCLUDED

	#ifndef WX_PRECOMP
		#include <wx/wx.h>
	#else
		#include <wx/wxprec.h>
	#endif

	#include "../types.h"

	// Calls to module igiomb
	WXIMPORT wxLanguage FindLang(void);
	WXIMPORT TVersion GetInstalledVersion(void);
	WXIMPORT wxDateTime Omb_StrToDate(wxString S);
	WXIMPORT wxString GetVersionFile(void);
	//WXIMPORT wxString GetWebAddress(void);

	#ifdef __WXMSW__
		// Calls to module bilopt.dll
		WXIMPORT wxString GetInstallationPath(void);
		#ifdef _OMB_USEREGISTRY
			WXEXPORT wxString GetSWKey(void);
		#endif // _OMB_USEREGISTRY

	#endif  // __WXMSW__

	#ifdef __WXGTK__
		WXIMPORT wxString GetUserConfigDir(void);
	#endif // __WXGTK__

	#ifdef __WXMAC__
		WXIMPORT wxString GetUserConfigDir(void);
	#endif // __WXMAC__

#endif  // OMBUPDATE_HEADER_INCLUDED
