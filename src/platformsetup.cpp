/***************************************************************
 * Name:      platformsetup.cpp
 * Purpose:   Specific definition for OS platform
 * Author:    Igor Cal� (igor.cali0@gmail.com)
 * Created:   2022-04-27
 * Copyright: Igor Cal� (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef PLATFORMSETUP_CPP_INCLUDED
#define PLATFORMSETUP_CPP_INCLUDED

#include <wx/string.h>

#ifdef __WXMSW__
  #include <wx/filefn.h>
  #include "platformsetup.h"

  wxString DataDir = wxGetCwd() + L"\\";
  wxString ShareDir = DataDir;
#else
	#ifdef __FLATPAK__
		// TODO (igor#1#): Check if can be retrieved programmatically
		wxString DataDir = L"/var/lib/flatpak/app/com.igisw.openmoneybox/current/active/files/etc/openmoneybox/";		// Program data directory
		wxString ShareDir = L"/var/lib/flatpak/app/com.igisw.openmoneybox/current/active/files/share/";			// OS shared durectory
	#else
		wxString DataDir = L"/etc/openmoneybox/";		// Program data directory
		wxString ShareDir = L"/usr/share/";			// OS shared durectory
	#endif // _FLATPAK
#endif // __WXMSW__

#ifdef __WXGTK__
	wxString Console = "xterm -hold -e ";	// Console prompt
#endif // __WXGTK__

#endif  // PLATFORMSETUP_CPP_INCLUDED
