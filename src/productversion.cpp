/***************************************************************
 * Name:      productversion.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Modified:   2022-01-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef PRODUCTVERSIONS_CPP_INCLUDED
#define PRODUCTVERSIONS_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "platformsetup.h"
#include "productversion.h"

// PRODUCT INFORMATION
wxString ProductName = _("OpenMoneyBox");
#if _OMB_HASNATIVEABOUT
	wxString Copyright = L"© 2000-2022 Igor Calì <igor.cali0@gmail.com>";
#endif // _OMB_HASNATIVEABOUT

// CONTACT INFORMATION
//wxString email=L"igor.cali0@gmail.com"; // email is provided by the ombLogo module
wxString WebAddress = L"http://igisw-bilancio.sourceforge.net/";	//Website
wxString BugAddress = L"https://bugs.launchpad.net/bilancio";
wxString DonateAddress = L"http://igisw-bilancio.sourceforge.net/donation.html";
wxString VersionFile=L"http://igisw-bilancio.sourceforge.net/version.txt";	// Version file URL

#ifdef _ALPHA_
	wxString MinorVersion_text=wxString::Format(L"%da",MinorVersion);
#elif defined ( _BETA )
	wxString MinorVersion_text=wxString::Format(L"%db",MinorVersion);
#elif defined ( _RELCANDIDATE )
	wxString MinorVersion_text=wxString::Format(L"%drc",MinorVersion);
#else
	wxString MinorVersion_text=wxString::Format(L"%d",MinorVersion);
#endif // _BETA

#ifdef __WXMSW__
    #ifdef __amd64__
        wxString LongVersion=::wxString::Format("%d.%s.%d.%u",MajorVersion,MinorVersion_text.c_str(),ReleaseVersion,(unsigned long long) &__BUILD_NUMBER);
    #else
        wxString LongVersion=::wxString::Format("%d.%s.%d.%u",MajorVersion,MinorVersion_text.c_str(),ReleaseVersion,(unsigned long) &__BUILD_NUMBER);
    #endif // __amd64__
  wxString ShortVersion=::wxString::Format("%d.%s",MajorVersion,MinorVersion_text.c_str());
#else
  wxString LongVersion=::wxString::Format(L"%d.%s.%d.%u",MajorVersion,MinorVersion_text.c_str(),ReleaseVersion,(unsigned long) &__BUILD_NUMBER);
  wxString ShortVersion=::wxString::Format(L"%d.%s",MajorVersion,MinorVersion_text.c_str());
#endif

#endif
