/***************************************************************
 * Name:      main_wx.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-06
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef MAINFRAME_H_INCLUDED
#define MAINFRAME_H_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/datetime.h>
//#include <wx/fileconf.h> // For wxFileConfig
#include <wx/calctrl.h>
#include <wx/sizer.h>
#include <wx/toolbar.h>
#include <wx/tbarbase.h>
#include <wx/gauge.h>
#include <wx/stattext.h>
#include <wx/treectrl.h>
#include <wx/listbox.h>
#include <wx/grid.h>
#include <wx/statusbr.h>
#include <wx/statbmp.h>
#include <wx/fdrepdlg.h>  // For wxFindReplaceDialog

#include "../../platformsetup.h"

#if _OMB_HASNATIVEABOUT == 1
	#include <wx/aboutdlg.h>
#endif // _OMB_HASNATIVEABOUT == 1

#ifdef _OMB_CHART_WXPIECTRL
	#include "wxPieCtrl.h"
	#include "viewstatistics.h"
#endif // _OMB_CHART_WXPIECTRL

#include <wx/timectrl.h>

#if (_OMB_ICONSIZE_ == 24)
	#ifdef _OMB_CHART_MATLIBPLOT
		#include "wxmainframe_24_mpl.h"
	#else
		#include "wxmainframe_24.h"
	#endif // _OMB_CHART_MATLIBPLOT
#else
	#include "wxmainframe_16.h"
#endif // _OMB_ICONSIZE_

#include "../../omb34core.h"
#include "topexpensepanel.h"

#ifdef _OMB_USE_CIPHER
	#include "../../wxsqlite3.h"
#endif // _OMB_USE_CIPHER

typedef enum TGridCol{
  gcDate = 0,
  gcTime,
  gcOperation,
  gcValue,
  gcReason,
  gcCategory,
  gcContact,
  gcLocation,
  }GridCol;

const int ombExtTool=2000;

class ombMainFrame : public wxMainFrame{
friend class BilApp;	// for ShowF()

private:
	// GUI Controls

	#ifdef _OMB_CHART_MATLIBPLOT
		wxBitmap Fund_bmp;
		wxBitmap Trend_bmp;
	#elif defined ( _OMB_CHART_WXPIECTRL )
		wxPieCtrl *FundChart;
		CPaintStatistics *TrendChart;
	#endif // _OMB_CHART_MATLIBPLOT

	wxFindReplaceDialog *FindDialog;

	TEPanel *TopCategoriesPanel;
	wxImageList *CategoryIconList;

	// Non-GUI Controls
	bool	Initialised,
				//menu_opendoc,  //  menu_opendoc: true if doc menus are active
				Master_Attached,
				FreshChartShown,	// true if charts are drawn out of last month data
				map_viewer_exist	// true if ombmapviewer is installed
														;
	wxArrayString *TreeNames;  // TreeList branch names
	wxTreeItemId Funds_tid,Creds_tid,Debts_tid,Lends_tid,Borrowed_tid;
 	wxDateTime calmindate, calmaxdate;
	wxFindReplaceData *FindData;

	wxColour backup_report_colour;

	// Routines
	void InitLanguage(void);
	void ResetTree(void);
	void ShowControls(bool V);
	void FundOperation(int Op);
	void Operation(int Op);
	void ObjectOperation(int Op);
	void ShopOperation(int O);
	void ObjectOperation2(int Op);
	void RebuildTools(void);
	void ShowF(bool doChart = true);
	void ShowMaster(void);
	void PaintMasterChart(bool hasfunds, wxSQLite3Table &funds, wxSQLite3Table &transactions);
	void SearchMasterMinDate(void);
	void AddNode(wxTreeItemId Node, wxString Name, wxString Value, int c_index = -1);
	void PaintChart(void);
	void UpdateMenu(void);
	bool SaveDatabase(void);
	void FormDestroy(void);
	int GetFirstSelectedRow(void);
	//void ResizeCols(int C);
  void ReportSelect(wxGridEvent& event);
  void ReportRangeSelect(wxGridRangeSelectEvent& event);
	wxString CreateTitle(bool type);
	void remove_obtained_shopitem(wxString obj);
	#ifdef _OMB_CHART_MATLIBPLOT
		wxBitmap GetScaledBitmap(wxString chart, wxStaticBitmap* panel, bool trend = false);
	#endif // _OMB_CHART_MATLIBPLOT
	void CheckDate(void);
	wxString GetTempFile(void);
	wxString GetContactImage(long c_id);
	int GetTreeViewContactImageIndex(long c_id);
	bool IsNewMonthShown(void);
	void SetCaption(void);
	wxString BrowseDocument(void);
	void AuthorWindow(void);
	wxString GetLocationImagePath(int Height);
protected:
	void SaveClick(wxCommandEvent& event);
	void ViewArchiveClick(wxCommandEvent& event);
	#ifdef _OMB_USE_CIPHER
		void ModifyPasswordClick(wxCommandEvent& event);
		void RemovePasswordClick(wxCommandEvent& event);
	#endif // _OMB_USE_CIPHER
	void RevertClick(wxCommandEvent& event);
	void ExitClick(wxCommandEvent& event);
	void CopyClick(wxCommandEvent& event);
	//void Find1Click(wxCommandEvent& event);
	void NewFundClick(wxCommandEvent& event);
	void RemoveFundClick(wxCommandEvent& event);
	void ResetFundClick(wxCommandEvent& event);
	void SetTotalClick(wxCommandEvent& event);
	void DefaultFundClick(wxCommandEvent& event);
	void GainClick(wxCommandEvent& event);
	void ExpenseClick(wxCommandEvent& event);
	void CategoryClick(wxCommandEvent& event);
	void NewCreditClick(wxCommandEvent& event);
	void RemoveCreditClick(wxCommandEvent& event);
	void CondoneCreditClick(wxCommandEvent& event);
	void NewDebtClick(wxCommandEvent& event);
	void RemoveDebtClick(wxCommandEvent& event);
	void CondoneDebtClick(wxCommandEvent& event);
	//void ContributionsClick(wxCommandEvent& event);
	void ReceivedClick(wxCommandEvent& event);
	void GiftedClick(wxCommandEvent& event);
	void LendClick(wxCommandEvent& event);
	void BorrowClick(wxCommandEvent& event);
	void AddShopItemClick(wxCommandEvent& event);
	void DelShopItemClick(wxCommandEvent& event);
	void GetBackClick(wxCommandEvent& event);
	void GiveBackClick(wxCommandEvent& event);
	void ShowFindDialog (wxCommandEvent& event);
	void Find(wxFindDialogEvent& event);
	int FindLoop(int StartIndex,int StopIndex, wxString find_str);
	void FindClose(wxFindDialogEvent& event);
	void WebSiteClick(wxCommandEvent& event);
	void BugClick(wxCommandEvent& event);
	void DonateClick(wxCommandEvent& event);
	void CheckDate(wxCalendarEvent& event);
	void WizClick(wxCommandEvent& event);
	void OldConv1Click(wxCommandEvent& event);
	void OptionsClick(wxCommandEvent& event);
	void HelpMenuClick(wxCommandEvent& event);
	void AuthorClick(wxCommandEvent& event);
	void PopRepPopup( wxGridEvent& event );
	void ExtClick(wxCommandEvent& event);
	void onResize(wxSizeEvent& event);
	void EditCategoriesClick(wxCommandEvent& event);
	#ifdef __WXMSW__
    void LicenseClick(wxCommandEvent& event);
  #endif // __WXMSW__
 	void ImportDocument(wxCommandEvent& event);
	#ifdef _OMB_CHART_MATLIBPLOT
		void FundResize( wxSplitterEvent& event );
	#endif // _OMB_CHART_MATLIBPLOT
	void ShowMap(wxCommandEvent& event);

	void GiveTime(wxDateEvent& event);

	public:
	// GUI components

	// Routines
	void Draw(wxDC& dc);
	//ombMainFrame(const wxString& title);
	explicit ombMainFrame(wxWindow* parent);
	~ombMainFrame(void);
	bool AutoOpen(void);
	void UpdateCalendar(wxDateTime master_mindate);
	void TextConvClick(wxCommandEvent& event);
	void FormClose(wxCloseEvent &event);

	//DECLARE_DYNAMIC_CLASS(ombMainFrame)
	//DECLARE_EVENT_TABLE()
};

//extern wxString Website;

/*
#ifdef _OMB_INSTALLEDUPDATE
	WXIMPORT bool GetCheckUpdates(void);
#endif // _OMB_INSTALLEDUPDATE
*/
WXIMPORT bool ShowBar(void);
WXIMPORT void Set_DefaultDocument(wxString Document);
WXIMPORT void DoFirstRun(void);
WXIMPORT bool ShowOptionsDialog(void);
WXIMPORT bool ShowTrendChart(void);
WXIMPORT bool ShowFundChart(void);
WXIMPORT bool GetAutoOpen(void);
WXIMPORT bool IsFirstRunEver(void);
WXIMPORT wxString GetDefaultDocument(void);
WXIMPORT int GetToolCount(void);
WXIMPORT wxString GetTool(int i);
WXIMPORT int RunWizard(wxGauge* Prog, wxString &S, bool def);
WXIMPORT void Logo(wxString Product);
WXIMPORT void DonateDialog(wxString olURL);
WXIMPORT int GetLang(void);
WXIMPORT bool CheckAndPromptForConversion(wxString File);
#ifdef __WXMSW__
  // Calls to module ombopt.dll
  WXIMPORT wxString GetInstallationPath(void);
  WXIMPORT wxString GetDataDir(void);
  extern void About(wxString Name, wxString ShortVer, wxString LongVer, wxString Date);
#endif

#ifdef __WXMAC__
  // Calls to module ombopt.dll
  WXIMPORT wxString GetInstallationPath(void);
  //WXIMPORT wxString GetDataDir(void);
#endif	// __WXMAC__

WXIMPORT wxString GetUserLocalDir(void);
#ifdef __WXGTK__
	WXIMPORT wxString GetDesktopEnv(void);
#endif // __WXGTK__

#if _OMB_HASNATIVEABOUT == 1
	WXIMPORT wxString GetShareDir(void);
#endif // _OMB_HASNATIVEABOUT

#endif	// MAINFRAME_H_INCLUDED
