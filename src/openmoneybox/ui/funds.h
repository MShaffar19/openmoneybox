/***************************************************************
 * Name:      funds.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef FundsH
#define FundsH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/dialog.h>
#include <wx/arrstr.h>
#include <wx/fileconf.h> // For wxFileConfig

#include "wxtopf.h"
#include "../../types.h"

class TOPF : public wxTOPF
{
//_published:
private:
	/*
	// GUI components
	wxStaticBox *Bevel1;
	wxStaticText *LNam;
	wxStaticText *LVal;
	*/

	// Non-GUI components
	wxArrayString *Values;

	// Routines
	void Focus(bool Ob);
	void NameText( wxKeyEvent& event);
	#ifdef __WXMSW__
    void MSW_ROCombo(void);
  #endif // __WXMSW__
protected:
	// Routines
	void OKBtnClick(wxCommandEvent& event);
public:
	/*
	// GUI components
	wxComboBox *Name;
	wxTextCtrl *Val;
	wxButton *OKBtn;
	wxButton *CancelBtn;
	*/

	// Routines
	explicit TOPF(wxWindow *parent);
	~TOPF(void);
	void InitLabels(int Kind);
	void AddValue(wxString Val);
	void NameChange(wxCommandEvent& event);
};

extern TOPF *OPF;

// Calls to module ombLogo
WXIMPORT wxString iUpperCase(wxString S);
// Calls to module igiomb
WXIMPORT wxString CheckValue(wxString Val);
// Calls to module omberr
WXIMPORT void Error(int Err,wxString Opt);

#endif

