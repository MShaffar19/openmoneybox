/***************************************************************
 * Name:      remcreddeb.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef RemCredDebH
#define RemCredDebH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxremcreddeb.h"
#include "../../types.h"

class TRCredDeb : public wxTRCredDeb
{
//__published:
private:
	// Non-GUI components
	bool Debt;

	// Routines
	void Focus(bool Err);
protected:
	void OptClick(wxCommandEvent& event);
	void OKBtnClick(wxCommandEvent& event);
public:
	// Routines
	explicit TRCredDeb(wxWindow *parent);
	void InitLabels(int E);
};

extern TRCredDeb *RCredDeb;

// Calls to module ombLogo
WXIMPORT wxString iUpperCase(wxString S);
// Calls to module igiomb
WXIMPORT wxString CheckValue(wxString Val);
//extern __declspec(dllimport) wxString GetLanguageFile(void);
WXIMPORT wxLanguage FindLang(void);
WXIMPORT void Error(int Err,wxString Opt);
WXIMPORT wxString GetCurrencySymbol(void);

#endif


