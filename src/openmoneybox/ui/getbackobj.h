/***************************************************************
 * Name:      getbackobj.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef GetBackObjH
#define GetBackObjH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxgetbackf.h"

class TGetBackF : public wxGetBackF
{
private:
	// GUI components
/*
	wxStaticBox *Bevel1;
	wxStaticText *LOgg;
	wxStaticText *LPer;
	wxButton *OKBtn;
	wxButton *CancelBtn;
*/

	//Non-GUI components

	// Routines
	void OKBtnClick(wxCommandEvent& event);
	void FormDestroy(void);
	void Focus(bool Err);
public:
	//Non-GUI components
	wxArrayString *Memo;

	// Routines
	explicit TGetBackF(wxWindow *parent);
	~TGetBackF(void);
	void InitLabels(bool O);
	void PerChange(wxCommandEvent& event);
};

// Calls to module ombLogo.DLL
WXIMPORT wxString iUpperCase(wxString S);
// Calls to module omberr.DLL
WXIMPORT void Error(int Err,wxString Opt);

#endif


