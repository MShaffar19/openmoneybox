///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Feb 16 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxmainframe_16.h"

#include "../../../rsrc/toolbar/16/condone.xpm"
#include "../../../rsrc/toolbar/16/copy.xpm"
#include "../../../rsrc/toolbar/16/creditnew.xpm"
#include "../../../rsrc/toolbar/16/creditremove.xpm"
#include "../../../rsrc/toolbar/16/debtnew.xpm"
#include "../../../rsrc/toolbar/16/debtremove.xpm"
#include "../../../rsrc/toolbar/16/expense.xpm"
#include "../../../rsrc/toolbar/16/find.xpm"
#include "../../../rsrc/toolbar/16/gain.xpm"
#include "../../../rsrc/toolbar/16/icon_xml.xpm"
#include "../../../rsrc/toolbar/16/new.xpm"
#include "../../../rsrc/toolbar/16/newfund.xpm"
#include "../../../rsrc/toolbar/16/newshop.xpm"
#include "../../../rsrc/toolbar/16/note_icon.xpm"
#include "../../../rsrc/toolbar/16/objectborrow.xpm"
#include "../../../rsrc/toolbar/16/objectgetback.xpm"
#include "../../../rsrc/toolbar/16/objectgiveback.xpm"
#include "../../../rsrc/toolbar/16/objectgiven.xpm"
#include "../../../rsrc/toolbar/16/objectlend.xpm"
#include "../../../rsrc/toolbar/16/objectreceived.xpm"
#include "../../../rsrc/toolbar/16/oldconv.xpm"
#include "../../../rsrc/toolbar/16/open.xpm"
#include "../../../rsrc/toolbar/16/removefund.xpm"
#include "../../../rsrc/toolbar/16/resetfund.xpm"
#include "../../../rsrc/toolbar/16/save.xpm"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxMainFrame, wxFrame )
	EVT_CLOSE( wxMainFrame::_wxFB_FormClose )
	EVT_MENU( bilNew, wxMainFrame::_wxFB_NewButtonClick )
	EVT_MENU( bilOpen, wxMainFrame::_wxFB_OpenClick )
	EVT_MENU( bilSave, wxMainFrame::_wxFB_SaveClick )
	EVT_MENU( bilSaveAs, wxMainFrame::_wxFB_SaveAsClick )
	EVT_MENU( bilRevert, wxMainFrame::_wxFB_RevertClick )
	EVT_MENU( bilXML, wxMainFrame::_wxFB_TextConvClick )
	EVT_MENU( bilModifyPassword, wxMainFrame::_wxFB_ModifyPasswordClick )
	EVT_MENU( wxID_EXIT, wxMainFrame::_wxFB_ExitClick )
	EVT_MENU( bilCopy, wxMainFrame::_wxFB_CopyClick )
	EVT_MENU( bilFind, wxMainFrame::_wxFB_ShowFindDialog )
	EVT_MENU( bilNewFund, wxMainFrame::_wxFB_NewFundClick )
	EVT_MENU( bilRemoveFund, wxMainFrame::_wxFB_RemoveFundClick )
	EVT_MENU( bilResetFund, wxMainFrame::_wxFB_ResetFundClick )
	EVT_MENU( bilSetTotal, wxMainFrame::_wxFB_SetTotalClick )
	EVT_MENU( bilDefaultFund, wxMainFrame::_wxFB_DefaultFundClick )
	EVT_MENU( bilGain, wxMainFrame::_wxFB_GainClick )
	EVT_MENU( bilExpense, wxMainFrame::_wxFB_ExpenseClick )
	EVT_MENU( bilCategory, wxMainFrame::_wxFB_CategoryClick )
	EVT_MENU( bilReceived, wxMainFrame::_wxFB_ReceivedClick )
	EVT_MENU( bilGifted, wxMainFrame::_wxFB_GiftedClick )
	EVT_MENU( bilLend, wxMainFrame::_wxFB_LendClick )
	EVT_MENU( bilGetBack, wxMainFrame::_wxFB_GetBackClick )
	EVT_MENU( bilBorrow, wxMainFrame::_wxFB_BorrowClick )
	EVT_MENU( bilGiveBack, wxMainFrame::_wxFB_GiveBackClick )
	EVT_MENU( bilNewCredit, wxMainFrame::_wxFB_NewCreditClick )
	EVT_MENU( bilRemoveCredit, wxMainFrame::_wxFB_RemoveCreditClick )
	EVT_MENU( bilCondoneCredit, wxMainFrame::_wxFB_CondoneCreditClick )
	EVT_MENU( bilNewDebt, wxMainFrame::_wxFB_NewDebtClick )
	EVT_MENU( bilRemoveDebt, wxMainFrame::_wxFB_RemoveDebtClick )
	EVT_MENU( bilCondoneDebt, wxMainFrame::_wxFB_CondoneDebtClick )
	EVT_MENU( bilAddShopItem, wxMainFrame::_wxFB_AddShopItemClick )
	EVT_MENU( bilDelShopItem, wxMainFrame::_wxFB_DelShopItemClick )
	EVT_MENU( bilWiz, wxMainFrame::_wxFB_WizClick )
	EVT_MENU( bilOldConv, wxMainFrame::_wxFB_OldConv1Click )
	EVT_MENU( bilOptions, wxMainFrame::_wxFB_OptionsClick )
	EVT_MENU( bilHelpMenu, wxMainFrame::_wxFB_HelpMenuClick )
	EVT_MENU( wxID_ABOUT, wxMainFrame::_wxFB_AuthorClick )
	EVT_MENU( bilWebSite, wxMainFrame::_wxFB_WebSiteClick )
	EVT_MENU( bilDonate, wxMainFrame::_wxFB_DonateClick )
	EVT_MENU( bilBug, wxMainFrame::_wxFB_BugClick )
	EVT_MENU( bilNewFund, wxMainFrame::_wxFB_NewFundClick )
	EVT_MENU( bilNewCredit, wxMainFrame::_wxFB_NewCreditClick )
	EVT_MENU( bilNewDebit, wxMainFrame::_wxFB_NewDebtClick )
	EVT_MENU( bilLend, wxMainFrame::_wxFB_LendClick )
	EVT_MENU( bilBorrow, wxMainFrame::_wxFB_BorrowClick )
	EVT_MENU( bilAddShopItem, wxMainFrame::_wxFB_AddShopItemClick )
	EVT_MENU( bilRemoveFund, wxMainFrame::_wxFB_RemoveFundClick )
	EVT_MENU( bilResetFund, wxMainFrame::_wxFB_ResetFundClick )
	EVT_MENU( bilGetBack, wxMainFrame::_wxFB_GetBackClick )
	EVT_MENU( bilGiveBack, wxMainFrame::_wxFB_GiveBackClick )
	EVT_MENU( bilDelShopItem, wxMainFrame::_wxFB_DelShopItemClick )
	EVT_CALENDAR_SEL_CHANGED( wxID_ANY, wxMainFrame::_wxFB_CheckDate )
	EVT_GRID_CELL_LEFT_CLICK( wxMainFrame::_wxFB_ReportSelect )
	EVT_GRID_CELL_RIGHT_CLICK( wxMainFrame::_wxFB_PopRepPopup )
	EVT_GRID_RANGE_SELECT( wxMainFrame::_wxFB_ReportRangeSelect )
END_EVENT_TABLE()

wxMainFrame::wxMainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
	MAINMENU = new wxMenuBar( 0 );
	MAINMENU->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 90, false, wxT("Ubuntu") ) );
	
	MainMenu_File_ = new wxMenu();
	New = new wxMenuItem( MainMenu_File_, bilNew, wxString( _("New") ) + wxT('\t') + wxT("CTRL+N"), _("Create a new document"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	New->SetBitmaps( wxBitmap( new_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	New->SetBitmap( wxBitmap( new_xpm ) );
	#endif
	MainMenu_File_->Append( New );
	
	Open = new wxMenuItem( MainMenu_File_, bilOpen, wxString( _("Open") ) + wxT('\t') + wxT("CTRL+O"), _("Open a document"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Open->SetBitmaps( wxBitmap( open_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Open->SetBitmap( wxBitmap( open_xpm ) );
	#endif
	MainMenu_File_->Append( Open );
	
	Save = new wxMenuItem( MainMenu_File_, bilSave, wxString( _("Save") ) + wxT('\t') + wxT("CTRL+S"), _("Save changes to current document"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Save->SetBitmaps( wxBitmap( save_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Save->SetBitmap( wxBitmap( save_xpm ) );
	#endif
	MainMenu_File_->Append( Save );
	
	SaveAs = new wxMenuItem( MainMenu_File_, bilSaveAs, wxString( _("Save as") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( SaveAs );
	
	Revert = new wxMenuItem( MainMenu_File_, bilRevert, wxString( _("Revert") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( Revert );
	
	XML = new wxMenuItem( MainMenu_File_, bilXML, wxString( _("Export XML") ) , _("Export the document in XML format"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	XML->SetBitmaps( wxBitmap( icon_xml_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	XML->SetBitmap( wxBitmap( icon_xml_xpm ) );
	#endif
	MainMenu_File_->Append( XML );
	
	MainMenu_File_->AppendSeparator();
	
	ModifyPassword = new wxMenuItem( MainMenu_File_, bilModifyPassword, wxString( _("Change password") ) + wxT('\t') + wxT("CTRL+P"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( ModifyPassword );
	
	MainMenu_File_->AppendSeparator();
	
	Exit = new wxMenuItem( MainMenu_File_, wxID_EXIT, wxString( _("Exit") ) + wxT('\t') + wxT("CTRL+E"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( Exit );
	
	MAINMENU->Append( MainMenu_File_, _("File") ); 
	
	MainMenu_Edit_ = new wxMenu();
	Copy = new wxMenuItem( MainMenu_Edit_, bilCopy, wxString( _("Copy") ) + wxT('\t') + wxT("CTRL+C"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Copy->SetBitmaps( wxBitmap( copy_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Copy->SetBitmap( wxBitmap( copy_xpm ) );
	#endif
	MainMenu_Edit_->Append( Copy );
	
	Find1 = new wxMenuItem( MainMenu_Edit_, bilFind, wxString( _("Find") ) + wxT('\t') + wxT("CTRL+T"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Find1->SetBitmaps( wxBitmap( find_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Find1->SetBitmap( wxBitmap( find_xpm ) );
	#endif
	MainMenu_Edit_->Append( Find1 );
	
	MAINMENU->Append( MainMenu_Edit_, _("Edit") ); 
	
	MainMenu_Funds_ = new wxMenu();
	NewFund = new wxMenuItem( MainMenu_Funds_, bilNewFund, wxString( _("New") ) + wxT('\t') + wxT("CTRL+ALT+N"), _("Add a fund"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	NewFund->SetBitmaps( wxBitmap( newfund_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	NewFund->SetBitmap( wxBitmap( newfund_xpm ) );
	#endif
	MainMenu_Funds_->Append( NewFund );
	
	RemoveFund = new wxMenuItem( MainMenu_Funds_, bilRemoveFund, wxString( _("Remove") ) + wxT('\t') + wxT("CTRL+ALT+R"), _("Remove a fund"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveFund->SetBitmaps( wxBitmap( removefund_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveFund->SetBitmap( wxBitmap( removefund_xpm ) );
	#endif
	MainMenu_Funds_->Append( RemoveFund );
	
	ResetFund = new wxMenuItem( MainMenu_Funds_, bilResetFund, wxString( _("Reset") ) + wxT('\t') + wxT("CTRL+ALT+I"), _("Reset the value of a fund"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	ResetFund->SetBitmaps( wxBitmap( resetfund_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	ResetFund->SetBitmap( wxBitmap( resetfund_xpm ) );
	#endif
	MainMenu_Funds_->Append( ResetFund );
	
	MainMenu_Funds_->AppendSeparator();
	
	SetTotal = new wxMenuItem( MainMenu_Funds_, bilSetTotal, wxString( _("Set total") ) + wxT('\t') + wxT("CTRL+ALT+T"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_Funds_->Append( SetTotal );
	
	DefaultFund = new wxMenuItem( MainMenu_Funds_, bilDefaultFund, wxString( _("Default fund") ) + wxT('\t') + wxT("CTRL+ALT+T"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_Funds_->Append( DefaultFund );
	
	MAINMENU->Append( MainMenu_Funds_, _("Funds") ); 
	
	MainMenu_Operations_ = new wxMenu();
	Gain = new wxMenuItem( MainMenu_Operations_, bilGain, wxString( _("Profit") ) + wxT('\t') + wxT("F2"), _("Store a profit"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Gain->SetBitmaps( wxBitmap( gain_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Gain->SetBitmap( wxBitmap( gain_xpm ) );
	#endif
	MainMenu_Operations_->Append( Gain );
	
	Expense = new wxMenuItem( MainMenu_Operations_, bilExpense, wxString( _("Expense") ) + wxT('\t') + wxT("F3"), _("Store an expense"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Expense->SetBitmaps( wxBitmap( expense_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Expense->SetBitmap( wxBitmap( expense_xpm ) );
	#endif
	MainMenu_Operations_->Append( Expense );
	
	MainMenu_Operations_->AppendSeparator();
	
	Category = new wxMenuItem( MainMenu_Operations_, bilCategory, wxString( _("Category") ) , _("Add a category to the report"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Category->SetBitmaps( wxBitmap( note_icon_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Category->SetBitmap( wxBitmap( note_icon_xpm ) );
	#endif
	MainMenu_Operations_->Append( Category );
	
	MAINMENU->Append( MainMenu_Operations_, _("Operations") ); 
	
	MainMenu_Objects_ = new wxMenu();
	Received = new wxMenuItem( MainMenu_Objects_, bilReceived, wxString( _("Received") ) + wxT('\t') + wxT("CTRL+SHIFT+F1"), _("Stores the reception of an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Received->SetBitmaps( wxBitmap( objectreceived_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Received->SetBitmap( wxBitmap( objectreceived_xpm ) );
	#endif
	MainMenu_Objects_->Append( Received );
	
	Gifted = new wxMenuItem( MainMenu_Objects_, bilGifted, wxString( _("Given") ) + wxT('\t') + wxT("CTRL+SHIFT+F2"), _("Stores the donation of an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Gifted->SetBitmaps( wxBitmap( objectgiven_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Gifted->SetBitmap( wxBitmap( objectgiven_xpm ) );
	#endif
	MainMenu_Objects_->Append( Gifted );
	
	MainMenu_Objects_->AppendSeparator();
	
	Lend = new wxMenuItem( MainMenu_Objects_, bilLend, wxString( _("Lend") ) + wxT('\t') + wxT("CTRL+SHIFT+F3"), _("Lend an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Lend->SetBitmaps( wxBitmap( objectlend_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Lend->SetBitmap( wxBitmap( objectlend_xpm ) );
	#endif
	MainMenu_Objects_->Append( Lend );
	
	GetBack = new wxMenuItem( MainMenu_Objects_, bilGetBack, wxString( _("Get back") ) + wxT('\t') + wxT("CTRL+SHIFT+F4"), _("Get back an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	GetBack->SetBitmaps( wxBitmap( objectgetback_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GetBack->SetBitmap( wxBitmap( objectgetback_xpm ) );
	#endif
	MainMenu_Objects_->Append( GetBack );
	
	MainMenu_Objects_->AppendSeparator();
	
	Borrow = new wxMenuItem( MainMenu_Objects_, bilBorrow, wxString( _("Borrow") ) + wxT('\t') + wxT("CTRL+SHIFT+F5"), _("Borrow an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Borrow->SetBitmaps( wxBitmap( objectborrow_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Borrow->SetBitmap( wxBitmap( objectborrow_xpm ) );
	#endif
	MainMenu_Objects_->Append( Borrow );
	
	GiveBack = new wxMenuItem( MainMenu_Objects_, bilGiveBack, wxString( _("Give back") ) + wxT('\t') + wxT("CTRL+SHIFT+F6"), _("Give back an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	GiveBack->SetBitmaps( wxBitmap( objectgiveback_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GiveBack->SetBitmap( wxBitmap( objectgiveback_xpm ) );
	#endif
	MainMenu_Objects_->Append( GiveBack );
	
	MAINMENU->Append( MainMenu_Objects_, _("Objects") ); 
	
	MainMenu_Credits_ = new wxMenu();
	NewCredit = new wxMenuItem( MainMenu_Credits_, bilNewCredit, wxString( _("Set") ) + wxT('\t') + wxT("CTRL+ALT+C"), _("Stores a credit"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	NewCredit->SetBitmaps( wxBitmap( creditnew_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	NewCredit->SetBitmap( wxBitmap( creditnew_xpm ) );
	#endif
	MainMenu_Credits_->Append( NewCredit );
	
	RemoveCredit = new wxMenuItem( MainMenu_Credits_, bilRemoveCredit, wxString( _("Remove") ) + wxT('\t') + wxT("CTRL+ALT+A"), _("Removes or reduces a credit"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveCredit->SetBitmaps( wxBitmap( creditremove_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveCredit->SetBitmap( wxBitmap( creditremove_xpm ) );
	#endif
	MainMenu_Credits_->Append( RemoveCredit );
	
	MainMenu_Credits_->AppendSeparator();
	
	CondoneCredit = new wxMenuItem( MainMenu_Credits_, bilCondoneCredit, wxString( _("Condone") ) , _("Condone a credit or part of it"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	CondoneCredit->SetBitmaps( wxBitmap( condone_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	CondoneCredit->SetBitmap( wxBitmap( condone_xpm ) );
	#endif
	MainMenu_Credits_->Append( CondoneCredit );
	
	MAINMENU->Append( MainMenu_Credits_, _("Credits") ); 
	
	MainMenu_Debts_ = new wxMenu();
	NewDebt = new wxMenuItem( MainMenu_Debts_, bilNewDebt, wxString( _("Set") ) + wxT('\t') + wxT("CTRL+ALT+D"), _("Stores a debt"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	NewDebt->SetBitmaps( wxBitmap( debtnew_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	NewDebt->SetBitmap( wxBitmap( debtnew_xpm ) );
	#endif
	MainMenu_Debts_->Append( NewDebt );
	
	RemoveDebt = new wxMenuItem( MainMenu_Debts_, bilRemoveDebt, wxString( _("Remove") ) + wxT('\t') + wxT("CTRL+ALT+B"), _("Removes or reduces a debt"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveDebt->SetBitmaps( wxBitmap( debtremove_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveDebt->SetBitmap( wxBitmap( debtremove_xpm ) );
	#endif
	MainMenu_Debts_->Append( RemoveDebt );
	
	MainMenu_Debts_->AppendSeparator();
	
	CondoneDebt = new wxMenuItem( MainMenu_Debts_, bilCondoneDebt, wxString( _("Condone") ) , _("Condone a debt or part of it"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	CondoneDebt->SetBitmaps( wxBitmap( condone_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	CondoneDebt->SetBitmap( wxBitmap( condone_xpm ) );
	#endif
	MainMenu_Debts_->Append( CondoneDebt );
	
	MAINMENU->Append( MainMenu_Debts_, _("Debts") ); 
	
	MainMenu_ShopList_ = new wxMenu();
	AddShopItem = new wxMenuItem( MainMenu_ShopList_, bilAddShopItem, wxString( _("Add item") ) + wxT('\t') + wxT("F5"), _("Add an item to the shopping list"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	AddShopItem->SetBitmaps( wxBitmap( newshop_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	AddShopItem->SetBitmap( wxBitmap( newshop_xpm ) );
	#endif
	MainMenu_ShopList_->Append( AddShopItem );
	
	DelShopItem = new wxMenuItem( MainMenu_ShopList_, bilDelShopItem, wxString( _("Remove item") ) + wxT('\t') + wxT("F6"), _("Remove an item from the shopping list"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	DelShopItem->SetBitmaps( wxBitmap( condone_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	DelShopItem->SetBitmap( wxBitmap( condone_xpm ) );
	#endif
	MainMenu_ShopList_->Append( DelShopItem );
	
	MAINMENU->Append( MainMenu_ShopList_, _("Shopping list") ); 
	
	MainMenu_Tools_ = new wxMenu();
	Wiz = new wxMenuItem( MainMenu_Tools_, bilWiz, wxString( _("Document creation wizard") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Tools_->Append( Wiz );
	
	OldConv = new wxMenuItem( MainMenu_Tools_, bilOldConv, wxString( _("Old document converter") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OldConv->SetBitmaps( wxBitmap( oldconv_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	OldConv->SetBitmap( wxBitmap( oldconv_xpm ) );
	#endif
	MainMenu_Tools_->Append( OldConv );
	
	MainMenu_Tools_->AppendSeparator();
	
	MainMenu_ExtTools = new wxMenu();
	wxMenuItem* MainMenu_ExtToolsItem = new wxMenuItem( MainMenu_Tools_, wxID_ANY, _("External tools"), wxEmptyString, wxITEM_NORMAL, MainMenu_ExtTools );
	MainMenu_Tools_->Append( MainMenu_ExtToolsItem );
	
	Options = new wxMenuItem( MainMenu_Tools_, bilOptions, wxString( _("Options") ) + wxT('\t') + wxT("CTRL+O"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_Tools_->Append( Options );
	
	MAINMENU->Append( MainMenu_Tools_, _("Tools") ); 
	
	MainMenu_Help_ = new wxMenu();
	Help1 = new wxMenuItem( MainMenu_Help_, bilHelpMenu, wxString( _("Help") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Help1 );
	
	MainMenu_Help_->AppendSeparator();
	
	Author = new wxMenuItem( MainMenu_Help_, wxID_ABOUT, wxString( _("Author") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Author );
	
	Website = new wxMenuItem( MainMenu_Help_, bilWebSite, wxString( _("Website") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Website );
	
	Donate = new wxMenuItem( MainMenu_Help_, bilDonate, wxString( _("Donate") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Donate );
	
	MainMenu_Help_->AppendSeparator();
	
	wxMenuItem* report_bug;
	report_bug = new wxMenuItem( MainMenu_Help_, bilBug, wxString( _("Report bug") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( report_bug );
	
	MAINMENU->Append( MainMenu_Help_, _("About") ); 
	
	this->SetMenuBar( MAINMENU );
	
	ToolBar = this->CreateToolBar( wxTB_HORIZONTAL, wxID_ANY );
	ToolBar->SetToolBitmapSize( wxSize( 16,16 ) );
	ToolBar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
	Tool_New = ToolBar->AddTool( bilNew, wxEmptyString, wxBitmap( new_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Create a new document"), NULL ); 
	
	Tool_Open = ToolBar->AddTool( bilOpen, wxEmptyString, wxBitmap( open_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Open a document"), NULL ); 
	
	Tool_Save = ToolBar->AddTool( bilSave, wxEmptyString, wxBitmap( save_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Save changes to current document"), NULL ); 
	
	Tool_XML = ToolBar->AddTool( bilXML, wxEmptyString, wxBitmap( icon_xml_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Export the document in XML format"), NULL ); 
	
	ToolBar->AddSeparator(); 
	
	Tool_NewFund = ToolBar->AddTool( bilNewFund, wxEmptyString, wxBitmap( newfund_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Add a fund"), NULL ); 
	
	Tool_RemoveFund = ToolBar->AddTool( bilRemoveFund, wxEmptyString, wxBitmap( removefund_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Remove a fund"), NULL ); 
	
	Tool_ResetFund = ToolBar->AddTool( bilResetFund, wxEmptyString, wxBitmap( resetfund_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Reset the value of a fund"), NULL ); 
	
	ToolBar->AddSeparator(); 
	
	Tool_Gain = ToolBar->AddTool( bilGain, wxEmptyString, wxBitmap( gain_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Store a profit"), NULL ); 
	
	Tool_Expense = ToolBar->AddTool( bilExpense, wxEmptyString, wxBitmap( expense_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Store an expense"), NULL ); 
	
	Tool_Category = ToolBar->AddTool( bilCategory, wxEmptyString, wxBitmap( note_icon_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Add a category to the report"), NULL ); 
	
	ToolBar->AddSeparator(); 
	
	Tool_Received = ToolBar->AddTool( bilReceived, wxEmptyString, wxBitmap( objectreceived_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores the reception of an object"), NULL ); 
	
	Tool_Gifted = ToolBar->AddTool( bilGifted, wxEmptyString, wxBitmap( objectgiven_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores the donation of an object"), NULL ); 
	
	Tool_Lend = ToolBar->AddTool( bilLend, wxEmptyString, wxBitmap( objectlend_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Lend an object"), NULL ); 
	
	Tool_GetBack = ToolBar->AddTool( bilGetBack, wxEmptyString, wxBitmap( objectgetback_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Get back an object"), NULL ); 
	
	Tool_Borrow = ToolBar->AddTool( bilBorrow, wxEmptyString, wxBitmap( objectborrow_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Borrow an object"), NULL ); 
	
	Tool_GiveBack = ToolBar->AddTool( bilGiveBack, wxEmptyString, wxBitmap( objectgiveback_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Give back an object"), NULL ); 
	
	ToolBar->AddSeparator(); 
	
	Tool_NewCredit = ToolBar->AddTool( bilNewCredit, wxEmptyString, wxBitmap( creditnew_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores a credit"), NULL ); 
	
	Tool_RemoveCredit = ToolBar->AddTool( bilRemoveCredit, wxEmptyString, wxBitmap( creditremove_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Removes or reduces a credit"), NULL ); 
	
	Tool_CondoneCredit = ToolBar->AddTool( bilCondoneCredit, wxEmptyString, wxBitmap( condone_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Condone a credit or part of it"), NULL ); 
	
	ToolBar->AddSeparator(); 
	
	Tool_NewDebt = ToolBar->AddTool( bilNewDebt, wxEmptyString, wxBitmap( debtnew_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores a debt"), NULL ); 
	
	Tool_RemoveDebt = ToolBar->AddTool( bilRemoveDebt, wxEmptyString, wxBitmap( debtremove_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Removes or reduces a debt"), NULL ); 
	
	Tool_CondoneDebt = ToolBar->AddTool( bilCondoneDebt, wxEmptyString, wxBitmap( condone_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Condone a debt or part of it"), NULL ); 
	
	ToolBar->AddSeparator(); 
	
	Tool_AddShopItem = ToolBar->AddTool( bilAddShopItem, wxEmptyString, wxBitmap( newshop_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Add an item to the shopping list"), NULL ); 
	
	Tool_RemShopItem = ToolBar->AddTool( bilDelShopItem, wxEmptyString, wxBitmap( condone_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Remove an item from the shopping list"), NULL ); 
	
	ToolBar->Realize(); 
	
	Container = new wxFlexGridSizer( 3, 1, 0, 0 );
	Container->AddGrowableCol( 0 );
	Container->AddGrowableRow( 1 );
	Container->SetFlexibleDirection( wxBOTH );
	Container->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	Title = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Title->Wrap( -1 );
	Title->SetFont( wxFont( 20, 74, 90, 90, false, wxT("Ubuntu") ) );
	Title->SetBackgroundColour( wxColour( 255, 128, 255 ) );
	
	Container->Add( Title, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_splitter7 = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter7->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter7OnIdle ), NULL, this );
	
	Panel2 = new wxPanel( m_splitter7, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer8;
	fgSizer8 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer8->AddGrowableCol( 0 );
	fgSizer8->AddGrowableRow( 0 );
	fgSizer8->SetFlexibleDirection( wxBOTH );
	fgSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_panel4 = new wxPanel( Panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );
	
	m_splitter3 = new wxSplitterWindow( m_panel4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter3->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter3OnIdle ), NULL, this );
	
	m_panel5 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );
	
	ItemList = new wxTreeCtrl( m_panel5, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE|wxTR_HIDE_ROOT );
	ItemList->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	ItemList->SetToolTip( _("List of available funds and objects") );
	
	PopList = new wxMenu();
	Funds2 = new wxMenu();
	wxMenuItem* Funds2Item = new wxMenuItem( PopList, wxID_ANY, _("Funds"), wxEmptyString, wxITEM_NORMAL, Funds2 );
	wxMenuItem* New2;
	New2 = new wxMenuItem( Funds2, bilNewFund, wxString( _("New") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	New2->SetBitmaps( wxBitmap( newfund_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	New2->SetBitmap( wxBitmap( newfund_xpm ) );
	#endif
	Funds2->Append( New2 );
	
	PopList->Append( Funds2Item );
	
	Cred2 = new wxMenu();
	wxMenuItem* Cred2Item = new wxMenuItem( PopList, wxID_ANY, _("Credits"), wxEmptyString, wxITEM_NORMAL, Cred2 );
	wxMenuItem* m_menuItem43;
	m_menuItem43 = new wxMenuItem( Cred2, bilNewCredit, wxString( _("New") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem43->SetBitmaps( wxBitmap( creditnew_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem43->SetBitmap( wxBitmap( creditnew_xpm ) );
	#endif
	Cred2->Append( m_menuItem43 );
	
	PopList->Append( Cred2Item );
	
	Deb2 = new wxMenu();
	wxMenuItem* Deb2Item = new wxMenuItem( PopList, wxID_ANY, _("Debts"), wxEmptyString, wxITEM_NORMAL, Deb2 );
	wxMenuItem* m_menuItem44;
	m_menuItem44 = new wxMenuItem( Deb2, bilNewDebit, wxString( _("New") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem44->SetBitmaps( wxBitmap( debtnew_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem44->SetBitmap( wxBitmap( debtnew_xpm ) );
	#endif
	Deb2->Append( m_menuItem44 );
	
	PopList->Append( Deb2Item );
	
	Obj2 = new wxMenu();
	wxMenuItem* Obj2Item = new wxMenuItem( PopList, wxID_ANY, _("Objects"), wxEmptyString, wxITEM_NORMAL, Obj2 );
	wxMenuItem* m_menuItem45;
	m_menuItem45 = new wxMenuItem( Obj2, bilLend, wxString( _("Lend") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem45->SetBitmaps( wxBitmap( objectlend_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem45->SetBitmap( wxBitmap( objectlend_xpm ) );
	#endif
	Obj2->Append( m_menuItem45 );
	
	wxMenuItem* m_menuItem46;
	m_menuItem46 = new wxMenuItem( Obj2, bilBorrow, wxString( _("Borrow") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem46->SetBitmaps( wxBitmap( objectborrow_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem46->SetBitmap( wxBitmap( objectborrow_xpm ) );
	#endif
	Obj2->Append( m_menuItem46 );
	
	PopList->Append( Obj2Item );
	
	ShopList2 = new wxMenu();
	wxMenuItem* ShopList2Item = new wxMenuItem( PopList, wxID_ANY, _("Shopping list"), wxEmptyString, wxITEM_NORMAL, ShopList2 );
	wxMenuItem* m_menuItem47;
	m_menuItem47 = new wxMenuItem( ShopList2, bilAddShopItem, wxString( _("Add item") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem47->SetBitmaps( wxBitmap( newshop_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem47->SetBitmap( wxBitmap( newshop_xpm ) );
	#endif
	ShopList2->Append( m_menuItem47 );
	
	PopList->Append( ShopList2Item );
	
	wxMenuItem* RemoveFund2;
	RemoveFund2 = new wxMenuItem( PopList, bilRemoveFund, wxString( _("Remove fund") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveFund2->SetBitmaps( wxBitmap( removefund_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveFund2->SetBitmap( wxBitmap( removefund_xpm ) );
	#endif
	PopList->Append( RemoveFund2 );
	
	wxMenuItem* ResetFund2;
	ResetFund2 = new wxMenuItem( PopList, bilResetFund, wxString( _("Reset fund") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	ResetFund2->SetBitmaps( wxBitmap( resetfund_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	ResetFund2->SetBitmap( wxBitmap( resetfund_xpm ) );
	#endif
	PopList->Append( ResetFund2 );
	
	wxMenuItem* Condone2;
	Condone2 = new wxMenuItem( PopList, wxID_ANY, wxString( _("Condone") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Condone2->SetBitmaps( wxBitmap( condone_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Condone2->SetBitmap( wxBitmap( condone_xpm ) );
	#endif
	PopList->Append( Condone2 );
	
	wxMenuItem* GetBack2;
	GetBack2 = new wxMenuItem( PopList, bilGetBack, wxString( _("Get back") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	GetBack2->SetBitmaps( wxBitmap( objectgetback_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GetBack2->SetBitmap( wxBitmap( objectgetback_xpm ) );
	#endif
	PopList->Append( GetBack2 );
	
	wxMenuItem* GiveBack2;
	GiveBack2 = new wxMenuItem( PopList, bilGiveBack, wxString( _("Give back") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	GiveBack2->SetBitmaps( wxBitmap( objectgiveback_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GiveBack2->SetBitmap( wxBitmap( objectgiveback_xpm ) );
	#endif
	PopList->Append( GiveBack2 );
	
	wxMenuItem* DelShopItem2;
	DelShopItem2 = new wxMenuItem( PopList, bilDelShopItem, wxString( _("Remove item") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	DelShopItem2->SetBitmaps( wxBitmap( condone_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	DelShopItem2->SetBitmap( wxBitmap( condone_xpm ) );
	#endif
	PopList->Append( DelShopItem2 );
	
	ItemList->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( wxMainFrame::ItemListOnContextMenu ), NULL, this ); 
	
	bSizer4->Add( ItemList, 1, wxALL|wxEXPAND, 5 );
	
	
	m_panel5->SetSizer( bSizer4 );
	m_panel5->Layout();
	bSizer4->Fit( m_panel5 );
	m_panel6 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );
	
	m_splitter4 = new wxSplitterWindow( m_panel6, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter4->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter4OnIdle ), NULL, this );
	
	FundPanel = new wxPanel( m_splitter4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	FundSizer = new wxFlexGridSizer( 1, 1, 0, 0 );
	FundSizer->AddGrowableCol( 0 );
	FundSizer->AddGrowableRow( 0 );
	FundSizer->SetFlexibleDirection( wxBOTH );
	FundSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	FundPanel->SetSizer( FundSizer );
	FundPanel->Layout();
	FundSizer->Fit( FundPanel );
	TrendPanel = new wxPanel( m_splitter4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	TrendSizer = new wxFlexGridSizer( 1, 1, 0, 0 );
	TrendSizer->AddGrowableCol( 0 );
	TrendSizer->AddGrowableRow( 0 );
	TrendSizer->SetFlexibleDirection( wxBOTH );
	TrendSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	TrendPanel->SetSizer( TrendSizer );
	TrendPanel->Layout();
	TrendSizer->Fit( TrendPanel );
	m_splitter4->SplitHorizontally( FundPanel, TrendPanel, 0 );
	bSizer5->Add( m_splitter4, 1, wxEXPAND, 5 );
	
	
	m_panel6->SetSizer( bSizer5 );
	m_panel6->Layout();
	bSizer5->Fit( m_panel6 );
	m_splitter3->SplitVertically( m_panel5, m_panel6, 258 );
	bSizer3->Add( m_splitter3, 1, wxEXPAND, 5 );
	
	
	m_panel4->SetSizer( bSizer3 );
	m_panel4->Layout();
	bSizer3->Fit( m_panel4 );
	fgSizer8->Add( m_panel4, 1, wxEXPAND, 5 );
	
	fgSizer9 = new wxFlexGridSizer( 4, 1, 0, 0 );
	fgSizer9->AddGrowableCol( 0 );
	fgSizer9->AddGrowableRow( 3 );
	fgSizer9->SetFlexibleDirection( wxBOTH );
	fgSizer9->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	Calendar = new wxCalendarCtrl( Panel2, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxCAL_MONDAY_FIRST|wxCAL_SEQUENTIAL_MONTH_SELECTION|wxCAL_SHOW_HOLIDAYS|wxCAL_SHOW_SURROUNDING_WEEKS );
	Calendar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	Calendar->SetToolTip( _("Select the day for operations to store") );
	
	fgSizer9->Add( Calendar, 0, wxALL, 5 );
	
	
	fgSizer9->Add( 0, 0, 1, wxEXPAND, 5 );
	
	
	fgSizer8->Add( fgSizer9, 1, wxEXPAND, 5 );
	
	
	Panel2->SetSizer( fgSizer8 );
	Panel2->Layout();
	fgSizer8->Fit( Panel2 );
	m_panel3 = new wxPanel( m_splitter7, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	m_splitter41 = new wxSplitterWindow( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter41->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter41OnIdle ), NULL, this );
	
	m_panel8 = new wxPanel( m_splitter41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer51;
	bSizer51 = new wxBoxSizer( wxVERTICAL );
	
	Report = new wxGrid( m_panel8, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	
	// Grid
	Report->CreateGrid( 1, 8 );
	Report->EnableEditing( false );
	Report->EnableGridLines( true );
	Report->EnableDragGridSize( false );
	Report->SetMargins( 0, 0 );
	
	// Columns
	Report->AutoSizeColumns();
	Report->EnableDragColMove( false );
	Report->EnableDragColSize( true );
	Report->SetColLabelSize( 30 );
	Report->SetColLabelValue( 0, _("Date") );
	Report->SetColLabelValue( 1, _("Hour") );
	Report->SetColLabelValue( 2, _("Operation") );
	Report->SetColLabelValue( 3, _("Value (€)") );
	Report->SetColLabelValue( 4, _("Reason") );
	Report->SetColLabelValue( 5, _("Category") );
	Report->SetColLabelValue( 6, _("Contact") );
	Report->SetColLabelValue( 7, _("Location") );
	Report->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	Report->SetRowSize( 0, 23 );
	Report->EnableDragRowSize( true );
	Report->SetRowLabelSize( 0 );
	Report->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	Report->SetLabelBackgroundColour( wxColour( 250, 147, 245 ) );
	Report->SetLabelFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 92, false, wxT("Ubuntu") ) );
	
	// Cell Defaults
	Report->SetDefaultCellFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 90, false, wxT("Ubuntu") ) );
	Report->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	Report->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 90, false, wxT("Ubuntu") ) );
	Report->SetToolTip( _("Report of stored operations") );
	
	PopRep = new wxMenu();
	Category2 = new wxMenuItem( PopRep, bilCategory, wxString( _("Category") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Category2->SetBitmaps( wxBitmap( note_icon_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Category2->SetBitmap( wxBitmap( note_icon_xpm ) );
	#endif
	PopRep->Append( Category2 );
	
	Report->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( wxMainFrame::ReportOnContextMenu ), NULL, this ); 
	
	bSizer51->Add( Report, 1, wxALL|wxEXPAND, 5 );
	
	
	m_panel8->SetSizer( bSizer51 );
	m_panel8->Layout();
	bSizer51->Fit( m_panel8 );
	m_panel9 = new wxPanel( m_splitter41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer71;
	fgSizer71 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer71->AddGrowableCol( 0 );
	fgSizer71->AddGrowableRow( 1 );
	fgSizer71->SetFlexibleDirection( wxBOTH );
	fgSizer71->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	ShopLabel = new wxStaticText( m_panel9, wxID_ANY, _("Shopping list"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
	ShopLabel->Wrap( -1 );
	ShopLabel->SetFont( wxFont( 16, 74, 90, 90, false, wxT("Ubuntu") ) );
	
	fgSizer71->Add( ShopLabel, 1, wxALIGN_CENTER|wxTOP|wxRIGHT|wxLEFT|wxALIGN_BOTTOM, 5 );
	
	ShopList = new wxListBox( m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 ); 
	ShopList->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
	fgSizer71->Add( ShopList, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	
	m_panel9->SetSizer( fgSizer71 );
	m_panel9->Layout();
	fgSizer71->Fit( m_panel9 );
	m_splitter41->SplitVertically( m_panel8, m_panel9, 800 );
	bSizer1->Add( m_splitter41, 1, wxEXPAND, 5 );
	
	
	m_panel3->SetSizer( bSizer1 );
	m_panel3->Layout();
	bSizer1->Fit( m_panel3 );
	m_splitter7->SplitHorizontally( Panel2, m_panel3, 0 );
	Container->Add( m_splitter7, 1, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer7;
	fgSizer7 = new wxFlexGridSizer( 2, 2, 0, 0 );
	fgSizer7->AddGrowableCol( 0 );
	fgSizer7->SetFlexibleDirection( wxBOTH );
	fgSizer7->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	Progress = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_HORIZONTAL );
	Progress->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
	fgSizer7->Add( Progress, 0, wxALL|wxEXPAND, 5 );
	
	
	Container->Add( fgSizer7, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( Container );
	this->Layout();
	StatusBar = this->CreateStatusBar( 2, wxST_SIZEGRIP, wxID_ANY );
	StatusBar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
}

wxMainFrame::~wxMainFrame()
{
	delete PopList; 
	delete PopRep; 
}
