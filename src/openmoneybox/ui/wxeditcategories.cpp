///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxeditcategories.h"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxFEditCategories, wxDialog )
	EVT_ACTIVATE( wxFEditCategories::_wxFB_FormShow )
	EVT_LIST_ITEM_DESELECTED( ombListCat, wxFEditCategories::_wxFB_OnListCtrl )
	EVT_LIST_ITEM_SELECTED( ombListCat, wxFEditCategories::_wxFB_OnListCtrl )
	EVT_BUTTON( ombAdd, wxFEditCategories::_wxFB_AddButtonClick )
	EVT_BUTTON( ombRemove, wxFEditCategories::_wxFB_RemoveButtonClick )
	EVT_BUTTON( ombIcon, wxFEditCategories::_wxFB_IconButtonClick )
	EVT_LIST_ITEM_DESELECTED( ombListIco, wxFEditCategories::_wxFB_OnListCtrl )
	EVT_LIST_ITEM_SELECTED( ombListIco, wxFEditCategories::_wxFB_OnListCtrl )
	EVT_BUTTON( ombChoose, wxFEditCategories::_wxFB_ChooseBtnClick )
	EVT_BUTTON( wxID_OK, wxFEditCategories::_wxFB_OKBtnClick )
END_EVENT_TABLE()

wxFEditCategories::wxFEditCategories( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	wxFlexGridSizer* fgSizer7;
	fgSizer7 = new wxFlexGridSizer( 4, 1, 0, 0 );
	fgSizer7->AddGrowableCol( 0 );
	fgSizer7->AddGrowableRow( 1 );
	fgSizer7->SetFlexibleDirection( wxBOTH );
	fgSizer7->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	CategoryList = new wxListCtrl( this, ombListCat, wxDefaultPosition, wxSize( -1,115 ), wxLC_ICON|wxLC_NO_HEADER|wxLC_REPORT|wxLC_SINGLE_SEL );
	bSizer3->Add( CategoryList, 1, wxALL|wxEXPAND, 5 );


	fgSizer7->Add( bSizer3, 1, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer5;
	fgSizer5 = new wxFlexGridSizer( 0, 4, 0, 0 );
	fgSizer5->AddGrowableCol( 0 );
	fgSizer5->SetFlexibleDirection( wxBOTH );
	fgSizer5->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	EditBox = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer5->Add( EditBox, 1, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_button3 = new wxButton( this, ombAdd, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer5->Add( m_button3, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	Remove = new wxButton( this, ombRemove, _("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	Remove->Enable( false );

	fgSizer5->Add( Remove, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	IconBtn = new wxButton( this, ombIcon, _("Icon"), wxDefaultPosition, wxDefaultSize, 0 );
	IconBtn->Enable( false );

	fgSizer5->Add( IconBtn, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );


	fgSizer7->Add( fgSizer5, 1, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer51;
	fgSizer51 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer51->SetFlexibleDirection( wxBOTH );
	fgSizer51->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	IconList = new wxListCtrl( this, ombListIco, wxDefaultPosition, wxSize( 380,50 ), wxLC_ICON|wxLC_NO_HEADER|wxLC_SINGLE_SEL );
	IconList->Hide();

	fgSizer51->Add( IconList, 0, wxTOP|wxBOTTOM|wxLEFT, 5 );

	ChooseBtn = new wxButton( this, ombChoose, _("Choose"), wxDefaultPosition, wxDefaultSize, 0 );
	ChooseBtn->Enable( false );
	ChooseBtn->Hide();

	fgSizer51->Add( ChooseBtn, 0, wxALIGN_RIGHT|wxTOP|wxBOTTOM|wxRIGHT, 5 );


	fgSizer7->Add( fgSizer51, 1, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer8;
	fgSizer8 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer8->AddGrowableCol( 0 );
	fgSizer8->AddGrowableRow( 0 );
	fgSizer8->SetFlexibleDirection( wxBOTH );
	fgSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );

	OKBtn->SetDefault();
	OKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer8->Add( OKBtn, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	CancelBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer8->Add( CancelBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );


	fgSizer7->Add( fgSizer8, 1, wxEXPAND, 5 );


	this->SetSizer( fgSizer7 );
	this->Layout();
}

wxFEditCategories::~wxFEditCategories()
{
}
