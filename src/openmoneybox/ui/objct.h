/***************************************************************
 * Name:      objct.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/dialog.h>

#ifndef ObjctH
#define ObjctH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxobjct.h"

class TObjects : public wxTObjects
{
private:
	// Non-GUI components
	bool Kind;

	// Routines
	void HideSysTime(void);
//private:
	void Focus(bool F);
	#ifdef __WXMSW__
    void MSW_ROCombo(void);
  #endif // __WXMSW__
protected:
	void OKBtnClick(wxCommandEvent& event);
	void AlmCheckBoxClick(wxCommandEvent& event);
public:

	// Routines
	explicit TObjects(wxWindow *parent);
	void InitLabels(int O);
	void HideAlarm(void);
};

extern /*PACKAGE*/ TObjects *Objects;

// Calls to module ombLogo
WXIMPORT wxString iUpperCase(wxString S);
// Calls to module igiomb
//extern __declspec(dllimport) wxString GetLanguageFile(void);
// Calls to module omberr
//WXIMPORT void Error(wxFileConfig *INI,LANGID Lan,int Err,wxString Opt);
WXIMPORT void Error(int Err,wxString Opt);

#endif


