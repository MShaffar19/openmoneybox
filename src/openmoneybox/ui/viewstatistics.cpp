/***************************************************************
 * Name:      main_wx.cpp
 * Purpose:   Code for OpenMoneyBox trend chart
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 *
 ** code derived from BOINC source (http://boinc.berkeley.edu), version 7.5.0
 *
 **************************************************************/

#include "viewstatistics.h"

wxString format_number(double x, int nprec) {
	return wxNumberFormatter::ToString(x, nprec);
}

BEGIN_EVENT_TABLE (CPaintStatistics, wxWindow)
	EVT_PAINT(CPaintStatistics::OnPaint)
	EVT_SIZE(CPaintStatistics::OnSize)
	EVT_LEFT_DOWN(CPaintStatistics::OnLeftMouseDown)
	EVT_LEFT_UP(CPaintStatistics::OnLeftMouseUp)
	EVT_RIGHT_DOWN(CPaintStatistics::OnRightMouseDown)
	EVT_RIGHT_UP(CPaintStatistics::OnRightMouseUp)
	EVT_MOTION(CPaintStatistics::OnMouseMotion)
	EVT_LEAVE_WINDOW(CPaintStatistics::OnMouseLeaveWindows)
	EVT_ERASE_BACKGROUND(CPaintStatistics::OnEraseBackground)
  //EVT_SCROLL(CPaintStatistics::OnLegendScroll)
END_EVENT_TABLE ()

// Set USE_MEMORYDC FALSE to aid debugging
#define USE_MEMORYDC TRUE

CPaintStatistics::CPaintStatistics(wxWindow* parent, wxWindowID id, const wxPoint& pos,	const wxSize& size, long style, const wxString& name
): wxWindow(parent, id, pos, size, style, name)
{	m_font_standart = wxSystemSettings::GetFont(wxSYS_SYSTEM_FONT);
	m_font_bold = wxSystemSettings::GetFont(wxSYS_SYSTEM_FONT);

	heading = wxT("");

	m_GraphLineWidth = 2;
	m_GraphPointWidth = 4;


	m_GraphMarker_X1 = 0;
	m_GraphMarker_Y1 = 0;
	m_GraphMarker1 = false;

	m_GraphZoom_X1 = 0;
	m_GraphZoom_Y1 = 0;
	m_GraphZoom_X2 = 0;
	m_GraphZoom_Y2 = 0;
	m_GraphZoom_X2_old = 0;
	m_GraphZoom_Y2_old = 0;

	m_GraphZoomStart = false;

	m_GraphMove_X1 = 0;
	m_GraphMove_Y1 = 0;
	m_GraphMove_X2 = 0;
	m_GraphMove_Y2 = 0;
	m_GraphMoveStart = false;
	m_GraphMoveGo = false;

	m_Zoom_max_val_X = 0;
	m_Zoom_min_val_X = 0;
	m_Zoom_max_val_Y = 0;
	m_Zoom_min_val_Y = 0;
	m_Zoom_Auto = true;

// XY
	m_main_X_start = 0;
	m_main_X_end = 0;
	m_main_Y_start = 0;
	m_main_Y_end = 0;

	m_WorkSpace_X_start = 0;
	m_WorkSpace_X_end = 0;
	m_WorkSpace_Y_start = 0;
	m_WorkSpace_Y_end = 0;

	m_Graph_X_start = 0;
	m_Graph_X_end = 0;
	m_Graph_Y_start = 0;
	m_Graph_Y_end = 0;

	m_Graph_draw_X_start = 0;
	m_Graph_draw_X_end = 0;
	m_Graph_draw_Y_start = 0;
	m_Graph_draw_Y_end = 0;

// Default colours
	m_pen_MarkerLineColour = wxColour(0, 0, 0);
	m_pen_ZoomRectColour = wxColour (128, 64, 95);
	m_brush_ZoomRectColour = wxColour(24, 31, 0);
	m_brush_AxisColour = wxColour(192, 224, 255);
	m_pen_AxisColour = wxColour(64, 128, 192);
	m_pen_AxisColourZoom = wxColour(255, 64, 0);
	m_pen_AxisColourAutoZoom = wxColour(64, 128, 192);
	m_pen_AxisXColour = wxColour(64, 128, 192);
	m_pen_AxisYColour = wxColour(64, 128, 192);
	m_pen_AxisXTextColour = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT );
	m_pen_AxisYTextColour = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT );

	m_brush_MainColour = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW );
	m_pen_MainColour = wxColour(64, 128, 192);
	m_pen_HeadTextColour = wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHT );
	m_pen_GraphTotalColour = wxColour(255, 0, 0);

	m_dc_bmp.Create(1, 1);
	m_full_repaint = true;
	m_bmp_OK = false;

}

CPaintStatistics::~CPaintStatistics() {}


static bool CrossTwoLine(const double X1_1, const double Y1_1, const double X1_2, const double Y1_2,
						 const double X2_1, const double Y2_1, const double X2_2, const double Y2_2,
						 double &Xcross, double &Ycross) {
	double A1 = Y1_1 - Y1_2;
	double B1 = X1_2 - X1_1;
	double C1 = - X1_1 * A1 - Y1_1 * B1;
	double A2 = Y2_1 - Y2_2;
	double B2 = X2_2 - X2_1;
	double C2 = - X2_1 * A2 - Y2_1 * B2;
	double tmp1 = (A1 * B2 - A2 * B1);
	if (0 == tmp1){
		Xcross = 0;
		Ycross = 0;
		return false;
	}else{
		Xcross = (B1 * C2 - B2 * C1) / tmp1;
		Ycross = (C1 * A2 - C2 * A1) / tmp1;
		return true;
	}
}

//----Draw "Point"
static void myDrawPoint(wxDC &dc,int X, int Y, wxColour graphColour,int numberTypePoint, int PointWidth) {
	dc.SetPen(wxPen(graphColour , 1 , wxSOLID));
	switch (numberTypePoint % 5){
	case 1: {wxPoint* points = new wxPoint[3];
		points[0] = wxPoint(X, Y - 1 - (PointWidth / 2));
		points[1] = wxPoint(X + (PointWidth / 2), Y + (PointWidth / 2));
		points[2] = wxPoint(X - (PointWidth / 2), Y + (PointWidth / 2));
		dc.DrawPolygon(3, points);
		delete[] points;
		break;}
	case 2: {wxPoint* points = new wxPoint[3];
		points[0] = wxPoint(X, Y + 1 + (PointWidth / 2));
		points[1] = wxPoint(X + (PointWidth / 2), Y - (PointWidth / 2));
		points[2] = wxPoint(X - (PointWidth / 2), Y - (PointWidth / 2));
		dc.DrawPolygon(3, points);
		delete[] points;
		break;}
	case 3:	dc.DrawRectangle(wxCoord(X - (PointWidth / 2)),wxCoord(Y - (PointWidth / 2)),wxCoord(PointWidth + 1),wxCoord(PointWidth + 1));
		break;
	case 4: {wxPoint* points = new wxPoint[4];
		points[0] = wxPoint(X, Y - 1 - (PointWidth / 2));
		points[1] = wxPoint(X + 1 + (PointWidth / 2), Y);
		points[2] = wxPoint(X, Y + 1 + (PointWidth / 2));
		points[3] = wxPoint(X - 1 - (PointWidth / 2), Y);
		dc.DrawPolygon(4, points);
		delete[] points;
		break;}
	default:dc.DrawCircle(wxCoord(X), wxCoord(Y), wxCoord(PointWidth / 2));
	}
}

//----Find minimum/maximum value----
static void MinMaxDayCredit(double &min_credit, double &max_credit, double &min_day, double &max_day, bool first = true, DAILY_STATS *stats = NULL, int nstats = 0, double trend_value = -1, wxDateTime trend_date = wxInvalidDateTime) {

	for (int j = 0; j < nstats; j++) {
		if (first){
			max_day = stats[j].day;
			max_credit = stats[j].total_credit;
			min_day = max_day;
			min_credit = max_credit;
			first = false;
		}
		else {
			if (stats[j].day < min_day) min_day = stats[j].day;
			if (stats[j].day > max_day) max_day = stats[j].day;

			if (stats[j].total_credit > max_credit) max_credit = stats[j].total_credit;
			if (stats[j].total_credit < min_credit) min_credit = stats[j].total_credit;
		}
	}

	if(trend_value > 0){
		if(trend_value > max_credit) max_credit = trend_value;
		else if (trend_value < min_credit) min_credit = trend_value;
		if(trend_date.IsValid()){
			int val = trend_date.GetTicks();
			if(val > max_day) max_day = val;
		}
	}
}

static void CheckMinMaxD(double &min_val, double &max_val) {
	if (min_val > max_val) min_val = max_val;
	if (max_val == min_val){
		max_val += 0.5;
		min_val -= 0.5;
	}
}

void CPaintStatistics::ClearXY(){
	m_main_X_start = 0;
	m_main_X_end = 0;
	m_main_Y_start = 0;
	m_main_Y_end = 0;

	m_WorkSpace_X_start = 0;
	m_WorkSpace_X_end = 0;
	m_WorkSpace_Y_start = 0;
	m_WorkSpace_Y_end = 0;

	m_Graph_X_start = 0;
	m_Graph_X_end = 0;
	m_Graph_Y_start = 0;
	m_Graph_Y_end = 0;

	m_Graph_draw_X_start = 0;
	m_Graph_draw_X_end = 0;
	m_Graph_draw_Y_start = 0;
	m_Graph_draw_Y_end = 0;
}

void CPaintStatistics::AB(const double x_coord1, const double y_coord1, const double x_coord2, const double y_coord2, const double x_val1, const double y_val1, const double x_val2, const double y_val2){
// Val -> Coord
	if (0.0 == (x_val2 - x_val1)){
		m_Ax_ValToCoord = 0.0;
		m_Bx_ValToCoord = 0.0;
	}else{
		m_Ax_ValToCoord = (x_coord2 - x_coord1) / (x_val2 - x_val1);
		m_Bx_ValToCoord = x_coord1 - (m_Ax_ValToCoord * x_val1);
	}
	if (0.0 == (y_val2 - y_val1)){
		m_Ay_ValToCoord = 0.0;
		m_By_ValToCoord = 0.0;
	}else{
		m_Ay_ValToCoord = (y_coord2 - y_coord1) / (y_val2 - y_val1);
		m_By_ValToCoord = y_coord1 - (m_Ay_ValToCoord * y_val1);
	}
// Coord -> Val
	if (0.0 == (x_coord2 - x_coord1)){
		m_Ax_CoordToVal = 0.0;
		m_Bx_CoordToVal = 0.0;
	}else{
		m_Ax_CoordToVal = (x_val2 - x_val1) / (x_coord2 - x_coord1);
		m_Bx_CoordToVal = x_val1 - (m_Ax_CoordToVal * x_coord1);
	}
	if (0.0 == (y_coord2 - y_coord1)){
		m_Ay_CoordToVal = 0.0;
		m_By_CoordToVal = 0.0;
	}else{
		m_Ay_CoordToVal = (y_val2 - y_val1) / (y_coord2 - y_coord1);
		m_By_CoordToVal = y_val1 - (m_Ay_CoordToVal * y_coord1);
	}
}

//----Draw Main Head----
void CPaintStatistics::DrawMainHead(wxDC &dc, const wxString head_name){
	wxCoord w_temp = 0, h_temp = 0, des_temp = 0, lead_temp = 0;
	dc.GetTextExtent(head_name, &w_temp, &h_temp, &des_temp, &lead_temp);
	dc.SetTextForeground (m_pen_HeadTextColour);
	wxCoord x0 = wxCoord(m_WorkSpace_X_start + ((m_WorkSpace_X_end - m_WorkSpace_X_start - double(w_temp)) / 2.0));
	wxCoord y0 = wxCoord(m_WorkSpace_Y_start + 1.0);
	if (x0 > wxCoord(m_WorkSpace_X_end)) x0 = wxCoord(m_WorkSpace_X_end);
	if (x0 < wxCoord(m_WorkSpace_X_start)) x0 = wxCoord(m_WorkSpace_X_start);
	if (x0 < 0) x0 = 0;
	if (y0 > wxCoord(m_WorkSpace_Y_end)) y0 = wxCoord(m_WorkSpace_Y_end);
	if (y0 < wxCoord(m_WorkSpace_Y_start)) y0 = wxCoord(m_WorkSpace_Y_start);
	if (y0 < 0) y0 = 0;
	dc.DrawText (head_name, x0, y0);
	m_WorkSpace_Y_start += double(h_temp) + 2.0;
	if (m_WorkSpace_Y_start > m_WorkSpace_Y_end) m_WorkSpace_Y_start = m_WorkSpace_Y_end;
	if (m_WorkSpace_Y_start < 0.0) m_WorkSpace_Y_start = 0.0;
}

void CPaintStatistics::DrawAxis(wxDC &dc, const double max_val_y, const double min_val_y, const double max_val_x, const double min_val_x,
								wxColour pen_AxisColour, const double max_val_y_all, const double min_val_y_all) {
	wxCoord x0 = wxCoord(m_WorkSpace_X_start);
	wxCoord y0 = wxCoord(m_WorkSpace_Y_start);
	wxCoord w0 = wxCoord(m_WorkSpace_X_end - m_WorkSpace_X_start);
	wxCoord h0 = wxCoord(m_WorkSpace_Y_end - m_WorkSpace_Y_start);
	wxCoord x1 = 0;
	wxCoord y1 = 0;
	if (x0 < 0) x0 = 0;
	if (y0 < 0) y0 = 0;
	if (w0 < 0) w0 = 0;
	if (h0 < 0) h0 = 0;
	dc.SetClippingRegion(x0, y0, w0, h0);

	dc.SetBrush(wxBrush(m_brush_AxisColour , wxSOLID));
	dc.SetPen(wxPen(pen_AxisColour , 1 , wxSOLID));

	wxCoord w_temp, h_temp, des_temp, lead_temp;
	wxCoord w_temp2;

	dc.GetTextExtent(wxString::Format(wxT(" %s"), format_number(max_val_y_all, 2)), &w_temp, &h_temp, &des_temp, &lead_temp);
	dc.GetTextExtent(wxString::Format(wxT(" %s"), format_number(min_val_y_all, 2)), &w_temp2, &h_temp, &des_temp, &lead_temp);

	if (w_temp < w_temp2) w_temp = w_temp2;

	m_WorkSpace_X_start += double(w_temp) + 3.0;
	m_WorkSpace_Y_end -= double(h_temp) + 3.0;

	dc.GetTextExtent(wxT("0"), &w_temp, &h_temp, &des_temp, &lead_temp);

	m_WorkSpace_X_end -= 3.0;//w_temp;
	const double radius1 = 5.0;//(double)(h_temp/2.0);
	double d_y = (double)(h_temp) / 2.0;
	if (d_y < 5.0) d_y = 5.0;

	wxDateTime dtTemp1;
	wxString strBuffer1;
	dtTemp1.Set((time_t)max_val_x);
	strBuffer1 = dtTemp1.Format(wxT("%d/%m/%Y"), wxDateTime::GMT0);
	dc.GetTextExtent(strBuffer1, &w_temp, &h_temp, &des_temp, &lead_temp);

	double d_x = (double)(w_temp) / 2.0;

// Draw background graph
	x0 = wxCoord(m_WorkSpace_X_start);
	y0 = wxCoord(m_WorkSpace_Y_start);
	w0 = wxCoord(m_WorkSpace_X_end - m_WorkSpace_X_start);
	h0 = wxCoord(m_WorkSpace_Y_end - m_WorkSpace_Y_start);
	if (x0 < 0) x0 = 0;
	if (y0 < 0) y0 = 0;
	if (w0 < 0) w0 = 0;
	if (h0 < 0) h0 = 0;
	dc.DrawRoundedRectangle(x0, y0, w0, h0, radius1);

	m_Graph_X_start = m_WorkSpace_X_start;	//x0;
	m_Graph_X_end = m_WorkSpace_X_end;		//x0 + w0;
	m_Graph_Y_start = m_WorkSpace_Y_start;	//y0;
	m_Graph_Y_end = m_WorkSpace_Y_end;		//y0 + h0;

	m_WorkSpace_X_start += d_x;
	m_WorkSpace_X_end -= d_x;
	m_WorkSpace_Y_start += d_y;
	m_WorkSpace_Y_end -= d_y;

	if (m_WorkSpace_X_end < m_WorkSpace_X_start) m_WorkSpace_X_start = m_WorkSpace_X_end = (m_WorkSpace_X_end + m_WorkSpace_X_start) / 2.0;
	if (m_WorkSpace_Y_end < m_WorkSpace_Y_start) m_WorkSpace_Y_start = m_WorkSpace_Y_end = (m_WorkSpace_Y_end + m_WorkSpace_Y_start) / 2.0;

	m_Graph_draw_X_start = m_WorkSpace_X_start;
	m_Graph_draw_X_end = m_WorkSpace_X_end;
	m_Graph_draw_Y_start = m_WorkSpace_Y_start;
	m_Graph_draw_Y_end = m_WorkSpace_Y_end;
// A B
	AB(m_WorkSpace_X_start, m_WorkSpace_Y_end, m_WorkSpace_X_end, m_WorkSpace_Y_start,
		min_val_x, min_val_y, max_val_x, max_val_y);
//Draw val and lines
	dc.SetPen(wxPen(m_pen_AxisYColour , 1 , wxDOT));
	dc.SetTextForeground (m_pen_AxisYTextColour);

	int d_oy_count = 1;
	if (h_temp > 0)	d_oy_count = (int)ceil((m_WorkSpace_Y_end - m_WorkSpace_Y_start) / ( 2.0 * double(h_temp)));
	if (d_oy_count <= 0) d_oy_count = 1;
	double d_oy_val = fabs((max_val_y - min_val_y) / double(d_oy_count));
	double d2 = pow(double(10.0) , floor(log10(d_oy_val)));

	if (d2 >= d_oy_val){
		d_oy_val = 1.0 * d2;
	} else	if (2.0 * d2 >= d_oy_val){
			d_oy_val = 2.0 * d2;
		} else	if (5.0 * d2 >= d_oy_val){
				d_oy_val = 5.0 * d2;
			} else {
				d_oy_val = 10.0 * d2;
			}
	if (0 == d_oy_val) d_oy_val = 0.01;
	double y_start_val = ceil(min_val_y / d_oy_val) * d_oy_val;
	d_oy_count = (int)floor((max_val_y - y_start_val) / d_oy_val);

	for (double ny = 0; ny <= double(d_oy_count); ++ny){
		dc.GetTextExtent(wxString::Format(wxT("%s"), format_number(y_start_val + ny * d_oy_val, 2)), &w_temp, &h_temp, &des_temp, &lead_temp);
		x0 = wxCoord(m_Graph_X_start + 1.0);
		y0 = wxCoord(m_Ay_ValToCoord * (y_start_val + ny * d_oy_val) + m_By_ValToCoord);
		x1 = wxCoord(m_Graph_X_end - 1.0);
		if ((y0 >= wxCoord(m_WorkSpace_Y_start)) && (y0 <= wxCoord(m_WorkSpace_Y_end))){
			if (x0 < 0) x0 = 0;
			if (y0 < 0) y0 = 0;
			if (x1 < 0) x1 = 0;
			dc.DrawLine(x0, y0, x1, y0);
			x0 = wxCoord(m_Graph_X_start - 2.0) - w_temp;
			y0 = wxCoord(m_Ay_ValToCoord * (y_start_val + ny * d_oy_val) + m_By_ValToCoord - double(h_temp) / 2.0);
			if (x0 < 0) x0 = 0;
			if (y0 < 0) y0 = 0;
			dc.DrawText(wxString::Format(wxT("%s"), format_number(y_start_val + ny * d_oy_val, 2)), x0, y0);
		}
	}

//Draw day numbers and lines marking the days
	dc.SetPen(wxPen(m_pen_AxisXColour , 1 , wxDOT));
	dc.SetTextForeground (m_pen_AxisXTextColour);

	dtTemp1.Set((time_t)max_val_x);
	strBuffer1 = dtTemp1.Format(wxT("%d/%m/%Y"), wxDateTime::GMT0);
	dc.GetTextExtent(strBuffer1, &w_temp, &h_temp, &des_temp, &lead_temp);

	int d_ox_count = 1;
	if (w_temp > 0)	d_ox_count = (int)((m_WorkSpace_X_end - m_WorkSpace_X_start) / (1.2 * double(w_temp)));
	if (d_ox_count <= 0) d_ox_count = 1;

	double d_ox_val = ceil(((double)(max_val_x - min_val_x) / double(d_ox_count)) / 86400.0) * 86400.0;
	if (0 == d_ox_val) d_ox_val = 1;

	double x_start_val = ceil(min_val_x / 86400.0) * 86400.0;
	d_ox_count = (int)floor((max_val_x - x_start_val) / d_ox_val);

	for (double nx = 0; nx <= double(d_ox_count); ++nx){
		dtTemp1.Set((time_t)(x_start_val + nx * d_ox_val));
		strBuffer1 = dtTemp1.Format(wxT("%d/%m/%Y"), wxDateTime::GMT0);
		dc.GetTextExtent(strBuffer1, &w_temp, &h_temp, &des_temp, &lead_temp);
		x0 = wxCoord(m_Ax_ValToCoord * (x_start_val + nx * d_ox_val) + m_Bx_ValToCoord);
		y0 = wxCoord(m_Graph_Y_start + 1.0);
		y1 = wxCoord(m_Graph_Y_end - 1.0);
		if ((x0 <= wxCoord(m_WorkSpace_X_end)) && (x0 >= wxCoord(m_WorkSpace_X_start))){
			if (x0 < 0) x0 = 0;
			if (y0 < 0) y0 = 0;
			if (y1 < 0) y1 = 0;
		    dc.DrawLine(x0, y0, x0, y1);
			x0 = wxCoord(m_Ax_ValToCoord * (x_start_val + nx * d_ox_val) + m_Bx_ValToCoord - (double(w_temp) / 2.0));
			y0 = (wxCoord)m_Graph_Y_end;
			if (x0 < 0) x0 = 0;
			if (y0 < 0) y0 = 0;
			dc.DrawText(strBuffer1, x0, y0);
		}
	}
	dc.DestroyClippingRegion();
}

void CPaintStatistics::DrawGraph(wxDC &dc, DAILY_STATS *stats, const wxColour graphColour, const int typePoint) {
	wxCoord x0 = wxCoord(m_Graph_X_start);
	wxCoord y0 = wxCoord(m_Graph_Y_start);
	wxCoord w0 = wxCoord(m_Graph_X_end - m_Graph_X_start);
	wxCoord h0 = wxCoord(m_Graph_Y_end - m_Graph_Y_start);
	if (x0 < 0) x0 = 0;
	if (y0 < 0) y0 = 0;
	if (w0 < 0) w0 = 0;
	if (h0 < 0) h0 = 0;
	dc.SetClippingRegion(x0, y0, w0, h0);

	dc.SetPen(wxPen(graphColour , m_GraphLineWidth , wxSOLID));

	wxCoord last_x = 0;
	wxCoord last_y = 0;
	wxCoord xpos = 0;
	wxCoord ypos = 0;

	double d_last_x = 0;
	double d_last_y = 0;
	bool last_point_in = false;

	bool point_in = false;

// cross
	double d_cross_x1 = 0;
	double d_cross_y1 = 0;
// first point (no line)
	bool first_point = true;
// end point
	double d_end_point_x = 0;
	double d_end_point_y = 0;
	bool end_point = false;

	int last_day = -1;
	int nstats_good;

	if(trend_value > 0)	{
		//for (int j = 0; j < nstats; j++) if(stats[j].total_credit > 0) last_day = j;
		last_day = nstats - 1;
		nstats_good = last_day + 1;
	}
	else nstats_good = nstats;

//
	//for (std::vector<DAILY_STATS>::const_iterator j = stats.begin(); j != stats.end(); ++j) {
	for (int j = 0; j < nstats_good; j++) {
		double d_xpos = 0;
		double d_ypos = 0;

		bool b_point2 = false;

		double d_x2 = 0;
		double d_y2 = 0;

		//if(stats[j].user_total_credit > 0) last_day = j;

		d_xpos = (m_Ax_ValToCoord * stats[j].day + m_Bx_ValToCoord);// äîáàâèòü îêðóãëåíèå
		d_ypos = (m_Ay_ValToCoord * stats[j].total_credit + m_By_ValToCoord);

		if (first_point) {
			if ((d_xpos < m_Graph_X_start) || (d_xpos > m_Graph_X_end) ||
				(d_ypos < m_Graph_Y_start) || (d_ypos > m_Graph_Y_end)){
				point_in = false;
				b_point2 = false;
				end_point = false;
			}else {
				point_in = true;
				d_x2 = d_xpos;
				d_y2 = d_ypos;
				b_point2 = true;
				d_end_point_x = d_xpos;
				d_end_point_y = d_ypos;
				end_point = true;
			}
			first_point = false;
		}else {
			bool b_point1 = false;

			double d_x1 = 0;
			double d_y1 = 0;

			double d_min1 = 0;
			double d_max1 = 0;
			double d_min2 = 0;
			double d_max2 = 0;

			dc.SetPen(wxPen(graphColour , m_GraphLineWidth , wxSOLID));
			// ïðîâåðêà ïîïàäàíèÿ ïåðâîé òî÷êè ëèíèè â îáëàñòü ðèñîâàíèÿ
			if (last_point_in){
				d_x1 = d_last_x;
				d_y1 = d_last_y;
				b_point1 = true;
			}else b_point1 = false;
			// ïðîâåðêà ïîïàäàíèÿ âòîðîé òî÷êè ëèíèè â îáëàñòü ðèñîâàíèÿ
			if ((d_xpos < m_Graph_X_start) || (d_xpos > m_Graph_X_end) ||
				(d_ypos < m_Graph_Y_start) || (d_ypos > m_Graph_Y_end)){
				point_in = false;
				b_point2 = false;
			}else {
				point_in = true;
				d_x2 = d_xpos;
				d_y2 = d_ypos;
				b_point2 = true;
			}
			// Èùåì òî÷êó âõîäà ëèíèè â îáëàñòü ðèñîâàíèÿ (1) x=const
			if (!b_point1 || !b_point2){
				if (CrossTwoLine(d_last_x, d_last_y, d_xpos, d_ypos,
								m_Graph_X_start, m_Graph_Y_end, m_Graph_X_start, m_Graph_Y_start,
								d_cross_x1, d_cross_y1)){
					if (d_last_x > d_xpos){
						d_min1 = d_xpos;
						d_max1 = d_last_x;
					}else{
						d_max1 = d_xpos;
						d_min1 = d_last_x;
					}
					if (m_Graph_Y_end > m_Graph_Y_start){
						d_min2 = m_Graph_Y_start;
						d_max2 = m_Graph_Y_end;
					}else{
						d_max2 = m_Graph_Y_end;
						d_min2 = m_Graph_Y_start;
					}
					if ((d_cross_x1 <= d_max1) && (d_cross_x1 >= d_min1) &&
						(d_cross_y1 <= d_max2) && (d_cross_y1 >= d_min2)){
						if (!b_point1){
							d_x1 = d_cross_x1;
							d_y1 = d_cross_y1;
							b_point1 = true;
						} else if (!b_point2){
							d_x2 = d_cross_x1;
							d_y2 = d_cross_y1;
							b_point2 = true;
						}
					}
				}
			}
			// Èùåì òî÷êó âõîäà ëèíèè â îáëàñòü ðèñîâàíèÿ (2) x=const
			if (!b_point1 || !b_point2){
				if (CrossTwoLine(d_last_x, d_last_y, d_xpos, d_ypos,
								m_Graph_X_end, m_Graph_Y_end, m_Graph_X_end, m_Graph_Y_start,
								d_cross_x1, d_cross_y1)){
					if (d_last_x > d_xpos){
						d_min1 = d_xpos;
						d_max1 = d_last_x;
					}else{
						d_max1 = d_xpos;
						d_min1 = d_last_x;
					}
					if (m_Graph_Y_end > m_Graph_Y_start){
						d_min2 = m_Graph_Y_start;
						d_max2 = m_Graph_Y_end;
					}else{
						d_max2 = m_Graph_Y_end;
						d_min2 = m_Graph_Y_start;
					}
					if ((d_cross_x1 <= d_max1) && (d_cross_x1 >= d_min1) &&
						(d_cross_y1 <= d_max2) && (d_cross_y1 >= d_min2)){
						if (!b_point1){
							d_x1 = d_cross_x1;
							d_y1 = d_cross_y1;
							b_point1 = true;
						} else if (!b_point2){
							d_x2 = d_cross_x1;
							d_y2 = d_cross_y1;
							b_point2 = true;
						}
					}
				}
			}
			// Èùåì òî÷êó âõîäà ëèíèè â îáëàñòü ðèñîâàíèÿ (3) y=const
			if (!b_point1 || !b_point2){
				if (CrossTwoLine(d_last_x, d_last_y, d_xpos, d_ypos,
								m_Graph_X_start, m_Graph_Y_start, m_Graph_X_end, m_Graph_Y_start,
								d_cross_x1, d_cross_y1)){
					if (d_last_y > d_ypos){
						d_min1 = d_ypos;
						d_max1 = d_last_y;
					}else{
						d_max1 = d_ypos;
						d_min1 = d_last_y;
					}
					if (m_Graph_X_end > m_Graph_X_start){
						d_min2 = m_Graph_X_start;
						d_max2 = m_Graph_X_end;
					}else{
						d_max2 = m_Graph_X_end;
						d_min2 = m_Graph_X_start;
					}
					if ((d_cross_y1 <= d_max1) && (d_cross_y1 >= d_min1) &&
						(d_cross_x1 <= d_max2) && (d_cross_x1 >= d_min2)){
						if (!b_point1){
							d_x1 = d_cross_x1;
							d_y1 = d_cross_y1;
							b_point1 = true;
						} else if (!b_point2){
							d_x2 = d_cross_x1;
							d_y2 = d_cross_y1;
							b_point2 = true;
						}
					}
				}
			}
			// Èùåì òî÷êó âõîäà ëèíèè â îáëàñòü ðèñîâàíèÿ (4) y=const
			if (!b_point1 || !b_point2){
				if (CrossTwoLine(d_last_x, d_last_y, d_xpos, d_ypos,
								m_Graph_X_start, m_Graph_Y_end, m_Graph_X_end, m_Graph_Y_end,
								d_cross_x1, d_cross_y1)){
					if (d_last_y > d_ypos){
						d_min1 = d_ypos;
						d_max1 = d_last_y;
					}else{
						d_max1 = d_ypos;
						d_min1 = d_last_y;
					}
					if (m_Graph_X_end > m_Graph_X_start){
						d_min2 = m_Graph_X_start;
						d_max2 = m_Graph_X_end;
					}else{
						d_max2 = m_Graph_X_end;
						d_min2 = m_Graph_X_start;
					}
					if ((d_cross_y1 <= d_max1) && (d_cross_y1 >= d_min1) &&
						(d_cross_x1 <= d_max2) && (d_cross_x1 >= d_min2)){
						if (!b_point1){
							d_x1 = d_cross_x1;
							d_y1 = d_cross_y1;
							b_point1 = true;
						} else if (!b_point2){
							d_x2 = d_cross_x1;
							d_y2 = d_cross_y1;
							b_point2 = true;
						}
					}
				}
			}
			if (b_point1 && b_point2){
				last_x = wxCoord(d_x1);
				last_y = wxCoord(d_y1);
				xpos = wxCoord(d_x2);
				ypos = wxCoord(d_y2);
				if (last_x > (wxCoord)m_Graph_X_end) last_x = (wxCoord)m_Graph_X_end;
				if (last_x < 0) last_x = 0;
				if (last_y > (wxCoord)m_Graph_Y_end) last_y = (wxCoord)m_Graph_Y_end;
				if (last_y < 0) last_y = 0;
				if (xpos > (wxCoord)m_Graph_X_end) xpos = (wxCoord)m_Graph_X_end;
				if (xpos < 0) xpos = 0;
				if (ypos > (wxCoord)m_Graph_Y_end) ypos = (wxCoord)m_Graph_Y_end;
				if (ypos < 0) ypos = 0;

				dc.DrawLine(last_x, last_y, xpos, ypos);
				if (last_point_in) myDrawPoint(dc, last_x, last_y, graphColour, typePoint ,m_GraphPointWidth);
				if (point_in){
					d_end_point_x = d_xpos;
					d_end_point_y = d_ypos;
					end_point = true;
				}else end_point = false;
			}else end_point = false;
		}
		d_last_x = d_xpos;
		d_last_y = d_ypos;
		last_point_in = point_in;
	}
	// draw last point
	if (end_point){
		xpos = wxCoord(d_end_point_x);
		ypos = wxCoord(d_end_point_y);
		if (xpos > (wxCoord)m_Graph_X_end) xpos = (wxCoord)m_Graph_X_end;
		if (xpos < 0) xpos = 0;
		if (ypos > (wxCoord)m_Graph_Y_end) ypos = (wxCoord)m_Graph_Y_end;
		if (ypos < 0) ypos = 0;
		myDrawPoint(dc, xpos, ypos, graphColour, typePoint ,m_GraphPointWidth);
	}

	if((last_day >= 0) && (trend_value > 0)){

		last_x = (m_Ax_ValToCoord * stats[last_day].day + m_Bx_ValToCoord);// äîáàâèòü îêðóãëåíèå
		last_y = (m_Ay_ValToCoord * stats[last_day].total_credit + m_By_ValToCoord);
		xpos = (m_Ax_ValToCoord * /*stats[nstats - 1].day*/trend_date.GetTicks() + m_Bx_ValToCoord);
		ypos = (m_Ay_ValToCoord * trend_value + m_By_ValToCoord);

		wxColour color;
		if(stats[last_day].total_credit == trend_value) color = *wxBLUE;
		else if (stats[last_day].total_credit < trend_value) color = *wxGREEN;
		else color = *wxRED;

		dc.SetPen(wxPen(color , m_GraphLineWidth , wxSOLID));
		dc.DrawLine(last_x, last_y, xpos, ypos);
		myDrawPoint(dc, xpos, ypos, color, typePoint ,m_GraphPointWidth);
	}

	dc.DestroyClippingRegion();
}
//----Draw marker----
void CPaintStatistics::DrawMarker(wxDC &dc) {
	if (m_GraphMarker1){
		wxCoord x0 = wxCoord(m_Graph_X_start);
		wxCoord y0 = wxCoord(m_Graph_Y_start);
		wxCoord w0 = wxCoord(m_Graph_X_end - m_Graph_X_start);
		wxCoord h0 = wxCoord(m_Graph_Y_end - m_Graph_Y_start);
		if (x0 < 0) x0 = 0;
		if (y0 < 0) y0 = 0;
		if (w0 < 0) w0 = 0;
		if (h0 < 0) h0 = 0;
		dc.SetClippingRegion(x0, y0, w0, h0);

		dc.SetPen(wxPen(m_pen_MarkerLineColour , 1 , wxSOLID));
		wxCoord x00 = wxCoord(m_Ax_ValToCoord * m_GraphMarker_X1 + m_Bx_ValToCoord);
		wxCoord y00 = wxCoord(m_Ay_ValToCoord * m_GraphMarker_Y1 + m_By_ValToCoord);
		if (x00 < 0) x00 = 0;
		if (y00 < 0) y00 = 0;
		if ((x00 < wxCoord(m_Graph_X_start)) || (x00 > wxCoord(m_Graph_X_end)) ||
			(y00 < wxCoord(m_Graph_Y_start)) || (y00 > wxCoord(m_Graph_Y_end))){
		}else{
			dc.CrossHair(x00, y00);
			wxDateTime dtTemp1;
			wxString strBuffer1;
			dtTemp1.Set((time_t)m_GraphMarker_X1);
			strBuffer1=dtTemp1.Format(wxT("%d/%m/%Y"), wxDateTime::GMT0);

			dc.SetFont(m_font_bold);
			dc.SetTextBackground (m_brush_AxisColour);
			dc.SetBackgroundMode(wxSOLID);
			x0 += 2;
			y0 += 2;
			x00 += 2;
			y00 += 2;
			if (x00 < 0) x00 = 0;
			if (y00 < 0) y00 = 0;
			if (x0 < 0) x0 = 0;
			if (y0 < 0) y0 = 0;

			dc.SetTextForeground (m_pen_AxisYTextColour);
			dc.DrawText(wxString::Format(wxT("%s"), format_number(m_GraphMarker_Y1, 2)) , x0, y00);
			dc.SetTextForeground (m_pen_AxisXTextColour);
			dc.DrawText(strBuffer1 ,x00, y0);
			dc.SetBackgroundMode(wxTRANSPARENT);
		}
		dc.DestroyClippingRegion();
	}
}

//-------- Draw All ---------
void CPaintStatistics::DrawAll(wxDC &dc) {
	if(trend_value > -1){
		wxDateTime::Month mon = trend_date.GetMonth();
		int year = trend_date.GetYear();
		int nof_days = wxDateTime::GetNumberOfDays(mon, year);
		trend_date.SetDay(nof_days);
	}
	else trend_date = wxInvalidDateTime;

//Init global
	m_WorkSpace_X_start = m_main_X_start;
	m_WorkSpace_X_end = m_main_X_end;
	m_WorkSpace_Y_start = m_main_Y_start;
	m_WorkSpace_Y_end = m_main_Y_end;

	dc.SetBackground(m_brush_MainColour);

	dc.SetTextForeground (GetForegroundColour ());
	dc.SetTextForeground (m_pen_HeadTextColour);
	dc.SetTextBackground (GetBackgroundColour ());

	m_font_standart.SetWeight(wxNORMAL);
	m_font_bold.SetWeight(wxBOLD);

	dc.SetFont(m_font_standart);
//Start drawing
	dc.Clear();
	dc.SetBrush(wxBrush(m_brush_MainColour , wxSOLID));
	dc.SetPen(wxPen(m_pen_MainColour , 1 , wxSOLID));

	wxCoord x0 = wxCoord(m_main_X_start);
	wxCoord y0 = wxCoord(m_main_Y_start);
	wxCoord w0 = wxCoord(m_main_X_end - m_main_X_start);
	wxCoord h0 = wxCoord(m_main_Y_end - m_main_Y_start);
	if (x0 < 0) x0 = 0;
	if (y0 < 0) y0 = 0;
	if (w0 < 0) w0 = 0;
	if (h0 < 0) h0 = 0;
	dc.SetBrush(wxBrush(m_brush_MainColour , wxSOLID));
	dc.SetPen(wxPen(m_pen_MainColour , 1 , wxSOLID));
	dc.DrawRectangle(x0, y0, w0, h0);

// Initial coord
	heading = _("Trend Chart");

		dc.SetFont(m_font_bold);
		DrawMainHead(dc, heading);
		dc.SetFont(m_font_standart);

		//Find minimum/maximum value
			double min_val_y = 10e32, max_val_y = 0;
			double min_val_x = 10e32, max_val_x = 0;

			MinMaxDayCredit(min_val_y, max_val_y, min_val_x, max_val_x, true, stats, nstats, trend_value, trend_date);

			wxColour pen_AxisColour1 = m_pen_AxisColourAutoZoom;

			if (m_Zoom_Auto){
				min_val_x = floor(min_val_x / 86400.0) * 86400.0;
				max_val_x = ceil(max_val_x / 86400.0) * 86400.0;
			}else{
				pen_AxisColour1 = m_pen_AxisColourZoom;
				min_val_x = m_Zoom_min_val_X;
				max_val_x = m_Zoom_max_val_X;
				min_val_y = m_Zoom_min_val_Y;
				max_val_y = m_Zoom_max_val_Y;
			}
			CheckMinMaxD(min_val_x, max_val_x);
			CheckMinMaxD(min_val_y, max_val_y);

			//}
			m_Zoom_min_val_X = min_val_x;
			m_Zoom_max_val_X = max_val_x;
			m_Zoom_min_val_Y = min_val_y;
			m_Zoom_max_val_Y = max_val_y;
		    // Draw axis
			DrawAxis(dc, max_val_y, min_val_y, max_val_x, min_val_x, pen_AxisColour1, max_val_y, min_val_y);
		    // Draw graph
			wxColour graphColour=wxColour(0,0,0);
			//color_cycle(m_SelectedStatistic, n_command_buttons, graphColour);
			DrawGraph(dc, stats, graphColour, 0);
		    // Draw marker
			DrawMarker(dc);
			//break;
}

//=================================================================
void CPaintStatistics::OnPaint(wxPaintEvent& WXUNUSED(event)) {
#if USE_MEMORYDC
	wxPaintDC pdc(this);
	wxMemoryDC mdc;
#else
    wxPaintDC mdc(this);
    m_full_repaint=true;
#endif
	wxCoord width = 0, height = 0;
	GetClientSize(&width, &height);
	if (m_full_repaint){
		if (!m_GraphZoomStart){
			ClearXY();
			//ClearLegendXY();

			m_main_X_start = 0.0;
			if (width > 0) m_main_X_end = double(width); else m_main_X_end = 0.0;
			m_main_Y_start = 0.0;
			if (height > 0) m_main_Y_end = double(height); else m_main_Y_end = 0.0;

			if (width < 1) width = 1;
			if (height < 1) height = 1;
#if USE_MEMORYDC
			m_dc_bmp.Create(width, height);
			mdc.SelectObject(m_dc_bmp);
#endif
			DrawAll(mdc);
			m_bmp_OK = true;
			m_full_repaint = false;
		}else if(m_bmp_OK){
#if USE_MEMORYDC
			mdc.SelectObject(m_dc_bmp);
#endif
		}
	}else{
		if (m_bmp_OK){
#if USE_MEMORYDC
			mdc.SelectObject(m_dc_bmp);
#endif
			if (m_GraphZoomStart && (width == m_dc_bmp.GetWidth()) &&(height == m_dc_bmp.GetHeight())){

				mdc.SetPen(wxPen(m_pen_ZoomRectColour , 1 , wxSOLID));
				mdc.SetBrush(wxBrush(m_brush_ZoomRectColour , wxSOLID));
				mdc.SetLogicalFunction(wxXOR);

				wxCoord x0 = 0;
				wxCoord y0 = 0;
				wxCoord w0 = 0;
				wxCoord h0 = 0;

				if (m_GraphZoom_X1 < m_GraphZoom_X2_old) x0 = m_GraphZoom_X1;
				else x0 = m_GraphZoom_X2_old;
				if (m_GraphZoom_Y1 < m_GraphZoom_Y2_old) y0 = m_GraphZoom_Y1;
				else y0 = m_GraphZoom_Y2_old;
				w0 = m_GraphZoom_X2_old - m_GraphZoom_X1;
				h0 = m_GraphZoom_Y2_old - m_GraphZoom_Y1;
				if (x0 < 0) x0 = 0;
				if (y0 < 0) y0 = 0;
				if (w0 < 0) w0 = -w0;
				if (h0 < 0) h0 = -h0;
				mdc.DrawRectangle(x0, y0, w0, h0);

				if (m_GraphZoom_X1 < m_GraphZoom_X2) x0 = m_GraphZoom_X1;
				else x0 = m_GraphZoom_X2;
				if (m_GraphZoom_Y1 < m_GraphZoom_Y2) y0 = m_GraphZoom_Y1;
				else y0 = m_GraphZoom_Y2;
				w0 = m_GraphZoom_X2 - m_GraphZoom_X1;
				h0 = m_GraphZoom_Y2 - m_GraphZoom_Y1;
				if (x0 < 0) x0 = 0;
				if (y0 < 0) y0 = 0;
				if (w0 < 0) w0 = -w0;
				if (h0 < 0) h0 = -h0;
				mdc.DrawRectangle(x0, y0, w0, h0);

				m_GraphZoom_X2_old = m_GraphZoom_X2;
				m_GraphZoom_Y2_old = m_GraphZoom_Y2;

				mdc.SetLogicalFunction(wxCOPY);
			}
		}
	}
#if USE_MEMORYDC
	if (m_bmp_OK && (width == m_dc_bmp.GetWidth()) &&(height == m_dc_bmp.GetHeight())){
		pdc.Blit(0, 0, width, height,& mdc, 0, 0);
	}
	mdc.SelectObject(wxNullBitmap);
#endif
}

void CPaintStatistics::OnLeftMouseDown(wxMouseEvent& event) {

// Graph
	wxClientDC dc (this);
	wxPoint pt(event.GetLogicalPosition(dc));
	if((double(pt.y) > m_Graph_Y_start) && (double(pt.y) < m_Graph_Y_end) && (double(pt.x) > m_Graph_X_start) && (double(pt.x) < m_Graph_X_end)){
		m_GraphMarker_X1 = m_Ax_CoordToVal * double(pt.x) + m_Bx_CoordToVal;
		m_GraphMarker_Y1 = m_Ay_CoordToVal * double(pt.y) + m_By_CoordToVal;
		m_GraphMarker1 = true;

		m_GraphZoom_X1 = wxCoord(pt.x);
		m_GraphZoom_Y1 = wxCoord(pt.y);
		m_GraphZoom_X2 = wxCoord(pt.x);
		m_GraphZoom_Y2 = wxCoord(pt.y);
		m_GraphZoom_X2_old = wxCoord(pt.x);
		m_GraphZoom_Y2_old = wxCoord(pt.y);

		m_GraphZoomStart = true;
	}
	event.Skip();
}

void CPaintStatistics::OnMouseMotion(wxMouseEvent& event) {
	if (m_GraphZoomStart){
		if (event.LeftIsDown()){
			wxClientDC cdc (this);
			wxPoint pt(event.GetLogicalPosition(cdc));
			if((double(pt.y) > m_Graph_Y_start) && (double(pt.y) < m_Graph_Y_end) && (double(pt.x) > m_Graph_X_start) && (double(pt.x) < m_Graph_X_end)){

			m_GraphZoom_X2 = wxCoord(pt.x);
			m_GraphZoom_Y2 = wxCoord(pt.y);

			m_full_repaint = false;
			Refresh(false);
			}
		}else{
			m_GraphZoomStart = false;

			m_full_repaint = true;
			Refresh(false);
		}
	}else if (m_GraphMoveStart){
		if (event.RightIsDown()){
			wxClientDC cdc (this);
			wxPoint pt(event.GetLogicalPosition(cdc));
			if((double(pt.y) > m_Graph_Y_start) && (double(pt.y) < m_Graph_Y_end) && (double(pt.x) > m_Graph_X_start) && (double(pt.x) < m_Graph_X_end)){

				m_GraphMove_X2 = wxCoord(pt.x);
				m_GraphMove_Y2 = wxCoord(pt.y);

				double X1 = m_Ax_CoordToVal * double(m_GraphMove_X1 - m_GraphMove_X2);
				double Y1 = m_Ay_CoordToVal * double(m_GraphMove_Y1 - m_GraphMove_Y2);

				if ( (X1 != 0) || (Y1 != 0)){
					m_GraphMove_X1 = m_GraphMove_X2;
					m_GraphMove_Y1 = m_GraphMove_Y2;

					m_Zoom_min_val_X = m_Zoom_min_val_X + X1;
					m_Zoom_max_val_X = m_Zoom_max_val_X + X1;
					m_Zoom_min_val_Y = m_Zoom_min_val_Y + Y1;
					m_Zoom_max_val_Y = m_Zoom_max_val_Y + Y1;

					m_GraphMoveGo = true;
					m_Zoom_Auto = false;
					m_full_repaint = true;
					Refresh(false);
				}

			}
		}else{
			m_GraphMoveStart = false;
			m_GraphMoveGo = false;

			m_full_repaint = true;
			Refresh(false);
		}
	}
	event.Skip();
}
void CPaintStatistics::OnLeftMouseUp(wxMouseEvent& event) {
	if (m_GraphZoomStart){
		if ((abs(int(m_GraphZoom_X1 - m_GraphZoom_X2)) > 2) && (abs(int(m_GraphZoom_Y1 - m_GraphZoom_Y2)) > 2)){
			double X1 = m_Ax_CoordToVal * double(m_GraphZoom_X1) + m_Bx_CoordToVal;
			double Y1 = m_Ay_CoordToVal * double(m_GraphZoom_Y1) + m_By_CoordToVal;
			double X2 = m_Ax_CoordToVal * double(m_GraphZoom_X2) + m_Bx_CoordToVal;
			double Y2 = m_Ay_CoordToVal * double(m_GraphZoom_Y2) + m_By_CoordToVal;

			if (X1 > X2){
				m_Zoom_max_val_X = X1;
				m_Zoom_min_val_X = X2;
			}else{
				m_Zoom_min_val_X = X1;
				m_Zoom_max_val_X = X2;
			}
			if (Y1 > Y2){
				m_Zoom_max_val_Y = Y1;
				m_Zoom_min_val_Y = Y2;
			}else{
				m_Zoom_min_val_Y = Y1;
				m_Zoom_max_val_Y = Y2;
			}
			m_GraphMarker1 = false;
			m_Zoom_Auto = false;
		}
		m_GraphZoomStart = false;
		m_full_repaint = true;
		Refresh(false);
	}
	event.Skip();
}
void CPaintStatistics::OnRightMouseDown(wxMouseEvent& event) {
	if (m_GraphZoomStart){       //???
		m_GraphZoomStart = false;
		m_GraphMarker1 = false;
		m_full_repaint = true;
		Refresh(false);
	}else{
		wxClientDC dc (this);
		wxPoint pt(event.GetLogicalPosition(dc));
		if((double(pt.y) > m_Graph_Y_start) && (double(pt.y) < m_Graph_Y_end) && (double(pt.x) > m_Graph_X_start) && (double(pt.x) < m_Graph_X_end)){
			m_GraphMove_X1 = wxCoord(pt.x);
			m_GraphMove_Y1 = wxCoord(pt.y);

			m_GraphMoveStart = true;
			m_GraphMoveGo = false;
		}
	}
	event.Skip();
}
void CPaintStatistics::OnRightMouseUp(wxMouseEvent& event) {
	if (m_GraphMoveGo){
		m_GraphMoveStart = false;
		m_GraphMoveGo = false;
	}else if (m_GraphMarker1){
		m_GraphMarker1 = false;
		m_full_repaint = true;
		Refresh(false);
	}else if (!m_Zoom_Auto){
		m_Zoom_Auto = true;
		m_full_repaint = true;
		Refresh(false);
	}
	event.Skip();
}
void CPaintStatistics::OnMouseLeaveWindows(wxMouseEvent& event) {
	if (m_GraphZoomStart){
		m_GraphMarker1 = false;
		m_GraphZoomStart = false;
		m_full_repaint = true;
		Refresh(false);
	}
	if (m_GraphMoveStart || m_GraphMoveGo){
		m_GraphMoveStart = false;
		m_GraphMoveGo = false;
	}
	event.Skip();
}

void CPaintStatistics::OnSize(wxSizeEvent& event) {
	m_full_repaint = true;
	Refresh(false);
	event.Skip();
}


