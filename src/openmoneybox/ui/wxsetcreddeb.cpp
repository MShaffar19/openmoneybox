///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Jun 12 2020)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxsetcreddeb.h"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxTSCredDeb, wxDialog )
	EVT_BUTTON( wxID_OK, wxTSCredDeb::_wxFB_OKBtnClick )
END_EVENT_TABLE()

wxTSCredDeb::wxTSCredDeb( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	wxFlexGridSizer* fgSizer7;
	fgSizer7 = new wxFlexGridSizer( 6, 1, 0, 0 );
	fgSizer7->AddGrowableCol( 0 );
	fgSizer7->AddGrowableRow( 5 );
	fgSizer7->SetFlexibleDirection( wxBOTH );
	fgSizer7->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	LNam = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LNam->Wrap( -1 );
	LNam->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer7->Add( LNam, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Name->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer7->Add( Name, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	LVal = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LVal->Wrap( -1 );
	LVal->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer7->Add( LVal, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Val = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Val->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Val->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &validator_string ) );

	fgSizer7->Add( Val, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	wxFlexGridSizer* fgSizer8;
	fgSizer8 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer8->AddGrowableCol( 0 );
	fgSizer8->AddGrowableCol( 1 );
	fgSizer8->AddGrowableRow( 0 );
	fgSizer8->SetFlexibleDirection( wxBOTH );
	fgSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	SysTimeCheck = new wxCheckBox( this, wxID_ANY, _("Use system time"), wxDefaultPosition, wxDefaultSize, 0 );
	SysTimeCheck->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer8->Add( SysTimeCheck, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 10 );

	OldItemCheck = new wxCheckBox( this, wxID_ANY, _("Keep current budget"), wxDefaultPosition, wxDefaultSize, 0 );
	OldItemCheck->SetToolTip( _("Check this to store a credit or debt from the past") );

	fgSizer8->Add( OldItemCheck, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxEXPAND, 5 );


	fgSizer7->Add( fgSizer8, 1, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer9;
	fgSizer9 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer9->AddGrowableCol( 0 );
	fgSizer9->AddGrowableCol( 1 );
	fgSizer9->AddGrowableRow( 0 );
	fgSizer9->SetFlexibleDirection( wxBOTH );
	fgSizer9->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );

	OKBtn->SetDefault();
	OKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer9->Add( OKBtn, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	CancelBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer9->Add( CancelBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	fgSizer7->Add( fgSizer9, 1, wxEXPAND, 5 );


	this->SetSizer( fgSizer7 );
	this->Layout();
}

wxTSCredDeb::~wxTSCredDeb()
{
}
