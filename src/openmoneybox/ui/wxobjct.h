///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Jun 12 2020)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxTObjects
///////////////////////////////////////////////////////////////////////////////
class wxTObjects : public wxDialog
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_AlmCheckBoxClick( wxCommandEvent& event ){ AlmCheckBoxClick( event ); }
		void _wxFB_OKBtnClick( wxCommandEvent& event ){ OKBtnClick( event ); }


	protected:
		wxStaticText* LObj;
		wxStaticText* LPer;
		wxButton* OKBtn;
		wxButton* CancelBtn;

		// Virtual event handlers, overide them in your derived class
		virtual void AlmCheckBoxClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxFlexGridSizer* Sizer;
		wxComboBox* Obj;
		wxTextCtrl* Per;
		wxFlexGridSizer* Sizer2;
		wxCheckBox* AlmCB;
		wxDatePickerCtrl* Alarm;
		wxCheckBox* SysTimeCheck;

		wxTObjects( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 322,275 ), long style = wxDEFAULT_DIALOG_STYLE );
		~wxTObjects();

};

