/***************************************************************
 * Name:      gainexpense.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-18
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/fileconf.h> // For wxFileConfig
#include <wx/stdpaths.h>	// For wxStandardPaths::Get()

#include "../../types.h"
#include "wxgainexpense.h"
#include "gainexpense.h"

extern wxLanguage Lan;

TOperationF::TOperationF(wxWindow *parent):wxTOperationF(parent,-1,wxEmptyString,wxDefaultPosition,wxSize( 315, 460 ),wxDEFAULT_DIALOG_STYLE){
	CheckValues = true;
	Focus(1);}

void TOperationF::Focus(int F){
  switch(F){
    case 1:
		FundName->SetFocus();
    break;
    case 2:
    OpValue->SetFocus();
    break;
    case 3:
    Matter->SetFocus();
    break;

    case 4:
    Currency->SetFocus();
    break;
    case 5:
    Rate->SetFocus();
    break;

    default:
		return;}}

void TOperationF::OKBtnClick(wxCommandEvent& event){
	if(CheckValues){
		double cur;
		SetReturnCode(wxID_NONE);
		if(FundName->GetValue().IsEmpty()){
			Error(23,wxEmptyString);
			Focus(1);
			return;}
		if(OpValue->GetValue().IsEmpty()){
			Error(25,wxEmptyString);
			Focus(2);
			return;}
		wxString v=CheckValue(OpValue->GetValue());
		if(v.IsEmpty()){
			Error(26,wxEmptyString);
			Focus(2);
			return;}
		else OpValue->SetValue(v);
		OpValue->GetValue().ToDouble(&cur);
		if(cur<0){
			Error(27,wxEmptyString);
			Focus(2);
			return;}
		if(Matter->GetValue().IsEmpty()){
			Error(29,wxEmptyString);
			Focus(3);
			return;}

		if(! CurrencyCheckbox->IsChecked()){
			if(Currency->GetValue().IsEmpty()){
				Error(49, wxEmptyString);
				Focus(4);
				return;}
			if(Rate->GetValue().IsEmpty()){
				Error(25,wxEmptyString);
				Focus(5);
				return;}
			Rate->GetValue().ToDouble(&cur);
			if(cur < 0){
				Error(27,wxEmptyString);
				Focus(5);
				return;}
		}

		Matter->SetValue(iUpperCase(Matter->GetValue()));}
	EndModal(wxID_OK);}

void TOperationF::InitLabels(int kind){
	int w, h, ch;
	switch(kind){
		case 1:
			SetTitle(_("Store an expense"));
			LFund->SetLabel(_("Choose the fund to subtract the expense from:"));
			LValue->SetLabel(_("Insert the expense value:"));
			LMat->SetLabel(_("Insert the expense reason:"));
			LCat->SetLabel(_("Choose the expense category:"));
			break;
		case 2:
			SetTitle(_("Category"));

			for(int i = 0; i < 6; i++)fgSizer3->Remove(0);
			LFund->Show(false);
			FundName->Show(false);
			LValue->Show(false);
			OpValue->Show(false);
			LMat->Show(false);
			Matter->Show(false);
			SysTimeCheck->Show(false);
			LCat->SetLabel(_("Choose the category:"));

			ch = 50;
			LCat->GetSize(&w, &h);
			ch += h;
			Category->GetSize(&w, &h);
			ch += h;
			OKBtn->GetSize(&w, &h);
			ch += h;
			GetSize(&w, &h);
			SetSize(w, ch);

			CheckValues = false;
			break;
		default:
			SetTitle(_("Store a profit"));
			LFund->SetLabel(_("Choose the fund to add the profit to:"));
			LValue->SetLabel(_("Insert the profit value:"));
			LMat->SetLabel(_("Insert the gain reason:"));
			LCat->SetLabel(_("Choose the profit category:"));

			Currency->SetValue(GetCurrencySymbol());
	}
	if(Lan==wxLANGUAGE_ITALIAN){
	GetSize(&w,&h);
	w+=100;
	SetSize(wxDefaultCoord,wxDefaultCoord,w,h,wxSIZE_AUTO);}}

void TOperationF::FundNameKeyPress(wxKeyEvent& event){
  int k=event.GetKeyCode();
	if(k==9){
		Focus(2);
		return;}
	int Start=FundName->GetSelection(),i;
  #ifdef __WXMSW__
    wxString Ch=wxString::Format("%c",k);
  #else
    wxString Ch=event.GetUnicodeKey();
  #endif // __WXMSW__
	for(i=Start+1;i<(int)FundName->GetCount();i++)if(FundName->GetString(i).Mid(0,1)==Ch){
		FundName->SetSelection(i);
		return;}
	for(i=0;i<Start;i++)if(FundName->GetString(i).Mid(0,1)==Ch){
		FundName->SetSelection(i);
		return;}}

void TOperationF::CurrencyClick(wxCommandEvent& event){
	bool Show = ! CurrencyCheckbox->IsChecked();

	/*
	if(Show){

	}
	else{

	}
	*/

	Currency->Show(Show);
	RateLabel->Show(Show);
	Rate->Show(Show);
	Layout();
}
