/***************************************************************
 * Name:      wxgridimagerenderer.cpp
 * Purpose:   Code for custom cell rendered Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2017-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef GRIDIMAGERENDERER_CPP_INCLUDED
#define GRIDIMAGERENDERER_CPP_INCLUDED

#include "wxgridimagerenderer.h"

myImageGridCellRenderer::myImageGridCellRenderer(wxString path){
	imgpath = path;
}

void myImageGridCellRenderer::Draw(wxGrid& grid, wxGridCellAttr& attr, wxDC& dc, const wxRect& rect, int row, int col, bool isSelected)
{
	wxPNGHandler *handler = new wxPNGHandler;
	wxImage::AddHandler(handler);

	wxImage cellImage;

	if (cellImage.LoadFile(imgpath))
	{
		cellImage.Rescale(rect.height, rect.height);

		wxBitmap cellBitmap(cellImage);
		dc.DrawBitmap(cellBitmap, rect.x, rect.y);
	}

	//wxGridCellStringRenderer::Draw(grid, attr, dc, rect, row, col, isSelected);

}

#endif	// GRIDIMAGERENDERER_CPP_INCLUDED
