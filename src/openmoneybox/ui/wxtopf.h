///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxTOPF
///////////////////////////////////////////////////////////////////////////////
class wxTOPF : public wxDialog
{
	private:

	protected:
		wxStaticText* LNam;
		wxStaticText* LVal;

		// Virtual event handlers, override them in your derived class
		virtual void NameText( wxKeyEvent& event ) { event.Skip(); }
		virtual void NameChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxFlexGridSizer* mSizer;
		wxComboBox* Name;
		wxTextCtrl* Val;
		wxFlexGridSizer* btnSizer;
		wxButton* OKBtn;
		wxButton* CancelBtn;
		wxString validator_string;

		wxTOPF( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 320,190 ), long style = wxDEFAULT_DIALOG_STYLE );

		~wxTOPF();

};

