///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Jun 12 2020)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxmainframe_24.h"

#include "../../../rsrc/toolbar/16/oldconv.xpm"
#include "../../../rsrc/toolbar/20/condone20.xpm"
#include "../../../rsrc/toolbar/20/creditnew20.xpm"
#include "../../../rsrc/toolbar/20/creditremove20.xpm"
#include "../../../rsrc/toolbar/20/debtnew20.xpm"
#include "../../../rsrc/toolbar/20/debtremove20.xpm"
#include "../../../rsrc/toolbar/20/document_save20.xpm"
#include "../../../rsrc/toolbar/20/edit_copy.xpm"
#include "../../../rsrc/toolbar/20/edit_find.xpm"
#include "../../../rsrc/toolbar/20/edit_paste20.xpm"
#include "../../../rsrc/toolbar/20/greenplus20.xpm"
#include "../../../rsrc/toolbar/20/newfund20.xpm"
#include "../../../rsrc/toolbar/20/note_icon20.xpm"
#include "../../../rsrc/toolbar/20/objectborrow20.xpm"
#include "../../../rsrc/toolbar/20/objectgetback20.xpm"
#include "../../../rsrc/toolbar/20/objectgiveback20.xpm"
#include "../../../rsrc/toolbar/20/objectgiven20.xpm"
#include "../../../rsrc/toolbar/20/objectlend20.xpm"
#include "../../../rsrc/toolbar/20/objectreceived20.xpm"
#include "../../../rsrc/toolbar/20/redminus20.xpm"
#include "../../../rsrc/toolbar/20/removefund20.xpm"
#include "../../../rsrc/toolbar/20/xml_20.xpm"
#include "../../../rsrc/toolbar/24/condone24.xpm"
#include "../../../rsrc/toolbar/24/creditnew24.xpm"
#include "../../../rsrc/toolbar/24/creditremove24.xpm"
#include "../../../rsrc/toolbar/24/debtnew24.xpm"
#include "../../../rsrc/toolbar/24/debtremove24.xpm"
#include "../../../rsrc/toolbar/24/delete_bin_line.xpm"
#include "../../../rsrc/toolbar/24/document_save.xpm"
#include "../../../rsrc/toolbar/24/edit_paste.xpm"
#include "../../../rsrc/toolbar/24/greenplus24.xpm"
#include "../../../rsrc/toolbar/24/newfund24.xpm"
#include "../../../rsrc/toolbar/24/note_icon.xpm"
#include "../../../rsrc/toolbar/24/objectborrow24.xpm"
#include "../../../rsrc/toolbar/24/objectgetback24.xpm"
#include "../../../rsrc/toolbar/24/objectgiveback24.xpm"
#include "../../../rsrc/toolbar/24/objectgiven24.xpm"
#include "../../../rsrc/toolbar/24/objectlend24.xpm"
#include "../../../rsrc/toolbar/24/objectreceived24.xpm"
#include "../../../rsrc/toolbar/24/redminus24.xpm"
#include "../../../rsrc/toolbar/24/shopping_bag_line.xpm"
#include "../../../rsrc/toolbar/24/xml_24.xpm"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxMainFrame, wxFrame )
	EVT_CLOSE( wxMainFrame::_wxFB_FormClose )
	EVT_SIZE( wxMainFrame::_wxFB_onResize )
	EVT_MENU( bilSave, wxMainFrame::_wxFB_SaveClick )
	EVT_MENU( bilRevert, wxMainFrame::_wxFB_RevertClick )
	EVT_MENU( bilXML, wxMainFrame::_wxFB_TextConvClick )
	EVT_MENU( ombArchive, wxMainFrame::_wxFB_ViewArchiveClick )
	EVT_MENU( bilModifyPassword, wxMainFrame::_wxFB_ModifyPasswordClick )
	EVT_MENU( ombRemovePassword, wxMainFrame::_wxFB_RemovePasswordClick )
	EVT_MENU( wxID_EXIT, wxMainFrame::_wxFB_ExitClick )
	EVT_MENU( bilCopy, wxMainFrame::_wxFB_CopyClick )
	EVT_MENU( bilFind, wxMainFrame::_wxFB_ShowFindDialog )
	EVT_MENU( bilNewFund, wxMainFrame::_wxFB_NewFundClick )
	EVT_MENU( bilRemoveFund, wxMainFrame::_wxFB_RemoveFundClick )
	EVT_MENU( bilResetFund, wxMainFrame::_wxFB_ResetFundClick )
	EVT_MENU( bilSetTotal, wxMainFrame::_wxFB_SetTotalClick )
	EVT_MENU( bilDefaultFund, wxMainFrame::_wxFB_DefaultFundClick )
	EVT_MENU( bilGain, wxMainFrame::_wxFB_GainClick )
	EVT_MENU( bilExpense, wxMainFrame::_wxFB_ExpenseClick )
	EVT_MENU( bilCategory, wxMainFrame::_wxFB_CategoryClick )
	EVT_MENU( bilEditCategories, wxMainFrame::_wxFB_EditCategoriesClick )
	EVT_MENU( bilReceived, wxMainFrame::_wxFB_ReceivedClick )
	EVT_MENU( bilGifted, wxMainFrame::_wxFB_GiftedClick )
	EVT_MENU( bilLend, wxMainFrame::_wxFB_LendClick )
	EVT_MENU( bilGetBack, wxMainFrame::_wxFB_GetBackClick )
	EVT_MENU( bilBorrow, wxMainFrame::_wxFB_BorrowClick )
	EVT_MENU( bilGiveBack, wxMainFrame::_wxFB_GiveBackClick )
	EVT_MENU( bilNewCredit, wxMainFrame::_wxFB_NewCreditClick )
	EVT_MENU( bilRemoveCredit, wxMainFrame::_wxFB_RemoveCreditClick )
	EVT_MENU( bilCondoneCredit, wxMainFrame::_wxFB_CondoneCreditClick )
	EVT_MENU( bilNewDebt, wxMainFrame::_wxFB_NewDebtClick )
	EVT_MENU( bilRemoveDebt, wxMainFrame::_wxFB_RemoveDebtClick )
	EVT_MENU( bilCondoneDebt, wxMainFrame::_wxFB_CondoneDebtClick )
	EVT_MENU( bilAddShopItem, wxMainFrame::_wxFB_AddShopItemClick )
	EVT_MENU( bilDelShopItem, wxMainFrame::_wxFB_DelShopItemClick )
	EVT_MENU( bilWiz, wxMainFrame::_wxFB_WizClick )
	EVT_MENU( bilOldConv, wxMainFrame::_wxFB_OldConv1Click )
	EVT_MENU( bilImport, wxMainFrame::_wxFB_ImportDocument )
	EVT_MENU( ombShowMap, wxMainFrame::_wxFB_ShowMap )
	EVT_MENU( bilOptions, wxMainFrame::_wxFB_OptionsClick )
	EVT_MENU( bilHelpMenu, wxMainFrame::_wxFB_HelpMenuClick )
	EVT_MENU( wxID_ABOUT, wxMainFrame::_wxFB_AuthorClick )
	EVT_MENU( bilWebSite, wxMainFrame::_wxFB_WebSiteClick )
	EVT_MENU( bilDonate, wxMainFrame::_wxFB_DonateClick )
	EVT_MENU( bilBug, wxMainFrame::_wxFB_BugClick )
	EVT_MENU( bilNewFund, wxMainFrame::_wxFB_NewFundClick )
	EVT_MENU( bilNewCredit, wxMainFrame::_wxFB_NewCreditClick )
	EVT_MENU( bilNewDebit, wxMainFrame::_wxFB_NewDebtClick )
	EVT_MENU( bilLend, wxMainFrame::_wxFB_LendClick )
	EVT_MENU( bilBorrow, wxMainFrame::_wxFB_BorrowClick )
	EVT_MENU( bilAddShopItem, wxMainFrame::_wxFB_AddShopItemClick )
	EVT_MENU( bilRemoveFund, wxMainFrame::_wxFB_RemoveFundClick )
	EVT_MENU( bilResetFund, wxMainFrame::_wxFB_ResetFundClick )
	EVT_MENU( bilGetBack, wxMainFrame::_wxFB_GetBackClick )
	EVT_MENU( bilGiveBack, wxMainFrame::_wxFB_GiveBackClick )
	EVT_MENU( bilDelShopItem, wxMainFrame::_wxFB_DelShopItemClick )
	EVT_CALENDAR_SEL_CHANGED( wxID_ANY, wxMainFrame::_wxFB_CheckDate )
	EVT_TIME_CHANGED( wxID_ANY, wxMainFrame::_wxFB_GiveTime )
	EVT_GRID_CELL_LEFT_CLICK( wxMainFrame::_wxFB_ReportSelect )
	EVT_GRID_CELL_RIGHT_CLICK( wxMainFrame::_wxFB_PopRepPopup )
	EVT_GRID_RANGE_SELECT( wxMainFrame::_wxFB_ReportRangeSelect )
END_EVENT_TABLE()

wxMainFrame::wxMainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	MAINMENU = new wxMenuBar( 0 );
	MAINMENU->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	MainMenu_File_ = new wxMenu();
	Save = new wxMenuItem( MainMenu_File_, bilSave, wxString( _("Save") ) + wxT('\t') + wxT("CTRL+S"), _("Save changes to current document"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Save->SetBitmaps( wxBitmap( document_save20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Save->SetBitmap( wxBitmap( document_save20_xpm ) );
	#endif
	MainMenu_File_->Append( Save );

	Revert = new wxMenuItem( MainMenu_File_, bilRevert, wxString( _("Revert") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( Revert );

	XML = new wxMenuItem( MainMenu_File_, bilXML, wxString( _("Export XML") ) , _("Export the document in XML format"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	XML->SetBitmaps( wxBitmap( xml_20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	XML->SetBitmap( wxBitmap( xml_20_xpm ) );
	#endif
	MainMenu_File_->Append( XML );

	ViewArchive = new wxMenuItem( MainMenu_File_, ombArchive, wxString( _("Browse archive") ) , _("Attaches the master backup archive"), wxITEM_CHECK );
	MainMenu_File_->Append( ViewArchive );

	MainMenu_File_->AppendSeparator();

	ModifyPassword = new wxMenuItem( MainMenu_File_, bilModifyPassword, wxString( _("Change password") ) + wxT('\t') + wxT("CTRL+P"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( ModifyPassword );

	RemovePassword = new wxMenuItem( MainMenu_File_, ombRemovePassword, wxString( _("Remove password") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( RemovePassword );

	MainMenu_File_->AppendSeparator();

	Exit = new wxMenuItem( MainMenu_File_, wxID_EXIT, wxString( _("Exit") ) + wxT('\t') + wxT("CTRL+E"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_File_->Append( Exit );

	MAINMENU->Append( MainMenu_File_, _("File") );

	MainMenu_Edit_ = new wxMenu();
	Copy = new wxMenuItem( MainMenu_Edit_, bilCopy, wxString( _("Copy") ) + wxT('\t') + wxT("CTRL+C"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Copy->SetBitmaps( wxBitmap( edit_copy_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Copy->SetBitmap( wxBitmap( edit_copy_xpm ) );
	#endif
	MainMenu_Edit_->Append( Copy );

	Find1 = new wxMenuItem( MainMenu_Edit_, bilFind, wxString( _("Find") ) + wxT('\t') + wxT("CTRL+T"), wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Find1->SetBitmaps( wxBitmap( edit_find_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Find1->SetBitmap( wxBitmap( edit_find_xpm ) );
	#endif
	MainMenu_Edit_->Append( Find1 );

	MAINMENU->Append( MainMenu_Edit_, _("Edit") );

	MainMenu_Funds_ = new wxMenu();
	NewFund = new wxMenuItem( MainMenu_Funds_, bilNewFund, wxString( _("New") ) + wxT('\t') + wxT("CTRL+ALT+N"), _("Add a fund"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	NewFund->SetBitmaps( wxBitmap( newfund20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	NewFund->SetBitmap( wxBitmap( newfund20_xpm ) );
	#endif
	MainMenu_Funds_->Append( NewFund );

	RemoveFund = new wxMenuItem( MainMenu_Funds_, bilRemoveFund, wxString( _("Remove") ) + wxT('\t') + wxT("CTRL+ALT+R"), _("Remove a fund"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveFund->SetBitmaps( wxBitmap( delete_bin_line_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveFund->SetBitmap( wxBitmap( delete_bin_line_xpm ) );
	#endif
	MainMenu_Funds_->Append( RemoveFund );

	ResetFund = new wxMenuItem( MainMenu_Funds_, bilResetFund, wxString( _("Reset") ) + wxT('\t') + wxT("CTRL+ALT+I"), _("Reset the value of a fund"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	ResetFund->SetBitmaps( wxBitmap( edit_paste20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	ResetFund->SetBitmap( wxBitmap( edit_paste20_xpm ) );
	#endif
	MainMenu_Funds_->Append( ResetFund );

	MainMenu_Funds_->AppendSeparator();

	SetTotal = new wxMenuItem( MainMenu_Funds_, bilSetTotal, wxString( _("Set total") ) + wxT('\t') + wxT("CTRL+ALT+T"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_Funds_->Append( SetTotal );

	DefaultFund = new wxMenuItem( MainMenu_Funds_, bilDefaultFund, wxString( _("Default fund") ) + wxT('\t') + wxT("CTRL+ALT+T"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_Funds_->Append( DefaultFund );

	MAINMENU->Append( MainMenu_Funds_, _("Funds") );

	MainMenu_Operations_ = new wxMenu();
	Gain = new wxMenuItem( MainMenu_Operations_, bilGain, wxString( _("Profit") ) + wxT('\t') + wxT("F2"), _("Store a profit"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Gain->SetBitmaps( wxBitmap( greenplus20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Gain->SetBitmap( wxBitmap( greenplus20_xpm ) );
	#endif
	MainMenu_Operations_->Append( Gain );

	Expense = new wxMenuItem( MainMenu_Operations_, bilExpense, wxString( _("Expense") ) + wxT('\t') + wxT("F3"), _("Store an expense"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Expense->SetBitmaps( wxBitmap( redminus20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Expense->SetBitmap( wxBitmap( redminus20_xpm ) );
	#endif
	MainMenu_Operations_->Append( Expense );

	MainMenu_Operations_->AppendSeparator();

	category = new wxMenuItem( MainMenu_Operations_, bilCategory, wxString( _("Category") ) , _("Add a category to the report"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	category->SetBitmaps( wxBitmap( note_icon20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	category->SetBitmap( wxBitmap( note_icon20_xpm ) );
	#endif
	MainMenu_Operations_->Append( category );

	wxMenuItem* EditCategories;
	EditCategories = new wxMenuItem( MainMenu_Operations_, bilEditCategories, wxString( _("Edit categories") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Operations_->Append( EditCategories );

	MAINMENU->Append( MainMenu_Operations_, _("Operations") );

	MainMenu_Objects_ = new wxMenu();
	Received = new wxMenuItem( MainMenu_Objects_, bilReceived, wxString( _("Received") ) + wxT('\t') + wxT("CTRL+SHIFT+F1"), _("Stores the reception of an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Received->SetBitmaps( wxBitmap( objectreceived20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Received->SetBitmap( wxBitmap( objectreceived20_xpm ) );
	#endif
	MainMenu_Objects_->Append( Received );

	Gifted = new wxMenuItem( MainMenu_Objects_, bilGifted, wxString( _("Given") ) + wxT('\t') + wxT("CTRL+SHIFT+F2"), _("Stores the donation of an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Gifted->SetBitmaps( wxBitmap( objectgiven20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Gifted->SetBitmap( wxBitmap( objectgiven20_xpm ) );
	#endif
	MainMenu_Objects_->Append( Gifted );

	MainMenu_Objects_->AppendSeparator();

	Lend = new wxMenuItem( MainMenu_Objects_, bilLend, wxString( _("Lend") ) + wxT('\t') + wxT("CTRL+SHIFT+F3"), _("Lend an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Lend->SetBitmaps( wxBitmap( objectlend20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Lend->SetBitmap( wxBitmap( objectlend20_xpm ) );
	#endif
	MainMenu_Objects_->Append( Lend );

	GetBack = new wxMenuItem( MainMenu_Objects_, bilGetBack, wxString( _("Get back") ) + wxT('\t') + wxT("CTRL+SHIFT+F4"), _("Get back an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	GetBack->SetBitmaps( wxBitmap( objectgetback20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GetBack->SetBitmap( wxBitmap( objectgetback20_xpm ) );
	#endif
	MainMenu_Objects_->Append( GetBack );

	MainMenu_Objects_->AppendSeparator();

	Borrow = new wxMenuItem( MainMenu_Objects_, bilBorrow, wxString( _("Borrow") ) + wxT('\t') + wxT("CTRL+SHIFT+F5"), _("Borrow an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	Borrow->SetBitmaps( wxBitmap( objectborrow20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Borrow->SetBitmap( wxBitmap( objectborrow20_xpm ) );
	#endif
	MainMenu_Objects_->Append( Borrow );

	GiveBack = new wxMenuItem( MainMenu_Objects_, bilGiveBack, wxString( _("Give back") ) + wxT('\t') + wxT("CTRL+SHIFT+F6"), _("Give back an object"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	GiveBack->SetBitmaps( wxBitmap( objectgiveback20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GiveBack->SetBitmap( wxBitmap( objectgiveback20_xpm ) );
	#endif
	MainMenu_Objects_->Append( GiveBack );

	MAINMENU->Append( MainMenu_Objects_, _("Objects") );

	MainMenu_Credits_ = new wxMenu();
	NewCredit = new wxMenuItem( MainMenu_Credits_, bilNewCredit, wxString( _("Set") ) + wxT('\t') + wxT("CTRL+ALT+C"), _("Stores a credit"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	NewCredit->SetBitmaps( wxBitmap( creditnew20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	NewCredit->SetBitmap( wxBitmap( creditnew20_xpm ) );
	#endif
	MainMenu_Credits_->Append( NewCredit );

	RemoveCredit = new wxMenuItem( MainMenu_Credits_, bilRemoveCredit, wxString( _("Remove") ) + wxT('\t') + wxT("CTRL+ALT+A"), _("Removes or reduces a credit"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveCredit->SetBitmaps( wxBitmap( creditremove20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveCredit->SetBitmap( wxBitmap( creditremove20_xpm ) );
	#endif
	MainMenu_Credits_->Append( RemoveCredit );

	MainMenu_Credits_->AppendSeparator();

	CondoneCredit = new wxMenuItem( MainMenu_Credits_, bilCondoneCredit, wxString( _("Condone") ) , _("Condone a credit or part of it"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	CondoneCredit->SetBitmaps( wxBitmap( condone20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	CondoneCredit->SetBitmap( wxBitmap( condone20_xpm ) );
	#endif
	MainMenu_Credits_->Append( CondoneCredit );

	MAINMENU->Append( MainMenu_Credits_, _("Credits") );

	MainMenu_Debts_ = new wxMenu();
	NewDebt = new wxMenuItem( MainMenu_Debts_, bilNewDebt, wxString( _("Set") ) + wxT('\t') + wxT("CTRL+ALT+D"), _("Stores a debt"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	NewDebt->SetBitmaps( wxBitmap( debtnew20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	NewDebt->SetBitmap( wxBitmap( debtnew20_xpm ) );
	#endif
	MainMenu_Debts_->Append( NewDebt );

	RemoveDebt = new wxMenuItem( MainMenu_Debts_, bilRemoveDebt, wxString( _("Remove") ) + wxT('\t') + wxT("CTRL+ALT+B"), _("Removes or reduces a debt"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveDebt->SetBitmaps( wxBitmap( debtremove20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveDebt->SetBitmap( wxBitmap( debtremove20_xpm ) );
	#endif
	MainMenu_Debts_->Append( RemoveDebt );

	MainMenu_Debts_->AppendSeparator();

	CondoneDebt = new wxMenuItem( MainMenu_Debts_, bilCondoneDebt, wxString( _("Condone") ) , _("Condone a debt or part of it"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	CondoneDebt->SetBitmaps( wxBitmap( condone20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	CondoneDebt->SetBitmap( wxBitmap( condone20_xpm ) );
	#endif
	MainMenu_Debts_->Append( CondoneDebt );

	MAINMENU->Append( MainMenu_Debts_, _("Debts") );

	MainMenu_ShopList_ = new wxMenu();
	AddShopItem = new wxMenuItem( MainMenu_ShopList_, bilAddShopItem, wxString( _("Add item") ) + wxT('\t') + wxT("F5"), _("Add an item to the shopping list"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	AddShopItem->SetBitmaps( wxBitmap( shopping_bag_line_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	AddShopItem->SetBitmap( wxBitmap( shopping_bag_line_xpm ) );
	#endif
	MainMenu_ShopList_->Append( AddShopItem );

	DelShopItem = new wxMenuItem( MainMenu_ShopList_, bilDelShopItem, wxString( _("Remove item") ) + wxT('\t') + wxT("F6"), _("Remove an item from the shopping list"), wxITEM_NORMAL );
	#ifdef __WXMSW__
	DelShopItem->SetBitmaps( wxBitmap( delete_bin_line_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	DelShopItem->SetBitmap( wxBitmap( delete_bin_line_xpm ) );
	#endif
	MainMenu_ShopList_->Append( DelShopItem );

	MAINMENU->Append( MainMenu_ShopList_, _("Shopping list") );

	MainMenu_Tools_ = new wxMenu();
	Wiz = new wxMenuItem( MainMenu_Tools_, bilWiz, wxString( _("Document creation wizard") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Tools_->Append( Wiz );

	OldConv = new wxMenuItem( MainMenu_Tools_, bilOldConv, wxString( _("Old document converter") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OldConv->SetBitmaps( wxBitmap( oldconv_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	OldConv->SetBitmap( wxBitmap( oldconv_xpm ) );
	#endif
	MainMenu_Tools_->Append( OldConv );

	Import = new wxMenuItem( MainMenu_Tools_, bilImport, wxString( _("Import") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Tools_->Append( Import );

	Map = new wxMenuItem( MainMenu_Tools_, ombShowMap, wxString( _("Show map") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Tools_->Append( Map );

	MainMenu_Tools_->AppendSeparator();

	MainMenu_ExtTools = new wxMenu();
	wxMenuItem* MainMenu_ExtToolsItem = new wxMenuItem( MainMenu_Tools_, wxID_ANY, _("External tools"), wxEmptyString, wxITEM_NORMAL, MainMenu_ExtTools );
	MainMenu_Tools_->Append( MainMenu_ExtToolsItem );

	Options = new wxMenuItem( MainMenu_Tools_, bilOptions, wxString( _("Options") ) + wxT('\t') + wxT("CTRL+O"), wxEmptyString, wxITEM_NORMAL );
	MainMenu_Tools_->Append( Options );

	MAINMENU->Append( MainMenu_Tools_, _("Tools") );

	MainMenu_Help_ = new wxMenu();
	Help1 = new wxMenuItem( MainMenu_Help_, bilHelpMenu, wxString( _("Help") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Help1 );

	MainMenu_Help_->AppendSeparator();

	Author = new wxMenuItem( MainMenu_Help_, wxID_ABOUT, wxString( _("Author") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Author );

	Website = new wxMenuItem( MainMenu_Help_, bilWebSite, wxString( _("Website") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Website );

	Donate = new wxMenuItem( MainMenu_Help_, bilDonate, wxString( _("Donate") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( Donate );

	MainMenu_Help_->AppendSeparator();

	wxMenuItem* report_bug;
	report_bug = new wxMenuItem( MainMenu_Help_, bilBug, wxString( _("Report bug") ) , wxEmptyString, wxITEM_NORMAL );
	MainMenu_Help_->Append( report_bug );

	MAINMENU->Append( MainMenu_Help_, _("About") );

	this->SetMenuBar( MAINMENU );

	ToolBar = this->CreateToolBar( wxTB_HORIZONTAL, wxID_ANY );
	ToolBar->SetToolBitmapSize( wxSize( 24,24 ) );
	ToolBar->SetToolSeparation( 0 );
	ToolBar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	ToolBar->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );
	ToolBar->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWFRAME ) );

	Tool_Save = ToolBar->AddTool( bilSave, wxEmptyString, wxBitmap( document_save_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Save changes to current document"), NULL );

	Tool_XML = ToolBar->AddTool( bilXML, wxEmptyString, wxBitmap( xml_24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Export the document in XML format"), NULL );

	ToolBar->AddSeparator();

	Tool_NewFund = ToolBar->AddTool( bilNewFund, wxEmptyString, wxBitmap( newfund24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Add a fund"), NULL );

	Tool_RemoveFund = ToolBar->AddTool( bilRemoveFund, wxEmptyString, wxBitmap( delete_bin_line_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Remove a fund"), NULL );

	Tool_ResetFund = ToolBar->AddTool( bilResetFund, wxEmptyString, wxBitmap( edit_paste_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Reset the value of a fund"), NULL );

	ToolBar->AddSeparator();

	Tool_Gain = ToolBar->AddTool( bilGain, wxEmptyString, wxBitmap( greenplus24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Store a profit"), NULL );

	Tool_Expense = ToolBar->AddTool( bilExpense, wxEmptyString, wxBitmap( redminus24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Store an expense"), NULL );

	Tool_Category = ToolBar->AddTool( bilCategory, wxEmptyString, wxBitmap( note_icon_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Add a category to the report"), NULL );

	ToolBar->AddSeparator();

	Tool_Received = ToolBar->AddTool( bilReceived, wxEmptyString, wxBitmap( objectreceived24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores the reception of an object"), NULL );

	Tool_Gifted = ToolBar->AddTool( bilGifted, wxEmptyString, wxBitmap( objectgiven24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores the donation of an object"), NULL );

	Tool_Lend = ToolBar->AddTool( bilLend, wxEmptyString, wxBitmap( objectlend24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Lend an object"), NULL );

	Tool_GetBack = ToolBar->AddTool( bilGetBack, wxEmptyString, wxBitmap( objectgetback24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Get back an object"), NULL );

	Tool_Borrow = ToolBar->AddTool( bilBorrow, wxEmptyString, wxBitmap( objectborrow24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Borrow an object"), NULL );

	Tool_GiveBack = ToolBar->AddTool( bilGiveBack, wxEmptyString, wxBitmap( objectgiveback24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Give back an object"), NULL );

	ToolBar->AddSeparator();

	Tool_NewCredit = ToolBar->AddTool( bilNewCredit, wxEmptyString, wxBitmap( creditnew24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores a credit"), NULL );

	Tool_RemoveCredit = ToolBar->AddTool( bilRemoveCredit, wxEmptyString, wxBitmap( creditremove24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Removes or reduces a credit"), NULL );

	Tool_CondoneCredit = ToolBar->AddTool( bilCondoneCredit, wxEmptyString, wxBitmap( condone24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Condone a credit or part of it"), NULL );

	ToolBar->AddSeparator();

	Tool_NewDebt = ToolBar->AddTool( bilNewDebt, wxEmptyString, wxBitmap( debtnew24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Stores a debt"), NULL );

	Tool_RemoveDebt = ToolBar->AddTool( bilRemoveDebt, wxEmptyString, wxBitmap( debtremove24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Removes or reduces a debt"), NULL );

	Tool_CondoneDebt = ToolBar->AddTool( bilCondoneDebt, wxEmptyString, wxBitmap( condone24_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Condone a debt or part of it"), NULL );

	ToolBar->AddSeparator();

	Tool_AddShopItem = ToolBar->AddTool( bilAddShopItem, wxEmptyString, wxBitmap( shopping_bag_line_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Add an item to the shopping list"), NULL );

	Tool_RemShopItem = ToolBar->AddTool( bilDelShopItem, wxEmptyString, wxBitmap( delete_bin_line_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, _("Remove an item from the shopping list"), NULL );

	ToolBar->Realize();

	Container = new wxFlexGridSizer( 3, 1, 0, 0 );
	Container->AddGrowableCol( 0 );
	Container->AddGrowableRow( 1 );
	Container->SetFlexibleDirection( wxBOTH );
	Container->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	Title = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Title->Wrap( -1 );
	Title->SetFont( wxFont( 20, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Title->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHT ) );
	Title->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );

	Container->Add( Title, 0, wxALIGN_CENTER_HORIZONTAL, 5 );

	m_splitter7 = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter7->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter7OnIdle ), NULL, this );

	Panel2 = new wxPanel( m_splitter7, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer8;
	fgSizer8 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer8->AddGrowableCol( 0 );
	fgSizer8->AddGrowableRow( 0 );
	fgSizer8->SetFlexibleDirection( wxBOTH );
	fgSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_panel4 = new wxPanel( Panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	m_splitter3 = new wxSplitterWindow( m_panel4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter3->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter3OnIdle ), NULL, this );

	m_panel5 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );

	ItemList = new wxTreeCtrl( m_panel5, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE|wxTR_HIDE_ROOT );
	ItemList->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	ItemList->SetToolTip( _("List of available funds and objects") );

	PopList = new wxMenu();
	Funds2 = new wxMenu();
	wxMenuItem* Funds2Item = new wxMenuItem( PopList, wxID_ANY, _("Funds"), wxEmptyString, wxITEM_NORMAL, Funds2 );
	wxMenuItem* New2;
	New2 = new wxMenuItem( Funds2, bilNewFund, wxString( _("New") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	New2->SetBitmaps( wxBitmap( newfund20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	New2->SetBitmap( wxBitmap( newfund20_xpm ) );
	#endif
	Funds2->Append( New2 );

	PopList->Append( Funds2Item );

	Cred2 = new wxMenu();
	wxMenuItem* Cred2Item = new wxMenuItem( PopList, wxID_ANY, _("Credits"), wxEmptyString, wxITEM_NORMAL, Cred2 );
	wxMenuItem* m_menuItem43;
	m_menuItem43 = new wxMenuItem( Cred2, bilNewCredit, wxString( _("New") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem43->SetBitmaps( wxBitmap( creditnew20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem43->SetBitmap( wxBitmap( creditnew20_xpm ) );
	#endif
	Cred2->Append( m_menuItem43 );

	PopList->Append( Cred2Item );

	Deb2 = new wxMenu();
	wxMenuItem* Deb2Item = new wxMenuItem( PopList, wxID_ANY, _("Debts"), wxEmptyString, wxITEM_NORMAL, Deb2 );
	wxMenuItem* m_menuItem44;
	m_menuItem44 = new wxMenuItem( Deb2, bilNewDebit, wxString( _("New") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem44->SetBitmaps( wxBitmap( debtnew20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem44->SetBitmap( wxBitmap( debtnew20_xpm ) );
	#endif
	Deb2->Append( m_menuItem44 );

	PopList->Append( Deb2Item );

	Obj2 = new wxMenu();
	wxMenuItem* Obj2Item = new wxMenuItem( PopList, wxID_ANY, _("Objects"), wxEmptyString, wxITEM_NORMAL, Obj2 );
	wxMenuItem* m_menuItem45;
	m_menuItem45 = new wxMenuItem( Obj2, bilLend, wxString( _("Lend") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem45->SetBitmaps( wxBitmap( objectlend20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem45->SetBitmap( wxBitmap( objectlend20_xpm ) );
	#endif
	Obj2->Append( m_menuItem45 );

	wxMenuItem* m_menuItem46;
	m_menuItem46 = new wxMenuItem( Obj2, bilBorrow, wxString( _("Borrow") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem46->SetBitmaps( wxBitmap( objectborrow20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem46->SetBitmap( wxBitmap( objectborrow20_xpm ) );
	#endif
	Obj2->Append( m_menuItem46 );

	PopList->Append( Obj2Item );

	ShopList2 = new wxMenu();
	wxMenuItem* ShopList2Item = new wxMenuItem( PopList, wxID_ANY, _("Shopping list"), wxEmptyString, wxITEM_NORMAL, ShopList2 );
	wxMenuItem* m_menuItem47;
	m_menuItem47 = new wxMenuItem( ShopList2, bilAddShopItem, wxString( _("Add item") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	m_menuItem47->SetBitmaps( wxBitmap( shopping_bag_line_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	m_menuItem47->SetBitmap( wxBitmap( shopping_bag_line_xpm ) );
	#endif
	ShopList2->Append( m_menuItem47 );

	PopList->Append( ShopList2Item );

	wxMenuItem* RemoveFund2;
	RemoveFund2 = new wxMenuItem( PopList, bilRemoveFund, wxString( _("Remove fund") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	RemoveFund2->SetBitmaps( wxBitmap( removefund20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	RemoveFund2->SetBitmap( wxBitmap( removefund20_xpm ) );
	#endif
	PopList->Append( RemoveFund2 );

	wxMenuItem* ResetFund2;
	ResetFund2 = new wxMenuItem( PopList, bilResetFund, wxString( _("Reset fund") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	ResetFund2->SetBitmaps( wxBitmap( edit_paste20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	ResetFund2->SetBitmap( wxBitmap( edit_paste20_xpm ) );
	#endif
	PopList->Append( ResetFund2 );

	wxMenuItem* Condone2;
	Condone2 = new wxMenuItem( PopList, wxID_ANY, wxString( _("Condone") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Condone2->SetBitmaps( wxBitmap( condone20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Condone2->SetBitmap( wxBitmap( condone20_xpm ) );
	#endif
	PopList->Append( Condone2 );

	wxMenuItem* GetBack2;
	GetBack2 = new wxMenuItem( PopList, bilGetBack, wxString( _("Get back") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	GetBack2->SetBitmaps( wxBitmap( objectgetback20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GetBack2->SetBitmap( wxBitmap( objectgetback20_xpm ) );
	#endif
	PopList->Append( GetBack2 );

	wxMenuItem* GiveBack2;
	GiveBack2 = new wxMenuItem( PopList, bilGiveBack, wxString( _("Give back") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	GiveBack2->SetBitmaps( wxBitmap( objectgiveback20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	GiveBack2->SetBitmap( wxBitmap( objectgiveback20_xpm ) );
	#endif
	PopList->Append( GiveBack2 );

	wxMenuItem* DelShopItem2;
	DelShopItem2 = new wxMenuItem( PopList, bilDelShopItem, wxString( _("Remove item") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	DelShopItem2->SetBitmaps( wxBitmap( condone20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	DelShopItem2->SetBitmap( wxBitmap( condone20_xpm ) );
	#endif
	PopList->Append( DelShopItem2 );

	ItemList->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( wxMainFrame::ItemListOnContextMenu ), NULL, this );

	bSizer4->Add( ItemList, 1, wxEXPAND|wxLEFT, 5 );


	m_panel5->SetSizer( bSizer4 );
	m_panel5->Layout();
	bSizer4->Fit( m_panel5 );
	m_panel6 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );

	m_splitter4 = new wxSplitterWindow( m_panel6, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter4->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter4OnIdle ), NULL, this );

	FundPanel = new wxPanel( m_splitter4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	FundSizer = new wxFlexGridSizer( 1, 1, 0, 0 );
	FundSizer->AddGrowableCol( 0 );
	FundSizer->AddGrowableRow( 0 );
	FundSizer->SetFlexibleDirection( wxBOTH );
	FundSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );


	FundPanel->SetSizer( FundSizer );
	FundPanel->Layout();
	FundSizer->Fit( FundPanel );
	TrendPanel = new wxPanel( m_splitter4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	TrendSizer = new wxFlexGridSizer( 1, 1, 0, 0 );
	TrendSizer->AddGrowableCol( 0 );
	TrendSizer->AddGrowableRow( 0 );
	TrendSizer->SetFlexibleDirection( wxBOTH );
	TrendSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );


	TrendPanel->SetSizer( TrendSizer );
	TrendPanel->Layout();
	TrendSizer->Fit( TrendPanel );
	m_splitter4->SplitHorizontally( FundPanel, TrendPanel, 0 );
	bSizer5->Add( m_splitter4, 1, wxEXPAND, 5 );


	m_panel6->SetSizer( bSizer5 );
	m_panel6->Layout();
	bSizer5->Fit( m_panel6 );
	m_splitter3->SplitVertically( m_panel5, m_panel6, 258 );
	bSizer3->Add( m_splitter3, 1, wxEXPAND, 5 );


	m_panel4->SetSizer( bSizer3 );
	m_panel4->Layout();
	bSizer3->Fit( m_panel4 );
	fgSizer8->Add( m_panel4, 1, wxEXPAND, 5 );

	fgSizer9 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer9->AddGrowableCol( 0 );
	fgSizer9->AddGrowableRow( 1 );
	fgSizer9->SetFlexibleDirection( wxBOTH );
	fgSizer9->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	Calendar = new wxCalendarCtrl( Panel2, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxCAL_MONDAY_FIRST|wxCAL_SEQUENTIAL_MONTH_SELECTION|wxCAL_SHOW_HOLIDAYS|wxCAL_SHOW_SURROUNDING_WEEKS );
	Calendar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Calendar->SetToolTip( _("Select the day for operations to store") );

	fgSizer9->Add( Calendar, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	TimePicker = new wxTimePickerCtrl( Panel2, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxTP_DEFAULT );
	fgSizer9->Add( TimePicker, 0, wxBOTTOM|wxRIGHT|wxLEFT|wxEXPAND, 5 );


	fgSizer8->Add( fgSizer9, 1, 0, 5 );


	Panel2->SetSizer( fgSizer8 );
	Panel2->Layout();
	fgSizer8->Fit( Panel2 );
	m_panel3 = new wxPanel( m_splitter7, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );

	m_splitter41 = new wxSplitterWindow( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter41->Connect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter41OnIdle ), NULL, this );

	m_panel8 = new wxPanel( m_splitter41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer51;
	bSizer51 = new wxBoxSizer( wxVERTICAL );

	Report = new wxGrid( m_panel8, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	Report->CreateGrid( 1, 8 );
	Report->EnableEditing( false );
	Report->EnableGridLines( true );
	Report->EnableDragGridSize( false );
	Report->SetMargins( 0, 0 );

	// Columns
	Report->AutoSizeColumns();
	Report->EnableDragColMove( false );
	Report->EnableDragColSize( true );
	Report->SetColLabelSize( 30 );
	Report->SetColLabelValue( 0, _("Date") );
	Report->SetColLabelValue( 1, _("Hour") );
	Report->SetColLabelValue( 2, _("Operation") );
	Report->SetColLabelValue( 3, _("Value (€)") );
	Report->SetColLabelValue( 4, _("Reason") );
	Report->SetColLabelValue( 5, _("Category") );
	Report->SetColLabelValue( 6, _("Contact") );
	Report->SetColLabelValue( 7, _("Location") );
	Report->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	Report->SetRowSize( 0, 23 );
	Report->EnableDragRowSize( true );
	Report->SetRowLabelSize( 0 );
	Report->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance
	Report->SetLabelBackgroundColour( wxColour( 250, 147, 245 ) );
	Report->SetLabelFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Ubuntu") ) );

	// Cell Defaults
	Report->SetDefaultCellFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Report->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	Report->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Report->SetToolTip( _("Report of stored operations") );

	PopRep = new wxMenu();
	Category2 = new wxMenuItem( PopRep, bilCategory, wxString( _("Category") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	Category2->SetBitmaps( wxBitmap( note_icon20_xpm ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	Category2->SetBitmap( wxBitmap( note_icon20_xpm ) );
	#endif
	PopRep->Append( Category2 );

	Report->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( wxMainFrame::ReportOnContextMenu ), NULL, this );

	bSizer51->Add( Report, 1, wxEXPAND|wxRIGHT|wxLEFT, 5 );


	m_panel8->SetSizer( bSizer51 );
	m_panel8->Layout();
	bSizer51->Fit( m_panel8 );
	m_panel9 = new wxPanel( m_splitter41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 4, 1, 0, 0 );
	fgSizer6->AddGrowableCol( 0 );
	fgSizer6->AddGrowableRow( 3 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	TopCategoryLabel = new wxStaticText( m_panel9, wxID_ANY, _("Top categories"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	TopCategoryLabel->Wrap( -1 );
	TopCategoryLabel->SetFont( wxFont( 16, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer6->Add( TopCategoryLabel, 1, wxTOP|wxRIGHT|wxLEFT|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	TopCategoriesSizer = new wxFlexGridSizer( 1, 1, 0, 0 );
	TopCategoriesSizer->SetFlexibleDirection( wxBOTH );
	TopCategoriesSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );


	fgSizer6->Add( TopCategoriesSizer, 1, wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	ShopLabel = new wxStaticText( m_panel9, wxID_ANY, _("Shopping list"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	ShopLabel->Wrap( -1 );
	ShopLabel->SetFont( wxFont( 16, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer6->Add( ShopLabel, 1, wxALIGN_CENTER|wxTOP|wxRIGHT|wxLEFT|wxALIGN_BOTTOM, 5 );

	ShopList = new wxListBox( m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	ShopList->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer6->Add( ShopList, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );


	m_panel9->SetSizer( fgSizer6 );
	m_panel9->Layout();
	fgSizer6->Fit( m_panel9 );
	m_splitter41->SplitVertically( m_panel8, m_panel9, 800 );
	bSizer1->Add( m_splitter41, 1, wxEXPAND, 5 );


	m_panel3->SetSizer( bSizer1 );
	m_panel3->Layout();
	bSizer1->Fit( m_panel3 );
	m_splitter7->SplitHorizontally( Panel2, m_panel3, 0 );
	Container->Add( m_splitter7, 1, wxEXPAND, 5 );

	Progress = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_HORIZONTAL );
	Progress->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Container->Add( Progress, 1, wxALL|wxEXPAND|wxALIGN_BOTTOM, 5 );


	this->SetSizer( Container );
	this->Layout();
	StatusBar = this->CreateStatusBar( 2, wxSTB_SIZEGRIP, wxID_ANY );
	StatusBar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

}

wxMainFrame::~wxMainFrame()
{
	delete PopList;
	delete PopRep;
}
