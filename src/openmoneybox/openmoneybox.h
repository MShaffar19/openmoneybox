/***************************************************************
 * Name:      openmoneybox.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBAPP_H
	#define OMBAPP_H

	#ifdef __WXMSW__
			#include "../platformsetup.h"
	#endif // __WXMSW__

	class BilApp : public wxApp{
		private:
			// Non-GUI components
			wxSingleInstanceChecker *m_checker_omb;	// checks if another instance is already running
			#ifndef __WXMSW__
				void CheckTimerFire( wxTimerEvent& event );
			#endif // __WXMSW__
		public:
			// Routines
			virtual bool OnInit();
			int OnExit();
	};

	WXIMPORT wxLanguage FindLang(void);
	WXIMPORT void Error(int Err,wxString Opt);
	WXIMPORT bool TrayActive(void);
	WXIMPORT wxString GetShareDir(void);
	#ifdef _OMB_INSTALLEDUPDATE
		WXIMPORT bool GetCheckUpdates(void);
		WXIMPORT void fnCheckUpdate(void);
	#endif // _OMB_INSTALLEDUPDATE

	#ifdef __WXGTK__
		WXIMPORT wxString GetUserConfigDir(void);
		WXIMPORT void CreateTrayAutostart(void);
	#endif // __WXGTK__

	#ifdef __WXMAC__
		WXIMPORT wxString GetUserConfigDir(void);
		//WXIMPORT void CreateTrayAutostart(void);
	#endif // __WXMAC__

#endif	// OMBAPP_H
