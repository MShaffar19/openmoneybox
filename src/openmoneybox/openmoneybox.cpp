/***************************************************************
 * Name:      openmoneybix.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-27
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OPENMONEYBOX_CPP_INCLUDED
#define OPENMONEYBOX_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifndef __WXMSW__
  #include <signal.h>
#endif // __WXMSW__

#ifdef _OMB_USE_GSETTINGS
	#include <gio/gio.h>	// For GSettings
#else
	#include <wx/fileconf.h> // For wxFileConfig
#endif // OMB_USE_GSETTINGS

#include <wx/app.h>
//#include <wx/cmdline.h>
#include <wx/filename.h>    // For wxFileName
#include <wx/intl.h> // For i18n
#include <wx/snglinst.h>	// for wxSingleInstanceChecker
#include <wx/stdpaths.h> // For wxStandardPathsBase

#ifdef __WXGTK__
	#include "../platformsetup.h"
#endif // __WXGTK__
#include "../types.h"
#include "openmoneybox.h"
//#include "constants.h"
#include "ui/main_wx.h"

#ifdef __WXMAC__
	const wxString AppDir = L"/Volumes/OpenMoneyBox_3.4.1.13/openmoneybox.app/Contents/MacOS";
#endif

// Code start

#include "../languages.cpp"	// Contains language definitions

// the arrays must be in sync
wxCOMPILE_TIME_ASSERT( WXSIZEOF(langNames)==WXSIZEOF(langIds),LangArraysMismatch);

#ifndef __WXMSW__
  bool Killed=false;	// Set to true when SIGTERM is received
  wxTimer *CheckTimer;
#endif // __WXMSW__
wxLanguage Lan;

wxLocale omb_locale;
#ifndef __WXMSW__
	#ifdef _OMB_USE_GSETTINGS
		GSettings *settings_general;
		GSettings *settings_charts;
		GSettings *settings_tools;
		#ifdef _OMB_INSTALLEDUPDATE
			GSettings *settings_advanced;
		#endif // _OMB_INSTALLEDUPDATE
	#else
  	wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
#endif // __WXMSW__
TData *Data;
ombMainFrame *frame;
FILE * m_pLogFile;

IMPLEMENT_APP(BilApp)

#ifndef __WXMSW__
  void term(int signum){	// Captures the SIGTERM signal
                          // http://airtower.wordpress.com/2010/06/16/catch-sigterm-exit-gracefully/
      Killed = true;}
#endif // __WXMSW__

bool BilApp::OnInit(){

	#ifdef __WXMSW__
    if (m_pLogFile == NULL)
    {
      m_pLogFile = fopen(GetInstallationPath() + L"\\omb.log", "w" );
      delete wxLog::SetActiveTarget(new wxLogStderr(m_pLogFile));
    }
    wxLogMessage("Program starts...");
	#else
		wxString home_dir = wxGetHomeDir();

		wxString homecfg = GetUserConfigDir();

		// Start logging
		if (m_pLogFile == NULL)
		{
			#ifdef __WXGTK__
				#ifndef __OPENSUSE__
					m_pLogFile = fopen( homecfg + "/omb.log", "w" );
				#else
					wxString f = homecfg + L"/omb.log";
					wxString am = L"w";
					m_pLogFile = fopen( f.fn_str(), am.fn_str() );
				#endif // __OPENSUSE__
			#endif // __WXGTK__
			delete wxLog::SetActiveTarget(new wxLogStderr(m_pLogFile));
		}
		wxLogMessage(L"Program starts...");

		#ifdef _OMB_USE_GSETTINGS
			settings_general = g_settings_new("org.igisw.openmoneybox.general");
			settings_charts = g_settings_new("org.igisw.openmoneybox.charts");
			settings_tools = g_settings_new("org.igisw.openmoneybox.tools");
			#ifdef _OMB_INSTALLEDUPDATE
				settings_advanced = g_settings_new("org.igisw.openmoneybox.advanced");
			#endif // _OMB_INSTALLEDUPDATE
		#else
			homecfg += L"/omb.ini";
			if(!::wxFileExists(homecfg)){
				wxFile *f=new wxFile(homecfg,wxFile::write);
				//f->Create(homecfg,false,wxS_DEFAULT);
				f->Close();}
			INI = new wxFileConfig(wxEmptyString, wxEmptyString, homecfg, wxEmptyString, wxCONFIG_USE_LOCAL_FILE, wxConvAuto());
		#endif // _OMB_USE_GSETTINGS

	#endif // __WXMSW__

	#ifdef _OMB_DEBUG
		#ifdef __WXGTK__
      wxLocale::AddCatalogLookupPathPrefix(L".");
    #endif // __WXGTK__
	#endif // _OMB_DEBUG

	#ifdef __WXMSW__
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir());
	#else
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir()+L"locale");
		#ifdef _OMB_DEBUG
			wxLocale::AddCatalogLookupPathPrefix(wxGetCwd());
		#endif // _OMB_DEBUG
	#endif // __WXMSW__
	#ifdef __WXMAC__
			wxLocale::AddCatalogLookupPathPrefix(AppDir);
	#endif // __WXMAC__
	omb_locale.Init(wxLANGUAGE_DEFAULT,wxLOCALE_LOAD_DEFAULT);
	omb_locale.AddCatalog(L"openmoneybox");
	Lan=FindLang();

  // Check if already running
  wxString user=wxGetUserId();
  const wxString name = wxString::Format(L"omb-%s",user.c_str());
	m_checker_omb = new wxSingleInstanceChecker(name);
	if (m_checker_omb->IsAnotherRunning()){
		wxMessageBox(_("OpenMoneyBox is already running!"),_("OpenMoneyBox"), wxICON_EXCLAMATION);
		return false;}

	#if defined (__WXMSW__) || defined (__WXMAC__)
    	bool frame_created = false;
	#endif // defined
	const wxString name2 = wxString::Format(L"ombtray-%s",user.c_str());
	wxSingleInstanceChecker *m_checker_ombtray = new wxSingleInstanceChecker(name2);
	if (m_checker_ombtray->IsAnotherRunning()){
		wxString Proc;
		#ifdef __WXMSW__
      long processid = 0;
      wxStandardPathsBase& Paths = ::wxStandardPaths::Get();
      wxString hm = Paths.GetTempDir() + L"\\bil";
      wxString tempfile = ::wxFileName::CreateTempFileName(hm);
      Proc = L"tasklist |find \"ombtray.exe\" >" + tempfile;

      // Following lines are necessary to avoid OpenMoneyBox exits because wxShell is recognized as top window
      frame = new ombMainFrame(wxTheApp->GetTopWindow());
      frame_created = true;

      ::wxShell(Proc);
      if(::wxFileExists(tempfile)){
        int L, b;
        char *Buffer;
        wxString S = wxEmptyString;
        wxFile *file = new wxFile(tempfile, wxFile::read);
        L = file->SeekEnd(0);
        file->Seek(0);
        Buffer = new char[L + 1];
        file->Read(Buffer, L);

        b = 0;
        while ((Buffer[b] < char(48)) | (Buffer[b] > char(57))){
          b++;}
        do{
          S += wxChar(Buffer[b]);
          b++;}
        while ((Buffer[b] >= char(48)) & (Buffer[b] <= char(57)));

        S.ToLong(&processid);
        file->Close();
        ::wxRemoveFile(tempfile);
        delete m_checker_ombtray;}
      if(processid > 0)wxKill(processid, wxSIGKILL);

		#else
			Proc = L"killall -u " + user + L" ombtray";
      wxExecute(Proc,wxEXEC_ASYNC);
      bool ombtray_killed = false;
      int s=0;
      while(s <= 60){
        if(m_checker_ombtray->IsAnotherRunning()){
          wxSleep(5);
          delete m_checker_ombtray;
          if(::wxFileExists(home_dir + L"/" + name2)) wxRemoveFile (home_dir + L"/" + name2);
          m_checker_ombtray = new wxSingleInstanceChecker(name2);
          s += 5;}
        else{
          delete m_checker_ombtray;
          ombtray_killed = true;
          break;}}
      if(! ombtray_killed) return false;
		#endif
  }
  else delete m_checker_ombtray;
	wxLogMessage(L"Existing instances cleared");

 	// Document memory-space creation
	Data = new TData(NULL); // Progress bar is set after frame creation
	wxLogMessage(L"Data created");

  // Main Window creation
  #if defined (__WXMSW__) || defined (__WXMAC__)
    if(! frame_created)
  #endif // defined
	frame = new ombMainFrame(wxTheApp->GetTopWindow());
	wxLogMessage(L"Main window created");

  Data->Progress = frame->Progress;

	if(! frame->AutoOpen()){
		delete Data;
		delete m_checker_omb;
		wxLogMessage(L"Error opening main window!");
		Exit();
		return false;
	}
	wxLogMessage(L"Auto document opening done");

	Data->Hour = ::wxDateTime::Now();

	frame->UpdateCalendar(wxInvalidDateTime);

	frame->Centre(wxBOTH);
	frame->Show();
	#ifdef _OMB_CHART_MATLIBPLOT
		frame->ShowF();
	#endif // _OMB_CHART_MATLIBPLOT
	SetTopWindow(frame);

	wxLogMessage(L"Main window set to top");

	#ifdef _OMB_INSTALLEDUPDATE
		if(GetCheckUpdates())fnCheckUpdate();	// Version update check
	#endif // _OMB_INSTALLEDUPDATE

	#ifndef __WXMSW__
    // Prepare timer to check SIGTERM
    Connect( wxEVT_TIMER, wxTimerEventHandler( BilApp::CheckTimerFire ) );
    CheckTimer=new wxTimer();
    CheckTimer->SetOwner(this);
    CheckTimer->Start(500,false);
  #endif // __WXMSW__

 	wxLogMessage(L"Initialization over... starting...");
	return true;}

#ifndef __WXMSW__
  void BilApp::CheckTimerFire( wxTimerEvent& event ){
    #ifdef __WXGTK__
      // Following code prepare the action to capture SIGTERM signals
      struct sigaction action;
      memset(&action, 0, sizeof(struct sigaction));
      action.sa_handler = term;
      sigaction(SIGTERM, &action, NULL);
    /*
    #elif __WXMSW__
      typedef void (*SignalHandlerPointer)(int);
      SignalHandlerPointer previousHandler;
      previousHandler = signal(SIGABRT, term);
    */
    #endif

    //int loop = 0;
    if(!Killed){
        //sleep(3);
        //printf("Finished loop run %d.\n", loop++);
        return;}

		wxLogMessage(L"Kill request received.");

    //printf("done.\n");
    wxCloseEvent evt(wxEVT_CLOSE_WINDOW,0);
    frame->FormClose(evt);
    return;}
#endif // __WXMSW__

int BilApp::OnExit(){
	#ifndef __WXMSW__
    CheckTimer->Stop();
    delete CheckTimer;
  #endif // __WXMSW__
	delete Data;
	delete m_checker_omb;

	if(TrayActive()){
		wxString Proc = wxEmptyString;
    wxStandardPaths std = wxStandardPaths::Get();
    wxString exePath = std.GetExecutablePath();
    wxString path;
    wxFileName::SplitPath (exePath, &path, NULL, NULL, wxPATH_NATIVE);

    #ifdef __WXMSW__
      Proc = "\"";
    #endif // __WXMSW__

    Proc = Proc + path;

    #ifdef __WXMSW__
      Proc = Proc + L"\\";
    #else
      Proc = Proc + L"/";
    #endif // __WXMSW__

    Proc = Proc + L"ombtray";

    #ifdef __WXMSW__
      Proc = Proc + L".exe\"";
    #endif // __WXMSW__

    #ifdef __WXMAC__
      Proc += L"-bin\"";
    #endif // __WXMSW__

	  wxLogMessage(L"Launching program " + Proc);
		wxExecute(Proc,wxEXEC_ASYNC);}

	#ifndef _OMB_USEREGISTRY
		#ifdef _OMB_USE_GSETTINGS
			/*
			delete settings_general;
			delete settings_charts;
			delete settings_tools;
			*/
		#else
    	delete INI;
		#endif // _OMB_USE_GSETTINGS
  #endif // _OMB_USEREGISTRY

	wxLogMessage(L"Program exiting...");
	delete wxLog::SetActiveTarget(NULL);
	if (m_pLogFile != NULL)
	{
		fclose(m_pLogFile);
	}

	return 0;}

#endif	// OPENMONEYBOX_CPP_INCLUDED
