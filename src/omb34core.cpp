/***************************************************************
 * Name:      omb34core.cpp
 * Purpose:   Core Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-27
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB34CORE_CPP_INCLUDED
#define OMB34CORE_CPP_INCLUDED

#ifdef _OMB_USE_CIPHER
	#include <exception>
	using namespace std;
	#ifndef __OMBCONVERT_BIN__
		#include "ui/password.h"
	#endif // __OMBCONVERT_BIN__
	#include "wxsqlite3.h"
#endif // _OMB_USE_CIPHER

#include <wx/filename.h>
#include <wx/textfile.h>

#ifndef __OMBCONVERT_BIN__
	#include <wx/gauge.h>
#endif

#ifdef __OMBCONVERT_BIN__
	#include "productversion.h"
#endif // __OMBCONVERT_BIN__
#include "omb34core.h"

#ifdef __OPENMONEYBOX_EXE__
	#include "openmoneybox/ui/main_wx.h"
#endif

#ifdef __WXGTK__
	extern wxLanguage Lan;
#endif // __WXGTK__

#ifdef __OPENMONEYBOX_EXE__
	extern "C" ombMainFrame *frame;
#endif // __OPENMONEYBOX_EXE__

extern wxString ShortVersion;

// db structure
#ifndef __OMBCONVERT_BIN__
	extern int dbVersion;
#endif // __OMBCONVERT_BIN__
extern wxString cs_information;
extern wxString cs_funds;
extern wxString cs_credits;
extern wxString cs_debts;
extern wxString cs_loans;
extern wxString cs_borrows;
extern wxString cs_shoplist;
extern wxString cs_transactions;
extern wxString cs_categories;
extern wxString cs_funds_master;
extern wxString cs_credits_master;
extern wxString cs_debts_master;
extern wxString cs_loans_master;
extern wxString cs_borrows_master;
extern wxString cs_categories_master;

#ifdef _OMB_USE_CIPHER
	extern bool TableExists(const wxString& tableName, const wxString& databaseName, sqlite3 *m_db);
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
	extern wxSQLite3Table GetTable(sqlite3 *m_db, const char* sql);
#endif // _OMB_USE_CIPHER

wxString is_transactions = L"INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index, contact_index, latitude, longitude \
								, currencyid, currencyrate, currencysymb \
								) SELECT isdate, date, time, type, value, reason, cat_index, contact_index, latitude, longitude \
								, currencyid, currencyrate, currencysymb \
								 FROM %sTransactions;";

#ifndef __OMBCONVERT_BIN__
	TData::TData(wxGauge *ProgressBar){
#else
	TData::TData(void){
#endif // __OMBCONVERT_BIN__

	#ifdef _OMB_USE_CIPHER
		IsEncrypted = IsEncrypted_master = false;
	#endif // _OMB_USE_CIPHER

	// Data creation
	Funds = new TVal[MAX_FUNDS];
	Credits = new TVal[MAX_CREDITS];
	Debts = new TVal[MAX_DEBTS];
	Lent = new TObj[MAX_LENT];
	Borrowed = new TObj[MAX_BORROWED];
	ShopItems = new TShopItem[MAX_SHOPS];
	Lines = new TLine[MAX_LINES];
	Categories = new TCategory[MAX_CATEGORIES];

	//__________________
	//NotEnoughMemory=false;
	MattersBuffer = new wxArrayString();
	MattersBuffer->Clear();

	/*
	CategoryDB = new wxArrayString();
	CategoryDB->Clear();
	*/

	#ifndef __OMBCONVERT_BIN__
		Progress = ProgressBar;
	#endif // __OMBCONVERT_BIN__

	#ifndef _OMB_USE_CIPHER
		database = new wxSQLite3Database();
	#endif // _OMB_USE_CIPHER

	Initialize(true);
}

void TData::Initialize(bool Creating){
  // Report initialization
  Lines[0].IsDate=false;
  Lines[0].Date=wxInvalidDateTime;
  Lines[0].Time=wxInvalidDateTime;
  Lines[0].Type=toNULL;
  Lines[0].Value=wxEmptyString;
  Lines[0].Reason=wxEmptyString;
 	Lines[0].CategoryIndex = -1;
	Lines[0].ContactIndex = -1;
}

void TData::ParseDatabase(void){
	Parsing = true;

	#ifdef _OMB_USE_CIPHER
		bool is_date;
		int Rows, id, i;	// Rows: number of rows in the query table
											// id: id of table item
		long ind,					// ind: category index of transaction
			contact_in;     // contact_in: contact index of transaction or object

		double val,
			lat, lon;
		TOpType type;
		wxString str, reason;
		wxDateTime date, time;
		wxSQLite3Table Table;

		// Read Funds
		NFun = 0;
		if(TableExists(L"Funds", wxEmptyString, database)){
			Table = GetTable(database, "select * from Funds order by name;");
			Tot_Funds = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvFou, id, str, val)){
					NFun++;
					Tot_Funds += val;}}}

		// Read default fund
		#ifdef __OPENSUSE__
      wxString Sql = L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";";
      Table = GetTable(database, Sql.c_str());
		#else
      Table = GetTable(database, L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";");
		#endif // __OPENSUSE__
		Table.SetRow(0);
		FileData.DefFund = Table.GetString(0, wxEmptyString);

		// Read Credits
		NCre = 0;
		if(TableExists(L"Credits", wxEmptyString, database)){
			Table = GetTable(database, "select * from Credits order by name;");
			Tot_Credits = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				contact_in = Table.GetInt(3, -1);
				if(AddValue(tvCre, id, str, val, contact_in)){
					NCre++;
					Tot_Credits += val;}}}

		// Read Debts
		NDeb = 0;
		if(TableExists(L"Debts", wxEmptyString, database)){
			Table = GetTable(database, "select * from Debts order by name;");
			Tot_Debts = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				contact_in = Table.GetInt(3, -1);
				if(AddValue(tvDeb, id, str, val, contact_in)){
					NDeb++;
					Tot_Debts += val;}}}

		// Read Loans
		NLen = 0;
		if(TableExists(L"Loans", wxEmptyString, database)){
			Table = GetTable(database, "select * from Loans order by name;");
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				contact_in = Table.GetInt(4, -1);
				if(AddObject(toPre, id, str, reason, date, contact_in)) NLen++;}}

		// Read Borrows
		NBor = 0;
		if(TableExists(L"Borrows", wxEmptyString, database)){
			Table = GetTable(database, "select * from Borrows order by name;");
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				contact_in = Table.GetInt(4, -1);
				if(AddObject(toInP, id, str, reason, date, contact_in)) NBor++;}}

		// Read Categories
		int iconIndex;
		Table = GetTable(database, "select * from Categories order by name;");
		NCat = 0;
		Rows = Table.GetRowCount();
		for (i = 0; i < Rows; i++){
			Table.SetRow(i)	;

			is_date = Table.GetInt(2, 0);	// is_date used for 'active'
			if(is_date){
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				iconIndex = Table.GetInt(3, -1);

				if(iconIndex >= 100){
					iconIndex -= 100;
					iconIndex += _OMB_TOPCATEGORIES_OEMICON;
				}

				AddCategory(id, str, iconIndex);
			}
		}

		// Read Transactions
		wxDateTime LastDate = wxDateTime(1, wxDateTime::Jan, 1970);
		Table = GetTable(database, "select * from Transactions order by date;");
		NLin = 0;
		Rows = Table.GetRowCount();
		if(Rows){
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(ombTColIdId, 0);
				is_date = Table.GetInt(ombTColIdIsDate, 0);
				date = wxDateTime((time_t) Table.GetInt(ombTColIdDate, 0));
				if(date.IsLaterThan(LastDate)) LastDate = date;
				time = wxDateTime((time_t) Table.GetInt(ombTColIdTime, 0));
				type = (TOpType) Table.GetInt(ombTColIdType, 0);
				str = Table.GetString(ombTColIdValue, wxEmptyString);
				reason = Table.GetString(ombTColIdReason, wxEmptyString);
				ind = Table.GetInt(ombTColIdCategoryIndex, 0);

				contact_in = Table.GetInt(ombTColIdContactIndex, -1);

				#ifndef __OMBCONVERT_BIN__
                    // Read Currency information
                    int CurrId = Table.GetInt(ombTColIdCurrencyId, -1);
                    double CurrRate = 1;
                    wxString CurrSymb = wxEmptyString;
                    if(CurrId >= 0){
                        Table.GetAsString(ombTColIdCurrencyRate).ToCDouble(&CurrRate);
                        CurrSymb = Table.GetAsString(ombTColIdCurrencySymbol);
                    }
				#endif // __OMBCONVERT_BIN__

				if(is_date){
					str.ToCDouble(&val);
					if(AddDate(id, date, val)) NLin++;}
				else{
					Table.GetAsString(ombTColIdLatitude).ToCDouble(&lat);
					Table.GetAsString(ombTColIdLongitude).ToCDouble(&lon);
					if((int) type < 9){
						str.ToCDouble(&val);
						str = FormDigits(val);}
					if(AddOper(id, date, time, type, str, reason, ind, contact_in, true, lat, lon
                        #ifndef __OMBCONVERT_BIN__
                        , CurrId, CurrRate, CurrSymb
                        #endif // __OMBCONVERT_BIN__
					)) NLin++;}}}
		else LastDate = wxDateTime::Today();

		FileData.Year = LastDate.GetYear();
		// Following code is a workaround for GetMonth() bug in Linux
		// http://forums.wxwidgets.org/viewtopic.php?t=25154&highlight=getmonth
		wxString temp;
		#ifdef __WXMSW__
			temp = LastDate.Format("%m", wxDateTime::Local).c_str();
		#else
			temp = LastDate.Format(L"%m", wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month, 10);

		Day = LastDate;

		// Read shopping list
		NSho = 0;
		if(TableExists(L"Shoplist", wxEmptyString, database)){
			Table = GetTable(database, "select * from Shoplist order by item collate nocase;");
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				ind = Table.GetInt(2, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddShopItem(id, str, date)) NSho++;}}
	#else
		bool is_date;
		int Rows, id, i;	// Rows: number of rows in the query table
											// id: id of table item
		long ind,					// ind: category index of transaction
			contact_in;     // contact_in: contact index of transaction or object

		double val,
			lat, lon;
		TOpType type;
		wxString str, reason;
		wxDateTime date, time;
		wxSQLite3Table Table;

		// Read Funds
		NFun = 0;
		if(database->TableExists(L"Funds")){
			Table = database->GetTable("select * from Funds order by name;");
			Tot_Funds = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvFou, id, str, val)){
					NFun++;
					Tot_Funds += val;}}}

		// Read default fund
		Table = database->GetTable(L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";");
		Table.SetRow(0);
		FileData.DefFund = Table.GetString(0, wxEmptyString);

		// Read Credits
		NCre = 0;
		if(database->TableExists(L"Credits")){
			Table = database->GetTable("select * from Credits order by name;");
			Tot_Credits = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				contact_in = Table.GetInt(3, -1);
				if(AddValue(tvCre, id, str, val, contact_in)){
					NCre++;
					Tot_Credits += val;}}}

		// Read Debts
		NDeb = 0;
		if(database->TableExists(L"Debts")){
			Table = database->GetTable("select * from Debts order by name;");
			Tot_Debts = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				contact_in = Table.GetInt(3, -1);
				if(AddValue(tvDeb, id, str, val, contact_in)){
					NDeb++;
					Tot_Debts += val;}}}

		// Read Loans
		NLen = 0;
		if(database->TableExists(L"Loans")){
			Table = database->GetTable("select * from Loans order by name;");
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				contact_in = Table.GetInt(4, -1);
				if(AddObject(toPre, id, str, reason, date, contact_in)) NLen++;}}

		// Read Borrows
		NBor = 0;
		if(database->TableExists(L"Borrows")){
			Table = database->GetTable("select * from Borrows order by name;");
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				contact_in = Table.GetInt(4, -1);
				if(AddObject(toInP, id, str, reason, date, contact_in)) NBor++;}}

		// Read Categories
		int iconIndex;

		Table = database->GetTable("select * from Categories order by name;");
		NCat = 0;
		Rows = Table.GetRowCount();
		for (i = 0; i < Rows; i++){
			Table.SetRow(i)	;
			is_date = Table.GetInt(2, 0);
			if(is_date){
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				iconIndex = Table.GetInt(3, -1);
				AddCategory(id, str, iconIndex);
			}
		}

		// Read Transactions
		wxDateTime LastDate = wxDateTime(1, wxDateTime::Jan, 1970);
		Table = database->GetTable("select * from Transactions order by date;");
		NLin = 0;
		Rows = Table.GetRowCount();
		if(Rows){
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				is_date = Table.GetInt(1, 0);
				date = wxDateTime((time_t) Table.GetInt(2, 0));
				if(date.IsLaterThan(LastDate)) LastDate = date;
				time = wxDateTime((time_t) Table.GetInt(3, 0));
				type = (TOpType) Table.GetInt(4, 0);
				str = Table.GetString(5, wxEmptyString);
				reason = Table.GetString(6, wxEmptyString);
				ind = Table.GetInt(7, 0);

				contact_in = Table.GetInt(8, -1);

				#ifndef __OMBCONVERT_BIN__
                    // Read Currency information
                    int CurrId = Table.GetInt(ombTColIdCurrencyId, -1);
                    double CurrRate = 1;
                    wxString CurrSymb = wxEmptyString;
                    if(CurrId >= 0){
                      Table.GetAsString(ombTColIdCurrencyRate).ToCDouble(&CurrRate);
											CurrSymb = Table.GetAsString(ombTColIdCurrencySymbol);
                    }
				#endif // __OMBCONVERT_BIN__

				if(is_date){
					str.ToCDouble(&val);
					if(AddDate(id, date, val)) NLin++;}
				else{
					Table.GetAsString(9).ToCDouble(&lat);
					Table.GetAsString(10).ToCDouble(&lon);
					if((int) type < 9){
						str.ToCDouble(&val);
						str = FormDigits(val);}

                        if(AddOper(id, date, time, type, str, reason, ind, contact_in, true, lat, lon
                            #ifndef __OMBCONVERT_BIN__
                                , CurrId, CurrRate, CurrSymb
                            #endif // __OMBCONVERT_BIN__
                            )) NLin++;
					}}}
		else LastDate = wxDateTime::Today();

		FileData.Year = LastDate.GetYear();
		// Following code is a workaround for GetMonth() bug in Linux
		// http://forums.wxwidgets.org/viewtopic.php?t=25154&highlight=getmonth
		wxString temp;
		#ifdef __WXMSW__
			temp = LastDate.Format("%m", wxDateTime::Local).c_str();
		#else
			temp = LastDate.Format(L"%m", wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month, 10);

		Day = LastDate;

		// Read shopping list
		NSho = 0;
		if(database->TableExists(L"Shoplist")){
			Table = database->GetTable("select * from Shoplist order by item collate nocase;");
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				ind = Table.GetInt(2, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddShopItem(id, str, date)) NSho++;}}
	#endif // _OMB_USE_CIPHER

	Parsing = false;
}

bool TData::AddOper(int id, wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, long N, long c_index,
												bool hasLocation, double lat, double lon, long Curr_id, double Curr_rate, wxString Curr_Symb){
	if(T==toNULL||V.IsEmpty()||M.IsEmpty()) return false;
	//wxString Tot=FormDigits(Tot_Funds);

	if(Parsing){
		Lines[NLin].Id = id;
		Lines[NLin].IsDate = false;
		Lines[NLin].Date = D;
		Lines[NLin].Time = O;
		Lines[NLin].Type = T;
		Lines[NLin].Value = V;
		Lines[NLin].Reason = M;
		Lines[NLin].CategoryIndex = N;
		Lines[NLin].ContactIndex = c_index;
		Lines[NLin].Latitude = lat;
		Lines[NLin].Longitude = lon;

		Lines[NLin].CurrencyIndex = Curr_id;
		Lines[NLin].CurrencyRate = Curr_rate;
		Lines[NLin].CurrencySymbol = Curr_Symb;
	}
	else{
		M = DoubleQuote(M);
		if(T > 8) V = DoubleQuote(V);

		// Necessry to make sure the operation is appended to other entries of same day
		D.Set(D.GetDay(), D.GetMonth(), D.GetYear(), O.GetHour(), O.GetMinute());

		// Last date check
		bool found = false;
		if(NLin < 1){
			AddDate(-1, D, Tot_Funds);
			found = true;}
		else for(int i = NLin - 1; i >= 0; i--)if(IsDate(i)){
			if( Lines[i].Date.IsSameDate(D)){
				found = true;
				break;}}
		if (! found) AddDate(-1, D, Tot_Funds);

		if(! hasLocation){
			lat = ombInvalidLatitude;
			lon = ombInvalidLongitude;
		}

		#ifdef _OMB_USE_CIPHER
			wxString Sql = L"insert into Transactions values (NULL, 0, " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", (int) O.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", T) +
																			L", '" +
																			V +
																			L"', '" +
																			M +
																			L"', " +
																			wxString::Format(L"%ld", N) +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L", " +
																			ombFromCDouble(lat) +
																			L", " +
																			ombFromCDouble(lon) +

																			L", " +
																			wxString::Format(L"%ld", Curr_id) +
																			L", " +
																			ombFromCDouble(Curr_rate) +
																			L", \"" +
																			Curr_Symb +

																			L"\");";
			ExecuteUpdate(database, Sql.c_str());
		#else
			database->ExecuteUpdate(L"insert into Transactions values (NULL, 0, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", " +
																		wxString::Format(L"%d", (int) O.GetTicks()) +
																		L", " +
																		wxString::Format(L"%d", T) +
																		L", '" +
																		V +
																		L"', '" +
																		M +
																		L"', " +
																		wxString::Format(L"%ld", N) +

																		L", " +
																		wxString::Format(L"%ld", c_index) +

																		L", " +
																		ombFromCDouble(lat) +
																		L", " +
																		ombFromCDouble(lon) +

																		L", " +
																		wxString::Format(L"%ld", Curr_id) +
																		L", " +
																		ombFromCDouble(Curr_rate) +
																		L", \"" +
																		Curr_Symb +

																		L"\");");
		#endif // _OMB_USE_CIPHER

		FileData.Modified=true;
		ParseDatabase();
		//Sort();
	}
	return true;}

bool TData::AddDate(int id, wxDateTime D, double T){
	if(!D.IsValid())return false;

	if(! Parsing){
		D.ResetTime(); // New dates are with time set to midnight

		#ifdef __OPENMONEYBOX_EXE__
			int M, A;
			A = Day.GetYear();
			M = Day.GetMonth() + 1;
			if((A != FileData.Year) || (M != FileData.Month)){
				//#ifdef __OPENMONEYBOX_EXE__
					if(AutoConvert()){
						wxCommandEvent evt(wxEVT_NULL,0);
						frame->TextConvClick(evt);}
				//#endif // __OPENMONEYBOX_EXE__
				wxString Path=::wxPathOnly(FileData.FileName);
				#ifdef __WXMSW__
					wxString month=::wxString::Format("%02d",FileData.Month);
				#else
					wxString month=::wxString::Format(L"%02d",int(FileData.Month));
				#endif // __WXMSW__
				wxString File = GetDocPrefix();
				File+=L"_";
				#ifdef __WXMSW__
					File+=::wxString::Format("%d",FileData.Year);
				#else
					File+=::wxString::Format(L"%d",FileData.Year);
				#endif // __WXMSW__
				File += L"-" + month + L".omb";
				FileData.Modified = true;

				//#ifdef __OPENMONEYBOX_EXE__
					wxString master = Get_MasterDB();
					wxString trailname = ::wxString::Format(L"_%d_%02d", FileData.Year, int(FileData.Month));

					bool has_cat = Prepare_MasterDB(master, trailname);

					#ifdef _OMB_USE_CIPHER
						sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);

						sqlite3 *pBk;
						sqlite3_open((Path + L"/" + File).c_str(), &pBk);
						sqlite3_backup *pBackup;
						pBackup = sqlite3_backup_init(pBk, "main", database, "main");
						if( pBackup ){
							(void)sqlite3_backup_step(pBackup, -1);
							(void)sqlite3_backup_finish(pBackup);
						}
					#else
						database->ReleaseSavepoint(L"rollback");
						database->Backup(Path + L"/" + File);
					#endif // _OMB_USE_CIPHER

					Archive_inMaster(master, trailname, ! has_cat);

					#ifdef _OMB_USE_CIPHER
						ExecuteUpdate(database, "DELETE FROM Transactions");
						ExecuteUpdate(database, "VACUUM");
						ExecuteUpdate(database, "SAVEPOINT rollback;");
					#else
						database->ExecuteUpdate("DELETE FROM Transactions");
						database->Vacuum();
						database->Savepoint(L"rollback");
					#endif // _OMB_USE_CIPHER

				//#endif // __OPENMONEYBOX_EXE__

				//CategoryDB->Sort(false);

				NLin = 0;
				FileData.Month = int (D.GetMonth()) + 1;
				FileData.Year = D.GetYear();}
			#endif // __OPENMONEYBOX_EXE__
	}
	else	// Robustness code to avoid creation of duplicate date entries
		for(int i = NLin - 1; i >= 0; i--)if(IsDate(i))
			if(Lines[i].Date == D)
				#ifndef __OMBCONVERT_BIN__
					return true;
				#else
					return false;
				#endif // __OMBCONVERT_BIN__

	if(Parsing){
		Lines[NLin].Id= id;
		Lines[NLin].IsDate = true;
		Lines[NLin].Date = D;
		Lines[NLin].Time = wxInvalidDateTime;
		Lines[NLin].Type = toNULL;
		Lines[NLin].Value = FormDigits(T);
		Lines[NLin].Reason = wxEmptyString;
		Lines[NLin].CategoryIndex = -1;
		Lines[NLin].ContactIndex = -1;
		Lines[NLin].Latitude = ombInvalidLatitude;
		Lines[NLin].Longitude = ombInvalidLongitude;

		Lines[NLin].CurrencyIndex = -1;
		Lines[NLin].CurrencyRate = 1;
		Lines[NLin].CurrencySymbol = wxEmptyString;
	}
	else
		#ifdef _OMB_USE_CIPHER
      #ifdef __OPENSUSE__
        {
          wxString Sql = L"insert into Transactions values (NULL, 1, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", 0, 0, '" +
																		ombFromCDouble(T) +
																		L"', 0, -1, -1, " +
																		wxString::Format(L"%d", (int) ombInvalidLatitude) +
																		L", " +
																		wxString::Format(L"%d", (int) ombInvalidLongitude) +

																		+ L", -1, 1, \"\"" +

																		L");";
          ExecuteUpdate(database, Sql.c_str());
        }
      #else
        ExecuteUpdate(database, L"insert into Transactions values (NULL, 1, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", 0, 0, '" +
																		ombFromCDouble(T) +
																		L"', 0, -1, -1, " +
																		wxString::Format(L"%d", (int) ombInvalidLatitude) +
																		L", " +
																		wxString::Format(L"%d", (int) ombInvalidLongitude) +

																		+ L", -1, 1, \"\"" +

																		L");");
      #endif // __OPENSUSE__
		#else
			database->ExecuteUpdate(L"insert into Transactions values (NULL, 1, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", 0, 0, '" +
																		ombFromCDouble(T) +
																		L"', 0, -1, -1, " +
																		wxString::Format(L"%d", (int) ombInvalidLatitude) +
																		L", " +
																		wxString::Format(L"%d", (int) ombInvalidLongitude) +

																		+ L", -1, 1, \"\"" +

																		L");");
	#endif // _OMB_USE_CIPHER

  FileData.Modified=true;
  return true;}

bool TData::FindContact(long contact_id){
		bool contact_exists = false;
		#ifdef _OMB_USE_CIPHER
			bool table_existed = TableExists(L"Contacts", wxEmptyString, database);
		#else
			bool table_existed = database->TableExists(L"Contacts");
		#endif // _OMB_USE_CIPHER
		if(table_existed){
			wxSQLite3Table ContactsTable;
			#ifdef _OMB_USE_CIPHER
        #ifdef __OPENSUSE__
          wxString Sql = L"select status from Contacts where mobile_id = " +
															wxString::Format(L"%ld", contact_id) +
															L";";
          ContactsTable = GetTable(database, Sql.c_str());
        #else
          ContactsTable = GetTable(database, L"select status from Contacts where mobile_id = " +
															wxString::Format(L"%ld", contact_id) +
															L";");
        #endif // __OPENSUSE__
			#else
				ContactsTable = database->GetTable(L"select status from Contacts where mobile_id = " +
															wxString::Format(L"%ld", contact_id) +
															L";");
			#endif // _OMB_USE_CIPHER

			if(ContactsTable.GetRowCount() > 0){
				ContactsTable.SetRow(0);
				contact_exists = (ContactsTable.GetInt(0) > 0);
			}
		}

		return contact_exists;
}

bool TData::IsDate(int R){
  return Lines[R].IsDate;
}

#if defined (__OMBCONVERT_BIN__) && defined (_OMB_USE_CIPHER)
	bool TData::OpenDatabase(wxString File, wxString pwd){
#else
	bool TData::OpenDatabase(wxString File){
#endif // defined

	bool file_exist = ::wxFileExists(File);
	wxString Name;

	#ifdef _OMB_USE_CIPHER
	#ifdef __OMBCONVERT_BIN__
		//wxString pwd = wxEmptyString;
	#else
		wxString pwd = GetKey();
		wxString pwd_archive = GetKey(true);
	#endif // __OMBCONVERT_BIN__
	#endif // _OMB_USE_CIPHER

	#ifndef __OMBCONVERT_BIN__
		if(file_exist){
			#ifdef _OMB_USE_CIPHER
				if(! CheckAndPromptForConversion(File, false, pwd)){
			#else
				if(! CheckAndPromptForConversion(File, false)){
			#endif // _OMB_USE_CIPHER
				Parsing = false;
				return false;}}

		wxString ArchiveName = Get_MasterDB();

		bool master_exist = ::wxFileExists(ArchiveName);

    if(master_exist){
    	#ifdef _OMB_USE_CIPHER
    		if(! CheckAndPromptForConversion(ArchiveName, true, pwd_archive)){
			#else
	    	if(! CheckAndPromptForConversion(ArchiveName, true)){
			#endif // _OMB_USE_CIPHER
			Parsing = false;
			return false;}}
	#endif // __OMBCONVERT_BIN__

	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Opening database...");
	#endif // __OPENMONEYBOX_EXE__

	#ifdef _OMB_USE_CIPHER
		int result = -1;
		int tryno = 1;

		IsEncrypted = IsEncryptedDB(File, pwd, wxEmptyString, true);
		#ifndef __OMBCONVERT_BIN__
			if(master_exist) IsEncrypted_master = IsEncryptedDB(ArchiveName, pwd_archive, wxEmptyString, true);
		#endif // __OMBCONVERT_BIN__

		#ifdef __OPENSUSE__
      sqlite3_open(File.c_str(), &database);
		#else
      sqlite3_open(File, &database);
    #endif // __OPENSUSE__

		while (tryno < 4)
		{
			try
			{
				if(IsEncrypted){
          #ifdef __OPENSUSE__
            sqlite3_key(database, pwd.c_str(), pwd.length());
          #else
            sqlite3_key(database, pwd, pwd.length());
          #endif // __OPENSUSE__
					int rc = sqlite3_exec(database, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
					switch(rc){
						case SQLITE_OK:
						result = 1;
						break;

					case SQLITE_NOTADB:
						#ifndef __OMBCONVERT_BIN__
							PasswordPrompt(database, false);
						#endif // __OMBCONVERT_BIN__
						break;

						//default:
					}

				}
				else result = 1;

			}
			catch(exception& e)
			{
				//
			}

			if(result == 1) break;
			tryno++;
		}
	#else
		database->Open(File, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
	#endif // _OMB_USE_CIPHER

	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Database loaded.");
	#endif // __OPENMONEYBOX_EXE__

	if(file_exist){
		FileData.FileName = File;
		::wxFileName::SplitPath(File, NULL, NULL, &Name, NULL, wxPATH_NATIVE);
		FileData.FileView = Name;

		#ifndef __OMBCONVERT_BIN__
			// Check if present Android backup to be archived in master db
			// Read backup entry
			wxSQLite3Table Table;
			#ifdef _OMB_USE_CIPHER
				wxString Sql;

				#ifdef __OPENSUSE__
					Sql = L"select data from Information where id = " +
																		wxString::Format(L"%d", dbMeta_mobile_export) +
																		L";";
					Table = GetTable(database, Sql.c_str());
				#else
					Table = GetTable(database, L"select data from Information where id = " +
																		wxString::Format(L"%d", dbMeta_mobile_export) +
																		L";");
				#endif // __OPENSUSE__
			#else
				Table = database->GetTable(L"select data from Information where id = " +
																	wxString::Format(L"%d", dbMeta_mobile_export) +
																	L";");
			#endif // _OMB_USE_CIPHER
			Table.SetRow(0);
			wxString archive = Table.GetString(0, wxEmptyString);

			if(! archive.IsEmpty()){
				wxString Path, backup_file;
				::wxFileName::SplitPath(File, &Path, NULL, NULL, wxPATH_NATIVE);

				#ifdef __WXGTK__
					backup_file = Path + L"/_" + archive;
				#elif defined ( __WXMSW__ )
					backup_file = Path + "\\_" + archive;
				#endif // __WXGTK__

				if(::wxFileExists(backup_file)){
					wxString master = Get_MasterDB();
					wxString trailname = L"_" + archive;
					bool has_cat = Prepare_MasterDB(master, trailname);

					#ifdef _OMB_USE_CIPHER
						AttachMaster();
						#ifdef __OPENSUSE__
							Sql = wxString::Format(L"ATTACH DATABASE '%s' AS ext KEY '%s';", backup_file, pwd);
							ExecuteUpdate(database, Sql.c_str());
						#else
							ExecuteUpdate(database, wxString::Format(L"ATTACH DATABASE '%s' AS ext KEY '%s';", backup_file, pwd));
						#endif // __OPENSUSE__

						// Archive data in master database from external archive
						Sql = wxString::Format(is_transactions, L"ext.");
						ExecuteUpdate(database, Sql.c_str());
						#ifdef __OPENSUSE__
							Sql = wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM ext.Funds%s;", trailname, trailname);
							ExecuteUpdate(database, Sql.c_str());
							Sql = wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM ext.Credits%s;", trailname, trailname);
							ExecuteUpdate(database, Sql.c_str());
							Sql = wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM ext.Debts%s;", trailname, trailname);
							ExecuteUpdate(database, Sql.c_str());
							Sql = wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM ext.Loans%s;", trailname, trailname);
							ExecuteUpdate(database, Sql.c_str());
							Sql = wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM ext.Borrows%s;", trailname, trailname);
							ExecuteUpdate(database, Sql.c_str());
						#else
							ExecuteUpdate(database, wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM ext.Funds%s;", trailname, trailname));
							ExecuteUpdate(database, wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM ext.Credits%s;", trailname, trailname));
							ExecuteUpdate(database, wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM ext.Debts%s;", trailname, trailname));
							ExecuteUpdate(database, wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM ext.Loans%s;", trailname, trailname));
							ExecuteUpdate(database, wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM ext.Borrows%s;", trailname, trailname));
						#endif // __OPENSUSE__
					#else
						database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS master;", master));
						database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS ext;", backup_file));

						// Archive data in master database from external archive
						database->ExecuteUpdate(wxString::Format(is_transactions, L"ext."));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM ext.Funds%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM ext.Credits%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM ext.Debts%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM ext.Loans%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM ext.Borrows%s;", trailname, trailname));
					#endif // _OMB_USE_CIPHER

					#ifdef _OMB_USE_CIPHER
						if(! has_cat) {
							Sql = L"INSERT INTO master.Categories SELECT * FROM ext.Categories;";
							ExecuteUpdate(database, Sql.c_str()/*, false*/);
						}
						Sql = L"DETACH DATABASE 'master';";
						ExecuteUpdate(database, Sql.c_str()/*, false*/);
						Sql = L"DETACH DATABASE 'ext';";
						ExecuteUpdate(database, Sql.c_str());

						#ifdef __OPENSUSE__
							Sql = L"update Information set data = \"\" where id = " +
																	wxString::Format(L"%d", dbMeta_mobile_export) +
																	L";";
							ExecuteUpdate(database, Sql.c_str());
						#else
							ExecuteUpdate(database, L"update Information set data = \"\" where id = " +
																	wxString::Format(L"%d", dbMeta_mobile_export) +
																	L";");
						#endif // __OPENSUSE__
					#else
						if(! has_cat) database->ExecuteUpdate(L"INSERT INTO master.Categories SELECT * FROM ext.Categories;");

						database->ExecuteUpdate(L"DETACH DATABASE 'master';");
						database->ExecuteUpdate(L"DETACH DATABASE 'ext';");

						database->ExecuteUpdate(L"update Information set data = \"\" where id = " +
																	wxString::Format(L"%d", dbMeta_mobile_export) +
																	L";");
					#endif // _OMB_USE_CIPHER

					::wxRemoveFile(backup_file);
				}
			}
		#endif // __OMBCONVERT_BIN__
	}

	#ifdef _OMB_USE_CIPHER
		else
      #ifdef __OPENSUSE__
        {
          wxString Sql = L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";";
          ExecuteUpdate(database, Sql.c_str());
        }
      #else
        ExecuteUpdate(database, L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";");
      #endif // __OPENSUSE__
		// Set restore savepoint
		ExecuteUpdate(database, "SAVEPOINT rollback;");

	#else
		else database->ExecuteUpdate(L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";");

		// Set restore savepoint
		database->Savepoint(L"rollback");
	#endif // _OMB_USE_CIPHER

	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Rollback savepoint set in database");
	#endif // __OPENMONEYBOX_EXE__

	// Write file metadata
	wxString metadata = L"OS: ";
	#ifdef __WXGTK__
		#ifndef __FREEBSD__
			metadata += wxGetLinuxDistributionInfo().Description;
		#else
			metadata += wxGetOsDescription();
		#endif // __FREEBSD__
	#elif defined ( __WXMSW__)
		metadata += wxGetOsDescription();
	#else
		metadata += L"unknown";
	#endif // __WXGTK__
	metadata += L"\n\n";
	#ifndef __OMBCONVERT_BIN__
		metadata += wxGetLibraryVersionInfo().ToString();
	#else
		metadata += _("Converted with ombconvert");
	#endif // __OMBCONVERT_BIN__

	#ifdef _OMB_USE_CIPHER
		if(! TableExists(L"Information", wxEmptyString, database)){
      #ifdef __OPENSUSE__
        ExecuteUpdate(database, cs_information.c_str());
      #else
        ExecuteUpdate(database, cs_information);
      #endif // __OPENSUSE__

			#ifdef __OPENSUSE__
				wxString Sql = L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');";
				ExecuteUpdate(database, Sql.c_str());
			#else
				ExecuteUpdate(database, L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');");
			#endif // __OPENSUSE__

			#ifdef __OPENSUSE__
        Sql = L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L", 'default');";
        ExecuteUpdate(database, Sql.c_str());
        Sql = L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_mobile_export) +
																L", '');";
        ExecuteUpdate(database, Sql.c_str());
			#else
        ExecuteUpdate(database, L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L", 'default');");
        ExecuteUpdate(database, L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_mobile_export) +
																L", '');");
      #endif // __OPENSUSE__
		}
		else
			#ifdef __OPENSUSE__
				{
					wxString Sql = L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";";
					ExecuteUpdate(database, Sql.c_str());
				}
			#else
				ExecuteUpdate(database, L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";");
			#endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Information")){
			database->ExecuteUpdate(cs_information);

			database->ExecuteUpdate(L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');");
			database->ExecuteUpdate(L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L", 'default');");
			database->ExecuteUpdate(L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_mobile_export) +
																L", '');");
		}
		else database->ExecuteUpdate(L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";");
	#endif // _OMB_USE_CIPHER

	// Set currency when if not present (added later in dbVersion 31)
	wxSQLite3Table currencyTable;
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      wxString Sql = L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_currency) +
																L";";
      currencyTable = GetTable(database, Sql.c_str());
    #else
      currencyTable = GetTable(database, L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_currency) +
																L";");
    #endif // __OPENSUSE__
	#else
		currencyTable = database->GetTable(L"select data from Information where id = " +
															wxString::Format(L"%d", dbMeta_currency) +
															L";");
	#endif // _OMB_USE_CIPHER
	if(currencyTable.GetRowCount() == 0){
		wxString curr = GetCurrencySymbol();
		#ifdef _OMB_USE_CIPHER
      #ifdef __OPENSUSE__
        Sql = L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_currency) +
																L", '" +
																curr +
																L"');";
        ExecuteUpdate(database, Sql.c_str());
      #else
        ExecuteUpdate(database, L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_currency) +
																L", '" +
																curr +
																L"');");
      #endif // __OPENSUSE__
		#else
			database->ExecuteUpdate(L"insert into Information values (" +
															wxString::Format(L"%d", dbMeta_currency) +
															L", '" +
															curr +
															L"');");
		#endif // _OMB_USE_CIPHER
	}

	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Database metadata updated");
	#endif // __OPENMONEYBOX_EXE__

	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      // Transaction table init
      if(! TableExists(L"Transactions", wxEmptyString, database)) ExecuteUpdate(database, cs_transactions.c_str());

      // Category table init
      if(! TableExists(L"Categories", wxEmptyString, database)) ExecuteUpdate(database, cs_categories.c_str());
    #else
      // Transaction table init
      if(! TableExists(L"Transactions", wxEmptyString, database)) ExecuteUpdate(database, cs_transactions);

      // Category table init
      if(! TableExists(L"Categories", wxEmptyString, database)) ExecuteUpdate(database, cs_categories);
    #endif // __OPENSUSE__
	#else
		// Transaction table init
		if(! database->TableExists(L"Transactions")) database->ExecuteUpdate(cs_transactions);

		// Category table init
		if(! database->TableExists(L"Categories")) database->ExecuteUpdate(cs_categories);
	#endif // _OMB_USE_CIPHER

	ParseDatabase();
	FileData.Modified = false;

	return true;}

bool TData::AddObject(TObjType T, int id, wxString N, wxString O, wxDateTime D, long c_index){
	if(N.IsEmpty() || O.IsEmpty())return false;
	if (D == invalidDate) return false;

	if(! Parsing){
		N = DoubleQuote(N);
		O = DoubleQuote(O);}

	switch(T){
		case toPre:
			if(Parsing){
				Lent[NLen].Id = id;
				Lent[NLen].Name = N;
				Lent[NLen].Object = O;
				Lent[NLen].Alarm = D;
				Lent[NLen].ContactIndex = c_index;
			}
			else{
				#ifdef _OMB_USE_CIPHER
          #ifdef __OPENSUSE__
            if(! TableExists(L"Loans", wxEmptyString, database)) ExecuteUpdate(database, cs_loans.c_str());
            wxString Sql = L"insert into Loans values (NULL, '" +
																			N +
																			L"', '" +
																			O +
																			L"', " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L");";
            ExecuteUpdate(database, Sql.c_str());
          #else
            if(! TableExists(L"Loans", wxEmptyString, database)) ExecuteUpdate(database, cs_loans);
            ExecuteUpdate(database, L"insert into Loans values (NULL, '" +
																			N +
																			L"', '" +
																			O +
																			L"', " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L");");
          #endif // __OPENSUSE__
				#else
					if(! database->TableExists(L"Loans")) database->ExecuteUpdate(cs_loans);
					database->ExecuteUpdate(L"insert into Loans values (NULL, '" +
																		N +
																		L"', '" +
																		O +
																		L"', " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +

																		L", " +
																		wxString::Format(L"%ld", c_index) +

																		L");");
				#endif // _OMB_USE_CIPHER
			}
			break;
		case toInP:
			if(Parsing){
				Borrowed[NBor].Id = id;
				Borrowed[NBor].Name = N;
				Borrowed[NBor].Object = O;
				Borrowed[NBor].Alarm = D;
				Borrowed[NBor].ContactIndex = c_index;
			}
			else{
				#ifdef _OMB_USE_CIPHER
          #ifdef __OPENSUSE__
            if(! TableExists(L"Borrows", wxEmptyString, database)) ExecuteUpdate(database, cs_borrows.c_str());
            wxString Sql = L"insert into Borrows values (NULL, '" +
																			N +
																			L"', '" +
																			O +
																			L"', " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L");";
            ExecuteUpdate(database, Sql.c_str());
          #else
            if(! TableExists(L"Borrows", wxEmptyString, database)) ExecuteUpdate(database, cs_borrows);
            ExecuteUpdate(database, L"insert into Borrows values (NULL, '" +
																			N +
																			L"', '" +
																			O +
																			L"', " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L");");
          #endif // __OPENSUSE__
				#else
					if(! database->TableExists(L"Borrows")) database->ExecuteUpdate(cs_borrows);
					database->ExecuteUpdate(L"insert into Borrows values (NULL, '" +
																		N +
																		L"', '" +
																		O +
																		L"', " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +

																		L", " +
																		wxString::Format(L"%ld", c_index) +

																		L");");
				#endif // _OMB_USE_CIPHER
			}
		}
	if(! Parsing){
		FileData.Modified = true;
		ParseDatabase();}
  return true;}

bool TData::AddValue(TTypeVal T, int id, wxString N, double V, long c_index){
	if(! Parsing){
		if(N.IsEmpty() || V == 0) return false;
		N = DoubleQuote(N);
	}

	bool E=false;
	int x;
	wxString DStr = ::wxString::FromCDouble(V, 2);

	switch(T){
		case tvFou:
			for(x = 0; x < NFun; x++)if(Funds[x].Name == N){
				Error(11,N);
				return false;}

			if(Parsing){
				Funds[NFun].Id = id;
				Funds[NFun].Name = N;
				Funds[NFun].Value = V;}
			else{
				#ifdef _OMB_USE_CIPHER
          #ifdef __OPENSUSE__
            if(! TableExists(L"Funds", wxEmptyString, database)) ExecuteUpdate(database, cs_funds.c_str());
            wxString Sql = L"insert into Funds values (NULL, '" +
																			N +
																			L"', " +
																			DStr +
																			L");";
            ExecuteUpdate(database, Sql.c_str());
          #else
            if(! TableExists(L"Funds", wxEmptyString, database)) ExecuteUpdate(database, cs_funds);
            ExecuteUpdate(database, L"insert into Funds values (NULL, '" +
																			N +
																			L"', " +
																			DStr +
																			L");");
          #endif // __OPENSUSE__
				#else
					if(! database->TableExists(L"Funds")) database->ExecuteUpdate(cs_funds);

					database->ExecuteUpdate(L"insert into Funds values (NULL, '" +
																		N +
																		L"', " +
																		DStr +
																		L");");
			#endif // _OMB_USE_CIPHER
			}

			break;
		case tvCre:
			for(x = 0; x < NCre; x++) if(Credits[x].Name == N){
				Credits[x].Value += V;
				ChangeFundValue(tvCre, Credits[x].Id, Credits[x].Value);
				E = true;
				break;}
			if(!E){
				if(Parsing){
					Credits[NCre].Id = id;
					Credits[NCre].Name = N;
					Credits[NCre].Value = V;
					Credits[NCre].ContactIndex = c_index;
				}
				else{
					#ifdef _OMB_USE_CIPHER
            #ifdef __OPENSUSE__
              if(! TableExists(L"Credits", wxEmptyString, database)) ExecuteUpdate(database, cs_credits.c_str());
              wxString Sql = L"insert into Credits values (NULL, '" +
																				N +
																				L"', " +
																				DStr +

																				L", " +
																				wxString::Format(L"%ld", c_index) +

																				L");";
              ExecuteUpdate(database, Sql.c_str());
            #else
              if(! TableExists(L"Credits", wxEmptyString, database)) ExecuteUpdate(database, cs_credits);
              ExecuteUpdate(database, L"insert into Credits values (NULL, '" +
																				N +
																				L"', " +
																				DStr +

																				L", " +
																				wxString::Format(L"%ld", c_index) +

																				L");");
            #endif // __OPENSUSE__
					#else
						if(! database->TableExists(L"Credits")) database->ExecuteUpdate(cs_credits);

						database->ExecuteUpdate(L"insert into Credits values (NULL, '" +
																			N +
																			L"', " +
																			DStr +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L");");
					#endif // _OMB_USE_CIPHER
				}
			}
			break;
		case tvDeb:
			for(x = 0; x < NDeb; x++) if(Debts[x].Name == N){
				Debts[x].Value += V;
				ChangeFundValue(tvDeb, Debts[x].Id, Debts[x].Value);
				E = true;
				break;}
			if(!E){
				if(Parsing){
					Debts[NDeb].Id = id;
					Debts[NDeb].Name = N;
					Debts[NDeb].Value = V;
					Debts[NDeb].ContactIndex = c_index;
				}
				else{
					#ifdef _OMB_USE_CIPHER
            #ifdef __OPENSUSE__
              if(! TableExists(L"Debts", wxEmptyString, database)) ExecuteUpdate(database, cs_debts.c_str());
              wxString Sql = L"insert into Debts values (NULL, '" +
																				N +
																				L"', " +
																				DStr +

																				L", " +
																				wxString::Format(L"%ld", c_index) +

																				L");";
              ExecuteUpdate(database, Sql.c_str());
            #else
              if(! TableExists(L"Debts", wxEmptyString, database)) ExecuteUpdate(database, cs_debts);
              ExecuteUpdate(database, L"insert into Debts values (NULL, '" +
																				N +
																				L"', " +
																				DStr +

																				L", " +
																				wxString::Format(L"%ld", c_index) +

																				L");");
            #endif // __OPENSUSE__
					#else
						if(! database->TableExists(L"Debts")) database->ExecuteUpdate(cs_debts);
						database->ExecuteUpdate(L"insert into Debts values (NULL, '" +
																			N +
																			L"', " +
																			DStr +

																			L", " +
																			wxString::Format(L"%ld", c_index) +

																			L");");
					#endif // _OMB_USE_CIPHER
				}
			}
			break;
		default:
			return false;}

	if(! Parsing){
		ParseDatabase();
		FileData.Modified=true;}
	return true;}

/*
void TData::CleanMaster(void){
	// TODO (igor#1#): To be implemented: ...
	//  - clean empty Credits tables
	//  - clean empty Debts tables
	//  - clean empty Loans tables
	//  - clean empty Borros tables
	//  - vacuum

}
*/

bool TData::ChangeFundValue(TTypeVal type, int id, double V){
	wxString table;
	switch(type){
		case tvFou:
			table = L"Funds";
			break;
		case tvCre:
			table = L"Credits";
			break;
		case tvDeb:
			table = L"Debts";
			break;
		default:
			table = wxEmptyString;}

	wxString DStr = ::wxString::FromCDouble(V, 2);

	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      wxString Sql = L"update " + table + L" set value = " +
																DStr +
																L" where id = " +
																wxString::Format(L"%d", id) +
																L";";
      ExecuteUpdate(database, Sql.c_str());
    #else
      ExecuteUpdate(database, L"update " + table + L" set value = " +
																DStr +
																L" where id = " +
																wxString::Format(L"%d", id) +
																L";");
    #endif // __OPENSUSE__
	#else
		database->ExecuteUpdate(L"update " + table + L" set value = " +
															DStr +
															L" where id = " +
															wxString::Format(L"%d", id) +
															L";");
	#endif // _OMB_USE_CIPHER

	ParseDatabase();
	FileData.Modified=true;
	return true;}

#ifndef __OMBCONVERT_BIN__
	void TData::XMLExport(wxString F1, wxString F2){
		bool FirstDate=false;	// set when first date is inserted
		wxString App;	// line to append
		double cur; // value storage
		//wxArrayString RemarkTokens;
		// path selection
		wxString dir = wxDirSelector(_("Select a folder"),GetBilDocPath());
		if(dir.IsEmpty())return;
		::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		// Folder creation
		#ifdef __WXMSW__
			dir = dir + L"\\" + GetDocPrefix() + L"_" + ::wxString::Format("%d", FileData.Year) + L"-"
				+ ::wxString::Format("%02d", FileData.Month);
		#else
			dir = dir + L"/" + GetDocPrefix() + L"_" + ::wxString::Format(L"%d", FileData.Year);
			dir = dir + L"-" + ::wxString::Format(L"%02d", int(FileData.Month));
		#endif // __WXMSW__
		if(!wxDirExists(dir))if(!wxMkdir(dir,0777))return;
		// xsl template copy
		#ifdef __WXGTK__
			wxString xsltempl = GetDataDir();
		#elif defined (__WXMSW__)
			wxString xsltempl = GetInstallationPath()+L"\\data";
		#elif defined (__WXMAC__)
			wxString xsltempl = AppDir + L"/Resources";
		#endif // __WXGTK__
		#if defined (__WXMSW__) || defined (__WXMAC__)
			wxLanguage Lan=FindLang();
		#endif
		switch(Lan){
			case wxLANGUAGE_ITALIAN:
				#ifndef __WXMSW__
					xsltempl+=L"/it/";
				#else
					xsltempl+=L"\\it\\";
				#endif // __WXMSW__
				break;
			default:
				#ifndef __WXMSW__
					xsltempl+=L"/en/";
				#else
					xsltempl+=L"\\en\\";
				#endif // __WXMSW__
		}
		xsltempl += L"ombexport.xsl";
		wxCopyFile(xsltempl, dir + L"/ombexport.xsl");
		// xml document creation
		wxString xmlfile = dir + L"/" + GetDocPrefix() + L".xml";
		wxTextFile *file = new wxTextFile(xmlfile);
		file->Create();
		file->AddLine(L"<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>");
		#ifdef __WXMSW__
			file->AddLine(L"<!--File Created By OpenMoneyBox " + ShortVersion + L" on " + wxDateTime::Today().Format(L"%d/%m/%Y") + L" " + wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#else
			file->AddLine(L"<!--File Created By OpenMoneyBox " + ShortVersion + L" on " + wxDateTime::Today().Format(L"%d/%02m/%Y") + L" "+wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#endif // __WXMSW__
		file->AddLine(L"<?xml-stylesheet type=\"text/xsl\" href=\"ombexport.xsl\"?>");
		file->AddLine(L"<groups><headers>");
		wxString Header=_("Monthly report of");
		#ifdef __WXMSW__
			file->AddLine(L"<title heading=\""+Header+L" \" month=\""+IntToMonth(FileData.Month)+L" "+::wxString::Format("%d",FileData.Year)+L"\" />");
		#else
			file->AddLine(L"<title heading=\""+Header+L" \" month=\""+IntToMonth(FileData.Month)+L" "+::wxString::Format(L"%d",FileData.Year)+L"\" />");
		#endif // __WXMSW__
		file->AddLine(L"</headers><days>");
		// line parsing
		for(int i = 0; i < NLin; i++){
			App=wxEmptyString;
			if(IsDate(i)){
				if(!FirstDate)FirstDate=true;
				else	file->AddLine(L"</day>");
				App+=L"<day date=\"";
				#ifdef __WXMSW__
					App+=Lines[i].Date.Format("%a %d/%m/%Y",wxDateTime::Local);
				#else
					App+=Lines[i].Date.Format(L"%a %d/%m/%Y",wxDateTime::Local);
				#endif // __WXMSW__
				App+=L"\" total=\"";
				Lines[i].Value.ToDouble(&cur);
				App+=FormDigits(cur);
				App+=L"\">";
				file->AddLine(App);}
			else{
				App+=L"<item time=\"";
				#ifdef __WXMSW__
					App+=Lines[i].Time.Format("%H:%M",wxDateTime::Local);
				#else
					App+=Lines[i].Time.Format(L"%H:%M",wxDateTime::Local);
				#endif // __WXMSW__
				App+=L"\" type=\"";
				switch(Lines[i].Type){
					case toGain:
					App+=L"1";
					break;
					case toExpe:
					App+=L"2";
					break;
					case toSetCre:
					App+=L"3";
					break;
					case toRemCre:
					App+=L"4";
					break;
					case toConCre:
					App+=L"5";
					break;
					case toSetDeb:
					App+=L"6";
					break;
					case toRemDeb:
					App+=L"7";
					break;
					case toConDeb:
					App+=L"8";
					break;
					case toGetObj:
					App+=L"9";
					break;
					case toGivObj:
					App+=L"10";
					break;
					case toLenObj:
					App+=L"11";
					break;
					case toBakObj:
					App+=L"12";
					break;
					case toBorObj:
					App+=L"13";
					break;
					case toRetObj:
					App+=L"14";
					break;
					default:
					App+=L"0";
					break;}
				App += L"\" value=\"";
				if(Lines[i].Type < 9){
					Lines[i].Value.ToDouble(&cur);
					App += FormDigits(cur);}
				else{
					if(Lines[i].Value.Find(L"\"") != wxNOT_FOUND){
						wxString vals = Lines[i].Value;
						vals.Replace(L"\"", L"[OMB_ESCAPEQUOTES]", true);	// intermediate string to be processed by SubstSpecialChars
						App += vals;
					}
					else App += Lines[i].Value;
				}

				App += L"\" reason=\"";

				if(Lines[i].Reason.Find(L"\"") != wxNOT_FOUND){
					wxString reas = Lines[i].Reason;
					reas.Replace(L"\"", L"[OMB_ESCAPEQUOTES]", true);	// intermediate string to be processed by SubstSpecialChars
					App += reas;
				}
				else App+=Lines[i].Reason;

				App += L"\" category=\"";
				App = SubstSpecialChars(App);
				file->AddLine(App);
				if(Lines[i].CategoryIndex != -1)
					for(int j = 0; j < NCat; j++)if(Categories[j].Id == Lines[i].CategoryIndex){
						file->AddLine(Categories[j].Name);
						break;}
				file->AddLine(L"\"/>");}}
		// file completion
		file->AddLine(L"</day></days></groups>");
		file->Write(wxTextFileType_Unix,wxConvUTF8);
		// file closure
		file->Close();
		delete file;

		// Logo picture copy
		#ifdef __WXGTK__
			xsltempl = GetDataDir() + L"logo.png";
		#elif defined (__WXMSW__)
			xsltempl = GetInstallationPath() + L"\\data\\logo.png";
		#elif defined (__WXMAC__)
			xsltempl = AppDir + L"/Resources/logo.png";
		#endif	// __WXGTK__
		wxCopyFile(xsltempl,dir+L"/logo.png");
		// Chart copy
		wxCopyFile(F1, dir+L"/chart1.png");
		wxCopyFile(F2, dir+L"/chart2.png");
		::wxEndBusyCursor();}
#endif // __OMBCONVERT_BIN__

#ifndef __OMBCONVERT_BIN__
void TData::Archive_inMaster(wxString master, wxString trailname, bool archive_categories){
	// master:		database to attach and archive data in
	// trailname:	suffix for tables
	// archive_categories:	true to archive also categories

	#ifdef _OMB_USE_CIPHER
		AttachMaster();

		// Archive data in master database
		#ifdef __OPENSUSE__
			wxString Sql = wxString::Format(is_transactions.c_str(), wxEmptyString);
			ExecuteUpdate(database, Sql.c_str());
			Sql = wxString::Format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			Sql = wxString::Format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			Sql = wxString::Format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			Sql = wxString::Format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			Sql = wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname);
			ExecuteUpdate(database, Sql.c_str());
		#else
			ExecuteUpdate(database, wxString::Format(is_transactions, wxEmptyString));
			ExecuteUpdate(database, wxString::Format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname));
			ExecuteUpdate(database, wxString::Format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname));
			ExecuteUpdate(database, wxString::Format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname));
			ExecuteUpdate(database, wxString::Format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname));
			ExecuteUpdate(database, wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname));
		#endif // __OPENSUSE__
	#else
		database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS master;", master));

		// Archive data in master database
		database->ExecuteUpdate(wxString::Format(is_transactions, wxEmptyString));
		database->ExecuteUpdate(wxString::Format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname));
		database->ExecuteUpdate(wxString::Format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname));
		database->ExecuteUpdate(wxString::Format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname));
		database->ExecuteUpdate(wxString::Format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname));
		database->ExecuteUpdate(wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname));
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		if(archive_categories) ExecuteUpdate(database, "INSERT INTO master.Categories SELECT * FROM Categories;");
		ExecuteUpdate(database, "DETACH DATABASE 'master';");
	#else
		if(archive_categories) database->ExecuteUpdate("INSERT INTO master.Categories SELECT * FROM Categories;");
		database->ExecuteUpdate("DETACH DATABASE 'master';");
	#endif // _OMB_USE_CIPHER
}
#endif // __OMBCONVERT_BIN__

#ifndef __OMBCONVERT_BIN__
bool TData::Prepare_MasterDB(wxString dbPath, wxString trailname){
	// master:		database to attach and archive data in
	// trailname:	suffix for tables

	bool file_exist = ::wxFileExists(dbPath);

	#ifdef _OMB_USE_CIPHER
		wxString pwd_archive = GetKey(true);

		sqlite3 *db;
		#ifdef __OPENSUSE__
      sqlite3_open(dbPath.c_str(), &db);
      sqlite3_key(db, pwd_archive.c_str(), pwd_archive.length());
		#else
      sqlite3_open(dbPath, &db);
      sqlite3_key(db, pwd_archive, pwd_archive.length());
    #endif // __OPENSUSE__

		int rc = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
		switch(rc){
			case SQLITE_OK:
				//result = 1;
				break;

			case SQLITE_NOTADB:
				PasswordPrompt(db, true);
				break;

			//default:
		}

	#else
		wxSQLite3Database *db = new wxSQLite3Database();
		db->Open(dbPath, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      if(! file_exist){
        wxString Sql = L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";";
        ExecuteUpdate(db, Sql.c_str());
      }
    #else
      if(! file_exist) ExecuteUpdate(db, L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";");
    #endif // __OPENSUSE__
	#else
		if(! file_exist) db->ExecuteUpdate(L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";");
	#endif // _OMB_USE_CIPHER

	// Write file metadata
	wxString metadata = L"OS: ";
	#ifdef __WXGTK__
		#ifndef __FREEBSD__
			metadata += wxGetLinuxDistributionInfo().Description;
		#else
			metadata += wxGetOsDescription();
		#endif // __FREEBSD__
	#elif defined ( __WXMSW__)
		metadata += wxGetOsDescription();
	#else
		metadata += L"unknown";
	#endif // __WXGTK__
	metadata += L"\n\n";
	#ifndef __OMBCONVERT_BIN__
		metadata += wxGetLibraryVersionInfo().ToString();
	#else
		metadata += _("Converted with ombconvert");
	#endif // __OMBCONVERT_BIN__

	#ifdef _OMB_USE_CIPHER
		if(! TableExists(L"Information", "master", db)){
      #ifdef __OPENSUSE__
        ExecuteUpdate(db, cs_information.c_str());
        wxString Sql = L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');";
        ExecuteUpdate(db, Sql.c_str());
      #else
        ExecuteUpdate(db, cs_information);
        ExecuteUpdate(db, L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');");
      #endif // __OPENSUSE__
		}
		else
      #ifdef __OPENSUSE__
        {
          wxString Sql = L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";";
          ExecuteUpdate(db, Sql.c_str());
        }
      #else
        ExecuteUpdate(db, L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";");
      #endif // __OPENSUSE__
	#else
		if(! db->TableExists(L"Information")){
			db->ExecuteUpdate(cs_information);
			db->ExecuteUpdate(L"insert into Information values (" +
															wxString::Format(L"%d", dbMeta_application_info) +
															L", '" +
															metadata +
															L"');");
		}
		else db->ExecuteUpdate(L"update Information set data = \"" +
															metadata +
															L"\" where id = " +
															wxString::Format(L"%d", dbMeta_application_info) +
															L";");
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      // Transaction table init
      if(! TableExists(L"Transactions", "master", db)) ExecuteUpdate(db, cs_transactions.c_str());
    #else
      // Transaction table init
      if(! TableExists(L"Transactions", "master", db)) ExecuteUpdate(db, cs_transactions);
    #endif // __OPENSUSE__
	#else
		// Transaction table init
		if(! db->TableExists(L"Transactions")) db->ExecuteUpdate(cs_transactions);
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		// Category table init
		bool master_had_categories = TableExists(L"Categories", "master", db);
	#else
		// Category table init
		bool master_had_categories = db->TableExists(L"Categories");
	#endif // _OMB_USE_CIPHER

	if(master_had_categories){
		#ifdef _OMB_USE_CIPHER
			if(! TableExists("Categories" + trailname, "master", db))
				#ifdef __OPENSUSE__
					{
						wxString Sql = wxString::Format(cs_categories_master, trailname);
						ExecuteUpdate(db, Sql.c_str());
					}
				#else
					ExecuteUpdate(db, wxString::Format(cs_categories_master, trailname));
				#endif // __OPENSUSE__
		#else
			if(! db->TableExists("Categories" + trailname)) db->ExecuteUpdate(wxString::Format(cs_categories_master, trailname));
		#endif // _OMB_USE_CIPHER
	}
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      else ExecuteUpdate(db, cs_categories.c_str());
    #else
      else ExecuteUpdate(db, cs_categories);
    #endif // __OPENSUSE__
	#else
		else db->ExecuteUpdate(cs_categories);
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		#ifdef __OPENSUSE__
			wxString Sql;
			if(! TableExists("Funds" + trailname, "master", db)){
				Sql = wxString::Format(cs_funds_master, trailname);
				ExecuteUpdate(db, Sql.c_str());
			}
			if(! TableExists("Credits" + trailname, "master", db)){
				Sql = wxString::Format(cs_credits_master, trailname);
				ExecuteUpdate(db, Sql.c_str());
			}
			if(! TableExists("Debts" + trailname, "master", db)){
				Sql = wxString::Format(cs_debts_master, trailname);
				ExecuteUpdate(db, Sql.c_str());
			}
			if(! TableExists("Loans" + trailname, "master", db)){
				Sql = wxString::Format(cs_loans_master, trailname);
				ExecuteUpdate(db, Sql.c_str());
			}
			if(! TableExists("Borrows" + trailname, "master", db)){
				Sql = wxString::Format(cs_borrows_master, trailname);
				ExecuteUpdate(db, Sql.c_str());
			}
		#else
			if(! TableExists("Funds" + trailname, "master", db)) ExecuteUpdate(db, wxString::Format(cs_funds_master, trailname));
			if(! TableExists("Credits" + trailname, "master", db)) ExecuteUpdate(db, wxString::Format(cs_credits_master, trailname));
			if(! TableExists("Debts" + trailname, "master", db)) ExecuteUpdate(db, wxString::Format(cs_debts_master, trailname));
			if(! TableExists("Loans" + trailname, "master", db)) ExecuteUpdate(db, wxString::Format(cs_loans_master, trailname));
			if(! TableExists("Borrows" + trailname, "master", db)) ExecuteUpdate(db, wxString::Format(cs_borrows_master, trailname));
		#endif // __OPENSUSE__
	#else
		if(! db->TableExists("Funds" + trailname)) db->ExecuteUpdate(wxString::Format(cs_funds_master, trailname));
		if(! db->TableExists("Credits" + trailname)) db->ExecuteUpdate(wxString::Format(cs_credits_master, trailname));
		if(! db->TableExists("Debts" + trailname)) db->ExecuteUpdate(wxString::Format(cs_debts_master, trailname));
		if(! db->TableExists("Loans" + trailname)) db->ExecuteUpdate(wxString::Format(cs_loans_master, trailname));
		if(! db->TableExists("Borrows" + trailname)) db->ExecuteUpdate(wxString::Format(cs_borrows_master, trailname));
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		sqlite3_close(db);
	#else
		db->Close();
	#endif // _OMB_USE_CIPHER

	return master_had_categories;
}
#endif // __OMBCONVERT_BIN__

#ifndef __OMBCONVERT_BIN__
	void TData::XMLExport_archive(wxString F1, wxString F2, wxDateTime date){
		bool FirstDate=false;	// set when first date is inserted
		wxString App;	// line to append
		double cur; // value storage
		// path selection
		wxString dir = wxDirSelector(_("Select a folder"),GetBilDocPath());
		if(dir.IsEmpty())return;
		::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		// Folder creation
		#ifdef __WXMSW__
			dir = dir + L"\\" + GetDocPrefix() + L"_" + date.Format(L"%Y-%m");
		#else
			dir = dir + L"/" + GetDocPrefix() + L"_" + date.Format(L"%Y-%m");
		#endif // __WXMSW__
		if(!wxDirExists(dir))if(!wxMkdir(dir,0777))return;
		// xsl template copy
		#ifdef __WXMSW__
			wxString xsltempl=GetInstallationPath()+L"\\data";
		#else
			wxString xsltempl=GetDataDir();
		#endif // __WXMSW__
		#if defined (__WXMSW__) || defined (__WXMAC__)
			wxLanguage Lan=FindLang();
		#endif
		switch(Lan){
			case wxLANGUAGE_ITALIAN:
				#ifndef __WXMSW__
					xsltempl+=L"/it/";
				#else
					xsltempl+=L"\\it\\";
				#endif // __WXMSW__
				break;
			default:
				#ifndef __WXMSW__
					xsltempl+=L"/en/";
				#else
					xsltempl+=L"\\en\\";
				#endif // __WXMSW__
		}
		xsltempl += L"ombexport.xsl";
		wxCopyFile(xsltempl, dir + L"/ombexport.xsl");
		// xml document creation
		wxString xmlfile = dir + L"/" + GetDocPrefix() + L".xml";
		wxTextFile *file=new wxTextFile(xmlfile);
		file->Create();
		file->AddLine(L"<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>");
		#ifdef __WXMSW__
			file->AddLine(L"<!--File Created By OpenMoneyBox " + ShortVersion + L" on " + wxDateTime::Today().Format(L"%d/%m/%Y") + L" " + wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#else
			file->AddLine(L"<!--File Created By OpenMoneyBox " + ShortVersion + L" on " + wxDateTime::Today().Format(L"%d/%02m/%Y") + L" " + wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#endif // __WXMSW__
		file->AddLine(L"<?xml-stylesheet type=\"text/xsl\" href=\"ombexport.xsl\"?>");
		file->AddLine(L"<groups><headers>");
		wxString Header=_("Monthly report of");
		#ifdef __WXMSW__
			file->AddLine(L"<title heading=\""+Header+L" \" month=\""+IntToMonth(FileData.Month)+L" "+::wxString::Format("%d",FileData.Year)+L"\" />");
		#else
			file->AddLine(L"<title heading=\"" + Header + L" \" month=\"" + IntToMonth(/*FileData.Month*/date.GetMonth() + 1) + L" " +
											::wxString::Format(L"%d", /*FileData.Year*/date.GetYear()) + L"\" />");
		#endif // __WXMSW__
		file->AddLine(L"</headers><days>");

		int min_date, max_date;
		wxDateTime min_datetime, max_datetime, d_date, time;
		wxSQLite3Table transaction_table;
		wxString value_string, reas;

		min_datetime = date;
		min_datetime.SetDay(1);
		min_datetime.SetHour(0);
		min_datetime.SetMinute(0);
		min_datetime.SetSecond(0);
		min_datetime.SetMillisecond(0);
		min_date = min_datetime.GetTicks();

		max_datetime = date;
		max_datetime.SetToLastMonthDay(max_datetime.GetMonth(), max_datetime.GetYear());
		max_datetime.SetHour(23);
		max_datetime.SetMinute(59);
		max_datetime.SetSecond(59);
		max_datetime.SetMillisecond(999);
		max_date = max_datetime.GetTicks();

		#ifdef _OMB_USE_CIPHER
      #ifdef __OPENSUSE__
        wxString Sql = ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d;", min_date, max_date);
        transaction_table = GetTable(database, Sql.c_str());
      #else
        transaction_table = GetTable(database, ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d;", min_date, max_date));
      #endif // __OPENSUSE__
		#else
			transaction_table = database->GetTable(::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d;", min_date, max_date));
		#endif // _OMB_USE_CIPHER

		// line parsing
		for(int i = 0; i < transaction_table.GetRowCount(); i++){
			transaction_table.SetRow(i);
			App = wxEmptyString;
			if(transaction_table.GetInt(L"isdate", false)){
				if(! FirstDate) FirstDate = true;
				else	file->AddLine(L"</day>");
				App += L"<day date=\"";
				d_date = wxDateTime((time_t) transaction_table.GetInt(2, 0));
				#ifdef __WXMSW__
					App += d_date.Format("%a %d/%m/%Y", wxDateTime::Local);
				#else
					App += d_date.Format(L"%a %d/%m/%Y", wxDateTime::Local);
				#endif // __WXMSW__
				App += L"\" total=\"";
				value_string = transaction_table.GetString(5, wxEmptyString);
				value_string.ToCDouble(&cur);
				App += FormDigits(cur);
				App += L"\">";
				file->AddLine(App);}
			else{
				App+=L"<item time=\"";
				#ifdef __WXMSW__
					App += time.Format("%H:%M",wxDateTime::Local);
				#else
					time = wxDateTime((time_t) transaction_table.GetInt(3, 0));
					App += time.Format(L"%H:%M",wxDateTime::Local);
				#endif // __WXMSW__
				App += L"\" type=\"";
				switch(transaction_table.GetInt(4, 0)){
					case toGain:
						App += L"1";
						break;
					case toExpe:
						App += L"2";
						break;
					case toSetCre:
						App += L"3";
						break;
					case toRemCre:
						App += L"4";
						break;
					case toConCre:
						App += L"5";
						break;
					case toSetDeb:
						App += L"6";
						break;
					case toRemDeb:
						App += L"7";
						break;
					case toConDeb:
						App += L"8";
						break;
					case toGetObj:
						App += L"9";
						break;
					case toGivObj:
						App += L"10";
						break;
					case toLenObj:
						App += L"11";
						break;
					case toBakObj:
						App += L"12";
						break;
					case toBorObj:
						App += L"13";
						break;
					case toRetObj:
						App += L"14";
						break;
					default:
						App += L"0";
						break;}
				App += L"\" value=\"";
				value_string = transaction_table.GetString(5, wxEmptyString);
				if(transaction_table.GetInt(4, 0) < 9){
					value_string.ToCDouble(&cur);
					App += FormDigits(cur);}
				else{
					if(value_string.Find(L"\"") != wxNOT_FOUND)
						value_string.Replace(L"\"", L"[OMB_ESCAPEQUOTES]", true);	// intermediate string to be processed by SubstSpecialChars
					App += value_string;
				}
				App += L"\" reason=\"";

				reas = transaction_table.GetString(6, wxEmptyString);
				if(reas.Find(L"\"") != wxNOT_FOUND)
					reas.Replace(L"\"", L"[OMB_ESCAPEQUOTES]", true);	// intermediate string to be processed by SubstSpecialChars
				App += reas;

				App += L"\" category=\"";
				App = SubstSpecialChars(App);
				file->AddLine(App);
				if(transaction_table.GetInt(7, 0) != -1)
					for(int j = 0; j < NCat; j++)if(Categories[j].Id == transaction_table.GetInt(7, 0)){
						file->AddLine(Categories[j].Name);
						break;}
				file->AddLine(L"\"/>");}}
		// file completion
		file->AddLine(L"</day></days></groups>");
		file->Write(wxTextFileType_Unix,wxConvUTF8);
		// file closure
		file->Close();
		delete file;
		// Logo picture copy
		#ifndef __WXMSW__
			xsltempl=GetDataDir()+L"logo.png";
		#else
			xsltempl=GetInstallationPath()+L"\\data\\logo.png";
		#endif
		wxCopyFile(xsltempl,dir+L"/logo.png");
		// Chart copy
		wxCopyFile(F1, dir+L"/chart1.png");
		wxCopyFile(F2, dir+L"/chart2.png");
		::wxEndBusyCursor();}
#endif // __OMBCONVERT_BIN__

bool TData::UpdateMatters(TOpType T){
	bool Ex;
  int i;
  unsigned int j;
  MattersBuffer->Clear();
  for(i = 0; i < NLin; i++)if(Lines[i].Type == T){
		Ex = false;
		for(j = 0; j < MattersBuffer->Count();j ++)if(MattersBuffer->Item(j).CmpNoCase(Lines[i].Reason) == 0){
			Ex = true;
			break;}
		if(! Ex)MattersBuffer->Add(Lines[i].Reason);}
	MattersBuffer->Sort();
	return true;}

void TData::ChangeTransactionCategory(int id, int new_index){
	#ifdef _OMB_USE_CIPHER
		wxString Sql = L"update Transactions set cat_index = " +
															wxString::Format(L"%d", new_index) +
															L" where id = " +
															wxString::Format(L"%d", id) +
															L";";
		ExecuteUpdate(database, Sql.c_str()/*, false*/);
	#else
		database->ExecuteUpdate(L"update Transactions set cat_index = " +
														wxString::Format(L"%d", new_index) +
														L" where id = " +
														wxString::Format(L"%d", id) +
														L";");
	#endif // _OMB_USE_CIPHER
	ParseDatabase();
}

#ifdef _OMB_USE_CIPHER
#ifndef __OMBCONVERT_BIN__
bool TData::PasswordPrompt(sqlite3 *db, bool Archive = false){
	int Response, rc, return_value = false;
	wxString Key;

	TPassF *PassF = new TPassF(wxTheApp->GetTopWindow());

	if(Archive)
		PassF->PassLab->SetLabel(_("Insert the archive password:"));

	do{
		Response = PassF->ShowModal();}
	while(Response == wxID_RETRY);

	switch(Response){
		case wxID_OK:
			Key = PassF->Pass->GetValue();
			if(SetKey(Key, Archive)){
        #ifdef __OPENSUSE__
          sqlite3_key(db, Key.c_str(), Key.length());
        #else
          sqlite3_key(db, Key, Key.length());
        #endif // __OPENSUSE__
				rc = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
				if (rc == SQLITE_OK)
					return_value = true;
			}
			break;
		case wxID_CANCEL:
		default:
			//Error(5, wxEmptyString); // This condition should never happen
			return_value = false;
	}
	delete PassF;
	return return_value;
}
#endif // __OMBCONVERT_BIN__
#endif // _OMB_USE_CIPHER

#ifndef __OMBCONVERT_BIN__
	#ifdef _OMB_USE_CIPHER
		void TData::ModPass(void){
			int Response;
			wxString Key;

			TPassF *PassF = new TPassF(wxTheApp->GetTopWindow());
			PassF->Pass2->Show(true);
			PassF->ConfirmLab->Show(true);
			do{
				Response = PassF->ShowModal();}
			while(Response == wxID_RETRY);

			wxString ArchiveName = Get_MasterDB();

			switch(Response){
				case wxID_OK:

					// Change main database password
					Key = PassF->Pass->GetValue();
					if(IsEncrypted){
            #ifdef __OPENSUSE__
              if(sqlite3_rekey(database, Key.c_str(), Key.length()) == SQLITE_OK)
            #else
              if(sqlite3_rekey(database, Key, Key.length()) == SQLITE_OK)
            #endif // __OPENSUSE__
							SetKey(Key);
					}
					else{
						sqlite3_close(database);
						IsEncrypted = IsEncryptedDB(FileData.FileName, Key);
            #ifdef __OPENSUSE__
              if(sqlite3_open(FileData.FileName.c_str(), &database) == SQLITE_OK){
                sqlite3_key(database, Key.c_str(), Key.length());
            #else
              if(sqlite3_open(FileData.FileName, &database) == SQLITE_OK){
                sqlite3_key(database, Key, Key.length());
            #endif // __OPENSUSE__
							if(sqlite3_exec(database, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL) == SQLITE_OK)
								SetKey(Key);
						}
					}

					// change archive password
					Key = GetKey(true);

					sqlite3 *Master;
					#ifdef __OPENSUSE__
            sqlite3_open(ArchiveName.c_str(), &Master);
            sqlite3_key(Master, Key.c_str(), Key.length());
					#else
            sqlite3_open(ArchiveName, &Master);
            sqlite3_key(Master, Key, Key.length());
          #endif // __OPENSUSE__

					Key = PassF->Pass->GetValue();
					if(IsEncrypted_master){
            #ifdef __OPENSUSE__
              if(sqlite3_rekey(Master, Key.c_str(), Key.length()) == SQLITE_OK)
            #else
              if(sqlite3_rekey(Master, Key, Key.length()) == SQLITE_OK)
						#endif // __OPENSUSE__
							SetKey(Key, true);
					}
					else{
						sqlite3_close(Master);
						IsEncrypted_master = IsEncryptedDB(ArchiveName, Key);
						#ifdef __OPENSUSE__
              if(sqlite3_open(ArchiveName.c_str(), &Master) == SQLITE_OK){
                sqlite3_key(Master, Key.c_str(), Key.length());
						#else
              if(sqlite3_open(ArchiveName, &Master) == SQLITE_OK){
                sqlite3_key(Master, Key, Key.length());
            #endif // __OPENSUSE__
							if(sqlite3_exec(Master, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL) == SQLITE_OK)
								SetKey(Key, true);
						}
					}

					break;
				case wxID_CANCEL:
					break;
				/*
				default:
					Error(5,wxEmptyString); // This condition should never happen
				*/
			}
			delete PassF;}
	#endif // _OMB_USE_CIPHER
#endif // __OMBCONVERT_BIN__

#ifndef __OMBCONVERT_BIN__
	#ifdef _OMB_USE_CIPHER
		bool TData::AttachMaster(void){
			wxString ArchiveName = Get_MasterDB();
			if(::wxFileExists(ArchiveName)){
				wxString pwd_archive = GetKey(true);
				if(! CheckAndPromptForConversion(ArchiveName, true, pwd_archive))
					return false;

				int tryno = 1;
				int result = -1;
				while (tryno < 4)
				{
					try
					{
						if(IsEncrypted_master){
							sqlite3 *Master;
							#ifdef __OPENSUSE__
                if(sqlite3_open(ArchiveName.c_str(), &Master) != SQLITE_OK) return false;
                sqlite3_key(Master, pwd_archive.c_str(), pwd_archive.length());
							#else
                if(sqlite3_open(ArchiveName, &Master) != SQLITE_OK) return false;
                sqlite3_key(Master, pwd_archive, pwd_archive.length());
              #endif // __OPENSUSE__
							int rc = sqlite3_exec(Master, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
							switch(rc){
								case SQLITE_OK:
								result = 1;
								break;

							case SQLITE_NOTADB:
								#ifndef __OMBCONVERT_BIN__
									PasswordPrompt(database, true);
									pwd_archive = GetKey(true);
								#endif // __OMBCONVERT_BIN__
								break;

								//default:
							}

						}
					}
					catch(exception& e)
					{
						//
					}

					if(result == 1) break;
					tryno++;
				}

				#ifdef __OPENSUSE__
          wxString Sql = wxString::Format(L"ATTACH DATABASE '%s' AS master KEY '%s';", ArchiveName, pwd_archive);
          ExecuteUpdate(database, Sql.c_str());
				#else
          ExecuteUpdate(database, wxString::Format(L"ATTACH DATABASE '%s' AS master KEY '%s';", ArchiveName, pwd_archive));
        #endif // __OPENSUSE__
				return true;
			}

			return false;
		}
	#endif // _OMB_USE_CIPHER
#endif // __OMBCONVERT_BIN__

void TData::SetDefaultFund(wxString def){
	#ifdef _OMB_USE_CIPHER
		wxString Sql = L"update Information set data = '" +
																def +
																L"' where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";";
		ExecuteUpdate(database, Sql.c_str()/*, false*/);
	#else
		database->ExecuteUpdate(L"update Information set data = '" +
															def +
															L"' where id = " +
															wxString::Format(L"%d", dbMeta_default_fund) +
															L";");
	#endif // _OMB_USE_CIPHER
	FileData.DefFund = def;
	FileData.Modified = true;
	ParseDatabase();
}

void TData::AddCategory(int id, wxString name, int IconIndex){
	Categories[NCat].Id = id;
	Categories[NCat].Name = name;
	Categories[NCat].test = false;
	Categories[NCat].IconIndex = IconIndex;
	NCat++;}

bool TData::AddShopItem(int id, wxString N, wxDateTime A){
	if(N.IsEmpty())return false;
	if(A == invalidDate) return false;
	if(Parsing){
		ShopItems[NSho].Id = id;
		ShopItems[NSho].Name = N;
		ShopItems[NSho].Alarm = A;
	}
	else{
		N = DoubleQuote(N);

		#ifdef _OMB_USE_CIPHER
			if(! TableExists(L"Shoplist", wxEmptyString, database)) ExecuteUpdate(database, cs_shoplist.c_str());
			wxString Sql = L"insert into Shoplist values (NULL, '" +
																	N +
																	L"', " +
																	wxString::Format(L"%d", (int) A.GetTicks()) +
																	L");";
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
		#else
			if(! database->TableExists(L"Shoplist")) database->ExecuteUpdate(cs_shoplist);
			database->ExecuteUpdate(L"insert into Shoplist values (NULL, '" +
																N +
																L"', " +
																wxString::Format(L"%d", (int) A.GetTicks()) +
																L");");
		#endif // _OMB_USE_CIPHER
	}

	if(! Parsing){
  	FileData.Modified=true;
  	ParseDatabase();}
  return true;}

wxString TData::ombFromCDouble(double value){
	// NOTE (igor#1#): Function added as workaround to precision loss of wxString::FromCDouble()
	#define TRIGGER 10000
	wxString result;
	if(value < TRIGGER) result = wxString::FromCDouble(value, 2);
	else{
		int lead = value / TRIGGER;
		value -= (lead * TRIGGER);
		result = wxString::FromDouble(lead);
		if(value < (TRIGGER / 10)) result += L"0";
		if(value < (TRIGGER / 100)) result += L"0";
		if(value < (TRIGGER / 1000)) result += L"0";
		if(value < (TRIGGER / 10000)) result += L"0";
		result += wxString::FromCDouble(value, 2);}
	return result;}

double TData::GetTot(TTypeVal T){
  double Ret;
  switch(T){
    case tvFou:
		Ret=Tot_Funds;
    break;
    case tvCre:
    Ret=Tot_Credits;
    break;
    case tvDeb:
    Ret=Tot_Debts;
    break;
    default:Ret=-1;}
  return Ret;}

void TData::SetDefaultFundValue(double value){
	for(int i = 0; i < NFun; i++)
		if(Funds[i].Name.CmpNoCase(FileData.DefFund) == 0){
			/*
			wxString DStr = ::wxString::FromCDouble(value, 2);
			database->ExecuteUpdate("update Funds set value = " +
																	DStr +
																	" where id = " +
																	wxString::Format("%d", Funds[i].Id) +
																	";");
			*/
			ChangeFundValue(tvFou, Funds[i].Id, value);

			ParseDatabase();
			break;}

}

bool TData::DelValue(TTypeVal T,int I){
	if(I < 0) return false;

	#ifdef _OMB_USE_CIPHER
		wxString Sql;
	#endif // _OMB_USE_CIPHER

	switch(T){
		case tvFou:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Funds where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Funds where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
		break;
		case tvCre:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Credits where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Credits where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		case tvDeb:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Debts where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Debts where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		default:
			return false;}
	ParseDatabase();
	FileData.Modified=true;
	return true;}

bool TData::DelShopItem(int I){
	#ifdef _OMB_USE_CIPHER
		wxString Sql = L"delete from Shoplist where id = " +
																wxString::Format(L"%d", I) +
																L";";
		ExecuteUpdate(database, Sql.c_str()/*, false*/);
	#else
		database->ExecuteUpdate(L"delete from Shoplist where id = " +
															wxString::Format(L"%d", I) +
															L";");
  #endif // _OMB_USE_CIPHER
  FileData.Modified=true;
  ParseDatabase();
  return true;}

bool TData::DelObject(TObjType T,int I){
	if(I < 0) return false;

	#ifdef _OMB_USE_CIPHER
		wxString Sql;
	#endif // _OMB_USE_CIPHER

	switch(T){
		case toPre:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Loans where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Loans where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		case toInP:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Borrows where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Borrows where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		default:
			return false;}
  FileData.Modified=true;
  ParseDatabase();
  return true;}

#ifndef __OMBCONVERT_BIN__
	void TData::UpdateCategories(wxListCtrl *lbox){
		bool exists;
		int i, x;	// x: pointer in Data->Lines
							// E: pointer to existing value
		wxString value;

		bool master_exist = false;
		#ifdef _OMB_USE_CIPHER
			sqlite3 *db;
		#else
			wxSQLite3Database *db = new wxSQLite3Database();
		#endif // _OMB_USE_CIPHER

		wxString master = Get_MasterDB();
		if(::wxFileExists(master)){
			#ifdef _OMB_USE_CIPHER
				sqlite3_open(master.c_str(), &db);
				if(TableExists(L"Categories", master.c_str(), db)) master_exist = true;
				sqlite3_close(db);
			#else
				db->Open(master, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
				if(db->TableExists(L"Categories")) master_exist = true;
				else db->Close();
			#endif // _OMB_USE_CIPHER
		}

		for (x = 0; x < lbox->GetItemCount(); x++){
			value = lbox->GetItemText(x);
			value = DoubleQuote(value);
			lbox->SetItemText(x, value);
		}

		// Check removed items
		for(i = NCat - 1; i >= 0; i--){
			exists = false;
			value = Categories[i].Name;
			for (x = 0; x < lbox->GetItemCount(); x++)if(value == lbox->GetItemText(x)){
				Categories[i].test = true;
				break;}}

		wxString Sql;

		for(i = NCat - 1; i >= 0; i--) if(! Categories[i].test){
			#ifdef _OMB_USE_CIPHER
				Sql = L"update Transactions set cat_index = -1 where cat_index = " +
																	wxString::Format(L"%d", Categories[i].Id) +
																	L";";
				ExecuteUpdate(database, Sql.c_str());
				if(master_exist) ExecuteUpdate(db, Sql.c_str());

				Sql = L"update Categories set active = 0 where id = " +
																	wxString::Format(L"%d", Categories[i].Id) +
																	L";";
				ExecuteUpdate(database, Sql.c_str());
				if(master_exist) ExecuteUpdate(db, Sql.c_str());
			#else
				database->ExecuteUpdate(L"update Transactions set cat_index = -1 where cat_index = " +
																wxString::Format(L"%d", Categories[i].Id) +
																L";");
				if(master_exist) db->ExecuteUpdate(L"update Transactions set cat_index = -1 where cat_index = " +
																wxString::Format(L"%d", Categories[i].Id) +
																L";");

				database->ExecuteUpdate(L"update Categories set active = 0 where id = " +
																wxString::Format(L"%d", Categories[i].Id) +
																L";");
				if(master_exist) db->ExecuteUpdate(L"update Categories set active = 0 where id = " +
																wxString::Format(L"%d", Categories[i].Id) +
																L";");
			#endif // _OMB_USE_CIPHER

		}

		// Check added items
		for(i = 0; i < lbox->GetItemCount(); i++){
			exists = false;
			value = lbox->GetItemText(i);
			for(x = 0; x < NCat; x++) if(value == Categories[x].Name){
				exists = true;
				break;}
			if(! exists){
				#ifdef _OMB_USE_CIPHER
					Sql = L"insert into Categories values (NULL, '" +
																			value +
																			L"', 1, -1);";
					ExecuteUpdate(database, Sql.c_str());
					if(master_exist) ExecuteUpdate(db, Sql.c_str());
				#else
					database->ExecuteUpdate(L"insert into Categories values (NULL, '" +
																		value +
																		L"', 1, -1);");
					if(master_exist) db->ExecuteUpdate(L"insert into Categories values (NULL, '" +
																		value +
																		L"', 1, -1);");
				#endif // _OMB_USE_CIPHER
			}
		}

		// Update icon indexes
		int Cat_id;
		wxListItem Item;
		for(i = 0; i < lbox->GetItemCount(); i++){
			value = lbox->GetItemText(i);
			for(x = 0; x < NCat; x++) if(value == Categories[x].Name){
				Item.SetId(i);
				Item.SetMask(wxLIST_MASK_IMAGE);
				lbox->GetItem(Item);

				// Check custom category indexes
				Cat_id = Item.GetImage();
				if(Cat_id >= _OMB_TOPCATEGORIES_OEMICON){
					Cat_id -= _OMB_TOPCATEGORIES_OEMICON;
					Cat_id += 100;
				}

				Sql = L"update Categories set iconid = " +
																	wxString::Format(L"%d", Cat_id) +
																	L" where id = " +
																	wxString::Format(L"%d", Categories[x].Id) +
																	L";";

				#ifdef _OMB_USE_CIPHER
					ExecuteUpdate(database, Sql.c_str());
					if(master_exist) ExecuteUpdate(db, Sql.c_str());
				#else
					#ifdef __WXMSW__
						database->ExecuteUpdate(Sql);
						if(master_exist) db->ExecuteUpdate(Sql);
					#else
						#ifdef __OPENSUSE__
							database->ExecuteUpdate(Sql.c_str());
							if(master_exist) db->ExecuteUpdate(Sql.c_str());
						#else
							database->ExecuteUpdate(Sql);
							if(master_exist) db->ExecuteUpdate(Sql);
						#endif // __OPENSUSE__
					#endif // __WXMSW__
				#endif // _OMB_USE_CIPHER

				break;
			}
		}

		if(master_exist)
			#ifdef _OMB_USE_CIPHER
				sqlite3_close(db);
			#else
				db->Close();
			#endif // _OMB_USE_CIPHER

		FileData.Modified = true;
		ParseDatabase();
	}
#endif // __OMBCONVERT_BIN__

wxString TData::GenerateTrailName(wxString dbPath){
	wxString return_string = wxEmptyString;

	#ifdef _OMB_USE_CIPHER
		sqlite3 *db;
	#else
		wxSQLite3Database *db = new wxSQLite3Database();
	#endif // _OMB_USE_CIPHER

	if(::wxFileExists(dbPath)){
		#ifdef _OMB_USE_CIPHER
			sqlite3_open(dbPath.c_str(), &db);
			if(TableExists(L"Transactions", L"db", db)){
				wxSQLite3Table table;
				table = GetTable(db, "select * from Transactions order by 1;");
				if(table.GetRowCount()){
					int month, year;
					table.SetRow(0);
					wxDateTime date = wxDateTime((time_t) table.GetInt(2, 0));
					month = date.GetMonth() + 1;
					year = date.GetYear();
					return_string = ::wxString::Format(L"_%d_%02d", year, month);
				}
			}
			sqlite3_close(db);
		#else
			db->Open(dbPath, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
			if(db->TableExists(L"Transactions")){
				wxSQLite3Table table;
				table = db->GetTable("select * from Transactions order by 1;");
				if(table.GetRowCount()){
					int month, year;
					table.SetRow(0);
					wxDateTime date = wxDateTime((time_t) table.GetInt(2, 0));
					month = date.GetMonth() + 1;
					year = date.GetYear();
					return_string = ::wxString::Format(L"_%d_%02d", year, month);
				}
			}
			db->Close();
		#endif // _OMB_USE_CIPHER
	}
	return return_string;
}

#ifndef __OMBCONVERT_BIN__
	void TData::Import_inMaster(wxString master, wxString trailname){
		// master:		database to attach and archive data in
		// trailname:	suffix for tables
		// archive_categories:	true to archive also categories

		::wxBeginBusyCursor();

		#ifdef _OMB_USE_CIPHER
			wxString Sql = wxString::Format("ATTACH DATABASE '%s' AS master;", master);
			ExecuteUpdate(database, Sql.c_str());

			// Archive data in master database
			Sql = wxString::Format(is_transactions, L"import.");
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(40);
			Sql = wxString::Format("INSERT INTO master.Funds%s SELECT * FROM import.Funds;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(50);
			Sql = wxString::Format("INSERT INTO master.Credits%s SELECT * FROM import.Credits;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(60);
			Sql = wxString::Format("INSERT INTO master.Debts%s SELECT * FROM import.Debts;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(70);
			Sql = wxString::Format("INSERT INTO master.Loans%s SELECT * FROM import.Loans;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(80);
			Sql = wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM import.Borrows;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(90);
			Sql = wxString::Format("INSERT INTO master.Categories%s SELECT * FROM import.Categories;", trailname);
			ExecuteUpdate(database, Sql.c_str());
			UpdateProgress(100);
			UpdateProgress(0);
		#else
			database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS master;", master));

			// Archive data in master database
			database->ExecuteUpdate(wxString::Format(is_transactions, L"import."));
			UpdateProgress(40);
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Funds%s SELECT * FROM import.Funds;", trailname));
			UpdateProgress(50);
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Credits%s SELECT * FROM import.Credits;", trailname));
			UpdateProgress(60);
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Debts%s SELECT * FROM import.Debts;", trailname));
			UpdateProgress(70);
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Loans%s SELECT * FROM import.Loans;", trailname));
			UpdateProgress(80);
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM import.Borrows;", trailname));
			UpdateProgress(90);
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Categories%s SELECT * FROM import.Categories;", trailname));
			UpdateProgress(100);
			UpdateProgress(0);
		#endif // _OMB_USE_CIPHER

		#ifdef _OMB_USE_CIPHER
			ExecuteUpdate(database, "DETACH DATABASE 'master';");
		#else
			database->ExecuteUpdate("DETACH DATABASE 'master';");
		#endif // _OMB_USE_CIPHER

		::wxEndBusyCursor();
	}
#endif // __OMBCONVERT_BIN__

#ifndef __OMBCONVERT_BIN__
void TData::UpdateProgress(int i){
		if(Progress != NULL){
			bool nonzero = false;
			if(Progress->GetValue()) nonzero=true;
			Progress->SetValue(i);
			if(nonzero){
				if(i == 0) Progress->Show(false);}
			else if(i > 0) Progress->Show(true);
			Progress->Update();
			wxTheApp->Yield(false);}}
#endif // __OMBCONVERT_BIN__

#endif	// OMB34CORE_CPP_INCLUDED
