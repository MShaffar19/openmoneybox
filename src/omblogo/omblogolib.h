/***************************************************************
 * Name:      omblogolib.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-12-27
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef __OMBCONVERT_BIN__
	#include <wx/wx.h>

	// String manipulation functions
	WXEXPORT wxString iUpperCase(wxString S);
	//	Dialogs functions
	#if _OMB_HASNATIVEABOUT == 0
		WXEXPORT void About(wxString Name,wxString ShortVer,wxString LongVer,wxString Date);
	#endif // _OMB_HASNATIVEABOUT == 0
	WXEXPORT void DonateDialog(wxString olURL/*,bool EN*/);
#endif // __OMBCONVERT_BIN__

