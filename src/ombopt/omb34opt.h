/***************************************************************
 * Name:      omb34opt.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB34OptH
#define OMB34OptH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

//#include <wx/string.h>
#include <wx/fileconf.h> // For wxFileConfig

#ifdef __WXMSW__
	#include "../platformsetup.h"
  #ifdef _OMB_USEREGISTRY
    #include <wx/msw/registry.h> // For wxRegKey
  #endif // _OMB_USEREGISTRY
#endif

#include "../types.h"
//#include "constants.h"

class TOpt
{
//private:
public:
	bool SuccessfulCreated;     // true if Option are properly created
	#ifdef __WXMSW__
		// HKLM Options
		wxString SWKey;
		wxString InstallationPath;    // Program Folder
	#endif
	#ifdef __WXMAC__
		wxString InstallationPath;    // Program Folder
	#endif	// __WXMAC__
	//bool InstConv;              // true if Conv utility is installed	// DON'T REMOVE - May be restore with new Product version
	//long InstWiz;     				  // true if Wizard utility is installed
	//long InstTray;              // true if OmbTray utility is installed
	//long InstUpdate;						// true if Update utility is installed
	// HKCU Options
	long FirstRunEver;          // Very first launch of application;
	//bool FirstRunCurrVersion;   // First lauch of new version;
                              // Preparation for future updates
	//long GAutoOpen;             // Automatic opening of last used file
	//wxString GLastFile;           // Latest opened document
	wxString	GDoc_folder,			// Document default folder
						GDDoc,						// Default document
						GMaster_db,				// Master database
						GDoc_prefix;			// Prefix for self generated documents (XML's and related folders, backups, etc.)
	long GBar;                  // Toolbar display
	long GConv;                 // Automatic report export in text on new months
	long GIcon;                 // Open TrayIcon on exit
	long CTrendGraph;           // Trend Graph display
	long CFundGraph;						// Fund Pie display
	#ifdef _OMB_INSTALLEDUPDATE
		long CheckUpdates;					// Verify new version availability in the web
	#endif // _OMB_INSTALLEDUPDATE
	//wxArrayString *LogStrings;
	wxArrayString *Tools;

	TOpt(void);

	//void LastFile(wxString Name);
	bool SetDefaultDoc(wxString Doc);
	void SetCurrSymbol(void);
    /*
    #ifdef __WXMSW__
        #ifdef _OMB_USEREGISTRY
            void ReadLogString(wxRegKey *Key, int S);
        #endif // _OMB_USEREGISTRY
    #else
        void ReadLogString(wxFileConfig *Key, int S);
    #endif
    */
	void FirstRunDone(void);

	~TOpt();
};

#ifdef __WXMSW__
  // Calls to module igiomb
  WXIMPORT TVersion GetInstalledVersion(void);
  WXIMPORT TVersion GetLastRunVersion(void);
  // Calls to module omberr.DLL
  WXIMPORT void Error(int Err, wxString Opt);
#endif
WXIMPORT wxString GetOSDocDir(void);
WXIMPORT wxString GetUserLocalDir(void);

#ifdef __WXMAC__
	WXIMPORT wxString GetUserConfigDir(void);
#endif	// __WXMAC__

#endif // OMB34OptH


