/***************************************************************
 * Name:      ombopt.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-10
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef BILOPT_HEADER_INCLUDED
#define BILOPT_HEADER_INCLUDED

#include "../types.h"

  #include "../platformsetup.h"
#ifdef __WXMSW__
  #include "../platformsetup.h"
#endif // __WXMSW__

/*
#ifdef __WXMSW__
  int wxEntry(int& argc, wxChar **argv);
  //BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fwdreason, LPVOID lpvReserved);
#endif
*/

// Imported functions
WXIMPORT wxString GetOSDocDir(void);
// ------------------------------------

WXEXPORT void CreateTrayAutostart(void);

#ifdef __WXMSW__
	WXEXPORT wxString GetInstallationPath(void);
#endif	// __WXMSW__

WXEXPORT bool ShowBar(void);
WXEXPORT bool IsFirstRunEver(void);
WXEXPORT wxString GetDocPath(void);
WXEXPORT void Set_DefaultDocument(wxString Document);
WXEXPORT wxString GetDefaultDocument(void);
WXEXPORT void DoFirstRun(void);
//WXEXPORT wxString GetLogString(int I);
WXEXPORT bool ShowOptionsDialog(void);
WXEXPORT bool ShowTrendChart(void);
WXEXPORT bool ShowFundChart(void);
WXEXPORT bool TrayActive(void);
WXEXPORT bool AutoConvert(void);
#ifdef _OMB_INSTALLEDUPDATE
	WXEXPORT bool GetCheckUpdates(void);
#endif // _OMB_INSTALLEDUPDATE
WXEXPORT int GetToolCount(void);
WXEXPORT wxString GetTool(int i);
#ifdef _OMB_USEREGISTRY
  WXEXPORT wxString GetSWKey(void);
#endif // _OMB_USEREGISTRY

WXEXPORT wxString GetDocPrefix(void);

#ifdef _OMB_USE_CIPHER
	WXEXPORT bool SetKey(wxString Key, bool Archive);
	WXEXPORT wxString GetKey(bool Archive = false);
#endif // _OMB_USE_CIPHER

#endif // BILOPT_HEADER_INCLUDED
