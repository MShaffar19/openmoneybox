/***************************************************************
 * Name:      ombopt.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef BILOPT_CPP_INCLUDED
#define BILOPT_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifdef _OMB_USE_GSETTINGS
	#include <gio/gio.h>	// For GSettings
#else
	#include <wx/fileconf.h> // For wxFileConfig
#endif // _OMB_USE_GSETTINGS

#ifdef __WXMSW__
  #include "wx/language.h"
  #include <wx/stdpaths.h> // For wxStandardPathsBase
#endif // __WXMSW__

#include "ombopt.h"

#include "omb34opt.h"

#include "ui/opzio.h"
#include "../../rsrc/icons/Icon_Application.xpm"

#ifdef __WXGTK__
	#ifdef _OMB_USE_GSETTINGS
		GSettings *settings_general;
		GSettings *settings_charts;
		GSettings *settings_tools;
		#ifdef _OMB_INSTALLEDUPDATE
			GSettings *settings_advanced;
		#endif // _OMB_INSTALLEDUPDATE
	#else
  	wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
  extern wxLanguage Lan;
  //extern TOpt *GeneralOptions;
#elif defined __WXMSW__
  wxLanguage Lan;
  #ifdef _OMB_USEREGISTRY
    //extern wxString SWKey;
  #endif // _OMB_USEREGISTRY
#elif defined __WXMAC__
  wxLanguage Lan;
  wxFileConfig *INI;
#endif // __WGTK__

/*#ifdef __WXMSW__
	// DON'T REMOVE - May be restore with new Product version
bool ConvInstalled(void){
	return GeneralOptions->InstConv;}
#endif
*/

#ifdef __WXMSW__
wxString GetInstallationPath(void){
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->InstallationPath;}
#endif	// __WXMSW__

bool ShowBar(void){
	TOpt *GeneralOptions=new TOpt();
  return GeneralOptions->GBar;}

bool IsFirstRunEver(void){
	TOpt *GeneralOptions=new TOpt();
  return GeneralOptions->FirstRunEver;}

WXEXPORT wxString GetBilDocPath(void){
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->GDoc_folder;}

void Set_DefaultDocument(wxString Document){
	TOpt *GeneralOptions=new TOpt();
	GeneralOptions->SetDefaultDoc(Document);}

wxString GetDefaultDocument(void){
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->GDDoc;}

WXEXPORT void DoFirstRun(void){
	TOpt *GeneralOptions=new TOpt();
  GeneralOptions->FirstRunDone();}

/*
wxString GetLogString(int I){
  if(I>14)return wxEmptyString;
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->LogStrings->Item(I);}
*/

wxLanguage FindLang(void){
	wxLocale loc;
	loc.Init(wxLANGUAGE_DEFAULT,wxLOCALE_LOAD_DEFAULT);
	Lan=(wxLanguage)loc.GetLanguage();
	return Lan;}

bool ShowOptionsDialog(void){
	unsigned int i;
	wxString Key;
	TOpt *GeneralOptions=new TOpt();
	TOptionsF *OptionsF=new TOptionsF(wxTheApp->GetTopWindow());

	OptionsF->SetIcon(wxIcon(Logo_xpm));

	// General Page initialization
	OptionsF->GeneralP->ShowBar->SetValue(GeneralOptions->GBar);
	OptionsF->GeneralP->Text->SetValue(GeneralOptions->GConv);
	/*
	if(!GeneralOptions->InstTray){
		OptionsF->GeneralP->Default->Enable(false);
		OptionsF->GeneralP->BrowseButton2->Enable(false);
		OptionsF->GeneralP->Sys->Enable(false);}
  else{
	*/
	if(::wxFileExists(GeneralOptions->GDDoc))
		OptionsF->GeneralP->BrowseButton2->SetPath(GeneralOptions->GDDoc);
	else
		OptionsF->GeneralP->BrowseButton2->SetInitialDirectory(GetOSDocDir());
	OptionsF->GeneralP->Sys->SetValue(GeneralOptions->GIcon);
	OptionsF->GeneralP->Sys->Enable(true);	// }

	OptionsF->GeneralP->DocPrefix->SetValue(GeneralOptions->GDoc_prefix);

  // Chart Page initialisation
	OptionsF->ChartsP->TrendGraph->SetValue(GeneralOptions->CTrendGraph);
	OptionsF->ChartsP->FundGraph->SetValue(GeneralOptions->CFundGraph);

	#ifdef _OMB_INSTALLEDUPDATE
		// Advanced Page initialisation
		OptionsF->AdvSheetP->cb_CheckUpdates->SetValue(GeneralOptions->CheckUpdates);
	#endif // _OMB_INSTALLEDUPDATE

	// External Tools initialisation
	unsigned int original_entries=GeneralOptions->Tools->Count();
	for(i=0;i<original_entries;i++)OptionsF->ToolsSheetP->ExtList->Append(GeneralOptions->Tools->Item(i));
	//OptionsF->Changed=false;

	if(OptionsF->ShowModal()==wxID_OK){
		GeneralOptions->GBar=OptionsF->GeneralP->ShowBar->IsChecked();
		GeneralOptions->GConv=OptionsF->GeneralP->Text->IsChecked();
    //if(InstTray){
		if(::wxFileExists(OptionsF->GeneralP->BrowseButton2->GetPath())){
			GeneralOptions->GDDoc=OptionsF->GeneralP->BrowseButton2->GetPath();
			wxFileName::SplitPath(GeneralOptions->GDDoc, &GeneralOptions->GDoc_folder, NULL, NULL, wxPATH_NATIVE);}
			GeneralOptions->GIcon=OptionsF->GeneralP->Sys->IsChecked();
		//}

		GeneralOptions->GDoc_prefix = OptionsF->GeneralP->DocPrefix->GetValue();

		GeneralOptions->CTrendGraph=OptionsF->ChartsP->TrendGraph->IsChecked();
		GeneralOptions->CFundGraph=OptionsF->ChartsP->FundGraph->IsChecked();

		/*
		GeneralOptions->LogStrings->Item(0)=OptionsF->LabelsP->Tot_Edit->GetValue();
		GeneralOptions->LogStrings->Item(1)=OptionsF->LabelsP->Gain_Edit->GetValue();
		GeneralOptions->LogStrings->Item(2)=OptionsF->LabelsP->Expe_Edit->GetValue();
		GeneralOptions->LogStrings->Item(3)=OptionsF->LabelsP->Cred_Edit->GetValue();
		GeneralOptions->LogStrings->Item(4)=OptionsF->LabelsP->Cred2_Edit->GetValue();
		GeneralOptions->LogStrings->Item(5)=OptionsF->LabelsP->Cred3_Edit->GetValue();
		GeneralOptions->LogStrings->Item(6)=OptionsF->LabelsP->Deb_Edit->GetValue();
		GeneralOptions->LogStrings->Item(7)=OptionsF->LabelsP->Deb2_Edit->GetValue();
		GeneralOptions->LogStrings->Item(8)=OptionsF->LabelsP->Deb3_Edit->GetValue();
		GeneralOptions->LogStrings->Item(9)=OptionsF->LabelsP->GetO_Edit->GetValue();
		GeneralOptions->LogStrings->Item(10)=OptionsF->LabelsP->Giv_O_Edit->GetValue();
		GeneralOptions->LogStrings->Item(11)=OptionsF->LabelsP->LenO_Edit->GetValue();
		GeneralOptions->LogStrings->Item(12)=OptionsF->LabelsP->GetBO_Edit->GetValue();
		GeneralOptions->LogStrings->Item(13)=OptionsF->LabelsP->BorO_Edit->GetValue();
		GeneralOptions->LogStrings->Item(14)=OptionsF->LabelsP->GivBO_Edit->GetValue();
		*/

		#ifdef _OMB_INSTALLEDUPDATE
			GeneralOptions->CheckUpdates=OptionsF->AdvSheetP->cb_CheckUpdates->IsChecked();
		#endif // _OMB_INSTALLEDUPDATE

		GeneralOptions->Tools->Clear();
		for(i=0;i<OptionsF->ToolsSheetP->ExtList->GetCount();i++)GeneralOptions->Tools->Add(OptionsF->ToolsSheetP->ExtList->GetString(i),1);
		delete OptionsF;
		#ifdef __WXMSW__
			#ifdef _OMB_USEREGISTRY
				Key="HKEY_CURRENT_USER\\"+GeneralOptions->SWKey+"\\General";
				wxRegKey *K=new wxRegKey(Key);
				// General options
				K->Open(wxRegKey::Write);
				//K->SetValue("AutoOpen",GeneralOptions->GAutoOpen);
				K->SetValue("Docs",GeneralOptions->GDoc_folder);
				K->SetValue("Bar",GeneralOptions->GBar);
				K->SetValue("Conv",GeneralOptions->GConv);
				//if(GeneralOptions->InstTray){
					if(!GeneralOptions->GDDoc.IsEmpty())K->SetValue("Default",GeneralOptions->GDDoc);
					else if(K->HasValue("Default"))K->DeleteValue("Default");
					K->SetValue("Icon",GeneralOptions->GIcon);  // }

				// Auto startup entry
				wxRegKey *Run=new wxRegKey("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run");
				Run->Open(wxRegKey::Write);
				if(GeneralOptions->GIcon){
					wxStandardPaths std = wxStandardPaths::Get();
					wxString exePath = std.GetExecutablePath();
					wxString path;
					wxFileName::SplitPath (exePath, &path, NULL, NULL, wxPATH_NATIVE);
					path = "\"" + path + "\\ombtray.exe\"";
					Run->SetValue("igisw_openmoneybox", path);
				}
				else{
					if(Run->HasValue("igisw_openmoneybox"))Run->DeleteValue("igisw_openmoneybox");
				}
				delete Run;

				K->SetValue("Prefix", GeneralOptions->GDoc_prefix);

				// Charts options
				Key="HKEY_CURRENT_USER\\"+GeneralOptions->SWKey+"\\Charts";
				K=new wxRegKey(Key);
				K->Open(wxRegKey::Write);
				K->SetValue("Trend",GeneralOptions->CTrendGraph);
				K->SetValue("Fund",GeneralOptions->CFundGraph);

				/*
				// Strings options
				Key="HKEY_CURRENT_USER\\"+GeneralOptions->SWKey+"\\Strings";
				K=new wxRegKey(Key);
				K->Open(wxRegKey::Write);
				K->SetValue("S00",GeneralOptions->LogStrings->Item(0));
				K->SetValue("S01",GeneralOptions->LogStrings->Item(1));
				K->SetValue("S02",GeneralOptions->LogStrings->Item(2));
				K->SetValue("S03",GeneralOptions->LogStrings->Item(3));
				K->SetValue("S04",GeneralOptions->LogStrings->Item(4));
				K->SetValue("S05",GeneralOptions->LogStrings->Item(5));
				K->SetValue("S06",GeneralOptions->LogStrings->Item(6));
				K->SetValue("S07",GeneralOptions->LogStrings->Item(7));
				K->SetValue("S08",GeneralOptions->LogStrings->Item(8));
				K->SetValue("S09",GeneralOptions->LogStrings->Item(9));
				K->SetValue("S10",GeneralOptions->LogStrings->Item(10));
				K->SetValue("S11",GeneralOptions->LogStrings->Item(11));
				K->SetValue("S12",GeneralOptions->LogStrings->Item(12));
				K->SetValue("S13",GeneralOptions->LogStrings->Item(13));
				K->SetValue("S14",GeneralOptions->LogStrings->Item(14));
				*/

				#ifdef _OMB_INSTALLEDUPDATE
					// Advanced options
					Key="HKEY_CURRENT_USER\\"+GeneralOptions->SWKey+"\\Advanced";
					K=new wxRegKey(Key);
					K->Open(wxRegKey::Write);
					K->SetValue("NewVersionCheck",GeneralOptions->CheckUpdates);
				#endif // _OMB_INSTALLEDUPDATE

				// External Tools option
				Key="HKEY_CURRENT_USER\\"+GeneralOptions->SWKey+"\\Tools";
				K=new wxRegKey(Key);
				K->Open(wxRegKey::Write);
				long ind = 1;
				size_t nSubValues;
				wxString strTemp;
				K->GetKeyInfo(NULL,NULL,&nSubValues,NULL);
				while(nSubValues>0){
					K->GetFirstValue(strTemp, ind);
					K->DeleteValue(strTemp);
					K->GetKeyInfo(NULL,NULL,&nSubValues,NULL);}
				for(i=0;i<GeneralOptions->Tools->Count();i++)K->SetValue(wxString::Format(L"Tool%d",i),GeneralOptions->Tools->Item(i));
				delete K;
			#endif // __OMB_USEREGISTRY
		#else
			#ifdef _OMB_USE_GSETTINGS
				// General options
				#ifdef __OPENSUSE__
					g_settings_set_string(settings_general, "gdocfolder", GeneralOptions->GDoc_folder.c_str());
				#else
					g_settings_set_string(settings_general, "gdocfolder", GeneralOptions->GDoc_folder);
				#endif // __OPENSUSE__
				g_settings_set_boolean (settings_general, "bar", GeneralOptions->GBar);
				g_settings_set_boolean (settings_general, "conv", GeneralOptions->GConv);

				#ifdef __OPENSUSE__
					g_settings_set_string(settings_general, "default", GeneralOptions->GDDoc.c_str());
				#else
					g_settings_set_string(settings_general, "default", GeneralOptions->GDDoc);
				#endif // __OPENSUSE__
				g_settings_set_boolean(settings_general, "icon", GeneralOptions->GIcon);

				// Auto startup entry
				wxString home;
				if(GeneralOptions->GIcon) CreateTrayAutostart();
				else{
					#ifdef __WXGTK__
						home = wxGetHomeDir() + L"/.config/autostart/ombtray.desktop";
						if(::wxFileExists(home))::wxRemoveFile(home);
					#elif defined ( __WXMSW__ )
						// TODO (igor#1#): Implement Windows version for portable version
					#endif // __WXGTK__
				}

				#ifdef __OPENSUSE__
					g_settings_set_string(settings_general, "prefix", GeneralOptions->GDoc_prefix.c_str());
				#else
					g_settings_set_string(settings_general, "prefix", GeneralOptions->GDoc_prefix);
				#endif // __OPENSUSE__

				// Charts options
				g_settings_set_boolean(settings_charts, "trend", GeneralOptions->CTrendGraph);
				g_settings_set_boolean(settings_charts, "fund", GeneralOptions->CFundGraph);

				#ifdef _OMB_INSTALLEDUPDATE
					// Advanced options
					g_settings_set_boolean(settings_advanced, "newversioncheck", GeneralOptions->CheckUpdates);
				#endif // _OMB_INSTALLEDUPDATE

				// External Tools option
				wxString tools_string;
				for(i = 0; i < GeneralOptions->Tools->Count(); i++){
					tools_string += GeneralOptions->Tools->Item(i);
					if(i < GeneralOptions->Tools->Count() - 1) tools_string.operator +=(char(59));
				}
				#ifdef __OPENSUSE__
					g_settings_set_string(settings_tools, "tools", tools_string.c_str());
				#else
					g_settings_set_string(settings_tools, "tools", tools_string);
				#endif // __OPENSUSE__

			#else
				// General options
				INI->SetPath(L"/General");
				INI->Write(L"Docs", GeneralOptions->GDoc_folder);
				INI->Write(L"Bar", GeneralOptions->GBar);
				INI->Write(L"Conv", GeneralOptions->GConv);

				if(!GeneralOptions->GDDoc.IsEmpty())INI->Write(L"Default",GeneralOptions->GDDoc);
				else if(INI->HasEntry(L"Default"))INI->DeleteEntry(L"Default");
				INI->Write(L"Icon",GeneralOptions->GIcon);

				// Auto startup entry
				wxString home;
				if(GeneralOptions->GIcon) CreateTrayAutostart();
				else{
					#ifdef __WXGTK__
						home = wxGetHomeDir() + L"/.config/autostart/ombtray.desktop";
						if(::wxFileExists(home))::wxRemoveFile(home);
					#elif defined ( __WXMSW__ )
						// TODO (igor#1#): Implement Windows version for portable version
					#endif // __WXGTK__
				}

				INI->Write(L"Prefix", GeneralOptions->GDoc_prefix);

				// Charts options
				INI->SetPath(L"/Charts");
				INI->Write(L"Trend",GeneralOptions->CTrendGraph);
				INI->Write(L"Fund",GeneralOptions->CFundGraph);

				#ifdef _OMB_INSTALLEDUPDATE
					// Advanced options
					INI->SetPath(L"/Advanced");
					INI->Write(L"NewVersionCheck",GeneralOptions->CheckUpdates);
				#endif // _OMB_INSTALLEDUPDATE

				// External Tools option
				INI->SetPath(L"/Tools");
				#ifdef __WXMSW__
					for(i = 0; i <= original_entries; i++) INI->DeleteEntry(wxString::Format("Tool%d",i), false);
				#else
					for(i = 0; i <= original_entries; i++) INI->DeleteEntry(wxString::Format(L"Tool%d", i), false);
				#endif // __WXMSW__
				for(i=0;i<GeneralOptions->Tools->Count();i++){
					#ifdef __WXMSW__
						INI->Write(wxString::Format("Tool%d",i),GeneralOptions->Tools->Item(i));
					#else
						INI->Write(wxString::Format(L"Tool%d",i),GeneralOptions->Tools->Item(i));
					#endif  // __WXMSW__
				}

				// Settings cleanup and archive
				INI->SetPath(L"/General");
				if(INI->HasEntry(L"AutoOpen")){
					bool old_auto_open = INI->Read(L"AutoOpen", 0l);
					INI->DeleteEntry(L"AutoOpen");
					INI->SetPath(L"/General_old");
					INI->Write(L"AutoOpen", old_auto_open);}
				INI->SetPath(L"/General");
				if(INI->HasEntry(L"LastFile")){
					wxString old_last_file = INI->Read(L"LastFile", wxEmptyString);
					INI->DeleteEntry((L"LastFile"));
					INI->SetPath(L"/General_old");
					INI->Write(L"LastFile", old_last_file);}

				INI->Flush();
			#endif // _OMB_USE_GSETTINGS
		#endif
		return true;}
	delete OptionsF;
  return false;}

bool ShowTrendChart(void){
	TOpt *GeneralOptions=new TOpt();
  return GeneralOptions->CTrendGraph;}

bool ShowFundChart(void){
	TOpt *GeneralOptions=new TOpt();
  return GeneralOptions->CFundGraph;}

WXEXPORT bool TrayActive(void){
	TOpt *GeneralOptions=new TOpt();
  return GeneralOptions->GIcon;}

bool AutoConvert(void){
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->GConv;}

#ifdef _OMB_INSTALLEDUPDATE
	bool GetCheckUpdates(void){
		TOpt *GeneralOptions=new TOpt();
		return GeneralOptions->CheckUpdates;}
#endif // _OMB_INSTALLEDUPDATE

WXEXPORT int GetToolCount(void){
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->Tools->Count();}

WXEXPORT wxString GetTool(int i){
	TOpt *GeneralOptions=new TOpt();
	return GeneralOptions->Tools->Item(i);}

	WXEXPORT wxString Get_MasterDB(void){
		TOpt *GeneralOptions=new TOpt();
		return GeneralOptions->GMaster_db;}


#ifdef _OMB_USEREGISTRY
  wxString GetSWKey(void){
    TOpt *GeneralOptions=new TOpt();
    return GeneralOptions->SWKey;
  }
#endif // _OMB_USEREGISTRY

	wxString GetDocPrefix(void){
		TOpt *GeneralOptions=new TOpt();
		return GeneralOptions->GDoc_prefix;}

	void CreateTrayAutostart(void){
			#ifdef __WXGTK__
				wxString home;
				home = wxGetHomeDir() + L"/.config";
				if(! wxDirExists(home)) wxMkdir(home,0777);
				home += L"/autostart";
				if(! wxDirExists(home)) wxMkdir(home,0777);

				wxCHANGE_UMASK(wxS_IXUSR|wxS_IXGRP|wxS_IXOTH);
				wxTextFile *file = new wxTextFile(home + L"/ombtray.desktop");
				file->Create();
				file->AddLine(L"[Desktop Entry]");
				file->AddLine(L"Version=1.0");
				file->AddLine(L"Type=Application");
				file->AddLine(L"Terminal=false");
				file->AddLine(L"Name=ombtray");
				wxString path=wxGetCwd();
				file->AddLine(L"Exec="+path+L"/ombtray");
				file->AddLine(L"Path="+path+L"/");
				file->AddLine(L"Icon="+path+L"/Icon_Application.xpm");
				file->AddLine(L"MimeType=application/openmoneybox");
				file->Write(wxTextFileType_Unix,wxConvUTF8);
				file->Close();
				delete file;
				wxCHANGE_UMASK(-1);
			//#endif
			#elif defined ( __WXMSW__ )
				// TODO (igor#1#): Implement Windows version for portable version

			#endif // __WXGTK__
	}

#ifdef _OMB_USE_CIPHER
	WXEXPORT bool SetKey(wxString Key, bool Archive = false){
		/*
		#ifdef __WXMSW__

		#else
			INI->SetPath(L"/Security");
			INI->Write(L"Key", key);
			INI->Flush();
		#endif
		*/

		wxString FilePath = GetUserLocalDir();
		if(Archive) FilePath += L"/.a";
		else FilePath += L"/.k";

		wxFile *f = new wxFile(FilePath, wxFile::write);
		if(! f->IsOpened()) return false;
		f->Write(Key, wxConvUTF8);
		f->Close();

		return true;
	}

	wxString GetKey(bool Archive){
		/*
		// Password check;
		INI->SetPath(L"/Security");
		if(INI->HasEntry(L"Key")) pwd = INI->Read(L"Key", wxEmptyString);
		else pwd = wxEmptyString;
		*/

		wxString pwd = wxEmptyString;
		wxString KeyFile = GetUserLocalDir();

		if(Archive) KeyFile += L"/.a";
		else KeyFile += L"/.k";

		if(wxFileExists(KeyFile)){
			char *buffer;
			wxFile *f = new wxFile(KeyFile, wxFile::read);
			if(f->IsOpened()){
				int L = f->SeekEnd(0);
				f->Seek(0);
				buffer = new char[L];
				/*int BR =*/ f->Read(buffer, L);
				pwd = wxString(buffer, L);
				f->Close();
			}
		}

		return pwd;
	}
#endif // _OMB_USE_CIPHER

#endif  // BILOPT_CPP_INCLUDED
