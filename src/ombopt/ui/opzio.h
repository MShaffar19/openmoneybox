/***************************************************************
 * Name:      opzio.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OpzioH
#define OpzioH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>

#include "../../types.h"

#include "wxoption.h"

#ifdef _OMB_INSTALLEDUPDATE
	#include "wxadvsheet.h"
#endif // _OMB_INSTALLEDUPDATE

class GenPanel : public General{
public:
	//bool Changed;
	GenPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,300 ), long style = wxTAB_TRAVERSAL );
};

class ToolsPanel : public ToolsSheet{
public:
	//bool Changed;
	ToolsPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,300 ), long style = wxTAB_TRAVERSAL );
	void ToolBrowseClick(wxCommandEvent& event);
	void ExtListClick(wxCommandEvent& event);
	void RemoveClick(wxCommandEvent& event);
};

class TOptionsF : public /*TForm*/OptionsF
{
//__published:
private:
	// GUI components
	//wxStaticBox *DefFolder;

	// Routines
	//void BrowseButton1Click(void);
	//void OKClick(wxCommandEvent& event);
	/*
	void ToolBrowseClick(void);
	void ExtListClick(void);
	void RemoveClick(void);
	*/
	//void OptChanged(void);
protected:
	//void DefaultClick(wxCommandEvent& event);
public:
	// GUI components
	GenPanel *GeneralP;
	Charts *ChartsP;
	//Labels *LabelsP;

	#ifdef _OMB_INSTALLEDUPDATE
		AdvSheet *AdvSheetP;
	#endif // _OMB_INSTALLEDUPDATE

	ToolsPanel *ToolsSheetP;

	// Non-GUI components
	//bool Changed;

	// Routines
	explicit TOptionsF(wxWindow* parent);
};

WXIMPORT wxLanguage FindLang(void);
//WXIMPORT void Error(int Err,wxString Opt);
WXIMPORT wxString GetOSDocDir(void);

#endif


