///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/filepicker.h>
#include <wx/checkbox.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/listbox.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/notebook.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class General
///////////////////////////////////////////////////////////////////////////////
class General : public wxPanel
{
	private:

	protected:

	public:
		wxStaticText* DefDocument;
		wxFilePickerCtrl* BrowseButton2;
		wxCheckBox* ShowBar;
		wxCheckBox* Text;
		wxCheckBox* Sys;
		wxStaticText* Lbl_DocPrefix;
		wxTextCtrl* DocPrefix;

		General( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,250 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~General();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Charts
///////////////////////////////////////////////////////////////////////////////
class Charts : public wxPanel
{
	private:

	protected:

	public:
		wxCheckBox* FundGraph;
		wxCheckBox* TrendGraph;

		Charts( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,250 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~Charts();

};

///////////////////////////////////////////////////////////////////////////////
/// Class ToolsSheet
///////////////////////////////////////////////////////////////////////////////
class ToolsSheet : public wxPanel
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_ExtListClick( wxCommandEvent& event ){ ExtListClick( event ); }
		void _wxFB_ToolBrowseClick( wxCommandEvent& event ){ ToolBrowseClick( event ); }
		void _wxFB_RemoveClick( wxCommandEvent& event ){ RemoveClick( event ); }


	protected:
		enum
		{
			bilAddTool = 1000,
			bilRemTool
		};


		// Virtual event handlers, overide them in your derived class
		virtual void ExtListClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToolBrowseClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemoveClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxListBox* ExtList;
		wxButton* btn_ToolBrowse;
		wxButton* Remove;

		ToolsSheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,250 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~ToolsSheet();

};

///////////////////////////////////////////////////////////////////////////////
/// Class OptionsF
///////////////////////////////////////////////////////////////////////////////
class OptionsF : public wxDialog
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_OKClick( wxCommandEvent& event ){ OKClick( event ); }


	protected:
		wxNotebook* TabPages;
		wxButton* OK;
		wxButton* Cancel;

		// Virtual event handlers, overide them in your derived class
		virtual void OKClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		OptionsF( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 470,300 ), long style = wxCAPTION|wxCLOSE_BOX|wxDEFAULT_DIALOG_STYLE );
		~OptionsF();

};

