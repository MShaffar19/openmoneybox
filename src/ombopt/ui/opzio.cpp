/***************************************************************
 * Name:      opzio.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/fileconf.h> // For wxFileConfig

#include "opzio.h"

#include "../../types.h"

#ifndef _OMB_USEREGISTRY
  extern wxFileConfig *INI;
#endif // _OMB_USEREGISTRY

GenPanel::GenPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : General( parent, id, pos, size, style ){}

ToolsPanel::ToolsPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : ToolsSheet( parent, id, pos, size, style ){
	//Changed=false;
}

void ToolsPanel::ToolBrowseClick(wxCommandEvent& event){
	wxFileDialog *Dialog=new wxFileDialog(this, _("Select a file"), GetOSDocDir(), wxEmptyString, L"*.*", wxFD_DEFAULT_STYLE,
																					wxDefaultPosition, wxDefaultSize, L"Dialog");
	if(Dialog->ShowModal()==wxID_OK){
		bool Ex=false;
		for(unsigned int i=0;i<ExtList->GetCount();i++){
			if(ExtList->GetString(i)==Dialog->GetFilename()){
				Ex=true;
				break;}}
		if(!Ex){
			ExtList->Append(Dialog->GetPath());
			//Changed=true;
	}}
  delete Dialog;}

void ToolsPanel::ExtListClick(wxCommandEvent& event){
	Remove->Enable(ExtList->GetSelection()>-1);}

void ToolsPanel::RemoveClick(wxCommandEvent& event){
	int OldIndex=ExtList->GetSelection();
	ExtList->Delete(ExtList->GetSelection());
	if(((int)ExtList->GetCount())>=(OldIndex+1))ExtList->SetSelection(OldIndex);
	else if(ExtList->GetCount())ExtList->SetSelection(ExtList->GetCount()-1);
	wxCommandEvent evt(wxEVT_NULL,0);
	ExtListClick(evt);
	//Changed=true;
}

TOptionsF::TOptionsF(wxWindow* parent):OptionsF(parent, -1, wxEmptyString, wxDefaultPosition, wxSize(470,300),
																									wxCAPTION|wxCLOSE_BOX|wxDEFAULT_DIALOG_STYLE){
	// GUI components creation

	// Language Initialisation
	wxString label;

	// General Page
	GeneralP=new /*General*/GenPanel(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	/*
	DefFolder=new wxStaticBox(this,-1,wxEmptyString,wxPoint(7,13),wxSize(410,72),0,L"DefFolder");
	DefFolder->SetParent(GeneralP);
	*/
	TabPages->AddPage(GeneralP,_("General"),true,-1);
	/*
	INI->Read(L"Gen_Browse",&label,_("Sfoglia"));
	GeneralP->BrowseButton1->SetLabel(label);
	*/
	// Chart Page
	ChartsP=new Charts(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	TabPages->AddPage(ChartsP,_("Charts"),true,-1);

	/*
	// Labels Page
	LabelsP=new Labels(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	TabPages->AddPage(LabelsP,_("Labels"),true,-1);
	*/

	#ifdef _OMB_INSTALLEDUPDATE
		// Advanced Page
		AdvSheetP=new AdvSheet(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
		TabPages->AddPage(AdvSheetP,_("Advanced"),true,-1);
	#endif // _OMB_INSTALLEDUPDATE

	// External Tools Page
	ToolsSheetP=new /*ToolsSheet*/ToolsPanel(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	TabPages->AddPage(ToolsSheetP,_("External tools"),true,-1);
	//GeneralP->DDoc->Enable(false);

	TabPages->SetSelection(0);
	SetTitle(_("Options"));
	//Changed=false;
}

/*
void TOptionsF::OKClick(wxCommandEvent& event){
	wxString S;

	// Strings Page check
	// Empty strings check
	if(LabelsP->Gain_Edit->IsEmpty()){
		LabelsP->Gain_Edit->SetFocus();
		goto EmptyStrError;}
	if(LabelsP->Expe_Edit->IsEmpty()){
		LabelsP->Expe_Edit->SetFocus();
		goto EmptyStrError;}
	if(LabelsP->Cred_Edit->IsEmpty()){
		LabelsP->Cred_Edit->SetFocus();
		goto EmptyStrError;}
	if(LabelsP->Cred2_Edit->IsEmpty()){
		LabelsP->Cred2_Edit->SetFocus();
		goto EmptyStrError;}
	if(LabelsP->Cred3_Edit->IsEmpty()){
		LabelsP->Cred3_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->Deb_Edit->IsEmpty()){
		LabelsP->Deb_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->Deb2_Edit->IsEmpty()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->Deb3_Edit->IsEmpty()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->GetO_Edit->IsEmpty()){
		LabelsP->GetO_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->Giv_O_Edit->IsEmpty()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->LenO_Edit->IsEmpty()){
		LabelsP->LenO_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->GetBO_Edit->IsEmpty()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EmptyStrError;}
	if(LabelsP->BorO_Edit->IsEmpty()){
		LabelsP->BorO_Edit->SetFocus();
		goto EmptyStrError;}
	if(LabelsP->GivBO_Edit->IsEmpty()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EmptyStrError;}
	// Gain string
	S=LabelsP->Gain_Edit->GetValue();
	if(S==LabelsP->Expe_Edit->GetValue()){
		LabelsP->Expe_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Cred_Edit->GetValue()){
		LabelsP->Cred_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Cred2_Edit->GetValue()){
		LabelsP->Cred2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Cred3_Edit->GetValue()){
		LabelsP->Cred3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb_Edit->GetValue()){
		LabelsP->Deb_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb2_Edit->GetValue()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Expense string
	S=LabelsP->Expe_Edit->GetValue();
	if(S==LabelsP->Cred_Edit->GetValue()){
		LabelsP->Cred_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Cred2_Edit->GetValue()){
		LabelsP->Cred2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Cred3_Edit->GetValue()){
		LabelsP->Cred3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb_Edit->GetValue()){
		LabelsP->Deb_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb2_Edit->GetValue()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// New Credit string
	S=LabelsP->Cred_Edit->GetValue();
	if(S==LabelsP->Cred2_Edit->GetValue()){
		LabelsP->Cred2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Cred3_Edit->GetValue()){
		LabelsP->Cred3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb_Edit->GetValue()){
		LabelsP->Deb_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb2_Edit->GetValue()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Remove Credit string
	S=LabelsP->Cred2_Edit->GetValue();
	if(S==LabelsP->Cred3_Edit->GetValue()){
		LabelsP->Cred3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb_Edit->GetValue()){
		LabelsP->Deb_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb2_Edit->GetValue()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Condone Credit string
	S=LabelsP->Cred3_Edit->GetValue();
	if(S==LabelsP->Deb_Edit->GetValue()){
		LabelsP->Deb_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb2_Edit->GetValue()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// New Debt string
	S=LabelsP->Deb_Edit->GetValue();
	if(S==LabelsP->Deb2_Edit->GetValue()){
		LabelsP->Deb2_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
		goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Remove Credit string
	S=LabelsP->Deb2_Edit->GetValue();
	if(S==LabelsP->Deb3_Edit->GetValue()){
		LabelsP->Deb3_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Condone Debt string
	S=LabelsP->Deb3_Edit->GetValue();
	if(S==LabelsP->GetO_Edit->GetValue()){
		LabelsP->GetO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Get Object string
	S=LabelsP->GetO_Edit->GetValue();
	if(S==LabelsP->Giv_O_Edit->GetValue()){
		LabelsP->Giv_O_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Given Object string
	S=LabelsP->Giv_O_Edit->GetValue();
	if(S==LabelsP->LenO_Edit->GetValue()){
		LabelsP->LenO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Lent Object string
	S=LabelsP->LenO_Edit->GetValue();
	if(S==LabelsP->GetBO_Edit->GetValue()){
		LabelsP->GetBO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Got Back Object string
	S=LabelsP->GetBO_Edit->GetValue();
	if(S==LabelsP->BorO_Edit->GetValue()){
		LabelsP->BorO_Edit->SetFocus();
    goto EqualStrError;}
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
    goto EqualStrError;}
	// Borrowed Object string
	S=LabelsP->BorO_Edit->GetValue();
	if(S==LabelsP->GivBO_Edit->GetValue()){
		LabelsP->GivBO_Edit->SetFocus();
		goto EqualStrError;}
	LabelsP->Tot_Edit->SetValue(LabelsP->Tot_Edit->GetValue().MakeUpper());
	EndModal(wxID_OK);
	return;
	EmptyStrError:
	Error(38,wxEmptyString);
	goto CommErr;
	EqualStrError:
	Error(34,wxEmptyString);
	CommErr:
	EndModal(wxID_NONE);
	TabPages->SetSelection(2);}
*/

/*
void TOptionsF::OptChanged(void){
	if(Changed)return;
	Changed=true;}
*/


