///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxoption.h"

///////////////////////////////////////////////////////////////////////////

General::General( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* General_;
	General_ = new wxBoxSizer( wxVERTICAL );

	DefDocument = new wxStaticText( this, wxID_ANY, _("Default document"), wxDefaultPosition, wxDefaultSize, 0 );
	DefDocument->Wrap( -1 );
	General_->Add( DefDocument, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	BrowseButton2 = new wxFilePickerCtrl( this, wxID_ANY, wxEmptyString, _("Select a file"), _("*.omb;*.OMB"), wxDefaultPosition, wxDefaultSize, wxFLP_FILE_MUST_EXIST|wxFLP_OPEN|wxFLP_USE_TEXTCTRL );
	General_->Add( BrowseButton2, 0, wxALIGN_RIGHT|wxEXPAND|wxRIGHT|wxLEFT, 5 );

	ShowBar = new wxCheckBox( this, wxID_ANY, _("Show toolbar"), wxDefaultPosition, wxDefaultSize, 0 );
	General_->Add( ShowBar, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	Text = new wxCheckBox( this, wxID_ANY, _("Automatically export XML at the end of the month"), wxDefaultPosition, wxDefaultSize, 0 );
	General_->Add( Text, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	Sys = new wxCheckBox( this, wxID_ANY, _("Activates icon in the system tray"), wxDefaultPosition, wxDefaultSize, 0 );
	General_->Add( Sys, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	Lbl_DocPrefix = new wxStaticText( this, wxID_ANY, _("Prefix for generated documents (XML, backups, etc.):"), wxDefaultPosition, wxDefaultSize, 0 );
	Lbl_DocPrefix->Wrap( -1 );
	General_->Add( Lbl_DocPrefix, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	DocPrefix = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !DocPrefix->HasFlag( wxTE_MULTILINE ) )
	{
	DocPrefix->SetMaxLength( 30 );
	}
	#else
	DocPrefix->SetMaxLength( 30 );
	#endif
	General_->Add( DocPrefix, 0, wxBOTTOM|wxRIGHT|wxLEFT|wxEXPAND, 5 );


	this->SetSizer( General_ );
	this->Layout();
}

General::~General()
{
}

Charts::Charts( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* Charts_;
	Charts_ = new wxBoxSizer( wxVERTICAL );

	FundGraph = new wxCheckBox( this, wxID_ANY, _("Show fund chart"), wxDefaultPosition, wxDefaultSize, 0 );
	Charts_->Add( FundGraph, 0, wxTOP|wxRIGHT|wxLEFT, 5 );

	TrendGraph = new wxCheckBox( this, wxID_ANY, _("Show trend chart"), wxDefaultPosition, wxDefaultSize, 0 );
	Charts_->Add( TrendGraph, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );


	this->SetSizer( Charts_ );
	this->Layout();
}

Charts::~Charts()
{
}

BEGIN_EVENT_TABLE( ToolsSheet, wxPanel )
	EVT_LISTBOX( wxID_ANY, ToolsSheet::_wxFB_ExtListClick )
	EVT_BUTTON( bilAddTool, ToolsSheet::_wxFB_ToolBrowseClick )
	EVT_BUTTON( bilRemTool, ToolsSheet::_wxFB_RemoveClick )
END_EVENT_TABLE()

ToolsSheet::ToolsSheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxFlexGridSizer* fgSizer4;
	fgSizer4 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer4->AddGrowableCol( 0 );
	fgSizer4->AddGrowableRow( 0 );
	fgSizer4->SetFlexibleDirection( wxBOTH );
	fgSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	ExtList = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	fgSizer4->Add( ExtList, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	btn_ToolBrowse = new wxButton( this, bilAddTool, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer11->Add( btn_ToolBrowse, 0, wxALL, 5 );

	Remove = new wxButton( this, bilRemTool, _("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	Remove->Enable( false );

	bSizer11->Add( Remove, 0, wxALL, 5 );


	fgSizer4->Add( bSizer11, 0, wxEXPAND, 5 );


	this->SetSizer( fgSizer4 );
	this->Layout();
}

ToolsSheet::~ToolsSheet()
{
}

BEGIN_EVENT_TABLE( OptionsF, wxDialog )
	EVT_BUTTON( wxID_OK, OptionsF::_wxFB_OKClick )
END_EVENT_TABLE()

OptionsF::OptionsF( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableRow( 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	TabPages = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_MULTILINE );

	fgSizer3->Add( TabPages, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );

	OK = new wxButton( this, wxID_OK, _("OK"), wxPoint( 142,271 ), wxSize( 75,25 ), 0 );
	bSizer3->Add( OK, 0, wxALL, 5 );

	Cancel = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxSize( 75,25 ), 0 );
	bSizer3->Add( Cancel, 0, wxALL, 5 );


	fgSizer3->Add( bSizer3, 1, wxALIGN_RIGHT, 5 );


	this->SetSizer( fgSizer3 );
	this->Layout();
}

OptionsF::~OptionsF()
{
}
