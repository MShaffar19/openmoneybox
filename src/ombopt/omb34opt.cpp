/***************************************************************
 * Name:      omb34opt.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB34OPT_CPP_INCLUDED
#define OMB34OPT_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifdef _OMB_USE_GSETTINGS
	#include <gio/gio.h>	// For GSettings
	#include <wx/tokenzr.h>	// For wxStringTokenize
#endif // _OMB_USE_GSETTINGS

#include "omb34opt.h"

#ifdef __WXMSW__
  #ifdef _OMB_USEREGISTRY
    #include <wx/msw/registry.h>
  #endif // _OMB_USEREGISTRY
  //extern wxString SWKey;
#else
	#ifdef _OMB_USE_GSETTINGS
		extern GSettings *settings_general;
		extern GSettings *settings_charts;
		extern GSettings *settings_tools;
		#ifdef _OMB_INSTALLEDUPDATE
			extern GSettings *settings_advanced;
		#endif // _OMB_INSTALLEDUPDATE
	#else
  	extern wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
#endif // __WXMSW__

TOpt::TOpt(void){
	SuccessfulCreated=false;
	#ifdef _OMB_USEREGISTRY
		#ifdef __WXMSW__
			SWKey = "SOFTWARE\\igiSW\\OpenMoneyBox\\3.4";   // root registry key
      InstallationPath = wxGetCwd();
		#endif
	#endif // _OMB_USEREGISTRY
	///*InstConv=*/InstWiz=InstTray=InstUpdate=false;	// DON'T REMOVE - May be restore with new Product version
	//if(K->ValueExists("Conv"))InstConv=K->ReadBool("Conv");	// DON'T REMOVE - May be restore with new Product version
	#ifdef __WXMSW__
		#ifdef _OMB_USEREGISTRY
		/*
			if(K->HasValue("Wiz"))K->QueryValue("Wiz",&InstWiz);
			if(K->HasValue("Tray"))K->QueryValue("Tray",&InstTray);
			if(K->HasValue("Update"))K->QueryValue("Update",&InstUpdate);
		*/
		#endif // _OMB_USEREGISTRY
	#else
		/*
			ini->SetPath(L"/Optional Modules");
			if(ini->HasEntry(L"Wiz"))InstWiz=ini->Read(L"Wiz",0l);
			if(ini->HasEntry(L"Tray"))InstTray=ini->Read(L"Tray",0l);
			if(ini->HasEntry(L"Update"))InstUpdate=ini->Read(L"Update",0l);
		*/
	#endif // __WXMSW__

	/* TODO -cEnhancement : Preparation for future enhancement */
	/*
	#ifdef __WXMSW__
		TVersion instVer=GetInstalledVersion();
		TVersion lrVer=GetLastRunVersion();
		if(instVer.Maj>lrVer.Maj)goto Update;
		else if(instVer.Min>lrVer.Min)goto Update;
		else if(instVer.Rel>lrVer.Rel)goto Update;
		else if(instVer.Bui>lrVer.Bui)goto Update;
		else goto NoUpdate;
		Update:{
	#endif
	*/
	/* TODO -cEnhancement : Preparation for future enhancement */
	/*
	#ifdef __WXMSW__
	#ifdef _OMB_USEREGISTRY

			// __________________________________________________________
			K->SetValue("MajVer_lr",instVer.Maj);
			K->SetValue("MinVer_lr",instVer.Min);
			K->SetValue("RelVer_lr",instVer.Rel);
			K->SetValue("BuiVer_lr",instVer.Bui);
	#endif // _OMB_USEREGISTRY
	#else

			// __________________________________________________________
			INI->SetPath(L"/Version lr");
			INI->Write(L"MajVer_lr",instVer.Maj);
			INI->Write(L"MinVer_lr",instVer.Min);
			INI->Write(L"RelVer_lr",instVer.Rel);
			INI->Write(L"BuiVer_lr",instVer.Bui);
			INI->Flush();
			//delete INI;
	#endif
	#ifdef __WXMSW__
		}
		NoUpdate:
	#endif
	*/

	#ifdef __WXMSW__
		#ifdef _OMB_USEREGISTRY
			wxRegKey *K = new wxRegKey(wxRegKey::HKCU, SWKey, wxRegKey::WOW64ViewMode_Default);
			K->SetName(wxRegKey::HKCU, SWKey);
			K->Create(true);
			// First run check
			if(K->HasValue("FirstRun")) K->QueryValue("FirstRun", &FirstRunEver);
			else FirstRunEver=true;
			// General tab
			K->SetName(wxRegKey::HKCU, SWKey + "\\General");
			K->Create(true);

			/*
			if(K->HasValue("AutoOpen"))K->QueryValue("AutoOpen",&GAutoOpen);
			else{
				K->SetValue("AutoOpen",false);
				GAutoOpen=false;}
			*/
			//if(!GAutoOpen)goto NoFile;

			/*
			if(K->HasValue("LastFile"))K->QueryValue("LastFile",GLastFile);
			else GLastFile=wxEmptyString;
			*/

			//NoFile:

			if(K->HasValue("Default")){
				K->QueryValue("Default", GDDoc);
				if(! GDDoc.IsEmpty()) if(! ::wxFileExists(GDDoc)) GDDoc = wxEmptyString;}
			else GDDoc = wxEmptyString;

			if(K->HasValue("Docs"))K->QueryValue("Docs",GDoc_folder);
			else{
				bool docdir_set = false;
				if(! GDDoc.IsEmpty()){

					wxString Path;
					::wxFileName::SplitPath(GDDoc, NULL, &Path, NULL, NULL, wxPATH_NATIVE);
					if(::wxFileName::DirExists(Path)){
						K->SetValue(L"Docs", Path);
						GDoc_folder = Path;
						docdir_set = true;}}
				if(! docdir_set){
					GDoc_folder = GetOSDocDir();
					K->SetValue("Docs", GDoc_folder);
				}
			}

			GMaster_db = wxEmptyString;
			if(K->HasValue("MasterDB"))K->QueryValue("MasterDB", GMaster_db);
			if(GMaster_db.IsEmpty()){
				if(! GDoc_folder.IsEmpty()){
					GMaster_db = GDoc_folder + "\\omb_master.ombdb";
					K->SetValue("MasterDB", GMaster_db);
				}
			}

			if(K->HasValue("Bar"))K->QueryValue("Bar",&GBar);
			else{
				K->SetValue("Bar",true);
				GBar=true;}
			if(K->HasValue("Conv"))K->QueryValue("Conv",&GConv);
			else{
				K->SetValue("Conv",false);
				GConv=false;}
			if(K->HasValue("Icon"))K->QueryValue("Icon",&GIcon);

			if(K->HasValue("Prefix")) K->QueryValue("Prefix",GDoc_prefix);
			else{
				GDoc_prefix = _("OpenMoneyBox");
				K->SetValue("Prefix", GDoc_prefix);
			}

			// Chart tab
			K->SetName(wxRegKey::HKCU,SWKey+"\\Charts");
			K->Create(true);
			if(K->HasValue("Trend"))K->QueryValue("Trend",&CTrendGraph);
			else {
				K->SetValue("Trend",true);
				CTrendGraph=true;}
			if(K->HasValue("Fund"))K->QueryValue("Fund",&CFundGraph);
			else{
				K->SetValue("Fund",true);
				CFundGraph=true;}

			/*
			// Strings tab
			K->SetName(wxRegKey::HKCU,SWKey+"\\Strings");
			K->Create(true);
			for(int i=0;i<15;i++)ReadLogString(K,i);
			*/

			#ifdef _OMB_INSTALLEDUPDATE
				// Advanced tab
				K->SetName(wxRegKey::HKCU,SWKey+"\\Advanced");
				K->Create(true);
				if(K->HasValue("NewVersionCheck"))K->QueryValue("NewVersionCheck",&CheckUpdates);
				else{
					K->SetValue("NewVersionCheck",false);
					CheckUpdates=false;}
			#endif // _OMB_INSTALLEDUPDATE

			// External tools tab
			Tools=new wxArrayString();
			Tools->Clear();
			K->SetName(wxRegKey::HKCU,SWKey+"\\Tools");
			K->Create(true);
			if(K->Open(wxRegKey::Read)){
				long ind = 1;
				wxString t;
				wxString strTemp;
				wxArrayString *Tls=new wxArrayString();
				size_t nSubValues;
				K->GetKeyInfo(NULL,NULL,&nSubValues,NULL);
				K->GetFirstValue(strTemp, ind);
				for(unsigned int i=0;i<nSubValues;i++){
					Tls->Add(strTemp,1);
					K->GetNextValue(strTemp, ind);}
				for(unsigned int j=0;j<Tls->Count();j++){
					K->QueryValue(Tls->Item(j),t);
					if(::wxFileExists(t))Tools->Add(t,1);}
				Tls->Clear();
				delete Tls;}
			SuccessfulCreated=true;
			delete K;
		#endif // _OMB_USEREGISTRY
	#else
		#ifdef _OMB_USE_GSETTINGS
			// First run check
			FirstRunEver = g_settings_get_boolean (settings_tools, "firstrun");

			GDDoc = g_settings_get_string(settings_general, "default");
			if(! GDDoc.IsEmpty()) if(! ::wxFileExists(GDDoc)) GDDoc = wxEmptyString;

			GDoc_folder = g_settings_get_string(settings_general, "gdocfolder");
			if(GDoc_folder.IsEmpty()){
				bool docdir_set = false;
				if(! GDDoc.IsEmpty()){
					wxString Path;
					::wxFileName::SplitPath(GDDoc, NULL, &Path, NULL, NULL, wxPATH_NATIVE);
					if(::wxFileName::DirExists(Path)){
						#ifdef __OPENSUSE__
							g_settings_set_string(settings_general, "gdocfolder", Path.c_str());
						#else
							g_settings_set_string(settings_general, "gdocfolder", Path);
						#endif // __OPENSUSE__
						GDoc_folder = Path;
						docdir_set = true;}}
				if(! docdir_set){
					GDoc_folder = GetOSDocDir();
					#ifdef __OPENSUSE__
						g_settings_set_string(settings_general, "gdocfolder", GDoc_folder.c_str());
					#else
						g_settings_set_string(settings_general, "gdocfolder", GDoc_folder);
					#endif // __OPENSUSE__
				}
			}

			GMaster_db = wxEmptyString;
			GMaster_db = g_settings_get_string(settings_general, "masterdb");
			if(GMaster_db.IsEmpty()){
				#ifdef __WXGTK__
					GMaster_db = GetUserLocalDir() + L"/omb_master.ombdb";
					#ifdef __OPENSUSE__
						g_settings_set_string(settings_general, "masterdb", GMaster_db.c_str());
					#else
						g_settings_set_string(settings_general, "masterdb", GMaster_db);
					#endif // __OPENSUSE__
				#else
					if(! GDoc_folder.IsEmpty()){
						GMaster_db = GDoc_folder + L"/omb_master.ombdb";
						INI->Write(L"MasterDB", GMaster_db);
					}
				#endif // __WXGTK__
			}

			GBar = g_settings_get_boolean (settings_general, "bar");
			GConv = g_settings_get_boolean (settings_general, "conv");
			GIcon = g_settings_get_boolean (settings_general, "icon");
			GDoc_prefix = g_settings_get_string (settings_general, "prefix");

			// Chart tab
			CTrendGraph = g_settings_get_boolean (settings_charts, "trend");
			CFundGraph = g_settings_get_boolean (settings_charts, "fund");

			#ifdef _OMB_INSTALLEDUPDATE
				// Advanced tab
				CheckUpdates = g_settings_get_boolean (settings_advanced, "newversioncheck");
			#endif // _OMB_INSTALLEDUPDATE

			// External tools tab
			Tools=new wxArrayString();
			Tools->Clear();
			wxArrayString tools_tokens;
			/*
			#ifdef __WXMSW__
				unsigned int entries;
			#else
				uint entries;
			#endif // __WXMSW__
			*/
			//entries=INI->GetNumberOfEntries(false);
			//long ind = 1;
			wxString tools_string;

			tools_string = g_settings_get_string (settings_tools, "tools");
			tools_tokens = ::wxStringTokenize(tools_string, char(59), wxTOKEN_DEFAULT);
			for(unsigned int k = 0; k < tools_tokens.GetCount(); k++)Tools->Add(tools_tokens.Item(k));
			SuccessfulCreated=true;
			//INI->Flush();
		#else
			// First run check

			#ifdef __WXMAC__
				if(INI == NULL){
					//wxLogMessage(L"debug test1");

					wxString homecfg = GetUserConfigDir() + L"/omb.ini";
					INI = new wxFileConfig(wxEmptyString, wxEmptyString, homecfg, wxEmptyString, wxCONFIG_USE_LOCAL_FILE, wxConvAuto());
				}
			#endif // __WXMAC__
			INI->SetPath(L"/Tools");
			if(INI->HasEntry(L"FirstRun"))FirstRunEver=INI->Read(L"FirstRun",0l);
			else FirstRunEver=true;
			INI->SetPath(L"/General");
			if(INI->HasEntry(L"Default")){
				GDDoc = INI->Read(L"Default", wxEmptyString);
				if(! GDDoc.IsEmpty()) if(! ::wxFileExists(GDDoc)) GDDoc = wxEmptyString;}
			else GDDoc = wxEmptyString;

			if(INI->HasEntry(L"Docs")) GDoc_folder = INI->Read(L"Docs",wxEmptyString);
			else{
				bool docdir_set = false;
				if(! GDDoc.IsEmpty()){
					wxString Path;
					::wxFileName::SplitPath(GDDoc, NULL, &Path, NULL, NULL, wxPATH_NATIVE);
					if(::wxFileName::DirExists(Path)){
						INI->Write(L"Docs", Path);
						GDoc_folder = Path;
						docdir_set = true;}}
				if(! docdir_set){
					GDoc_folder = GetOSDocDir();
					INI->Write(L"Docs", GDoc_folder);
				}
			}

			GMaster_db = wxEmptyString;
			if(INI->HasEntry(L"MasterDB"))GMaster_db = INI->Read(L"MasterDB", wxEmptyString);
			if(GMaster_db.IsEmpty()){
				#ifdef __WXGTK__
					GMaster_db = GetUserLocalDir() + L"/omb_master.ombdb";
					INI->Write(L"MasterDB", GMaster_db);
				#else
					if(! GDoc_folder.IsEmpty()){
						GMaster_db = GDoc_folder + L"/omb_master.ombdb";
						INI->Write(L"MasterDB", GMaster_db);
					}
				#endif // __WXGTK__
			}

			if(INI->HasEntry(L"Bar"))GBar=INI->Read(L"Bar",1l);
			else{
				INI->Write(L"Bar",true);
				GBar=true;}
			if(INI->HasEntry(L"Conv"))GConv=INI->Read(L"Conv",0l);
			else{
				INI->Write(L"Conv",false);
				GConv=false;}
			if(INI->HasEntry(L"Icon"))GIcon=INI->Read(L"Icon",0l);
			else{
				INI->Write(L"Icon",false);
				GIcon=false;}

			if(INI->HasEntry(L"Prefix")) GDoc_prefix = INI->Read(L"Prefix", wxEmptyString);
			else{
				GDoc_prefix = _("OpenMoneyBox");
				INI->Write(L"Prefix", GDoc_prefix);
				}

			// Chart tab
			INI->SetPath(L"/Charts");
			if(INI->HasEntry(L"Trend"))CTrendGraph=INI->Read(L"Trend",1l);
			else {
				INI->Write(L"Trend",true);
				CTrendGraph=true;}
			if(INI->HasEntry(L"Fund"))CFundGraph=INI->Read(L"Fund",1l);
			else{
				INI->Write(L"Fund",true);
				CFundGraph=true;}

			#ifdef _OMB_INSTALLEDUPDATE
				// Advanced tab
				INI->SetPath("/Advanced");
				if(INI->HasEntry(L"NewVersionCheck")) CheckUpdates = INI->Read(L"NewVersionCheck", 0l);
				else{
					INI->Write(L"NewVersionCheck", false);
					CheckUpdates = false;}
			#endif // _OMB_INSTALLEDUPDATE

			// External tools tab
			Tools=new wxArrayString();
			Tools->Clear();
			INI->SetPath(L"/Tools");
			#ifdef __WXMSW__
				unsigned int entries;
			#else
				uint entries;
			#endif // __WXMSW__
			entries=INI->GetNumberOfEntries(false);
			long ind = 1;
			wxString strTemp;
			wxArrayString *Tls=new wxArrayString();
			INI->GetFirstEntry(strTemp, ind);
			for(unsigned int i=0;i<entries;i++){
				Tls->Add(strTemp,1);
				INI->GetNextEntry(strTemp, ind);}
			for(unsigned int j=0;j<Tls->Count();j++){
				strTemp=INI->Read(Tls->Item(j),wxEmptyString);
				if(::wxFileExists(strTemp))Tools->Add(strTemp,1);}
			Tls->Clear();
			delete Tls;
			SuccessfulCreated=true;
			INI->Flush();
		#endif // _OMB_USE_GSETTINGS
	#endif // __WXMSW__
}

/*void TOpt::LastFile(wxString Name){
#ifdef __WXMSW__
#ifdef _OMB_USEREGISTRY
	wxString Key="HKEY_CURRENT_USER\\";
	Key=Key+SWKey;
	Key=Key+"\\General";
	wxRegKey *K=new wxRegKey(Key);
	if(K->Open(wxRegKey::Read))
		K->SetValue("LastFile",Name);
	delete K;
	GLastFile=Name;
#endif // _OMB_USEREGISTRY
#else
	INI->SetPath(L"/General");
	INI->Write(L"LastFile",Name);
	INI->Flush();
	GLastFile=Name;
	//delete ini;
#endif
}
*/

TOpt::~TOpt(){
	/*
	LogStrings->Clear();
	delete LogStrings;
	*/
	Tools->Clear();
	delete Tools;}

bool TOpt::SetDefaultDoc(wxString Doc){
#ifdef __WXMSW__
	#ifdef _OMB_USEREGISTRY
		GDDoc=Doc;
		wxString Key="HKEY_CURRENT_USER\\"+SWKey+"\\General";
		wxRegKey *K=new wxRegKey(Key);
		K->Open(wxRegKey::Write);
		K->SetValue("Default", /*GDDoc*/ Doc);
		delete K;
	#endif // _OMB_USEREGISTRY
#else
	#ifdef _OMB_USE_GSETTINGS
		#ifdef __OPENSUSE__
			g_settings_set_string(settings_general, "default", Doc.c_str());
		#else
			g_settings_set_string(settings_general, "default", Doc);
		#endif // __OPENSUSE__
	#else
		INI->SetPath(L"/General");
		INI->Write(L"Default", /*GDDoc*/ Doc);
		INI->Flush();
	#endif // _OMB_USE_GSETTINGS
#endif
  return true;}

/*
#ifdef __WXMSW__
#ifdef _OMB_USEREGISTRY
void TOpt::ReadLogString(wxRegKey *Key, int S){
	wxString RegString="S";
	wxString value;
	if(S<10)RegString+="0";
	RegString+=::wxString::Format("%d",S);
	if(Key->HasValue(RegString)){
		Key->QueryValue(RegString,value);
		LogStrings->Item(S)=value;}
	else{
		//F->SetPath(L"/LogStrings");
		//F->Read(RegString+"_String",&value,RegString+"_String");
		switch(S){
			case 0:
				value=_("Total");
				break;
			case 1:
				value=_("Profit");
				break;
			case 2:
				value=_("Expense");
				break;
			case 3:
				value=_("New credit");
				break;
			case 4:
				value=_("Removed credit");
				break;
			case 5:
				value=_("Condoned credit");
				break;
			case 6:
				value=_("New debt");
				break;
			case 7:
				value=_("Removed debt");
				break;
			case 8:
				value=_("Condoned debt");
				break;
			case 9:
				value=_("Received object");
				break;
			case 10:
				value=_("Given object");
				break;
			case 11:
				value=_("Lent object");
				break;
			case 12:
				value=_("Object got back");
				break;
			case 13:
				value=_("Borrowed object");
				break;
			case 14:
				value=_("Object given back");
				break;
      default:
        value=wxEmptyString;}
		Key->SetValue(RegString,value);
		LogStrings->operator[](S)=RegString+"_String";}}
#endif // _OMB_USEREGISTRY
#else
void TOpt::ReadLogString(wxFileConfig *Key, int S){
	wxString RegString=L"S";
	wxString value;
	if(S<10)RegString+=L"0";
	#ifdef __WXMSW__
    RegString+=::wxString::Format("%d",S);
  #else
    RegString+=::wxString::Format(L"%d",S);
  #endif // __WXMSW__
	if(Key->HasEntry(RegString)){
		value=Key->Read(RegString,wxEmptyString);
		LogStrings->Item(S)=value;}
	else{
		Key->SetPath(L"/LogStrings");
		switch(S){
			case 0:
				value=_("Total");
				break;
			case 1:
				value=_("Profit");
				break;
			case 2:
				value=_("Expense");
				break;
			case 3:
				value=_("New credit");
				break;
			case 4:
				value=_("Removed credit");
				break;
			case 5:
				value=_("Condoned credit");
				break;
			case 6:
				value=_("New debt");
				break;
			case 7:
				value=_("Removed debt");
				break;
			case 8:
				value=_("Condoned debt");
				break;
			case 9:
				value=_("Received object");
				break;
			case 10:
				value=_("Given object");
				break;
			case 11:
				value=_("Lent object");
				break;
			case 12:
				value=_("Object got back");
				break;
			case 13:
				value=_("Borrowed object");
				break;
			case 14:
				value=_("Object given back");
				break;
      default:
        value=wxEmptyString;}
		Key->Write(RegString,value);
		LogStrings->operator[](S)=value;}}
#endif
*/

void TOpt::FirstRunDone(void){
#ifdef __WXMSW__
	#ifdef _OMB_USEREGISTRY
		wxRegKey *K=new wxRegKey(wxRegKey::HKCU,SWKey,wxRegKey::WOW64ViewMode_Default);
		K->Open(wxRegKey::Write);
		K->SetValue("FirstRun",false);
		delete K;
	#endif // _OMB_USEREGISTRY
#else
	#ifdef _OMB_USE_GSETTINGS
		g_settings_set_boolean(settings_tools, "firstrun", false);
	#else
		INI->SetPath(L"/Tools");
		INI->Write(L"FirstRun",false);
		INI->Flush();
	#endif // _OMB_USE_GSETTINGS
#endif
}

#endif // OMB34OPT_CPP_INCLUDED
