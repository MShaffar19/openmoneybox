/***************************************************************
 * Name:      datawithalarms.cpp
 * Purpose:   Alarm extension for TData
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-27
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/sound.h>

#include "datawithalarms.h"
#include "ui/Alarm.h"
#include "../constants.h"

#ifdef _OMB_USE_CIPHER
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
#endif // _OMB_USE_CIPHER

TDataWithAlarms::TDataWithAlarms(void):TData(NULL){
	#ifndef __WXMAC__
		SoundFile = GetDataDir() + L"alarm_clock.wav";
	#else
		SoundFile = AppDir + L"/Resources/alarm_clock.wav";
	#endif //__WXMAC__
}

bool TDataWithAlarms::HasAlarms(void){
	bool result=false;
	int i;
	wxDateTime checkdate=wxDateTime::Today().Add(wxDateSpan(90,0,0,0));
	for(i = 0; i < NSho; i++)if(ShopItems[i].Alarm < checkdate){
			result=true;
			break;}
	if(result)return true;
	for(i = 0; i < NLen; i++)if(Lent[i].Alarm<checkdate){
			result=true;
			break;}
	if(result)return true;
	for(i = 0; i < NBor; i++)if(Borrowed[i].Alarm<checkdate){
			result=true;
			break;}
	return result;}

void TDataWithAlarms::CheckShopList(void){
	bool PostAll=false;
	wxDateTime Now = wxDateTime::Now();
	TAlarmF *AlarmF = new TAlarmF(wxTheApp->GetTopWindow());
	wxString Msg;
	AlarmF->DatePicker->SetValue(Now);
	AlarmF->DatePicker->SetRange(Now,wxInvalidDateTime);
	for(int i = 0; i < NSho; i++)if(ShopItems[i].Alarm <= Now){
		Msg=::wxString::Format(_("The alarm for the shopping list item \"%s\" is expired!"),ShopItems[i].Name.c_str());
		AlarmF->Text->SetLabel(Msg);
    if(wxFileExists(SoundFile)){
	    wxSound *sound;
    	sound=new wxSound(SoundFile,false);
    	if(!PostAll)if(sound->IsOk()) sound->Play(wxSOUND_ASYNC);
 	   	delete sound;}
		if(PostAll){
			ShopItems[i].Alarm = AlarmF->DatePicker->GetValue();
			#ifdef _OMB_USE_CIPHER
				wxString update = L"update Shoplist set alarm = " +
																wxString::Format(L"%d", (int) ShopItems[i].Alarm.GetTicks()) +
																L" where id = " +
																wxString::Format(L"%d", ShopItems[i].Id) +
																L";";
        #ifdef __OPENSUSE__
          ExecuteUpdate(database, update.c_str());
        #else
          ExecuteUpdate(database, update);
        #endif
			#else
				database->ExecuteUpdate(L"update Shoplist set alarm = " +
																wxString::Format(L"%d", (int) ShopItems[i].Alarm.GetTicks()) +
																L" where id = " +
																wxString::Format(L"%d", ShopItems[i].Id) +
																L";");
			#endif // _OMB_USE_CIPHER
		}
		else{
			#ifdef _OMB_USE_CIPHER
				wxString Sql;
			#endif // _OMB_USE_CIPHER
			switch(AlarmF->ShowModal()){
				case wxID_OK:
					ShopItems[i].Alarm=AlarmF->DatePicker->GetValue();
					#ifdef _OMB_USE_CIPHER
						Sql = L"update Shoplist set alarm = " +
																		wxString::Format(L"%d", (int) ShopItems[i].Alarm.GetTicks()) +
																		L" where id = " +
																		wxString::Format(L"%d", ShopItems[i].Id) +
																		L";";
            #ifdef __OPENSUSE__
              ExecuteUpdate(database, Sql.c_str());
            #else
              ExecuteUpdate(database, Sql);
            #endif
					#else
						database->ExecuteUpdate(L"update Shoplist set alarm = " +
																		wxString::Format(L"%d", (int) ShopItems[i].Alarm.GetTicks()) +
																		L" where id = " +
																		wxString::Format(L"%d", ShopItems[i].Id) +
																		L";");
					#endif // _OMB_USE_CIPHER
					if(AlarmF->cb_PostponeAll->IsChecked())PostAll=true;
					break;
			case wxID_CANCEL:
					ShopItems[i].Alarm.Add(wxDateSpan(100,0,0,0));}
			FileData.Modified=true;}}
		delete AlarmF;}

void TDataWithAlarms::CheckLentObjects(void){
	bool PostAll = false;
	wxDateTime Now = wxDateTime::Now();
	TAlarmF *AlarmF=new TAlarmF(wxTheApp->GetTopWindow());
	wxString Msg;
	AlarmF->DatePicker->SetValue(Now);
	AlarmF->DatePicker->SetRange(Now, wxInvalidDateTime);
	for(int i = 0; i < NLen; i++)if(Now.IsLaterThan(Lent[i].Alarm)){
		Msg=::wxString::Format(_("The alarm for the item \"%s\" is expired!"),Lent[i].Object.c_str());
		AlarmF->Text->SetLabel(Msg);
		if(wxFileExists(SoundFile)){
			wxSound *sound;
			sound=new wxSound(SoundFile,false);
			if(!PostAll)if(sound->IsOk()) sound->Play(wxSOUND_ASYNC);
			delete sound;}
		if(PostAll){
		Lent[i].Alarm=AlarmF->DatePicker->GetValue();
		#ifdef _OMB_USE_CIPHER
			wxString update = L"update Loans set alarm = " +
															wxString::Format(L"%d", (int) Lent[i].Alarm.GetTicks()) +
															L" where id = " +
															wxString::Format(L"%d", Lent[i].Id) +
															L";";
			#ifdef __OPENSUSE__
        ExecuteUpdate(database, update.c_str());
			#else
        ExecuteUpdate(database, update);
      #endif
		#else
			database->ExecuteUpdate(L"update Loans set alarm = " +
															wxString::Format(L"%d", (int) Lent[i].Alarm.GetTicks()) +
															L" where id = " +
															wxString::Format(L"%d", Lent[i].Id) +
															L";");
		#endif // _OMB_USE_CIPHER
		}
		else{
			#ifdef _OMB_USE_CIPHER
				wxString Sql;
			#endif // _OMB_USE_CIPHER
			switch(AlarmF->ShowModal()){
				case wxID_OK:
					Lent[i].Alarm=AlarmF->DatePicker->GetValue();
					#ifdef _OMB_USE_CIPHER
						Sql = L"update Loans set alarm = " +
																		wxString::Format(L"%d", (int) Lent[i].Alarm.GetTicks()) +
																		L" where id = " +
																		wxString::Format(L"%d", Lent[i].Id) +
																		L";";
						#ifdef __OPENSUSE__
              ExecuteUpdate(database, Sql.c_str());
						#else
              ExecuteUpdate(database, Sql);
            #endif
					#else
						database->ExecuteUpdate(L"update Loans set alarm = " +
																			wxString::Format(L"%d", (int) Lent[i].Alarm.GetTicks()) +
																			L" where id = " +
																			wxString::Format(L"%d", Lent[i].Id) +
																			L";");
					#endif // _OMB_USE_CIPHER
					if(AlarmF->cb_PostponeAll->IsChecked())PostAll=true;
					break;
				case wxID_CANCEL:
					Lent[i].Alarm.Add(wxDateSpan(100,0,0,0));}
				FileData.Modified=true;}}
  delete AlarmF;}

void TDataWithAlarms::CheckBorrowedObjects(void){
	bool PostAll=false;
	wxDateTime Now = wxDateTime::Now();
	TAlarmF *AlarmF=new TAlarmF(wxTheApp->GetTopWindow());
	wxString Msg;
	AlarmF->DatePicker->SetValue(Now);
	AlarmF->DatePicker->SetRange(Now, wxInvalidDateTime);
	for(int i = 0; i < NBor; i++)if(Borrowed[i].Alarm<=Now){
		Msg = ::wxString::Format(_("The alarm for the item \"%s\" is expired!"), Borrowed[i].Object.c_str());
		AlarmF->Text->SetLabel(Msg);
    if(wxFileExists(SoundFile)){
	    wxSound *sound;
    	sound=new wxSound(SoundFile,false);
    	if(!PostAll)if(sound->IsOk())sound->Play(wxSOUND_ASYNC);
  	  delete sound;}
		if(PostAll){
			Borrowed[i].Alarm=AlarmF->DatePicker->GetValue();
			#ifdef _OMB_USE_CIPHER
				wxString update = L"update Borrows set alarm = " +
																wxString::Format(L"%d", (int) Borrowed[i].Alarm.GetTicks()) +
																L" where id = " +
																wxString::Format(L"%d", Borrowed[i].Id) +
																L";";
        #ifdef __OPENSUSE__
          ExecuteUpdate(database, update.c_str());
        #else
          ExecuteUpdate(database, update);
        #endif
			#else
				database->ExecuteUpdate(L"update Borrows set alarm = " +
																wxString::Format(L"%d", (int) Borrowed[i].Alarm.GetTicks()) +
																L" where id = " +
																wxString::Format(L"%d", Borrowed[i].Id) +
																L";");
			#endif // _OMB_USE_CIPHER
		}
		else{
			switch(AlarmF->ShowModal()){
				case wxID_OK:
						Borrowed[i].Alarm=AlarmF->DatePicker->GetValue();
						if(AlarmF->cb_PostponeAll->IsChecked())PostAll=true;
						break;
				case wxID_CANCEL:
						Borrowed[i].Alarm.Add(wxDateSpan(100,0,0,0));}
			FileData.Modified=true;}}
   delete AlarmF;}
