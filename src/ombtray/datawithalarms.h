/***************************************************************
 * Name:      datawithalarms.h
 * Purpose:   Alarm extension for TData
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-09-30
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef DATAWITHALARMS_H
#define DATAWITHALARMS_H

#include "../omb34core.h"

class TDataWithAlarms : public TData
{
	private:
		wxString SoundFile;
	public:
		TDataWithAlarms(void);
		bool HasAlarms(void);
		void CheckShopList(void);
		void CheckLentObjects(void);
		void CheckBorrowedObjects(void);
};

WXIMPORT wxString GetDataDir(void);

#endif // DATAWITHALARMS_H
