/***************************************************************
 * Name:      omberr.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-06-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef __OMBCONVERT_BIN__
	#include <wx/wx.h>
	#include <wx/filename.h> // For wxFileName

	#include "../types.h"

	// Internal routines
	#ifdef _OMB_FORMAT2x
		bool Check2x(wxString File);
	#endif // _OMB_FORMAT2x
	#ifdef _OMB_FORMAT30x
		bool Check301(wxString File);
		bool Check302(wxString File);
	#endif // _OMB_FORMAT30x
	#ifdef _OMB_FORMAT31x
		bool Check31(wxString file);
	#endif // _OMB_FORMAT31x

	#ifdef __WXMSW__
		// Calls to module igiomb.dll
		WXIMPORT wxString GetShareDir(void);
	#endif // __WXMSW__

	#ifdef __WXMAC__
		// Calls to module igiomb.dll
		WXIMPORT wxString GetShareDir(void);
	#endif // __WXMSW__

	WXEXPORT void Error(int Err, wxString Opt);
	#ifdef _OMB_USE_CIPHER
		WXEXPORT int CheckFileFormat(wxString File, wxString pwd = wxEmptyString, bool archive = false);
		WXEXPORT bool CheckAndPromptForConversion(wxString File, bool master, wxString pwd);
		WXEXPORT bool IsEncryptedDB(wxString File, wxString Pwd, wxString OldPwd = wxEmptyString, bool CheckOnly = false);
	#else
		WXEXPORT int CheckFileFormat(wxString File);
		WXEXPORT bool CheckAndPromptForConversion(wxString File, bool master);
	#endif // _OMB_USE_CIPHER
#endif // __OMBCONVERT_BIN__

