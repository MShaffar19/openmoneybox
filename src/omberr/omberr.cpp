/***************************************************************
 * Name:      omberr.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2021-10-10
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifdef _OMB_FORMAT30x
	#include <wx/file.h>
#endif // _OMB_FORMAT30x

#ifndef _OMB_USE_CIPHER
	#include <wx/wxsqlite3.h>
#endif // _OMB_USE_CIPHER

/*
#ifndef __OMBCONVERT_BIN__
	#include <wx/dlimpexp.h>
#endif // __OMBCONVERT_BIN__
*/

#include "../platformsetup.h"
#include "../constants.h"

#ifdef __OMBCONVERT_BIN__
	#include <wx/file.h>
	#include <wx/filename.h>
#else
	#include "omberr.h"
#endif // __OMBCONVERT_BIN__

#ifdef __WXGTK__
	extern wxString Console;
#endif // __WXGTK__

#ifdef _OMB_USE_CIPHER
	#include <sqlite3.h>
	#include <wx/stdpaths.h>	// For wxStandardPaths::Get()

	#ifdef __OMBCONVERT_BIN__
		extern int dbVersion;
	#else
		#include "../productversion.h"
		#include "../ui/password.h"
	#endif // __OMBCONVERT_BIN__

	const wxString cipher_compat_sql = "PRAGMA cipher_default_compatibility = 3;";

	extern int queryUserVersion (sqlite3* db);
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
#endif // _OMB_USE_CIPHER

void Error(int Err,wxString Opt){
	wxString Msg;
	switch(Err){
		case 1:
			Msg=::wxString::Format(_("The file %s is already opened!"),Opt.c_str());
			break;
		case 2:
			Msg=::wxString::Format(_("The file %s does not contain any valid budget data!"),Opt.c_str());
			break;
		case 3:
			Msg=_("The file is already in Bil2.x format!");
			break;
		case 4:
			Msg=_("Wrong password!");
			break;
		case 5:
			Msg=_("You must select a debt!");
			break;
		/*
		case 6:
			Msg=_("Configuration error! It is necessary to reinstall the application.");
			break;
		*/
		case 8:
			Msg=_("No fund can be removed!");
			break;
		case 9:
			Msg=_("No fund can be modified!");
			break;
		case 10:
			Msg=_("Error in budget data!");
			break;
		case 11:
			Msg=::wxString::Format(_("The fund %s already exists!"),Opt.c_str());
			break;
		case 12:
			Msg=_("The fund value must be less or equal to the default fund!");
			break;
		case 13:
			Msg=_("Not enough money for the operation!");
			break;
		case 14:
			Msg=_("No lent object!");
			break;
		case 15:
			Msg=_("No borrowed object!");
			break;
		case 16:
			Msg=_("No item in the shopping list!");
			break;
		case 17:
			Msg=_("This operation is only valid to increase the budget!");
			break;
		case 18:
			Msg=_("Error in budget data! Check that the default fund is set.");
			break;
		case 19:
			Msg=_("Not possible to set a credit greater than the default fund!");
			break;
		case 20:
			Msg=_("No credit stored!");
			break;
		case 21:
			Msg=::wxString::Format(_("Inserted value is greater than the chosen %s!"),Opt.c_str());
			break;
		case 22:
			Msg=_("No debt stored!");
			break;
		case 23:
			Msg=_("No fund selected!");
			break;
		case 24:
			Msg=_("Not enough money in the default fund!");
			break;
		case 25:
			Msg=_("You must specify a value!");
			break;
		case 26:
			Msg=_("You must insert a numeric value!");
			break;
		case 27:
			Msg=_("You must insert a value greater than zero!");
			break;
		/*
			case 28:
				Msg=_("Not possible to open last file!");
				break;
		*/
		case 29:
			Msg=_("You must insert a reason!");
			break;
		case 30:
			Msg=_("You must insert a name!");
			break;
		case 31:
			Msg=_("No item selected in the shopping list!");
			break;
		case 32:
			Msg=_("You must insert an object!");
			break;
		case 33:
			Msg=_("You must insert a contact!");
			break;
		/*
		case 34:
			Msg=_("Two labels are identical!");
			break;
		*/
		/*
		case 35:
			Msg=_("Unexisting folder! Insert a valid path or click the Browse button");
			break;
		*/
		case 36:
			Msg=_("Cannot open the file!");
			break;
		/*
		case 37:
			Msg=_("Not possible to sort data!");
			break;
		*/
		/*
		case 38:
			Msg=_("Empty string!");
			break;
		*/
		case 39:
			Msg=_("You must select a credit!");
			break;
		case 40:
			Msg=_("You must insert the password!");
			break;
		case 41:
			Msg=::wxString::Format(_("File %s is read-only!"),Opt.c_str());
			break;
		case 42:
			Msg=_("You must use this application from OpenMoneyBox application!");
			break;
		case 43:
			Msg=_("You can convert only one file at a time!");
			break;
		case 44:
			Msg=_("Error during the document conversion!");
			break;
		case 45:
			Msg = ::wxString::Format(_("Not possible to find file \"%s\"!"), Opt.c_str());
			break;
		case 46:
			Msg = _("Invalid date");
			break;
		case 47:
			Msg = _("You have to select a file name!");
			break;
		case 48:
			Msg=::wxString::Format(_("The item %s already exists!"),Opt.c_str());
			break;

		case 49:
			Msg = _("Please specify the currency symbol!");
			break;

		default:
			Msg=_("Unknown error");};
  #ifndef __OMBCONVERT_BIN__
    #ifdef __WXMSW__
      MessageBeep(MB_ICONHAND);
    #else
        wxBell();
    #endif // __WXMSW__
	#endif // __OMBCONVERT_BIN__

  #ifndef __OMBCONVERT_BIN__
  	wxMessageBox(Msg,_T("OpenMoneyBox"),wxICON_ERROR);
	#else
		const char *output_string;
		output_string = Msg.c_str();
		printf("%s", output_string);
	#endif // __OMBCONVERT_BIN__
	}

#ifdef _OMB_FORMAT2x
	bool Check2x(wxString File){
		bool Ret;
		//int FH,BR;	// FH: file handler

									// BR: buffer lenght
		wxFileOffset FL;	// FL: file lenght
		char *Buffer;
		wxString S=wxEmptyString;
		wxFile *fh=new wxFile(File,wxFile::read);
		if(!fh->IsOpened()){
			delete fh;
			return false;}
		//FL=fh->Len();
		FL=fh->SeekEnd(0);
		fh->Seek(0);
		Buffer=new char[FL+1];
		if(fh->Read(Buffer,6)==6)
			//S=Buffer;
		{
			for(int i=0;i<6;i++)S+=Buffer[i];
		}
		else{
			fh->Close();
			delete fh;
			return false;}
		fh->Close();
		delete fh;
		delete[] Buffer;
		if(S==L"Bil2.0")Ret=true;
		else if(S==L"Bil2.2")Ret=true;
		else if(S==L"Bil2.3")Ret=true;
		else Ret=false;
		return Ret;}
#endif // _OMB_FORMAT2x

#ifdef _OMB_FORMAT30x
	bool Check301(wxString File){
		bool Ret;
		wxFileOffset FL;	// FL: file lenght
		char *Buffer;
		wxString S = wxEmptyString;
		wxFile *fh = new wxFile(File, wxFile::read);
		if(!fh->IsOpened()){
			delete fh;
			return false;}
		FL=fh->SeekEnd(0);
		fh->Seek(0);
		Buffer=new char[FL + 1];
		if(fh->Read(Buffer, 6) == 6) for(int i=0; i<6; i++) S += Buffer[i];
		else{
			fh->Close();
			delete fh;
			return false;}
		fh->Close();
		delete fh;
		delete[] Buffer;
		if(S == L"Bil3.0") Ret = true;
		else Ret = false;
		return Ret;}

	bool Check302(wxString File){
		bool Ret;
		wxFileOffset FL;	// FL: file lenght
		char *Buffer;
		wxString S = wxEmptyString;
		wxFile *fh = new wxFile(File, wxFile::read);
		if(!fh->IsOpened()){
			delete fh;
			return false;}
		FL=fh->SeekEnd(0);
		fh->Seek(0);
		Buffer=new char[FL + 1];
		if(fh->Read(Buffer, 6) == 6) for(int i=0; i<6; i++) S += Buffer[i];
		else{
			fh->Close();
			delete fh;
			return false;}
		fh->Close();
		delete fh;
		delete[] Buffer;
		if(S == L"Bil302") Ret = true;
		else Ret = false;
		return Ret;}
#endif // _OMB_FORMAT30x

#ifdef _OMB_FORMAT31x
	bool Check31(wxString file){
		#ifdef _OMB_USE_CIPHER
			sqlite3 *db;
			#ifdef __OPENSUSE__
				sqlite3_open(file.c_str(), &db);
			#else
				sqlite3_open(file, &db);
			#endif
		#else
			wxSQLite3Database *db = new wxSQLite3Database();
			db->Open(file, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
		#endif // _OMB_USE_CIPHER
		try{
			#ifdef _OMB_USE_CIPHER
				int version = queryUserVersion(db);
				sqlite3_close(db);
			#else
				wxSQLite3ResultSet test = db->ExecuteQuery("pragma user_version;");
				int version = test.GetInt(0);
				db->Close();
			#endif // _OMB_USE_CIPHER

			if(version >= 0) return true;	// 0: pre-existing empty file
																		// 1 or greater: schema updates (existing database
																		// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm
			else return false;
		}
		catch(...){
			return false;
		}}
#endif // _OMB_FORMAT31x

#ifdef _OMB_FORMAT32x
	bool Check32(wxString file){
		#ifdef _OMB_USE_CIPHER
			sqlite3 *db;
			#ifdef __OPENSUSE__
				sqlite3_open(file.c_str(), &db);
			#else
				sqlite3_open(file, &db);
			#endif // __OPENSUSE__
		#else
			wxSQLite3Database *db = new wxSQLite3Database();
			db->Open(file, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
		#endif // _OMB_USE_CIPHER
		try{
			#ifdef _OMB_USE_CIPHER
				int version = queryUserVersion(db);
				sqlite3_close(db);
			#else
				wxSQLite3ResultSet test = db->ExecuteQuery("pragma user_version;");
				int version = test.GetInt(0);
				db->Close();
			#endif // _OMB_USE_CIPHER

			if(version == 32) return true;	// 0: pre-existing empty file
																			// 1 or greater: schema updates (existing database
																			// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm
			else return false;
		}
		catch(...){
			return false;
		}}
#endif // _OMB_FORMAT32x

#ifdef _OMB_FORMAT33x
	#ifdef _OMB_USE_CIPHER
		int Check33(wxString file, wxString pwd = wxEmptyString, bool Archive = false){
						// -2: not a sqlite3 database
						// -1: wrong password
						//  0: database is not omb33 version
						//  1: database is omb33 version
						//  2: database is omb35 version

			sqlite3 *db;
			#ifdef __OPENSUSE__
				if(sqlite3_open(file.c_str(), &db) != SQLITE_OK) return -2;
			#else
				if(sqlite3_open(file, &db) != SQLITE_OK) return -2;
			#endif // __OPENSUSE__
			int Result = 0;

			#ifdef __OPENSUSE__
				// Set default compatibility
				ExecuteUpdate(db, cipher_compat_sql.c_str());

				sqlite3_key(db, pwd.c_str(), pwd.length());
			#else
				// Set default compatibility
				ExecuteUpdate(db, cipher_compat_sql);

				sqlite3_key(db, pwd, pwd.length());
			#endif // __OPENSUSE__
			int rc = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
			switch(rc){
				case SQLITE_OK:
					try{
						int version = queryUserVersion(db);

						if((version == dbVersion) || (version == (dbVersion -1)))
							Result = 2;	// 0: pre-existing empty file or new encrypted database
													// 1 or greater: schema updates (existing database
													// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm
						else if(version == 33) Result = 1;
						else if((version == 0) || (version == 32)){
							#ifdef __OPENSUSE__
								wxString Sql = L"pragma user_version = " +
															::wxString::Format(L"%d", dbVersion) +
															L";";
								ExecuteUpdate(db, Sql.c_str());
							#else
								ExecuteUpdate(db, L"pragma user_version = " +
															::wxString::Format(L"%d", dbVersion) +
															L";");
							#endif // __OPENSUSE__
							Result = 1;
						}
						/*
						else{
							return false;
						}
						*/
					}
					catch(...){
						sqlite3_close(db);
						return -2;
					}
					break;
				case SQLITE_NOTADB:
					#ifndef __OMBCONVERT_BIN__
						int Response;
						wxString Key;

						TPassF *PassF = new TPassF(wxTheApp->GetTopWindow());

						if(Archive)
							PassF->PassLab->SetLabel(_("Insert the archive password:"));

						do{
							Response = PassF->ShowModal();}
						while(Response == wxID_RETRY);

						switch(Response){
							case wxID_OK:
								Key = PassF->Pass->GetValue();
								#ifdef __OPENSUSE__
									sqlite3_key(db, Key.c_str(), Key.length());
								#else
									sqlite3_key(db, Key, Key.length());
								#endif // __OPENSUSE__
								rc = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
								if (rc == SQLITE_OK)
									Result = /*1*/2;
								else Result = -1;
								break;
							case wxID_CANCEL:
							default:
								Result = 0;}
						delete PassF;
					#else
						Result = 0;
					#endif // __OMBCONVERT_BIN__

					break;

			}

			sqlite3_close(db);

			return Result;
		}
		#else
				bool Check33(wxString file){
						wxSQLite3Database *db = new wxSQLite3Database();
						db->Open(file, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
						try{
								wxSQLite3ResultSet test = db->ExecuteQuery("pragma user_version;");
								int version = test.GetInt(0);
								db->Close();

								if((version == 34) || (version == 35)) return true;	// 0: pre-existing empty file
																								// 1 or greater: schema updates (existing database
																								// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm
								else return false;
						}
						catch(...){
								return false;
						}}
	#endif // _OMB_USE_CIPHER
#endif // _OMB_FORMAT33x

#ifdef _OMB_USE_CIPHER
int Check34(wxString file, wxString pwd = wxEmptyString, bool Archive = false){
				// -2: not a sqlite3 database
				// -1: wrong password
				//  0: database is not omb33 version
				//  1: database is omb35 version
				//  2: database is omb37 version

	sqlite3 *db;
	#ifdef __OPENSUSE__
    if(sqlite3_open(file.c_str(), &db) != SQLITE_OK) return -2;
  #else
    if(sqlite3_open(file, &db) != SQLITE_OK) return -2;
  #endif // __OPENSUSE__
	int Result = 0;

	#ifdef __OPENSUSE__
  	// Set default compatibility
  	ExecuteUpdate(db, cipher_compat_sql.c_str());

    sqlite3_key(db, pwd.c_str(), pwd.length());
  #else
  	// Set default compatibility
  	ExecuteUpdate(db, cipher_compat_sql);

    sqlite3_key(db, pwd, pwd.length());
  #endif // __OPENSUSE__
	int rc = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
	switch(rc){
		case SQLITE_OK:
			try{
				int version = queryUserVersion(db);

				if((version == dbVersion) || (version == (dbVersion -1)))
					Result = 2;	// 0: pre-existing empty file or new encrypted database
											// 1 or greater: schema updates (existing database
											// http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm
				else if(version == 35) Result = 1;
				else if((version == 0) || (version == 32)){
          #ifdef __OPENSUSE__
            wxString Sql = L"pragma user_version = " +
													::wxString::Format(L"%d", dbVersion) +
													L";";
            ExecuteUpdate(db, Sql.c_str());
          #else
            ExecuteUpdate(db, L"pragma user_version = " +
													::wxString::Format(L"%d", dbVersion) +
													L";");
					#endif // __OPENSUSE__
					Result = 1;
				}
			}
			catch(...){
				sqlite3_close(db);
				return -2;
			}
			break;
		case SQLITE_NOTADB:
			#ifndef __OMBCONVERT_BIN__
				int Response;
				wxString Key;

				TPassF *PassF = new TPassF(wxTheApp->GetTopWindow());

				if(Archive)
					PassF->PassLab->SetLabel(_("Insert the archive password:"));

				do{
					Response = PassF->ShowModal();}
				while(Response == wxID_RETRY);

				switch(Response){
					case wxID_OK:
						Key = PassF->Pass->GetValue();
						#ifdef __OPENSUSE__
              sqlite3_key(db, Key.c_str(), Key.length());
            #else
              sqlite3_key(db, Key, Key.length());
            #endif // __OPENSUSE__
						rc = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master;", NULL, NULL, NULL);
						if (rc == SQLITE_OK)
							Result = 2;
						else Result = -1;
						break;
					case wxID_CANCEL:
					default:
						Result = 0;}
				delete PassF;
			#else
				Result = 0;
			#endif // __OMBCONVERT_BIN__

			break;

	}

	sqlite3_close(db);

	return Result;
}
#else
    bool Check34(wxString file){
        wxSQLite3Database *db = new wxSQLite3Database();
        db->Open(file, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
        try{
            wxSQLite3ResultSet test = db->ExecuteQuery("pragma user_version;");
            int version = test.GetInt(0);
            db->Close();

            if((version == 36) || (version == 37)) return true;	// 0: pre-existing empty file
                                            // 1 or greater: schema updates (existing database
                                            // http://www.tutorialspoint.com/sqlite/sqlite_pragma.htm
            else return false;
        }
        catch(...){
            return false;
        }}
#endif // _OMB_USE_CIPHER

#ifdef _OMB_USE_CIPHER
	int CheckFileFormat(wxString File, wxString pwd, bool archive){
#else
	int CheckFileFormat(wxString File){
#endif // _OMB_USE_CIPHER

	int Ret = ombFFORMAT_UNKNOWN;

	#ifdef _OMB_USE_CIPHER
		bool IsSQL = true;

		switch (Check34(File, pwd, archive)){
			case -2:
				IsSQL = false;
				break;
			case -1:
				return ombWRONGPASSWORD;
				break;
			case 1:
				return ombFFORMAT_332;
				break;
			case 2:
				return ombFFORMAT_34;
				break;
			//default:
		}

		#ifdef _OMB_FORMAT33x
			if(IsSQL){

				switch (Check33(File, pwd, archive)){
					case -2:
						IsSQL = false;
						break;
					case -1:
						return ombWRONGPASSWORD;
						break;
					case 1:
						return ombFFORMAT_33;
						break;
					case 2:
						return ombFFORMAT_332;
						break;
					//default:
				}
			}
		#endif // _OMB_FORMAT33x

		if(IsSQL){
	#else
		if(Check34(File)) Ret = ombFFORMAT_34;
		#ifdef _OMB_FORMAT33x
            else if(Check33(File)) Ret = ombFFORMAT_332;
        #endif // _OMB_FORMAT33x
		else
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_FORMAT32x
		if(Check32(File)) Ret = ombFFORMAT_32;
	#endif // _OMB_FORMAT32x
	#ifdef _OMB_FORMAT31x
		else if(Check31(File)) Ret = ombFFORMAT_31;
	#endif // _OMB_FORMAT31x

	#ifndef __OMBCONVERT_BIN__
	#ifdef _OMB_USE_CIPHER
		}
		else{
	#endif // _OMB_USE_CIPHER
	#endif // __OMBCONVERT_BIN__

	#ifdef _OMB_FORMAT30x
		#ifndef _OMB_USE_CIPHER
			else
		#endif // _OMB_USE_CIPHER
		else if(Check302(File)) Ret = bilFFORMAT_302;
		else if(Check301(File))Ret = bilFFORMAT_301;
	#endif // _OMB_FORMAT30x
	#ifdef _OMB_FORMAT2x
		else if(Check2x(File))Ret = bilFFORMAT_2;
	#endif // _OMB_FORMAT2x

	//#ifndef __OMBCONVERT_BIN__
	#ifdef _OMB_USE_CIPHER
			Ret = ombFFORMAT_UNKNOWN;
		}
	#else
		/*else*/ Ret = ombFFORMAT_UNKNOWN; // Uncomment the 'else' statement id any compiler switch for old format is enabled
	#endif // _OMB_USE_CIPHER
	return Ret;}

#ifndef __OMBCONVERT_BIN__
	#ifdef _OMB_USE_CIPHER
		bool CheckAndPromptForConversion(wxString File, bool master = false, wxString pwd = wxEmptyString){
	#else
		bool CheckAndPromptForConversion(wxString File, bool master = false){
	#endif // _OMB_USE_CIPHER
		wxString Name;
		#ifdef _OMB_USE_CIPHER
			switch(CheckFileFormat(File, pwd, master)){
		#else
			switch(CheckFileFormat(File)){
		#endif // _OMB_USE_CIPHER
			case ombFFORMAT_UNKNOWN:	// Unknown Format
				Error(2, File);
				return false;
			#ifdef _OMB_USE_CIPHER
			case ombWRONGPASSWORD:
				Error(4, wxEmptyString);
				return false;

				#ifdef _OMB_FORMAT33x
				case ombFFORMAT_33:
					::wxFileName::SplitPath(File, NULL, NULL, &Name, NULL, wxPATH_NATIVE);
					if(wxMessageBox(wxString::Format(_("The format of file %s is old.\nDo you want to convert it?"), Name), _("OpenMoneyBox"), wxYES_NO) == wxYES){
					wxString Proc;
					#ifdef __WXGTK__
						Proc = Console + wxGetCwd() + L"/ombconvert ";
						if(master) Proc += L"-master ";
						Proc = Proc + L"\"" + File + L"\"";
						wxExecute(Proc, wxEXEC_SYNC);
					#elif defined __WXMSW__
						Proc = "\"" + GetShareDir() + L"ombconvert.exe\" ";
						if(master) Proc += "-master ";
						Proc = Proc + "\"" + File + "\"";
						wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
					#elif defined __WXMAC__
						//TODO: finalize command
						Proc = "\"" + GetShareDir() + L"ombconvert.exe\" ";
						if(master) Proc += "-master ";
						Proc = Proc + "\"" + File + "\"";
						wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
					#endif
					if(CheckFileFormat(File) != ombFFORMAT_332) return false;}
					else return false;
					break;
				#endif // _OMB_FORMAT33x

			#else
				//case ombFFORMAT_332:
					//break;
			#endif // _OMB_USE_CIPHER

			case ombFFORMAT_34:
				break;
			case ombFFORMAT_332:
				::wxFileName::SplitPath(File, NULL, NULL, &Name, NULL, wxPATH_NATIVE);
				if(wxMessageBox(wxString::Format(_("The format of file %s is old.\nDo you want to convert it?"), Name), _("OpenMoneyBox"), wxYES_NO) == wxYES){
				wxString Proc;
				#ifdef __WXGTK__
					Proc = Console + wxGetCwd() + L"/ombconvert ";
					if(master) Proc += L"-master ";
					Proc = Proc + L"\"" + File + L"\"";
					wxExecute(Proc, wxEXEC_SYNC);
				#elif defined __WXMSW__
					Proc = "\"" + GetShareDir() + L"ombconvert.exe\" ";
					if(master) Proc += "-master ";
					Proc = Proc + "\"" + File + "\"";
					wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
				#elif defined __WXMAC__
					//TODO: finalize command
					Proc = "\"" + GetShareDir() + L"ombconvert.exe\" ";
					if(master) Proc += "-master ";
					Proc = Proc + "\"" + File + "\"";
					wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
				#endif
				if(CheckFileFormat(File) != ombFFORMAT_34) return false;}
				else return false;
				break;

			#ifdef _OMB_FORMAT2x
				case bilFFORMAT_2:	// v2
			#endif // _OMB_FORMAT2x
			#ifdef _OMB_FORMAT32x
				case ombFFORMAT_32:
			#endif // _OMB_FORMAT32x
			#ifdef _OMB_FORMAT31x
				case ombFFORMAT_31:
			#endif // _OMB_FORMAT31x
			#ifdef _OMB_FORMAT30x
				case bilFFORMAT_302:
				case bilFFORMAT_301:	// v3
			#endif // _OMB_FORMAT30x
				::wxFileName::SplitPath(File, NULL, NULL, &Name, NULL, wxPATH_NATIVE);
				if(wxMessageBox(wxString::Format(_("The format of file %s is old.\nDo you want to convert it?"), Name), _("OpenMoneyBox"), wxYES_NO) == wxYES){
				wxString Proc;
				#ifdef __WXGTK__
					Proc = Console + wxGetCwd() + L"/ombconvert ";
					if(master) Proc += L"-master ";
					Proc = Proc + L"\"" + File + L"\"";
					wxExecute(Proc, wxEXEC_SYNC);
				#elif defined __WXMSW__
					Proc = "\"" + GetShareDir() + L"ombconvert.exe\" ";
					if(master) Proc += "-master ";
					Proc = Proc + "\"" + File + "\"";
					wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
				#elif defined __WXMAC__
					//TODO: finalize command
					Proc = "\"" + GetShareDir() + L"ombconvert.exe\" ";
					if(master) Proc += "-master ";
					Proc = Proc + "\"" + File + "\"";
					wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
				#endif
				if(CheckFileFormat(File) != ombFFORMAT_332) return false;}
				else return false;
				break;
			default:
				return false;}
		return true;
	}
#endif // __OMBCONVERT_BIN__

#ifdef _OMB_USE_CIPHER
bool IsEncryptedDB(wxString File, wxString Pwd, wxString OldPwd, bool CheckOnly){
		// Check if database is encrypted https://github.com/sqlcipher/android-database-sqlcipher/issues/381

		if(! ::wxFileExists(File)) return false;

		bool IsEncrypted = true;
		sqlite3 *database;

		wxString s = "SQLite format 3";
		char *Buffer;
		Buffer = new char[s.length()];
		wxFile *file = new wxFile(File, wxFile::read);
		file->Seek(0);
		unsigned int bytesRead = file->Read(Buffer, s.length());
		file->Close();
		if(bytesRead == s.length() && (s.Cmp(Buffer) == 0)) IsEncrypted = false;

		if(! CheckOnly){

			#ifndef __OMBCONVERT_BIN__
				::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
			#endif // __OMBCONVERT_BIN__

			wxString Vol, FilePath, FileName, Ext, BackupFile, CryptedFile;
			#ifdef __OPENSUSE__
        if(sqlite3_open(File.c_str(), &database) == SQLITE_OK){
      #else
        if(sqlite3_open(File, &database) == SQLITE_OK){
      #endif // __OPENSUSE__
				if ((Pwd != wxEmptyString) && (! IsEncrypted))
				{
					// https://www.zetetic.net/sqlcipher/sqlcipher-api/#sqlcipher_export

					// Get temporary file for export
					wxStandardPathsBase& Paths=::wxStandardPaths::Get();
					#ifdef __WXMSW__
						CryptedFile = Paths.GetTempDir() + L"\\omb";
					#else
						CryptedFile = Paths.GetTempDir() + L"/omb";
					#endif // __WXMSW__

					#ifdef __OPENSUSE__
            wxString Sql = "ATTACH DATABASE '" + CryptedFile + "' AS encrypted KEY '" + Pwd + "';";
            ExecuteUpdate(database, Sql.c_str());
            Sql = "SELECT sqlcipher_export('encrypted');";
            ExecuteUpdate(database, Sql.c_str());
            Sql = "DETACH DATABASE encrypted;";
            ExecuteUpdate(database, Sql.c_str());
					#else
            ExecuteUpdate(database, "ATTACH DATABASE '" + CryptedFile + "' AS encrypted KEY '" + Pwd + "';");
            ExecuteUpdate(database, "SELECT sqlcipher_export('encrypted');");
            ExecuteUpdate(database, "DETACH DATABASE encrypted;");
          #endif // __OPENSUSE__

					::wxFileName::SplitPath(File, &Vol, &FilePath, &FileName, &Ext, wxPATH_NATIVE);

					// Create backup of unencrypted file
					#ifdef __WXMSW__
						newfile = vol + L":" + FilePath + L"\\" + FileName + "-un.bak";
					#else
						BackupFile = Vol + FilePath + L"/" + FileName + "-un.bak";
					#endif // __WXMSW__
					::wxRenameFile(File, BackupFile, true);

					// Move encrypted file to the user dir
					::wxRenameFile(CryptedFile, File, true);

					IsEncrypted = true;
				}
				else if ((Pwd == wxEmptyString) && (IsEncrypted))
				{
					// https://www.zetetic.net/sqlcipher/sqlcipher-api/#sqlcipher_export

					// Get temporary file for export
					wxStandardPathsBase& Paths=::wxStandardPaths::Get();
					#ifdef __WXMSW__
						CryptedFile = Paths.GetTempDir() + L"\\omb";
					#else
						CryptedFile = Paths.GetTempDir() + L"/omb";
					#endif // __WXMSW__

					#ifdef __OPENSUSE__
            wxString Sql = wxString::Format("PRAGMA key = '%s';", OldPwd);
            ExecuteUpdate(database, Sql.c_str());
            Sql = "ATTACH DATABASE '" + CryptedFile + "' AS plaintext KEY '';";
            ExecuteUpdate(database, Sql.c_str());
            Sql = "SELECT sqlcipher_export('plaintext');";
            ExecuteUpdate(database, Sql.c_str());
            Sql = "DETACH DATABASE plaintext;";
            ExecuteUpdate(database, Sql.c_str());
					#else
            ExecuteUpdate(database, wxString::Format("PRAGMA key = '%s';", OldPwd));
            ExecuteUpdate(database, "ATTACH DATABASE '" + CryptedFile + "' AS plaintext KEY '';");
            ExecuteUpdate(database, "SELECT sqlcipher_export('plaintext');");
            ExecuteUpdate(database, "DETACH DATABASE plaintext;");
          #endif // __OPENSUSE__

					::wxFileName::SplitPath(File, &Vol, &FilePath, &FileName, &Ext, wxPATH_NATIVE);

					// Create backup of encrypted file
					#ifdef __WXMSW__
						newfile = vol + L":" + FilePath + L"\\" + FileName + "-en.bak";
					#else
						BackupFile = Vol + FilePath + L"/" + FileName + "-en.bak";
					#endif // __WXMSW__
					::wxRenameFile(File, BackupFile, true);

					// Move encrypted file to the user dir
					::wxRenameFile(CryptedFile, File, true);

					IsEncrypted = false;
				}

				sqlite3_close(database);
			}

			#ifndef __OMBCONVERT_BIN__
				::wxEndBusyCursor();
			#endif // __OMBCONVERT_BIN__
		}

		delete[] Buffer;
		return IsEncrypted;
	}
#endif

