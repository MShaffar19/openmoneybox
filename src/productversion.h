/***************************************************************
 * Name:      productversion.h
 * Purpose:   Version identifiers for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-09-30
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef PRODUCTVERSIONS_H_INCLUDED
#define PRODUCTVERSIONS_H_INCLUDED

// VERSION INFORMATION
//#define _ALPHA_						// alpha development
//#define _BETA							// beta development
//#define _RELCANDIDATE			// release candidate

int MajorVersion = 3;
int MinorVersion = 4;
int ReleaseVersion = 1;

// OpenMoneyBox database version
#ifdef _OMB_USE_CIPHER
	int dbVersion = 37;
#else
	int dbVersion = 36;
#endif // _OMB_USE_CIPHER

#ifdef __WXMAC__
	#include "mac_build.h"
#else
	extern char __BUILD_NUMBER;
	extern char   __BUILD_DATE;
#endif
#endif // PRODUCTVERSIONS_H_INCLUDED
