/***************************************************************
 * Name:      wiz.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include "wxwizard.h"

#ifndef WizH
#define WizH

#include <wx/string.h>

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TWizard : public wxTWiz
{
private:
	// GUI components
	Wel_Sheet *Wel_SheetP;
	End_Sheet *End_SheetP;

	// Non-GUI components
	bool UserChange;

	// Routines
	//void PageControlChanging(bool &AllowChange);
protected:
	void BackBtnClick(wxCommandEvent& event);
	void ForwardBtnClick(wxCommandEvent& event);
	void DisablePageChange(wxNotebookEvent& event);
public:
	// GUI components
	Saved_Sheet *Saved_SheetP;
	Cash_Sheet *Cash_SheetP;
	Doc_Sheet *Doc_SheetP;
	// Routines
	explicit TWizard(wxWindow* parent);
};

WXIMPORT wxString CheckValue(wxString Val);
WXIMPORT void Error(int Err,wxString Opt);
WXIMPORT wxString GetOSDocDir(void);

#endif
