
static const wxLanguage langIds[]={
	wxLANGUAGE_DEFAULT,
	wxLANGUAGE_ENGLISH,
	wxLANGUAGE_ITALIAN};

const wxString langNames[]={
	_("System language"),
	L"English",
  L"Italiano"};

