<!-- last update: 17/03/2022 -->

![Image](https://launchpadlibrarian.net/321444387/logo.png "icon")  

OpenMoneyBox
============

## Budget management application
OpenMoneyBox is an application designed to manage small personal money budgets in the easiest way.

[Homepage](http://igisw-bilancio.sourceforge.net/)

---
## Install on Linux:  
	`> make`  
	`> sudo make install`

Tested on:  
  * **Ubuntu Focal (20.04.4) x64 [v3.4.1.x]**  
  * Ubuntu Bionic (18.04.4) x64 [v3.4.1.3] [v3.3]  
  * Ubuntu Xenial (16.04.4) x64 [v3.2]  
  * Ubuntu Trusty (14.04.3) x64 [v3.1] [v3.0]  
  * Debian Bullseye (11)  
  * Debian Buster (10)  
  * Raspbian Bullseye (11)  
  * Suse Leap (**15.3**, 15.2, 15.0) - OpenSuse (42.3, 42.2)  
  * Fedora (**35** down to 25)  

---
## Install on FreeBSD:

1. Install dependencies:  
	`> pkg install gmake`  
	`> pkg install wx30-gtk3`  
	`> pkg install llvm`  
	`> pkg install sqlcipher`  
	`> pkg install osm-gps-map`  

2. build openmoneybox:  
	`> gmake -f makefile.bsd`  
	`> gmake -f makefile.bsd install`

Tested on:  
  * **FreeBSD 13.0**  
  * FreeBSD 12.x 

---
## Install on Windows:  
Requirements: install MinGW64 and MSys2

1. build wxWidgets:  
(disable MSYS binary beforehand)  
debug build:  
	`> mingw32-make.exe -f makefile.gcc USE_XRC=1 SHARED=1 MONOLITHIC=0 BUILD=debug UNICODE=1`  
release build mingw32:  
	`> mingw32-make.exe -f makefile.gcc USE_XRC=1 SHARED=1 MONOLITHIC=0 BUILD=release UNICODE=1 DEBUG_FLAG=0`  
	(set wxDEBUG_LEVEL to 0 in /include/wx/debug.h)
release build mingw64:
	`mingw32-make -f makefile.gcc CPP="gcc -E -D_M_AMD64" USE_XRC=1 SHARED=1 DEBUG_FLAG=0 BUILD=release UNICODE=1 MONOLITHIC=0`  
	(set wxDEBUG_LEVEL to 0 in /include/wx/debug.h)

2. build wxsqlite3.dll  
	**TODO:** list commands

3. build openmoneybox:  
	`> mingw32-make -f makefile.win`  
	`> mingw32-make -f makefile.win install`

Tested on:  
  * **Win11 x64**  
  * Win10 x64 

---
## Install on macOS:  
Requirements: install Homebrew

1. Install dependencies:  
	`> brew install wxwidgets`  
2. build openmoneybox:  
	`> make -f makefile.mac WARNING_ONLY=1`  
	`> make -f makefile.mac install`

Tested on:  
  * **macOS Sierra 10.12.5**  
