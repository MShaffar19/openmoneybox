��    0      �  C         (     )     2     E     X     u     ~     �  %   �  -   �     �            !        5  E   U  1   �  
   �     �     �     �               /     A     \     v     �     �     �     �     �     �     �               $     1  
   A     L     b     w     �     �     �     �  
   �     �  �  �     �	     �	     �	  #   �	  
   �	     �	     
  -   
  1   L
  	   ~
     �
     �
     �
     �
  J   �
  3        J     \     c     s     �     �     �     �     �     �          ,     H     b     {     �     �     �     �     �     �     �     �          #     =     N     d     y  
        �        %             -       !      )              	   "       &   
               +   /   *                 .              '      ,               #   $                                     (                      0                            Bilancio Bilancio [No name] Bilancio documents Bilancio is already running! Borrowed Budget management Choose the default fund: Choose the fund to add the profit to: Choose the fund to subtract the expense from: Credits Debts Default Document conversion i progress... Document opening in progress... File %s has been changed.
Do you really want to revert to saved file? Fund "%s" is exhausted. Do you want to delete it? Fund Chart Funds Get back an object Give back an object Insert a note Insert the borrowed object: Insert the donor: Insert the expense reason: Insert the expense value: Insert the fund name: Insert the gain reason: Insert the lent object: Insert the object receiver: Insert the profit value: Items Lent Monthly report of Objects Remove a fund Reset a fund Select a folder Set a fund Set the budget value: Set the default fund Set the total budget Store a profit Store an expense System language Total Value (%s) Zero Project-Id-Version: bilancio
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-10-21 21:42+0200
PO-Revision-Date: 2014-11-28 16:19+0000
Last-Translator: Rodrigo Zimmermann <bilufe@yahoo.com.br>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-11-29 06:51+0000
X-Generator: Launchpad (build 17267)
 Bilancio Bilancio [Sem nome] Documentos do Bilancio Bilancio já está sendo executado! Emprestado Gestão de orçamento Escolha o fundo padrão: Escolha um fundo para adicionar o lucro para: Escolha um fundo para extrair a despesa do mesmo: Créditos Débitos Padrão Convertendo o documento... Abrindo o documento... O arquivo %s foi modificado.
Realmente quer reverter para o arquivo salvo? O fundo "%s" está esgotado. Gostaria de apagá-lo? Gráfico do Fundo Fundos Volte um objeto Devolva um objeto Inserir uma nota Insira o objeto emprestado: Insira o doador: Insira a razão da despesa: Insira o valor da despesa: Digite o nome do fundo: Insira a razão do ganho: Insira o objeto emprestado: Insira o objeto recebido: Insira o valor do lucro: Itens Alugado Relatório mensal de Objetos Remover um fundo Reiniciar um fundo Selecione uma pasta Defina um fundo Defina o valor do orçamento: Defina o fundo padrão Defina o orçamento total Guardar um lucro Armazenar uma despesa Linguagem do sistema Total Valor (%s) Zero 